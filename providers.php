<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <title>MyEMRMyWay :: Providers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="MyEMRMyWay App">
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->
    <!--    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" /> -->
    <link href="Content/styles.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="admin/css/font-awesome.min.css" rel="stylesheet">


    <link href="admin/css/base-admin-3.css" rel="stylesheet">
    <link href="admin/css/base-admin-3-responsive.css" rel="stylesheet">
    <link href="Content/sweetalert2.min.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php



    include("include/incConfig.php");
    include("include/incFunctions.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;
	
    session_start();

    //make sure we have a valid sesion
	include("include/session.php");

    if ($subType == "F") {
        $dont_hide_Edit_Buttons = true;
    } else {
        $dont_hide_Edit_Buttons = (strcmp($loggedin_userid, $userid) == 0) || ($admin_user == 0);
    }


    ?>

    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            width: 40%;
            border-radius: 5px;
            display: none;
        }

            .card:hover {
                box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
            }

        img {
            border-radius: 5px 5px 0 0;
        }

        .container {
            padding: 2px 16px;
        }

        @media (max-width: 768px) { /* use the max to specify at each container level */
            .widget .widget-content {
                padding: 10px 10px;
                background: #FFF;
                border: 1px solid #D5D5D5;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
            }

            .col-md-12 {
                padding-right: 4px !important;
                padding-left: 4px !important;
            }

            .col-xs-12 {
                padding-right: 4px !important;
                padding-left: 4px !important;
            }

            .well-sm {
                padding-right: 4px !important;
                padding-left: 4px !important;
            }

            h2, .h2 {
                font-size: 26px !important;
            }
        }

        .form-control {
            margin-bottom:-20px !important;
        }

        label {
            color : #3B50A5;
            font-weight:800 !important;
        }
hr
{
width: 100%;
color: #CCCCCC;
height: 1px;


}
.modal-body {
    padding: 25px;
}

    </style>

</head>

<body>


    <!-- Navbar -->
	<?php include("include/navbar.php"); ?>
    <!-- End navbar -->


    <div class="well well-sm">
        <div class="text-center">
            <h2>
                <img src="img/provider-circle.png" class="img-circle" alt="profile" width="64" height="64" style="border: solid; border-width: medium" />
                Providers for <?php echo $firstname . " " . $lastname ?>
            </h2>
        </div>
        <!-- End container -->
    </div>

    <div class="main">

        <div class="container">
            <?php
            $instruction_content = "Enter your Doctors and other providers below.<br /><br />";
            $instruction_content = $instruction_content . "These may include Physicans, Hospitals, Home health agencies, Dialysis facilities, Inpatient Rehab Facilities, etc..<br /><br />";
            include("include/incInstructions.php");
			
            ?>
            <section>
                        <div id="providers"><span class="clearfix"></span></div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="panel-group accordion_providers">
                                    <div class="panel panel-success" style="border-color: black;">
                                        <div class="panel-heading text-center">
                                            <div class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion_providers" href="#collapseProviders">
                                                    <span style="font-size: large"><i class="icon-user-md" style="color: #3C763D"></i>&nbsp;&nbsp;<u><strong>Providers</strong></u></span>
                                                </a>
                                                <h5>Tap "Add Provider" to add or tap an icon to edit or delete.</h5>
                                            </div>
                                        </div>
                                        <div id="collapseProviders" class="panel-collapse in">
                                            <div class="panel-body" id="providers_panel_body">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <table class="table table-striped center" style="width: 100%;" id="provider_table">
                                                            <thead>
                                                                <tr class="text-left input-lg">
                                                                    <th style="text-align: left">Name</th>
                                                                    <th style="text-align: left">Phone</th>
                                                                    <th style="text-align: left" class="hidden-xs">Speciality</th>
                                                                    <th style="text-align: left" class="hidden-xs">Primary ?</th>
                                                                    <th style="text-align: left" class="hidden-xs">Edit Date</th>
                                                                    <th style="text-align: left">&nbsp;</th>
                                                                </tr>
                                                            </thead>
                                                              <tbody>

                                                                <?php 
                                                                    $recordset = $database->select("user_providers", [
                                                                        "ProviderId",
                                                                        "Name",
                                                                        "PhoneNumber",
                                                                        "Specialty",
                                                                        "Primary",
                                                                        "EditDate"
                                                                    ], [
                                                                        "uid" => $uid,
                                                                         "ORDER" => ['Name']
                                                                    ]);

                                                                foreach($recordset as $data)
                                                                {
                                                                    $ProviderId = $data["ProviderId"];
                                                                    $Name = $data["Name"];
                                                                    $PhoneNumber = $data["PhoneNumber"];
                                                                    $Speciality = $data["Specialty"];

                                                                    if ($data["Primary"] == 1) {
                                                                        $Primary = "Yes";
                                                                    } else {
                                                                      $Primary = "No";
                                                                    }
                                                                    
                                                                    $EditDate  = $data["EditDate"];

                                                                    //date_default_timezone_set('Africa/Nairobi');
                                                                    // Then call the date functions
                                                                    //$date = date('Y-m-d H:i:s');
														            $EditDate = new DateTime($data["EditDate"]);
														
                                                                    echo ("<tr class=\"text-left input-lg\">");
                                                                    echo ("  <td>" . $Name . "</td>");
                                                                    echo ("  <td>" . $PhoneNumber . "</td>");
                                                                    echo ("  <td>" . $Speciality . "</td>");
                                                                    echo ("  <td>" . $Primary . "</td>");
                                                                    echo ("  <td class=\"hidden-xs\">" . $EditDate->format('m/d/y h:i') . "</td>");
                                                                    echo ("  <td style=\"text-align:left;padding:10px\">");
														
														            $encrypted = Urlcrypt::encrypt($ProviderId . "|" . "edit". "|" . time());
                                                        
                                                                    echo ("    <div class=\"prov_record\"><span class=\"name hide\">" . $encrypted . "</span>");
                                                                    if ($dont_hide_Edit_Buttons == true) {
                                                                        echo ("      <button style=\"color:green\" type=\"button\" class=\"editProviderButton btn btn-sm btn-success glyphicon glyphicon-new-window glyphicon-edit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit Provider\"></button>");
                                                                        echo ("      <button style=\"color:red\"  type=\"button\" class=\"deleteProviderButton btn btn-sm btn-danger glyphicon glyphicon-trash\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Provider\"></button>");
                                                                    }
                                                                    echo ("    </div>");
                                                                    echo ("  </td>");
                                                                    echo ("</tr>");
                                                                }   

                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer text-center">
                                                <div class="alert alert-danger hide" role="alert" id="contact-deleted">Provider Deleted.</div>
                                                <br>
                                                <button type="button" id="btn_add_provider" class="btn btn-success btn-md">Add Provider</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 center-block">
                    <div class="card">
                        <img src="img/provider-avatar.png" alt="Provider Avatar" style="width:64px;height:64px" />
                        <div class="container">
                            <h4>
                                <b>John Doe</b>
                            </h4>
                            <p>Architect & Engineer</p>
                        </div>
                    </div>

                    <div class="well hidden-xs">
                        <h2>Be Sure To Add Your Providers</h2>
                        <p>You can use this area to manage your Provider information.</p>

                        <p>You now have one central location to access and share that information.</p>
                        <br />
                        <center>
                            <img src="img/medical_providers_sm.jpg" class="img-responsive img-rounded" alt="medical providers" border="5" />
                        </center>
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    </div>

    <div class="text-center">
        <a href="Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">Return To Home Screen</a>
    </div>
    <br />
    <!-- Footer -->
    <?php  include("include/incFooter-sm.php"); ?>

    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/jquery.form.js"></script>
    <script src="Scripts/modernizr.custom.34982.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap-datepicker.min.js"></script>
    <script src="Scripts/jquery.maskedinput.min.js"></script>
    <script src="Scripts/bootstrap-dialog.min.js"></script>
    <script src="Scripts/sweetalert2.min.js"></script>




    <!-- Le javascript
================================================== -->


    <script type="text/javascript">
        // When the document is ready
        $.wait = function (callback, seconds) {
            return window.setTimeout(callback, seconds * 1000);
        }

        $(document).ready(function () {

            //####################################################### Add Provider button  ##################################
            $('#btn_add_provider').on('click', function (e) {
                e.preventDefault();
                window.location = "admin/provider_manage.php";
            })


            //################################################## provider edit button ###############################################
            $('.editProviderButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {

                    //set which record we're editing so we can update it later
                    record = $(this).parents('.prov_record');
                    //populate the editing form within the dialog
                    ////$('#myInput').val(record.find('.name').html());
                    var prov_item = record.find('.name').html();

                    $window_url = "admin/provider_manage.php?id=" + prov_item;
                    //alert($window_url);
                    $(location).attr('href', $window_url);

                });


            //################################################## provider delete button ###############################################
            $('.deleteProviderButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {
                    swal({
                        title: 'Delete Provider?',
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#41A24D',
                        cancelButtonColor: '#D54F49',
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            record = $(this).parents('.prov_record');
                            var prov_item = record.find('.name').html();
                            //alert(ins_item);

                            var values = {
                                'itemno': prov_item,
                            };

                            $.ajax({
                                type: "POST",
                                url: "ws/delete-provider.php",
                                data: values,
                                success: function (msg) {
                                    swal({
                                        title: "Deleted",
                                        text: "Provider has been deleted." + msg,
                                        type: "success"
                                    }).then(function () {
                                        location.reload();
                                    });

                                },
                                error: function () {
                                    alert("Error deleting provider");
                                },
                                complete: function () {
                                }
                            });
                        }
                    })
                });


            function strcmp(str1, str2) {
                return str1.localeCompare(str2) / Math.abs(str1.localeCompare(str2));
            }



        }) //document ready


    </script>
</body>
</html>
