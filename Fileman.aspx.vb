﻿
Imports System.IO
Imports System.Security.Cryptography
Imports GleamTech.FileUltimate

Namespace FileManager

    Partial Class Fileman
        Inherits Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            If Not IsPostBack Then
                'PopulateUserSelector()
            End If

            SetDynamicFolderAndPermissions(UserSelector.SelectedValue)
        End Sub

        Private Sub SetDynamicFolderAndPermissions(userName As String)
            Dim rootFolder = New FileManagerRootFolder() With {
                    .Name = String.Format("Folder of {0}", userName),
                    .Location = String.Format("~/App_Data/RootFolder3/{0}", userName)
                    }

            Dim accessControl = New FileManagerAccessControl() With {
                    .Path = "\"
                    }

            Select Case userName
                Case "User1"
                    accessControl.AllowedPermissions = FileManagerPermissions.Full
                    Exit Select
                Case "User2"
                    accessControl.AllowedPermissions = FileManagerPermissions.ReadOnly Or FileManagerPermissions.Upload
                    Exit Select
            End Select

            rootFolder.AccessControls.Add(accessControl)
            FileManager.RootFolders.Add(rootFolder)
        End Sub

        Public Function DecryptRJ256(ByVal prm_key As String, ByVal prm_iv As String, ByVal prm_text_to_decrypt As String)

            Dim sEncryptedString As String = prm_text_to_decrypt

            Dim myRijndael As New RijndaelManaged
            myRijndael.Padding = PaddingMode.Zeros
            myRijndael.Mode = CipherMode.CBC
            myRijndael.KeySize = 256
            myRijndael.BlockSize = 256

            Dim key() As Byte
            Dim IV() As Byte

            key = System.Text.Encoding.ASCII.GetBytes(prm_key)
            IV = System.Text.Encoding.ASCII.GetBytes(prm_iv)

            Dim decryptor As ICryptoTransform = myRijndael.CreateDecryptor(key, IV)

            Dim sEncrypted As Byte() = Convert.FromBase64String(sEncryptedString)

            Dim fromEncrypt() As Byte = New Byte(sEncrypted.Length) {}

            Dim msDecrypt As New MemoryStream(sEncrypted)
            Dim csDecrypt As New CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)

            csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length)

            Return (System.Text.Encoding.ASCII.GetString(fromEncrypt))

        End Function


        Public Function EncryptRJ256(ByVal prm_key As String, ByVal prm_iv As String, ByVal prm_text_to_encrypt As String)

            Dim sToEncrypt As String = prm_text_to_encrypt

            Dim myRijndael As New RijndaelManaged
            myRijndael.Padding = PaddingMode.Zeros
            myRijndael.Mode = CipherMode.CBC
            myRijndael.KeySize = 256
            myRijndael.BlockSize = 256

            Dim encrypted() As Byte
            Dim toEncrypt() As Byte
            Dim key() As Byte
            Dim IV() As Byte

            key = System.Text.Encoding.ASCII.GetBytes(prm_key)
            IV = System.Text.Encoding.ASCII.GetBytes(prm_iv)

            Dim encryptor As ICryptoTransform = myRijndael.CreateEncryptor(key, IV)

            Dim msEncrypt As New MemoryStream()
            Dim csEncrypt As New CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)

            toEncrypt = System.Text.Encoding.ASCII.GetBytes(sToEncrypt)

            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length)
            csEncrypt.FlushFinalBlock()

            encrypted = msEncrypt.ToArray()

            Return (Convert.ToBase64String(encrypted))

        End Function

    End Class
End Namespace
