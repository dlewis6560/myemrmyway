<?php

include("../include/incConfig.php");
include("../include/incFunctions.php");

use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';
Urlcrypt::$key = $mykey;

session_start();

//make sure we have a valid sesion
include("../include/session.php");

//make sure the upload directory exists
makeDir ("C:\\inetpub\\wwwroot\\emtelink\\new\\Uploads\\" . $subId . "\\");

$target_dir = "C:\\inetpub\\wwwroot\\emtelink\\new\\Uploads\\" . $subId . "\\";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$target_filename_only = basename($_FILES["fileToUpload"]["name"]);

//var_dump($_FILES);
//var_dump($_REQUEST);

$docnameInput = $_POST["docnameInput"];
$docdescInput = $_POST["docdescInput"];
//$uid = $_POST["uid"];

$file_size = $_POST["file_size"];
$file_ext_type = $_POST["file_ext_type"];
$mime_type = $_POST["mime_type"];

$file_name = basename( $_FILES["fileToUpload"]["name"]);

//echo $target_filename_only;
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "\n\nSorry, file already exists.";
    $uploadOk = 0;
}
// Check file size, must be less than 25mb
if ($_FILES["fileToUpload"]["size"] > 25000000) {
    echo "\n\nSorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
&& $imageFileType != "gif" && $imageFileType != "docx" ) {
    echo "\n\nSorry, only pdf, jpg, jpeg, png & docx files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    //echo "\n\nSorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        //insert document record into the database
        $database->insert("user_docs", [
           "uid" => $uid,
           "doc_name" => $docnameInput,
           "doc_description" => $docdescInput,
           "file_name" => $file_name,
           "file_size" => $file_size,
           "file_type" => $file_ext_type,
           "mime_type" => $mime_type,
           "status" => "uploaded by user"
           //"createdate" => "uploaded by user"
        ]);
        //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded." . " - file_ext_type:" . $file_ext_type;
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}


?>