<?php
require '../PhpMailer/PHPMailerAutoload.php';

use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';

include("../Include/incConfig.php");


session_start();

//make sure we have a valid sesion
if ($_SESSION["valid"] != "TRUE")
{
    echo "invalid session";
    exit;
};

$firstname = $_SESSION["firstname"];
$lastname = $_SESSION["lastname"];
$uid = $_SESSION["uid"];
$userid = $_SESSION["userid"];
$admin_user = $_SESSION["admin_user"];
$subType = $_SESSION["subType"];
$subMaxUsers = $_SESSION["subMaxUsers"];
$subId = $_SESSION["subId"];


if (isset($_POST['email_userid'])) {

    //get the posted values from the consent page ajax

    $recp_name = $_POST['recp_name'];
    $recp_email = $_POST['recp_email'];
    $email_subject = $_POST['email_subject'];
    $email_message = $_POST['email_message'];
    $email_userid = $_POST['email_userid'];


    
    //echo $recp_name . "<br />";
    //echo $recp_email . "<br />";
    //echo $email_subject . "<br />";
    //echo $email_message . "<br />";
    //echo $email_userid  . "<br />";

    $datas = $database->select("user", [
        "firstname",
        "lastname",
        "userid"
    ], [
        "uid" => $email_userid
    ]);

    foreach($datas as $data)
    {
        //echo "name: " . $data["firstname"] . " " . $data["lastname"] . " - " . $data["userid"];
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $userid = $data["userid"];
    }

    //exit;
}

//exit;

function ajaxResponse($status, $message, $data = NULL, $mg = NULL) {
    $response = array (
      'status' => $status,
      'message' => $message,
      'data' => $data,
      'mailgun' => $mg
      );
    $output = json_encode($response);
    exit($output);
}


$recp_name = $_POST['recp_name'];
$recp_email = $_POST['recp_email'];
$email_subject = $_POST['email_subject'];
$email_message = $_POST['email_message'];
$email_userid = $_POST['email_userid'];


Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
$encrypted = Urlcrypt::encrypt($userid . "|" . time());


$mail = new PHPMailer;

//$mail->SMTPDebug = 2;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
//$mail->Host = 'secure.emailsrvr.com';  // Specify main and backup SMTP servers // secure.emailsrvr.com
//$mail->SMTPAuth = true;                               // Enable SMTP authentication
//$mail->Username = 'support@emtelink.com';                 // SMTP username
//$mail->Password = 'Fish12';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
//$mail->Port = 587;                                    // TCP port to connect to

$mail->Host = 'smtp.mailgun.org';  // Specify main and backup SMTP servers // secure.emailsrvr.com
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'postmaster@emtelink.com';                 // SMTP username
$mail->Password = 'f6175a9ce7d5a17d09ba7a2e81c54109';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
//$mail->Port = 587;                                    // TCP port to connect to


$mail->From = 'support@EMTeLink.com';
$mail->FromName = 'support@EMTeLink.com';
//$mail->addAddress('dlewis6560@aol.com', 'Donnie Lewis');     // Add a recipient
//$mail->addAddress($recp_email, $recp_name);     // Add a recipient
//$mail->addAddress('jsewell@dcboe.com', 'James Sewell');     // Add a recipient
//$mail->addAddress('dlewis6560@gmail.com');               // Name is optional
$mail->addAddress($recp_email);     // Add a recipient

//$mail->AddBCC("dean@emfotech.com","Dean Massey");
$mail->AddBCC("dlewis6560@aol.com","Donnie Lewis");


$mail->addReplyTo('noreply@emtelink.com', 'NoReply');
//$mail->addCC('cc@example.com');
////$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Emergency Health Record for ' . $firstname . " " . $lastname;
if ($subType != "F") {
      $mail->Body    = '<!DOCTYPE html> <html xmlns=http://www.w3.org/1999/xhtml> <head> <title></title> <style type=text/css> .text-style { font-family: Arial, Helvetica, sans-serif; font-size: large; font-weight: 500; font-style: normal; } .text-xsmall { font-family: Arial, Helvetica, sans-serif; font-size: x-small; font-weight: 500; font-style: normal; } </style> </head> <body> <div class=text-style> This is an important message from EMTeLink.<br /><br /> It has been sent to you by <u>James Sewell</u> of <u>Bainbridge High School</u>.<br /><br /> Please use the secure link below to display the Emergency Information for <u>' .  $firstname  . ' ' . $lastname . '</u>.<br /> <br /> <a href=https://www.emtelink.com/school/SecureEmergencyInfo.php?id='.$encrypted.'=>Click to View Emergency Info for ' .  $firstname  . ' ' . $lastname . '</a> &nbsp;&nbsp;(NOTE: This Link will expire in 24 Hours.) </div> <br /> <br /> <div class=text-xsmall> This email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager by responding to support@EMTeLink.com. Please do not disseminate, distribute or copy this e-mail. Please notify the EMTeLink immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. </div> </body> </html>';
    }
    else {
      $mail->Body    = '<!DOCTYPE html> <html xmlns=http://www.w3.org/1999/xhtml> <head> <title></title> <style type=text/css> .text-style { font-family: Arial, Helvetica, sans-serif; font-size: large; font-weight: 500; font-style: normal; } .text-xsmall { font-family: Arial, Helvetica, sans-serif; font-size: x-small; font-weight: 500; font-style: normal; } </style> </head> <body> <div class=text-style> This is an important message from EMTeLink.<br /><br />Please use the secure link below to display the Emergency Information for <u>' .  $firstname  . ' ' . $lastname . '</u>.<br /> <br /> <a href=https://www.emtelink.com/tracker/SecureEmergencyInfo.php?id='.$encrypted.'=>Click to View Emergency Info for ' .  $firstname  . ' ' . $lastname . '</a> &nbsp;&nbsp;(NOTE: This Link will expire in 24 Hours.) </div> <br /> <br /> <div class=text-xsmall> This email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager by responding to support@EMTeLink.com. Please do not disseminate, distribute or copy this e-mail. Please notify the EMTeLink immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. </div> </body> </html>';    
    }
//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

/*
$mail->smtpConnect([
    'tls' => [
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    ]
]);

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
*/

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}

?>