<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <title>MyEMRMyWay :: User Manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="description" content="MyEMRMyWay App" />

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">
    
    <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
    
    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">


    <link href="../Content/bootstrap-dialog.min.css" rel="stylesheet" />

    <link href="./css/pages/dashboard.css" rel="stylesheet">

    <link href="./css/custom.css" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php


    include("../include/incConfig.php");
    session_start();

	include("../include/session.php");
	
    $filter_text = "( not filtered )";

    if (isset($_POST["current_page"])) {
        $current_page = $_POST["current_page"];
    }else{
        $current_page=1;
    }


    if (isset($_POST["page_filter"])) {
        $filter = $_POST["page_filter"];
    }else{
        $filter="";
    }

    $where = "where dbo.[user].subID=" . $subId;

    if (strlen($filter) > 0) {
        switch ($filter) {
            case 'a-d':
                $where = "where ((lastname like 'a%') or (lastname like 'b%') or (lastname like 'c%') or (lastname like 'd%')) AND dbo.[user].subID=" . $subId;
                $filter_text = "( last name filter applied - A thru D )";
                break;
            case 'e-h':
                $where = "where ((lastname like 'e%') or (lastname like 'f%') or (lastname like 'g%') or (lastname like 'h%')) AND dbo.[user].subID=" . $subId;
                $filter_text = "( last name filter applied - E thru H )";
                break;
            case 'i-l':
                $where = "where ((lastname like 'i%') or (lastname like 'j%') or (lastname like 'k%') or (lastname like 'l%')) AND dbo.[user].subID=" . $subId;
                $filter_text = "( last name filter applied - I thru L )";
                break;
            case 'm-p':
                $where = "where ((lastname like 'm%') or (lastname like 'n%') or (lastname like 'o%') or (lastname like 'p%')) AND dbo.[user].subID=" . $subId;
                $filter_text = "( last name filter applied - M thru P )";
                break;
            case 'q-t':
                $where = "where ((lastname like 'q%') or (lastname like 'r%') or (lastname like 's%') or (lastname like 't%')) AND dbo.[user].subID=" . $subId;
                $filter_text = "( last name filter applied - Q thru T )";
                break;
            case 'u-w':
                $where = "where ((lastname like 'u%') or (lastname like 'v%') or (lastname like 'w%')) AND dbo.[user].subID=" . $subId;
                $filter_text = "( last name filter applied - U thru W )";
                break;
            case 'x-z':
                $where = "where ((lastname like 'x%') or (lastname like 'y%') or (lastname like 'z%')) AND dbo.[user].subID=" . $subId;
                $filter_text = "( last name filter applied - X thru Z )";
                break;
            default:
                $where = "where ((lastname like '" . $filter . "%') ) AND dbo.[user].subID=" . $subId;
                $filter_text = "( last name filter applied - " . $filter . " )";
        }

    }

    ?>
	
	<style>
	
		@media (max-width: 768px)
		{
		  .table-responsive {
			width: 100%;
			margin-bottom: 15px;
			/* overflow-x: scroll; */
			overflow-y: -webkit-paged-x;
			border: 1px solid #ddd;
		  }
        }		
	
	
	
	</style>

</head>

<body>

    <?php

    if ($subType == "F") {
      $PageSize = 10;
    }
    else {
      $PageSize = 10;
    }

    $MaxPages = 0;

    $strSQLCount = "SELECT Count(userid) as REC_COUNT ";
    //$strSQLCount = $strSQLCount . "FROM user ";
    $strSQLCount = $strSQLCount . "FROM [user] " . $where;

    $recordset = $database->query($strSQLCount);
    foreach($recordset as $data)
    {
        $count = $data["REC_COUNT"];
    }

    $MaxPages = ceil($count / $PageSize);

    $strSQL = "SELECT dbo.[user].uid, dbo.[user].userid, dbo.[user].firstname, dbo.[user].lastname, dbo.[user].student_email, dbo.groups.gname ";
    $strSQL = $strSQL . "FROM dbo.groups INNER JOIN ";
    $strSQL = $strSQL . "dbo.user_groups ON dbo.groups.gid = dbo.user_groups.gid RIGHT OUTER JOIN ";
    $strSQL = $strSQL . "dbo.[user] ON dbo.user_groups.uid = dbo.[user].uid ";
    $strSQL = $strSQL . " " . $where . " ";

    //$strSQL = "SELECT uid,userid,firstname,lastname,student_email ";
    //$strSQL = $strSQL . "FROM [user] " . $where . " ";
    //$strSQL = $strSQL . "FROM user ";
    $strSQL = $strSQL . "ORDER BY lastname ";
    $strSQL = $strSQL . "OFFSET (".$PageSize." * (".$current_page." - 1)) ROWS ";
    $strSQL = $strSQL . "FETCH NEXT ".$PageSize." ROWS ONLY;";

    //echo $strSQL;
    //exit;

    //echo "<script> alert('server-strSQL= " . $strSQL . "');</script>";

    //$strSQL = "WITH TempResult AS( ";
    //$strSQL = $strSQL . "SELECT userid,firstname,lastname ";
    //$strSQL = $strSQL . " FROM student ";
    //$strSQL = $strSQL . " ), TempCount AS ( ";
    //$strSQL = $strSQL . " SELECT COUNT(*) AS MaxRows FROM TempResult ";
    //$strSQL = $strSQL . " ) ";
    //$strSQL = $strSQL . " SELECT * ";
    //$strSQL = $strSQL . " FROM TempResult, TempCount ";
    //$strSQL = $strSQL . " ORDER BY TempResult.lastname ";
    //$strSQL = $strSQL . " OFFSET (@PageNum-1)*@PageSize ROWS ";
    //$strSQL = $strSQL . " FETCH NEXT @PageSize ROWS ONLY";


    ?>

		<!-- Navbar -->
		<?php include("../include/navbar.php"); ?>
		<!-- End navbar -->

        <div class="well well-sm">
        <div class="text-center">
            <?php if ($subType != "F") {  ?>
                    <h2><i class="icon-user" style="color:#F99500;"></i> Manage Users</h2>
            <?php  }
                  else { ?>
                     <h2><i class="icon-user" style="color:#F99500;"></i> Manage Family Members</h2>
            <?php }  ?>
        </div>
        <!-- End container -->
    </div>



    <div class="main">

        <div class="container">
            <?php 
            $instruction_content = "Enter additional family members below.<br /><br />";
            $instruction_content = $instruction_content . "Use the \"Add New User\" button to add family members.<br /><br />";
            $instruction_content = $instruction_content . "Click on the Action button to see additional actions : View Report, Email Report, Reset Password, Delete User";
            include("../include/incInstructions.php");
            ?>
            <br />
            <div class="row">

                <div class="col-md-12">

                    <!--    <div class="well">

                        <h4>Manage Users</h4>

                        <p>Select an Action ...</p>

                    </div> -->

                    <div class="widget stacked ">

                        <div class="widget-header" style="vertical-align: middle">
                            <i class="icon-user" style="color:#F99500;"></i>
                            <?php if ($subType == "F") {  ?>
                              <h3>Manage Family (max of <?php echo $subMaxUsers . ")" ?></h3>
                            <?php }
                                  else {?>
                              <h3>Manage Users</h3>
                            <?php }  ?>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content text-center">

                            <?php if ($subType != "F") {  ?>
                            <h2>User List</h2>
                            <div style="font-size: 16px; color: red" id="filter_text"><?php echo $filter_text; ?>
                                <!--<h2>User List</h2> -->

                                <!-- <h4>(Filtered By LastName : X - Z)</h4> -->
                            </div>
                            <div class="well text-center" style="vertical-align: middle; width: 95%; margin: 0 auto;">
                                <div class="btn-toolbar">
                                    <button type="button" class="btn btn-default btn-md" style="color: black" id="btn1" value="a-d">&nbsp;A-D&nbsp;</button>
                                    <button type="button" class="btn btn-default btn-md" id="btn2" value="e-h">&nbsp;E-H&nbsp;</button>
                                    <button type="button" class="btn btn-default btn-md" style="color: black" id="btn3" value="i-l">&nbsp;I-L&nbsp;</button>
                                    <button type="button" class="btn btn-default btn-md" id="btn4" value="m-p">&nbsp;M-P&nbsp;</button>
                                    <button type="button" class="btn btn-default btn-md" style="color: black" id="btn5" value="q-t">&nbsp;Q-T&nbsp;</button>
                                    <button type="button" class="btn btn-default btn-md" id="btn6" value="u-w">&nbsp;U-W&nbsp;</button>
                                    <button type="button" class="btn btn-default btn-md" style="color: black" id="btn7" value="x-z">&nbsp;X-Z&nbsp;</button>
                                    <button type="button" class="btn btn-default btn-md" style="color: black" id="btn_all" value="all">&nbsp;ALL&nbsp;</button>
                                </div>
                                <br />
                                <form class="form-search form-inline">
                                    <div class="input-group" style="width: 45%; margin: 0 auto;">
                                        <input type="text" class="form-control search-query" id="search_value" name="search_value" placeholder="enter characters in last name..." />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary btn-md" id="btn8">Search</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <br />
                            <?php } ?>
                            <div class="table-responsive text-center" style="width: 95%; margin: 0 auto; font-size: 16px" id="user-div">
                                <table class="table table-bordered table-hover table-striped table-responsive" id="user-table">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center" style="font-size: 18px">Current</th> -->
                                            <th class="text-center" style="font-size: 18px">Action</th>
                                            <th class="text-left" style="font-size: 18px">User ID</th>
                                            <th class="text-left" style="font-size: 18px">Name</th>
                                            <th class="text-left hidden-xs" style="font-size: 18px">Email</th>
                                            <?php if ($subType != "F") {  ?>
                                            <th class="text-left hidden-xs" style="font-size: 18px">Group</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //echo $strSQL;
                                        $recordset = $database->query($strSQL);
                                        foreach($recordset as $data)
                                        {
                                            $uid = $data["uid"];
                                            $studentid = $data["userid"];
                                            $firstname = $data["firstname"];
                                            $lastname = $data["lastname"];
                                            $student_email = $data["student_email"];
                                            $groupname = $data["gname"];

                                            //echo "student - " . $studentid . "<br />";
                                            //echo "firstname - " . $firstname;
                                        ?>
                                        <tr>
                                            <!-- <td class="text-left" style="font-size: 24px"><input type="radio" id="bolCurrentUser" name="bolCurrentUser" <?php if ($studentid==$userid) { echo "checked"; } ?> /></td> -->
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default btn-md dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" style="z-order:1000;">
                                                        <li><a href="../EmergencyInfo.php?report_user_id=<?php echo $studentid; ?>" target="_blank" style="font-size: 16px">View Report</a></li>
                                                        <li><a href="#" style="font-size: 16px" class="email_rpt" id="<?php echo $uid; ?>">Email Report</a></li>
                                                        <!--<li><a href="#" style="font-size: 16px">View Details</a></li>-->    
                                                        <li><a href="#" style="font-size: 16px" class="notify_contacts" id="<?php echo $uid; ?>">Notify Contacts</a></li>
                                                        <?php if ($_SESSION["admin_user"] == 1) {  //don't show reset and delete actions for non-admin users ?>
                                                        <li><a href="#" style="font-size: 16px" class="reset_pw" id="<?php echo $uid; ?>">Reset Password</a></li>
                                                        <li><a href="#" style="font-size: 16px" class="delete_user" id="<?php echo $uid; ?>">Delete User</a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                                <!-- <button type="button" class="btn-md btn-success">Manage</button>&nbsp;&nbsp; -->
                                            </td>
                                            <td class="text-left" style="font-size: 16px"><?php echo $studentid;?></td>
                                            <td class="text-left" style="font-size: 16px"><?php echo $firstname . " " . $lastname;?></td>
                                            <td class="text-left hidden-xs" style="font-size: 16px"><?php echo $student_email;?></td>
                                            <?php if ($subType != "F") {  ?>
                                            <td class="text-left hidden-xs" style="font-size: 16px">
                                                <?php echo $groupname;?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        }   
                                        ?>
                                    </tbody>
                                </table>

                                <!-- <ul class="pagination">
                                    <li><a href="javascript:;">&laquo;</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="javascript:;">2</a></li>
                                    <li><a href="javascript:;">3</a></li>
                                    <li><a href="javascript:;">4</a></li>
                                    <li><a href="javascript:;">5</a></li>
                                    <li><a href="javascript:;">&raquo;</a></li>
                                </ul> -->
                                <div class="well well-sm">
                                    <div class="btn-toolbar" style="margin: 0 auto;">
                                        <?php if ($_SESSION["admin_user"] == 1) {  //don't show add new user button for non-admin users ?>
                                        <button id="btn_add_user" class="btn btn-md btn-primary <?php if ($subType != "F") { ?>pull-left<?php } ?>">Add New User</button>
                                        <?php } ?>

                                        
                                        <?php if ($subType != "F") {  //don't show buttons for family account ?>
                                        <button id="btn_last" class="btn btn-md btn-default pull-right">Last</button>&nbsp;&nbsp;
                                        <button id="btn_next" class="btn btn-md btn-primary pull-right">Next</button>
                                        <button id="btn_prev" name="btn_prev" class="btn btn-md btn-primary pull-right">Prev</button>&nbsp;&nbsp;
                                        <button id="btn_first" class="btn btn-md btn-default pull-right">First</button>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div>
                                    <form id="user_form" class="user_form" method="post" action="user-manager.php">
                                        <input type="hidden" value="<?php echo $current_page?>" id="current_page" name="current_page" />
                                        <input type="hidden" value="<?php echo $filter?>" id="page_filter" name="page_filter" />
                                        <input type="hidden" value="<?php echo $MaxPages?>" id="MaxPages" name="MaxPages" />
                                        <input type="hidden" value="<?php echo $subId?>" id="subId" name="subId" />
                                    </form>
                                </div>


                                <div id="page_footer">
                                    <?php
                                    if ($subType != "F") {   //don't show if family account
                                      if ($MaxPages > 0) {
                                        
                                        echo "Page " . $current_page . " of " . $MaxPages; 
                                        
                                      }
                                      else {
                                        
                                        echo "no records found";
                                      }
                                    }
                                    ?>
                                </div>

                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>


            </div>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 center-block">
                    <?php if ($subType == "F") {?>
                    <div class="well">
                        <h2>Register Your Family Members</h2>
                        <p>Use this section to register your family members. Select "Add New User" to create a new family member account.<br />
                            <br />
                            Also, you can view, email, print family reports, and change passwords using the Action menu.</p>
                        <br />
                        <center>
                        <img src="../img/family_circle.jpg" class="img-responsive img-rounded hidden-xs" alt="family image">
                            </center>
                    </div>
                    <?php } else {?>
                    <div class="well">
                        <h2>Register Your Students</h2>
                        <p>
                            Use this section to register your student accounts and send parent emails. Select "Add New User" to create a new student account.
                            <br />
                            <br />
                            Also, you can view, email, print family reports, and change passwords using the Action menu.
                        </p>
                        <br />
                        <center>
                            <img src="../img/student-circle-sm2.jpg" class="img-responsive img-rounded hidden-xs" alt="student image" />
                        </center>
                    </div>
                    <?php }?>

                </div>
                <div class="col-sm-1"></div>
            </div>

        </div>
        <div id="chgPassword" class="modal fade" role="dialog">
            <form id="chg_password_form" class="chg_password_form">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="ch_pw_modal_title">Change Password</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                                <div class="col-xs-3">
                                    <label style="font-size: 16px; color: black; font-weight: 500" for="new_pw">New Password</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control input-md" id="new_pw" name="new_pw" placeholder="enter new password" value="">
                                </div>
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                                <div class="col-xs-3">
                                    <label style="font-size: 16px; color: black; font-weight: 500" for="confirm_pw">Confirm</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control input-md" id="confirm_pw" name="confirm_pw" placeholder="confirm" value="">
                                </div>
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" id="btn_save">Save</button>
                            <br />
                            <br />
                            <div>
                                <div class="alert alert-success hide text-center" role="alert" id="pw-chg-success">Password changed successfully.</div>
                                <div class="alert alert-danger hide text-center" role="alert" id="pw-chg-nomatch"><b>Password</b> and <b>Confirm</b> fields do not match. Try again.</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <input type="hidden" id="chg_pw_userid" name="pw_userid" value="<?php echo $userid ?>" />
                    </div>
                </div>
            </form>
        </div>


        <div id="mailReport" class="modal fade" role="dialog">
            <form id="mail_report_form" class="mail_report_form">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="mail_report_modal_title">Email Report</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row hide">
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                                <div class="col-xs-3">
                                    <label style="font-size: 16px; color: black; font-weight: 500" for="recp_name">Recipient&apos;s Name</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control input-md" id="recp_name" name="recp_name" placeholder="enter name" value="">
                                </div>
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">&nbsp;</div>
                                <div class="col-xs-8">
                                    <label style="font-size: 16px; color: black; font-weight: 500" for="recp_email">Enter Recipient&apos;s Email Address</label>
                                </div>
                                <div class="col-xs-2">&nbsp;</div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-xs-2">&nbsp;</div>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control input-md required" id="recp_email" name="recp_email" placeholder="email address" value="">
                                </div>
                                <div class="col-xs-2">&nbsp;</div>
                            </div>
                            <div class="row hide">
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                                <div class="col-xs-3">
                                    <label style="font-size: 16px; color: black; font-weight: 500" for="email_subject">Subject</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="email" class="form-control input-md" id="email_subject" name="email_subject" placeholder="subject" value="" required>
                                </div>
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row hide">
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                                <div class="col-xs-3">
                                    <label style="font-size: 16px; color: black; font-weight: 500" for="email_message">Message</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="email" class="form-control input-md" id="email_message" name="email_message" placeholder="message" value="" required>
                                </div>
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" id="btn_send_email">Send</button>
                            &nbsp;
                            <img src="../img/spinner.gif" alt="sending...." width="25" height="25" class="hide" id="busy" />
                            <br />
                            <br />
                            <div>
                                <div class="alert alert-success hide text-center" role="alert" id="email-success">Email sent successfully.</div>
                                <div class="alert alert-danger hide text-center" role="alert" id="email-fail">Email address is required.</div>
                                <div class="alert alert-danger hide text-center" role="alert" id="email-address-not-valid">Email address is not valid.&nbsp;&nbsp;Make sure it includes an @ character.</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <input type="hidden" id="email_userid" name="email_userid" value="" />
                    </div>
                </div>
            </form>
        </div>

        <div id="mailContact" class="modal fade" role="dialog">
            <form id="mail_contact_form" class="mail_contact_form">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="mail_contact_modal_title">Notify Emergency Contact(s)</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-xs-12">
                                    <label style="font-size: 20px; color: black; font-weight: 500" for="notify_message">Enter A Message To Send</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea id="notify_message" name="notify_message" cols="70" rows="10">
                                    </textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">&nbsp;</div>
                            </div>

                            <div class="row">
                                <div class="col-xs-2">&nbsp;</div>
                                <div class="col-xs-4">
                                    <input type="checkbox" id="ckNotifyType1" name="ckNotifyType" />
                                    <label for="ckNotifyType1" style="font-size: 20px; color: black; font-weight: 500" >Voice Call</label>
                                    <input type="tel" name="notify_phone">
                                </div>
                                <div class="col-xs-4">
                                    <input type="checkbox" id="ckNotifyType2" name="ckNotifyType" />
                                    <label for="ckNotifyType2" style="font-size: 20px; color: black; font-weight: 500" >Email</label>
                                    <input type="email" name="notify_email">
                                </div>
                                <div class="col-xs-2">&nbsp;</div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" id="btn_send_notify">Send</button>
                            &nbsp;
                            <img src="../img/spinner.gif" alt="sending...." width="25" height="25" class="hide" id="busy_notify" />
                            <br />
                            <br />
                            <div>
                                <div class="alert alert-success hide text-center" role="alert" id="notify-success">Notification sent successfully.</div>
                                <div class="alert alert-danger hide text-center" role="alert" id="notify-fail">Notificaion failed.</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <input type="hidden" id="notify_userid" name="notify_userid" value="" />
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br />
    <div class="text-center">
        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">Return To Home Screen</a>
    </div>
    <br />
    <footer>
        <div class="extra">
        <div class="container text-center">
            <p>&copy; Copyright @ 2016 EMTeLink</p>
        </div>
        </div>
        <!-- end Container-->
    </footer>



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="../Scripts/bootstrap-dialog.min.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $.wait = function (callback, seconds) {
            return window.setTimeout(callback, seconds * 1000);
        }

        $(document).ready(function () {



            //####################################################### Add User  ##################################
            $('#btn_add_user').on('click', function (e) {
                e.preventDefault();
                //alert("test");
                window.location = "user-add.php";
            })
            //####################################################### Next Button ##################################
            $(document).on("click", "#btn_next", function (e) {
                e.preventDefault();

                //alert($("#current_page").val());
                $('input[name=current_page]').val(parseInt($("#current_page").val(), 10) + 1);

                //var url = "user-grid.php";
                $.ajax({
                    type: 'POST',
                    url: "user-grid.php",
                    data: $('form.user_form').serialize(),
                    success: function (msg) {
                        //replace the html with data from the server
                        $("#user-table").html(msg);
                        //iterate the current page
                        //var next_page = parseInt($("#current_page").val(), 10) + 1;
                        //$('input[name=current_page]').val(next_page);
                        //update page footer
                        $("#page_footer").html("Page " + $("#current_page").val() + " of " + $("#MaxPages").val());
                        //enable Prev button
                        if ($("#current_page").val() != 1) {
                            $("#btn_prev").prop('disabled', false);
                        }
                        if ($("#current_page").val() == $("#MaxPages").val()) {
                            $("#btn_next").prop('disabled', true);
                        }
                    },
                    error: function () {
                        alert("User grid error: unable to move to next page, please try again.");
                    }
                });
            });

            //####################################################### Prev Button ##################################
            $(document).on("click", "#btn_prev", function (e) {
                e.preventDefault();

                //alert($("#current_page").val());

                $('input[name=current_page]').val(parseInt($("#current_page").val(), 10) - 1);

                //var url = "user-grid.php";
                $.ajax({
                    type: 'POST',
                    url: "user-grid.php",
                    data: $('form.user_form').serialize(),
                    success: function (msg) {
                        //replace the html with data from the server
                        $("#user-table").html(msg);
                        //iterate the current page
                        //var next_page = parseInt($("#current_page").val(), 10) - 1;
                        //$('input[name=current_page]').val(next_page);
                        //update page footer
                        $("#page_footer").html("Page " + $("#current_page").val() + " of " + $("#MaxPages").val());
                        //disable prev button is on first page
                        if ($("#current_page").val() == 1) {
                            $("#btn_prev").prop('disabled', true);
                        }
                        //enable next button
                        if ($("#current_page").val() != $("#MaxPages").val()) {
                            $("#btn_next").prop('disabled', false);
                        }
                    },
                    error: function () {
                        alert("User grid error: unable to move to previous page, please try again.");
                    }
                });
            });

            //####################################################### Last Button ##################################
            $(document).on("click", "#btn_last", function (e) {
                e.preventDefault();

                //alert($("#current_page").val());

                //var url = "user-grid.php";
                $('input[name=current_page]').val($("#MaxPages").val());

                $.ajax({
                    type: 'POST',
                    url: "user-grid.php",
                    data: $('form.user_form').serialize(),
                    success: function (msg) {
                        //replace the html with data from the server
                        $("#user-table").html(msg);
                        //iterate the current page
                        var next_page = parseInt($("#current_page").val(), 10);
                        $('input[name=current_page]').val(next_page);
                        //update page footer
                        $("#page_footer").html("Page " + $("#current_page").val() + " of " + $("#MaxPages").val());
                        $("#btn_next").prop('disabled', true);
                        $("#btn_prev").prop('disabled', false);
                    },
                    error: function () {
                        alert("User grid error: unable to move to last page, please try again.");
                    }
                });
            });

            //####################################################### First Button ##################################
            $(document).on("click", "#btn_first", function (e) {
                e.preventDefault();

                //alert($("#current_page").val());

                //var url = "user-grid.php";
                $('input[name=current_page]').val(1);

                $.ajax({
                    type: 'POST',
                    url: "user-grid.php",
                    data: $('form.user_form').serialize(),
                    success: function (msg) {
                        //replace the html with data from the server
                        $("#user-table").html(msg);
                        //iterate the current page
                        //var next_page = parseInt($("#current_page").val(), 10);
                        //$('input[name=current_page]').val(next_page);
                        //update page footer
                        $("#page_footer").html("Page " + $("#current_page").val() + " of " + $("#MaxPages").val());
                        $("#btn_prev").prop('disabled', true);
                        $("#btn_next").prop('disabled', false);
                    },
                    error: function () {
                        alert("User grid error: unable to move to next page, please try again.");
                    }
                });
            });

            //####################################################### Filter button #########################################
            $(document).on("click", "#btn1, #btn2, #btn3, #btn4, #btn5, #btn6, #btn7", function (e) {
                e.preventDefault();
                //alert($(this).val());
                var filter = $(this).val().toString();

                $('input[name=page_filter]').val(filter);
                $('input[name=current_page]').val("1");
                //alert($("#page_filter").val());
                $("#user_form").submit();

                $("#filter_text").html = "( last name filter applied - " + filter + " )";

                //alert($("#current_page").val());
                //alert($("#filter").val());

                //window.location.href = window.location.href + "&filter=" + $(this).val();
                //alert($("#current_page").val());

            });

            //####################################################### Search button #########################################
            $(document).on("click", "#btn8", function (e) {
                e.preventDefault();
                //alert($(this).val());
                var filter = $('#search_value').val().toString();
                $('input[name=page_filter]').val(filter);
                $('input[name=current_page]').val("1");
                //alert($("#page_filter").val());
                $("#user_form").submit();

            });

            //####################################################### All button #########################################
            $(document).on("click", "#btn_all", function (e) {
                e.preventDefault();
                //alert($(this).val());
                var filter = "";

                $('input[name=page_filter]').val(filter);
                $('input[name=current_page]').val("1");
                //alert($("#page_filter").val());
                $("#user_form").submit();

            });
            //pressing the enter key
            $('#search_value').keypress(function (e) {
                if (e.which == 13) {
                    $("#btn8").click();
                }
            });

            //################################################### Delete button ################################################
            $(document).on("click", ".delete_user", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var post_data = $(this).attr('id');
                //alert(post_data);

                BootstrapDialog.confirm({
                    title: 'Confirmation',
                    message: "Are you sure you want to delete this user?",
                    type: BootstrapDialog.TYPE_SUCCESS, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                    closable: false, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelLabel: 'No', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Yes', // <-- Default value is 'OK',
                    btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                    btnCancelClass: 'btn-danger',
                    animate: false,

                    callback: function (result) {
                        // result will be true if button was click, while it will be false if users close the dialog directly.
                        if (result) {
                            $.ajax({
                                type: "POST",
                                url: "../ws/delete-user.php",
                                data: { "uid": post_data },
                                success: function (msg) {
                                    //alert(msg);
                                    //alert(strcmp(msg,"success"));
                                    if (strcmp(msg, "success")) {
                                        BootstrapDialog.alert('User deleted successfully.', function () {
                                            location.reload();
                                        });
                                    }
                                    else {
                                        alert(msg);
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    alert("An processing error occured.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                                }
                            });
                        } else {
                            //alert('Nope.');
                        }
                    }
                });


            });

            function strcmp(str1, str2) {
                return str1.localeCompare(str2) / Math.abs(str1.localeCompare(str2));
            }


            //####################################################### Change Password Button #######################################
            $(document).on("click", ".reset_pw", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var user_id_to_change = $(this).attr('id');
                $("#chg_pw_userid").val(user_id_to_change);

                //alert($(this).attr('id'));
                $("#new_pw").val("");
                $("#confirm_pw").val("");

                $('#chgPassword').modal('show');

            })

            $('#btn_save').on('click', function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                if ($("#new_pw").val() != $("#confirm_pw").val()) {
                    //alert("values do NOT match");
                    $('#pw-chg-nomatch').removeClass('hide');
                    $.wait(function () { $('#pw-chg-nomatch').addClass('hide') }, 3);
                    //more processing here
                }
                else {
                    //alert("values DO match");
                    var new_pw = $("#new_pw").val();
                    var user_id_to_change = $("#chg_pw_userid").val();
                    //alert(new_pw);
                    $.ajax({
                        type: "POST",
                        url: "ws-chg-pw.php",
                        data: { "uid": user_id_to_change, "npw": new_pw },
                        success: function (msg) {
                            //alert(msg);
                            $('#pw-chg-success').removeClass('hide');
                            $.wait(function () { $('#pw-chg-success').addClass('hide'); $('#chgPassword').modal('hide'); }, 2);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("An processing error occured.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                        }
                    });
                }
            })


            //####################################################### Mail Report #######################################
            $(document).on("click", ".email_rpt", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var user_id_to_email = $(this).attr('id');
                $("#email_userid").val(user_id_to_email);

                //alert($(this).attr('id'));
                //blank out the fields
                $("#recp_name").val("");
                $("#recp_email").val("");
                $("#email_subject").val("");
                $("#email_message").val("");

                //alert($("#email_userid").val());

                $('#mailReport').modal('show');

            })

            $('#btn_send_email').on('click', function (e) {
                e.preventDefault();

                //alert($("#recp_email").val().indexOf("@"));
                //return;

                if ($('#recp_email').val() == '') {
                    $('#email-fail').removeClass('hide')
                    $.wait(function () { $('#email-fail').addClass('hide'); }, 2);
                    return;
                }

                //make sure email address has @ in it

                if ($("#recp_email").val().indexOf("@") == -1) {
                    $('#email-address-not-valid').removeClass('hide')
                    $.wait(function () { $('#email-address-not-valid').addClass('hide'); }, 2);
                    return;
                }

                $('#busy').removeClass('hide');
                //alert("about to send email");
                var user_id_to_email = $("#email_userid").val();
                $.ajax({
                    type: "POST",
                    url: "email.php",
                    //data: { "uid": user_id_to_email },
                    data: $('form.mail_report_form').serialize(),
                    success: function (msg) {
                        //alert(msg);
                        $('#busy').addClass('hide');
                        $('#email-success').removeClass('hide');
                        $.wait(function () { $('#email-success').addClass('hide'); $('#mailReport').modal('hide'); }, 2);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An processing error occured while attempting to email report.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                    }
                });



            })


            //####################################################### Notify Contacts #######################################
            $(document).on("click", ".notify_contacts", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var user_id_to_notify_about = $(this).attr('id');
                $("#notify_userid").val(user_id_to_notify_about);

                //alert($(this).attr('id'));
                //blank out the fields
                //$("#notify_message").val("");

                //alert($("#email_userid").val());

                $('#mailContact').modal('show');

            })

            $('#btn_send_notify').on('click', function (e) {
                e.preventDefault();

                //alert($("#recp_email").val().indexOf("@"));
                //return;

                //if ($('#recp_email').val() == '') {
                //    $('#email-fail').removeClass('hide')
                //    $.wait(function () { $('#email-fail').addClass('hide'); }, 2);
                //    return;
                //}

                //make sure email address has @ in it

                //if ($("#recp_email").val().indexOf("@") == -1) {
                //    $('#email-address-not-valid').removeClass('hide')
                //    $.wait(function () { $('#email-address-not-valid').addClass('hide'); }, 2);
                //    return;
                 // }

                $('#busy_notify').removeClass('hide');
                //alert("about to send email");
                var user_id_to_email = $("#email_userid").val();
                $.ajax({
                    type: "POST",
                    url: "ws-voicecall.php",
                    //data: { "uid": user_id_to_email },
                    data: $('form.mail_contact_form').serialize(),
                    success: function (msg) {
                        //alert(msg);
                        $('#busy_notify').addClass('hide');
                        $('#notify-success').removeClass('hide');
                        $.wait(function () { $('#notify-success').addClass('hide');}, 2);
                        //$.wait(function () { $('#notify-success').addClass('hide'); $('#mailContact').modal('hide'); }, 2);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#busy_notify').addClass('hide');
                        $('#notify-fail').removeClass('hide')
                        $.wait(function () { $('#notify-fail').addClass('hide'); }, 2);
                        alert("An processing error occured while attempting to notify contacts.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                    }
                });



            })

            //if there is only one page
            if ($("#current_page").val() == $("#MaxPages").val()) {
                $("#btn_prev").prop('disabled', true);
                $("#btn_next").prop('disabled', true);
                $("#btn_first").prop('disabled', true);
                $("#btn_last").prop('disabled', true);
            }

        }) //document ready


    </script>
</body>
</html>
