<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8" />
    <title>EMTeLink Tracker :: Forms Manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="EMTeLink Tracker App" />

    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="./css/font-awesome.min.css" rel="stylesheet" />

    <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" />

    <link href="./css/base-admin-3.css" rel="stylesheet" />
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet" />


    <link href="../Content/bootstrap-dialog.min.css" rel="stylesheet" />

    <link href="./css/pages/dashboard.css" rel="stylesheet" />

    <link href="./css/custom.css" rel="stylesheet" />


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php


    include("../include/incConfig.php");
    session_start();

    $filter_text = "( not filtered )";

    //make sure we have a valid sesion
    if ($_SESSION["valid"] != "TRUE")
    {
        header("Location: index.html");
    };

    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];
    $admin_user = $_SESSION["admin_user"];
    $subType = $_SESSION["subType"];
    $subMaxUsers = $_SESSION["subMaxUsers"];
    $subId = $_SESSION["subId"];

    if (isset($_POST["current_page"])) {
        $current_page = $_POST["current_page"];
    }else{
        $current_page=1;
    }


    if (isset($_POST["page_filter"])) {
        $filter = $_POST["page_filter"];
    }else{
        $filter="";
    }

    $where = "where subID=" . $subId;


    ?>

</head>

<body>

    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                </button>
                <a class="navbar-brand" href="./index.html">EMTeLink Tracker</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu">
                            <!--<li><a href="./account.html">Account Settings</a></li>
                             <li><a href="javascript:;">Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:;">Help</a></li> -->
                        </ul>

                    </li>

                    <li>
                        <a href="../home.php" style="font-size: 16px">
                            <i class="icon-home"></i>
                            &nbsp;Home <?php //echo $strSQL ?>
                        </a>
                    </li>

                    <li class="dropdown">
                        <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px">
                            <i class="icon-edit"></i>
                            <?php echo $firstname . " " . $lastname ?>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <!--  <li><a href="javascript:;">My Profile</a></li>
                            <li><a href="javascript:;">My Groups</a></li> 
                            <li class="divider"></li> -->
                            <li>
                                <a href="../Index.html" style="font-size: 14px">Logout</a>
                            </li>
                        </ul>

                    </li>
                </ul>

                <!-- <form class="navbar-form navbar-right" role="search">
                   <div class="form-group">
                     <input type="text" class="form-control input-sm search-query" placeholder="Search">
                   </div>
                 </form> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <!-- /subnavbar -->

    <div class="well well-sm">
        <div class="text-center">
            <h2>
                <i class=" icon-edit" style="color:#F99500;"></i>
                Manage Forms
            </h2>
        </div>
        <!-- End container -->
    </div>

    <div class="main">

        <div class="container">
            <br />
            <div class="row">

                <div class="col-md-12">

                    <!--    <div class="well">

                        <h4>Manage Users</h4>

                        <p>Select an Action ...</p>

                    </div> -->

                    <div class="widget stacked ">

                        <div class="widget-header" style="vertical-align: middle">
                            <i class="icon-edit" style="color:#F99500;"></i>
                            <h3>Manage Forms</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content text-center">

                            <div class="table-responsive text-center" style="width: 95%; margin: 0 auto; font-size: 16px" id="user-div">
                                <table class="table table-bordered table-hover table-striped table-responsive" id="user-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="font-size: 18px;width:10%">Action</th>
                                            <th class="text-left" style="font-size: 18px">Form Name</th>
                                            <th class="text-left" style="font-size: 18px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">
                                                <a href="../expectations.php" class="btn btn-success btn-sm" role="button">Open</a>
                                                <!-- <button type="button" class="btn-md btn-success">Manage</button>&nbsp;&nbsp; -->
                                            </td>
                                            <td class="text-left" style="font-size: 16px;vertical-align:middle;">
                                                Band Member Expectations
                                            </td>
                                            <td class="text-left" style="font-size: 16px">
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <a href="../consent.php" class="btn btn-success btn-sm" role="button">Open</a>
                                            </td>
                                            <td class="text-left" style="font-size: 16px;vertical-align:middle;">
                                                Parental Consent
                                            </td>
                                            <td class="text-left" style="font-size: 16px"></td>
                                        </tr>

                                    </tbody>
                                </table>

                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>


            </div>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 center-block">
                    <div class="well">
                        <h2>Complete Your Forms</h2>
                        <p>
                            Use this section to review and sign the required forms.
                            <br />
                        </p>
                        <p>
                            <?php
                            $instruction_content = "Select an Action from the Action list next to each form to complete it.  Some forms require review and signatures, 
others (if applicable) can be downloaded, signed by the appropriate party (e.g. typically a physician), and then uploaded to the student's forms library and routed to an administator for review and approval.
";
                            echo $instruction_content;
                            ?>
                        </p>
                        <center>
                           <!-- <img src="../img/student-circle-sm2.jpg" class="img-responsive img-rounded hidden-xs" alt="student image" /> -->
                        </center>
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>

        </div>


        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    <div class="text-center">
        <a href="../Home.php" class="btn btn-warning btn-group-md" style="color:black" role="button">Return To Home Screen</a>
    </div>
    <br />
    <footer>
        <div class="extra">
            <div class="container text-center">
                <p>&copy; Copyright @ 2016 EMTeLink</p>
            </div>
        </div>
        <!-- end Container-->
    </footer>
        </div>

</body>
</html>
