<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>EMTeLink Tracker App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="EMTeLink Traker App">
    <!-- Latest compiled and minified CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/bootstrap-dialog.min.css" rel="stylesheet" />
    <?php

    include("../include/incConfig.php");
    include("../include/incFunctions.php");

    session_start();

    //make sure we have a valid sesion
    if ($_SESSION["valid"] != "TRUE")
    {
        header("Location: index.php");
    };
    

    if (isset($_GET["userid"])) {
        $userid = $_GET["userid"];
    } 
    else{  
        echo "Report generation cancelled.  User ID not found.";
    }

    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    //$userid = $_SESSION["userid"];
    
    ?>

    <style>
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

        /*** basic styles ***/

        body {
            margin: 30px;
            padding-top: 50px;
        }

        label {
            font-size: 18px;
            color: #337AB7;
        }


        /*** custom checkboxes ***/
        input[type=checkbox] {
            display: none;
        }
            /* to hide the checkbox itself */
            input[type=checkbox] + label:before {
                font-family: FontAwesome;
                display: inline-block;
                color: #337AB7;
            }

            input[type=checkbox] + label:before {
                content: "\f096";
            }
            /* unchecked icon */
            input[type=checkbox] + label:before {
                letter-spacing: 10px;
            }
            /* space between checkbox and label */

            input[type=checkbox]:checked + label:before {
                content: "\f046";
            }
            /* checked icon */
            input[type=checkbox]:checked + label:before {
                letter-spacing: 5px;
            }
        /* allow space for check mark */
    </style>
</head>




<body>

    <div class="well">
        <div class="text-center">
            <h1>User Medical Information</h1>
        </div>
        <!-- End container -->
    </div>
    <!-- End jumbotron-->

    <div>

        <?php 
        $recordset = $database->select("user", [
            "med_conditions",
            "hospitalizations",
            "serious_injuries",
            "special_diets",
            "add_info"
        ], [
            "userid" => "$userid"
        ]);

        foreach($recordset as $data)
        {
            $med_conditions = $data["med_conditions"];
            $textarea_notes = $data["add_info"];
            $textarea_special_diets = $data["special_diets"];
            $textarea_serious_injuries = $data["serious_injuries"];
            $textare_hospitalizations = $data["hospitalizations"];
        }   

        $medcond=json_decode($med_conditions,true);

        ?>

    </div>

    <script>

        var jo = JSON.parse(JSON.stringify(var_med_conditions));

        var med_record;
        var allergy_record;
    </script>
    <div class="container">
        <section>
            <div id="medical_conditions"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">


                    <table class="table" style="width: 100%">
                        <tbody>
                            <tr>
                                <td colspan="5" class="text-center">
                                    <h3>Medical Conditions</h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">&nbsp;</td>
                                <td style="text-align: left">
                                    <ul class="nav nav-list">
                                        <li>
                                            <input id="med_mumps" name="med_mumps" type="checkbox" <?php if($medcond[0]['checked']==1){echo ('checked');} ?> /><label for="med_mumps">Mumps</label>
                                        </li>
                                        <li>
                                            <input id="med_measles" name="med_measles" type="checkbox" <?php if($medcond[1]['checked']==1){echo ('checked');} ?> /><label for="med_measles">Measles</label>
                                        </li>
                                        <li>
                                            <input id="med_chickenpox" name="med_chickenpox" type="checkbox" <?php if($medcond[2]['checked']==1){echo ('checked');} ?> /><label for="med_chickenpox">Chicken Pox</label>
                                        </li>
                                        <li>
                                            <input id="med_pneumonia" name="med_pneumonia" type="checkbox" <?php if($medcond[3]['checked']==1){echo ('checked');} ?> /><label for="med_pneumonia">Pneumonia</label>
                                        </li>
                                        <li>
                                            <input id="med_diabetes" name="med_diabetes" type="checkbox" <?php if($medcond[4]['checked']==1){echo ('checked');} ?> /><label for="med_diabetes">Diabetes</label>
                                        </li>
                                        <li>
                                            <input id="med_cancer" name="med_cancer" type="checkbox" <?php if($medcond[5]['checked']==1){echo ('checked');} ?> /><label for="med_cancer">Cancer</label>
                                        </li>
                                        <li>
                                            <input id="med_malaria" name="med_malaria" type="checkbox" <?php if($medcond[6]['checked']==1){echo ('checked');} ?> /><label for="med_malaria">Malaria</label>
                                        </li>
                                    </ul>
                                </td>
                                <td style="width: 5%">&nbsp;</td>
                                <td style="text-align: left">
                                    <ul class="nav nav-list">
                                        <li>
                                            <input id="med_seizures" name="med_seizures" type="checkbox" <?php if($medcond[7]['checked']==1){echo ('checked');} ?>  /><label for="med_seizures">Seizures</label>
                                        </li>
                                        <li>
                                            <input id="med_heartdisease" name="med_heartdisease" type="checkbox" <?php if($medcond[8]['checked']==1){echo ('checked');} ?>  /><label for="med_heartdisease">Heart Disease</label>
                                        </li>
                                        <li>
                                            <input id="med_kidneydisease" name="med_kidneydisease" type="checkbox" <?php if($medcond[9]['checked']==1){echo ('checked');} ?>  /><label for="med_kidneydisease">Kidney Disease</label>
                                        </li>
                                        <li>
                                            <input id="med_tuberculosis" name="med_tuberculosis" type="checkbox" <?php if($medcond[10]['checked']==1){echo ('checked');} ?>  /><label for="med_tuberculosis">Tuberculosis</label>
                                        </li>
                                        <li>
                                            <input id="med_rheumaticfever" name="med_rheumaticfever" type="checkbox" <?php if($medcond[11]['checked']==1){echo ('checked');} ?>  /><label for="med_rheumaticfever">Rhematic Fever</label>
                                        </li>
                                        <li>
                                            <input id="med_hepatitis" name="med_hepatitis" type="checkbox" <?php if($medcond[12]['checked']==1){echo ('checked');} ?>  /><label for="med_hepatitis">Hepatitis</label>
                                        </li>
                                        <li>
                                            <input id="med_bloodpressureproblems" name="med_bloodpressureproblems" type="checkbox" <?php if($medcond[13]['checked']==1){echo ('checked');} ?>  /><label for="med_bloodpressureproblems">Blood Pressure Problems</label>
                                        </li>
                                        <li>
                                            <input id="med_tetanus" name="med_tetanus" type="checkbox" <?php if($medcond[14]['checked']==1){echo ('checked');} ?>  /><label for="med_tetanus">Tetanus Toxiod Shot (in last 12 months)</label>
                                        </li>
                                    </ul>
                                </td>
                                <td style="width: 15%">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>


                </div>
            </div>
        </section>
    </div>

    <div class="row">
        <div class="col-xs-8" style="margin:0 auto;">
            <table class="table table-striped" style="width: 80%;margin:0 auto;" id="medication_table">
                <thead>
                    <tr class="input-md">
                        <th style="width: 15%">&nbsp;</th>
                        <th colspan="3" class="text-center">
                            <h3>Medications</h3>
                        </th>
                        <th style="width: 15%">&nbsp;</th>
                    </tr>
                    <tr class="text-left input-md">
                        <th style="width: 15%">&nbsp;</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Dosage</th>
                        <th class="text-left">How Often</th>
                        <th style="width: 15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>

                    <?php 
                    $recordset = $database->select("user_medications", [
                        "medicationId",
                        "medication_name",
                        "dosage",
                        "howoften"
                    ], [
                        "userid" => "$userid",
                         "ORDER" => ['medication_name ASC']
                    ]);

                    foreach($recordset as $data)
                    {
                        $medicationId = $data["medicationId"];
                        $medication_name = $data["medication_name"];
                        $dosage = $data["dosage"];
                        $howoften = $data["howoften"];

                        echo ("<tr class=\"text-left input-md\">");
                        echo ("  <td  style=\"width: 15%\">" . "&nbsp;" . "</td>");
                        echo ("  <td>" . $medication_name . "</td>");
                        echo ("  <td>" . $dosage . "</td>");
                        echo ("  <td>" . $howoften . "</td>");
                        echo ("  <td  style=\"width: 15%\">" . "&nbsp;" . "</td>");
                        echo ("</tr>");
                    }   
                    echo ("</td></tr></table>")

                    ?>

                </tbody>
            </table>

        </div>
    </div>


    <br />
    <br />

    <div class="container">
        <section>
            <div id="allergies_div"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-success text-center">
                        <div class="panel-heading">
                            <h2 class="title"><u>Allergies</u></h2>
                        </div>
                        <div class="panel-body" id="allergy_panel_body">
                            <table class="table table-striped center" style="width: 80%;" id="allergy_table">
                                <thead>
                                    <tr class="text-left input-lg">
                                        <th class="text-left">Description</th>
                                        <th class="text-left">Notes</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                    $recordset = $database->select("user_allergies", [
                                        "allergyId",
                                        "description",
                                        "notes"
                                    ], [
                                        "userid" => "$userid",
                                         "ORDER" => ['description ASC']
                                    ]);

                                    foreach($recordset as $data)
                                    {
                                        $allergyId = $data["allergyId"];
                                        $description = $data["description"];
                                        $notes = $data["notes"];

                                        echo ("<tr class=\"text-left input-lg\">");
                                        echo ("  <td>" . $description . "</td>");
                                        echo ("  <td>" . $notes . "</td>");
                                        echo ("  <td>");
                                        echo ("    <div class=\"record_allergy\"><span class=\"name hide\">" . $allergyId . "</span>");
                                        echo ("      <button class=\"editAllergyButton btn btn-xs btn-success glyphicon glyphicon-edit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit Allergy\"></button>");
                                        echo ("      <button class=\"deleteAllergyButton btn btn-xs btn-danger glyphicon glyphicon-trash\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Allergy\"></button>");
                                        echo ("    </div>");
                                        echo ("  </td>");
                                        echo ("</tr>");
                                    }   
                                    echo ("</td></tr></table>")

                                    ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <div class="alert alert-danger hide" role="alert" id="allergy-deleted">Allergy Deleted.</div>
                            <br />
                            <button type="button" id="btn_add_allergy" class="btn btn-success btn-lg">Add Allergy</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <br />
    <br />

    <div class="container">
        <form id="medical_misc_form" class="medical_misc_form">
            <section>
                <div id="notes"><span class="clearfix"></span></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-primary text-center">
                            <div class="panel-heading">
                                <h2 class="title"><u>Miscellaneous Information</u></h2>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped center" style="width: 98%;">
                                    <tbody>
                                        <tr class="text-left input-lg">
                                            <td>
                                                <label for="textarea-hospitalizations" style="font-size: 16px">Hospitalizations (when, where, why, Dr.&apos;s Name)</label>
                                                <textarea id="textarea-hospitalizations" style="font-size: 16px" name="textarea-hospitalizations" readonly rows="6" class="form-control" placeholder="Hospitalizations (when, where, why, Dr&apos;s Name)"><?php echo $textare_hospitalizations ?></textarea>
                                            </td>
                                        </tr>

                                        <tr class="text-left input-lg">
                                            <td>
                                                <label for="textarea-serious-injuries" style="font-size: 16px">Serious Injuries or Illness</label>
                                                <textarea id="textarea-serious-injuries" style="font-size: 16px" name="textarea-serious-injuries" readonly rows="6" class="form-control" placeholder="Serious Injuries or Illness"><?php echo $textarea_serious_injuries ?></textarea>
                                            </td>
                                        </tr>
                                        <tr class="text-left input-lg">
                                            <td>
                                                <label for="textarea-special-diets" style="font-size: 16px">Special Diets</label>
                                                <textarea id="textarea-special-diets" style="font-size: 16px" name="textarea-special-diets" readonly rows="4" class="form-control" placeholder="Special Diets"><?php echo $textarea_special_diets ?></textarea>
                                            </td>
                                        </tr>
                                        <tr class="text-left input-lg">
                                            <td>
                                                <label for="textarea-notes" style="font-size: 16px">Additional Information</label>
                                                <textarea id="textarea-notes" style="font-size: 16px" name="textarea-notes" rows="6" readonly class="form-control" placeholder="please provide us with any additional information of which you feel we should be aware"><?php echo $textarea_notes ?></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

    <div id="addMedication" class="modal fade" role="dialog">
        <form id="add_medication_form" class="add_medication_form">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="medication_modal_title">Add Medication</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="med_name">Medication Name</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control input-md" id="med_name" name="med_name" placeholder="enter medication name" value="<?php echo "" ?>">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="dosage">Dosage</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control input-md" id="dosage" name="dosage" placeholder="enter dosage" value="<?php echo "" ?>">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="how_often">Frequency</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control input-md" id="how_often" name="how_often" placeholder="how often" value="<?php echo "" ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


    <div id="addAllergy" class="modal fade" role="dialog">
        <form id="add_allergy_form" class="add_allergy_form">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="allergy_modal_title">Add Allergy</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="allergy_desc">Description</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control input-md" id="allergy_desc" name="allergy_desc" placeholder="enter allergy description" value="<?php echo "" ?>">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="allergy_notes">Notes</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control input-md" id="allergy_notes" name="allergy_notes" placeholder="enter notes" value="<?php echo "" ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <br />
    <br />
    <!-- Footer -->

    <footer>
        <div class="container text-center">
            <p>&copy; Copyright @ 2015 EMTeLink</p>
        </div>
        <!-- end Container-->
    </footer>

    <script src="Scripts/jquery-1.9.1.min.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap-datepicker.min.js"></script>

    <script src="Scripts/jquery.maskedinput.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function () {


        });
    </script>




</body>
</html>
