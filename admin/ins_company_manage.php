<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>MyEMRMyWay :: Insurance Company</title>
    <meta name="description" content="MyEMRMyWay App" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="./css/font-awesome.min.css" rel="stylesheet" />

    <!-- <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet"> -->

    <link href="./css/base-admin-3.css" rel="stylesheet" />
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet" />

    <link href="./css/pages/dashboard.css" rel="stylesheet" />

    <link href="./css/custom.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="./css/bootstrap-toggle.min.css" rel="stylesheet" />

    <link href="../components/datepicker/css/datepicker.css" rel="stylesheet" />
    <link href="../Content/sweetalert2.min.css" rel="stylesheet" />

    <?php

    include("../include/incConfig.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    session_start();
	include("../include/session.php");

    if( isset($_GET["id"]) ) {
        $id_value =  htmlspecialchars($_GET["id"]);
        $decrypted = Urlcrypt::decrypt($id_value);
    } else {
        $id_value =  "";
        $decrypted = "";
    }

    $mode = "";
    $starttime="";
    $companyid = "";
    $back_filename = "";
    //echo "id_value = " . $id_value;
    //echo "decrypted = " . $decrypted;

    if ($id_value == "") {
        $mode = "insert";
        $WIDGET_HEADER_TITLE = "Add New Insurance Company";
        $instruction_content = "Enter the new Insurance Company info below and then tap the Save button.<br />";
    } else {
        list($companyid, $mode, $starttime) = explode("|", $decrypted);
    }

    //echo $mode;
    $endtime = time();
    $timediff = $endtime - $starttime;

    $CompanyName = "";
    $GroupNumber = "";
    $ContractNumber = "";
    $EffectiveDate = "";
    $CertificationNumber  = "";
    $PhoneNumber  = "";
    $PlanWith  = "";
    $EditDate  = "";

    if ($mode == "edit") {

        $recordset = $database->select("user_ins_companies", [
            "CompanyName",
            "GroupNumber",
            "ContractNumber",
            "EffectiveDate",
            "CertificationNumber",
            "PhoneNumber",
            "PlanWith",
            "EditDate"
        ], [
            "CompanyId" => $companyid
        ]);

        foreach($recordset as $data)
        {
            $CompanyName = $data["CompanyName"];
            $GroupNumber = $data["GroupNumber"];
            $ContractNumber = $data["ContractNumber"];
            $EffectiveDate = $data["EffectiveDate"];

            if ($EffectiveDate == "1900-01-01") { $EffectiveDate = ""; }

            $CertificationNumber = $data["CertificationNumber"];
            $PhoneNumber = $data["PhoneNumber"];
            $PlanWith = $data["PlanWith"];
            $EditDate = $data["EditDate"];
        }

        $WIDGET_HEADER_TITLE = "Editing " . $CompanyName . " record.";

        $instruction_content = "Make changes and then tap the Save button.<br />";
    }

    $instruction_content = $instruction_content . "<br />Select the Upload button underneath each image to upload or replace it."

    ?>
    <style>
        ul, li {
            margin: 0;
            padding: 0px;
            list-style-type: none;
            text-align: left;
        }

        #pswd_info {
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_info h5 {
                margin: 0 0 10px 0;
                padding: 0;
                font-weight: normal;
            }


        .invalid {
            background: url(../../images/redx.jpg) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #ec3f41;
        }

        .valid {
            background: url(../../images/green-ck.jpg) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #3a7d34;
        }

        .form-control {
            color: #000000 !important;
            background-color: #F7F7F7 !important;
            font-weight: 600 !important;
        }


        @media screen and (max-width: 767px) {

            .widget .widget-content {
                padding: 0px 0px 0px !important;
                background: #fff;
                border: 1px solid #D5D5D5;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
            }
        }
    </style>
</head>

<body>

    <?php

	include("../include/navbar.php");

    ?>

    <div class="main">
        <br />
        <br />
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h3>
                        <strong>Insurance Company</strong>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 hidden-xs"></div>
                <div class="col-md-10 col-xs-12 center-block">
                    <div class="panel-group accordion">
                        <div class="panel panel-warning open" style="border-color:black">

                            <div class="panel-heading" style="text-align:center">

                                <h4 class="panel-title">

                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                                        <span style="font-size: large;color:#525252;">
                                            <i class="icon-info-sign" style="color: #525252"></i>&nbsp;
                                            <u>Instructions</u>
                                        </span>
                                        <span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to Toggle)</span>
                                    </a>
                                </h4>

                            </div>

                            <div id="collapseOne33" class="panel-collapse collapse in">

                                <div class="panel-body">

                                    <p style="font-size: medium">
                                        <?php echo $instruction_content; ?>
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-1 hidden-xs"></div>
            </div>
            <br />

            <div class="row">


                <div class="col-md-1 hidden-xs"></div>
                <!-- /span4 -->

                <div class="col-md-10 col-xs-12">
                    <div class="text-center">
                        <button type="button" class="btn btn-warning" id="btn_cancel" style="color:black">
                            <i class="icon-arrow-left"></i>&nbsp;Back To Companies
                        </button>
                        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">
                            <i class="icon-home" style="color: #ffffff;"></i>&nbsp;Return Home
                        </a>
                    </div>
                    <br />
                    <div class="widget stacked ">

                        <div class="widget-header">
                            <i class="icon-book"></i>
                            <h3>
                                <strong>
                                    <?php echo $WIDGET_HEADER_TITLE ?>
                                </strong>
                            </h3>
                        </div>
                        <!-- /widget-header -->
                        <div class="widget-content">
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile">
                                    <form id="ins_company_form" class="ins_company_form form-horizontal col-md-12">
                                        <fieldset>
                                            <div class="form-group">
                                                <label for="ins_company" class="col-md-3">Insurance Company</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="ins_company" name="ins_company" placeholder="enter insurance company name" value="<?php echo $CompanyName ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="groupno" class="col-md-3">Group Number</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="groupno" name="groupno" placeholder="enter group number" value="<?php echo $GroupNumber ?>" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="contractno">Contract Number</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="contractno" name="contractno" placeholder="enter contract number" value="<?php echo $ContractNumber ?>" />
                                                </div>

                                            </div>



                                            <div class="form-group">
                                                <label class="col-md-3" for="effective_date_picker">Effective Date</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="effective_date_picker" name="effective_date_picker" placeholder="mm/dd/yyyy" value="<?php echo $EffectiveDate ?>" />
                                                </div>

                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3" for="certification_no">Certification Number</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="certification_no" name="certification_no" placeholder="enter certification number" value="<?php echo $CertificationNumber ?>" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="phone_number">Phone Number</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="enter phone number" value="<?php echo $PhoneNumber ?>" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="plan_with">Plan With</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="plan_with" name="plan_with" placeholder="enter who plan is with" value="<?php echo $PlanWith ?>" />
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="form-group">
                                                <div class="col-xs-12 text-center">
                                                   <button type="button" class="btn btn-success" id="btn_add_update_company">Save</button>
                                                </div>
                                            </div>
                                            <div id="card_pictures" name="card_pictures" <?php if ($mode == "insert") { echo "class=\"hide\"";}  ?>>
                                                <hr style="border-top: 1px solid #8c8b8b;margin: 1px 0px 1px 0px;" />
                                                <div style="background-color:#EBEBEB;height:24px;" class="text-center">
                                                    <h4>
                                                        <strong>
                                                            <u>Insurance Card Pictures</u>
                                                        </strong>
                                                    </h4>
                                                </div>

                                                <div class="form-group" style="padding-top:10px;">
                                                    <div class="col-md-12 text-center">
                                                        <h4>
                                                            <strong>
                                                                <u>Card Front</u>
                                                            </strong>
                                                        </h4>

                                                        <?php

                                                    $path = "C:/inetpub/wwwroot/emtelink/new/uploads/" . $subId . "/";

                                                    $filename_jpg = $path .  "ins_card_" . $companyid . "_front.jpg";
                                                    $filename_jpeg = $path .  "ins_card_" . $companyid . "_front.jpeg";
                                                    $filename_png = $path .  "ins_card_" . $companyid . "_front.png";

                                                    $filename_does_not_exist = "C:/inetpub/wwwroot/emtelink/new/uploads/dne.png";

                                                    if (file_exists($filename_jpg)) {
                                                        $filename = $filename_jpg;
                                                    }

                                                    if (file_exists($filename_jpeg)) {
                                                        $filename = $filename_jpeg;
                                                    }

                                                    if (file_exists($filename_png)) {
                                                        $filename = $filename_png;
                                                    }

                                                    if (file_exists($filename)) {
                                                        $type = pathinfo($filename, PATHINFO_EXTENSION);
                                                        $data = file_get_contents($filename);
                                                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                        $image_output = "<img width=\"312\" height=\"194\" src=\"" . $base64 . "\"  class=\"img-round img-responsive center-block\" style=\"margin:0 auto;border-width:thin;border-style:solid;border-color:black;box-shadow: 4px 4px #888888;\" >";
                                                    } else {
                                                        $type = pathinfo($filename_does_not_exist, PATHINFO_EXTENSION);
                                                        $data = file_get_contents($filename_does_not_exist);
                                                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                        $image_output = "<img width=\"312\" height=\"194\" src=\"" . $base64 . "\"  class=\"img-round img-responsive center-block\" style=\"margin:0 auto;border-width:thin;border-style:solid;border-color:black;box-shadow: 4px 4px #888888;\" >";
                                                    }

                                                    echo $image_output;
                                                        ?>

                                                        <br />

                                                    </div>
                                                    <div class="col-md-6 text-center col-md-push-3">
                                                        <div class="form-group" style="margin-bottom:10px;">
                                                            <div class="input-group">
                                                                <input type="text" id="fileToUploadTextbox_front" class="form-control" readonly />
                                                                <label class="input-group-btn">
                                                                    <span class="btn btn-warning btn" style="color:black;">
                                                                        Select&hellip;
                                                                        <input type="file" style="display: none;" id="fileToUpload_front" name="fileToUpload_front" />
                                                                    </span>
                                                                </label>
                                                                <button type="button" class="btn btn-success" id="button-upload-front">Upload</button>
                                                            </div>

                                                            <div class="progress hide" style="margin-top: 10px; margin-left: 100px; margin-right: 100px;" id="progress_bar_front">
                                                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                                    <span id="progresstext_front">0% complete</span>
                                                                </div>
                                                            </div>
                                                            <div style="margin-top: 10px; margin-left: 5px; margin-right: 5px; background-color:cornsilk">
                                                                <span id="responsetext_front"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <hr style="border-top: 1px dashed #8c8b8b;margin: 1px 0px 15px 0px;" />

                                                <!--  Back Card Image -->

                                                <div class="form-group" style="padding-top:10px;">
                                                    <div class="col-md-12 text-center">
                                                        <h4>
                                                            <strong>
                                                                <u>Card Back</u>
                                                            </strong>
                                                        </h4>
                                                        <?php

                                                    $path = "C:/inetpub/wwwroot/emtelink/new/uploads/" . $subId . "/";

                                                    $back_filename_jpg = $path .  "ins_card_" . $companyid . "_back.jpg";
                                                    $back_filename_jpeg = $path .  "ins_card_" . $companyid . "_back.jpeg";
                                                    $back_filename_png = $path .  "ins_card_" . $companyid . "_back.png";



                                                    if (file_exists($back_filename_jpg)) {
                                                        $back_filename = $back_filename_jpg;
                                                    }

                                                    if (file_exists($back_filename_jpeg)) {
                                                        $back_filename = $back_filename_jpeg;
                                                    }

                                                    if (file_exists($back_filename_png)) {
                                                        $back_filename = $back_filename_png;
                                                    }


                                                    if (file_exists($back_filename)) {
                                                        $back_type = pathinfo($back_filename, PATHINFO_EXTENSION);
                                                        $back_data = file_get_contents($back_filename);
                                                        $back_base64 = 'data:image/' . $back_type . ';base64,' . base64_encode($back_data);
                                                        $back_image_output = "<img width=\"312\" height=\"194\" src=\"" . $back_base64 . "\"  class=\"img-round img-responsive center-block\" style=\"margin:0 auto;border-width:thin;border-style:solid;border-color:black;box-shadow: 4px 4px #888888;\" >";
                                                    } else {
                                                        $back_type = pathinfo($filename_does_not_exist, PATHINFO_EXTENSION);
                                                        $back_data = file_get_contents($filename_does_not_exist);
                                                        $back_base64 = 'data:image/' . $back_type . ';base64,' . base64_encode($back_data);
                                                        $back_image_output = "<img width=\"312\" height=\"194\" src=\"" . $back_base64 . "\"  class=\"img-round img-responsive center-block\" style=\"margin:0 auto;border-width:thin;border-style:solid;border-color:black;box-shadow: 4px 4px #888888;\" >";
                                                    }

                                                    echo $back_image_output;
                                                        ?>
                                                        <br />
                                                    </div>
                                                    <div class="col-md-6 text-center col-md-push-3">
                                                        <div class="form-group" style="margin-bottom:10px;">
                                                            <div class="input-group">
                                                                <input type="text" id="fileToUploadTextbox_back" class="form-control" readonly />
                                                                <label class="input-group-btn">
                                                                    <span class="btn btn-warning btn" style="color:black;">
                                                                        Select&hellip;
                                                                        <input type="file" style="display: none;" id="fileToUpload_back" name="fileToUpload_back" />
                                                                    </span>
                                                                </label>
                                                                <button type="button" class="btn btn-success" id="button-upload_back">Upload</button>
                                                            </div>

                                                            <div class="progress hide" style="margin-top: 10px; margin-left: 100px; margin-right: 100px;" id="progress_bar_back">
                                                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                                    <span id="progresstext_back">0% complete</span>
                                                                </div>
                                                            </div>
                                                            <div style="margin-top: 10px; margin-left: 5px; margin-right: 5px; background-color:cornsilk">
                                                                <span id="responsetext_back"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <input type="hidden" value="<?php echo $id_value ?>" id="hidden" name="hidden" />
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="col-md-1 hidden-xs">

                    <!--     <div class="well">

                        <h4>Add User</h4>

                        <p>In order to add a new user, please enter the UserID, Firstname, Lastname and Password, then click the Create Company Button..</p>

                    </div> -->

                </div>
                <!-- /span4 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->
    <div class="text-center">
        <button type="button" class="btn btn-warning" id="btn_cancel1" style="color:black">
            <i class="icon-arrow-left"></i>&nbsp;Back To Companies
        </button>
        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">
            <i class="icon-home" style="color: #ffffff;"></i>&nbsp;Return Home
        </a>
    </div>
    <br />

    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- /footer -->



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="./js/libs/bootstrap-toggle.min.js"></script>

    <script src="../components/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../Scripts/sweetalert2.min.js"></script>

    <script>

	    $(document).ready(function() {


			//$('.effective_date_picker').datepicker();
            /*
	        $('#user_type').multiselect({
	            buttonWidth: '100%',
	            maxHeight: 100
	        });
            */
	        //$(".chzn-select").chosen();

	    });


        $('#btn_add_update_company').on('click', function (e) {

            if ($('#ins_company').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Company Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h4>Please enter a <u>Company Name</u>.</h4></span>');
                $('#myModal').modal('show');
                return;
            }

            //alert("here")
                    <?php
                    if ($mode == "edit") {
                        $url = "../ws/edit-ins-company.php";
                        //$message = "Company has been updated successfully.";
                    }
                    else {
                        $url = "../ws/put-ins-company.php";
                        //$message = "Company has been created successfully.";
                    }
                    ?>

                    $.ajax({
                type: "POST",
                url: "<?php echo $url; ?>",
                data: $('form.ins_company_form').serialize(),
                success: function (msg) {
                   //alert(msg);

                   var msg_type = "error";
                   var msg_title = "Not Saved";

                   if (msg.includes("success")) {
                      msg_type = "success";
                      msg_title = "Successful";

                      msg_arr = msg.split('|');

                      msg_hidden = msg_arr[1];
                      if (msg_hidden == "") {
                          msg_body = msg_arr[0];
                      }
                      else {
                          msg_body = msg_arr[0] + '<br />You may now upload the card pictures.';
                      }
                   }

                    swal({
                        title: msg_title,
                        html: msg_body,
                        type: msg_type,
                    }).then(function () {
                        if (msg_type == "success") {
                            //window.location.href = "../info.php#insurance_companies";
                            //$('#btn_add_update_company').hide();
                            //$("#card_pictures").removeClass("hide");
                            //$('#hidden').val(msg_hidden);
                            //window.location.reload();
                            if (msg_hidden != "") {
                                $window_url = "ins_company_manage.php?id=" + msg_hidden + '#card_pictures';
                                //alert($window_url);
                                $(location).attr('href', $window_url);
                            }
                        }
                    });
                },
                error: function () {
                    alert("unable to create company");
                }

            });
        })


        $('#btn_cancel').on('click', function (e) {
             e.preventDefault();
		     window.location.href = "../info.php#insurance_companies";
        })

        $('#btn_cancel1').on('click', function (e) {
            e.preventDefault();
            window.location.href = "../info.php#insurance_companies";
        })

        //front card image
        $(document).on('change', '#fileToUpload_front', function () {

            var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

            //input.trigger('fileselect', [numFiles, label]);
            $('#fileToUploadTextbox_front').val(label);
            $('#responsetext_front').text('');
            $('#progress_bar_front').addClass("hide");
            $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
            var selectedFile = document.getElementById('fileToUpload_front').files[0];

            //alert(selectedFile.name);
            //alert(selectedFile.size);
            //alert(selectedFile.type);
            //alert(selectedFile.lastModifiedDate);
            //alert(selectedFile.lastModified);

            var max_file_size = 5242880; // max size 5mb

            var file_ext_type = "";

            //only accept image formats
            switch (selectedFile.type) {
                case 'image/png':
                    file_ext_type = "png";
                    break;
                case 'image/jpg':
                    file_ext_type = "jpg";
                    break;
                case 'image/jpeg':
                    file_ext_type = "jpeg";
                    break;
                default:
                    file_ext_type = "???";
            }

            if (selectedFile.size > max_file_size) {
                $('#responsetext_front').text("The file size ( " + (selectedFile.size / 1000000).toFixed(2) + "MB ) exceeds the max file size of ( " + max_file_size / 1048576 + "MB ) and cannot be uploaded");
                $('#button-upload-front').prop('disabled', true);
                $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
                $('#progresstext_front').text(0 + '%');
            }
            else {
                if (file_ext_type == "???") {
                    $('#responsetext_front').html("The file type is not permitted and cannot be uploaded.<br />Please select jpg, jpeg, or png");
                    $('#button-upload-front').prop('disabled', true);
                    $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
                    $('#progresstext_front').text(0 + '%');
                }
                else {
                    $('#button-upload-front').prop('disabled', false);
                }
            }


            $("#button-upload-front").click(sendFormData);

            function sendFormData() {

                var fileName = $("#fileToUpload_front").val();

                if (fileName) { // returns true if the string is not empty
                    //alert(fileName + " was selected");
                } else { // no file was selected
                    $('#responsetext_front').text("no file selected");
                    return;
                }

                $('#progress_bar_front').removeClass("hide");

                var selectedFile = document.getElementById('fileToUpload_front').files[0];

                var file_ext_type = "";

                switch (selectedFile.type) {
                    case 'image/png':
                        file_ext_type = "png";
                        break;
                    case 'image/jpg':
                        file_ext_type = "jpg";
                        break;
                    case 'image/jpeg':
                        file_ext_type = "jpeg";
                        break;
                    default:
                        file_ext_type = "???";
                }

                $('#ins_company_form').append('<input type="hidden" id="file_size" name="file_size" value="' + selectedFile.size + '" />');
                $('#ins_company_form').append('<input type="hidden" id="file_ext_type" name="file_ext_type" value="' + file_ext_type + '" />');
                $('#ins_company_form').append('<input type="hidden" id="mime_type" name="mime_type" value="' + selectedFile.type + '" />');
                $('#ins_company_form').append('<input type="hidden" id="card_side" name="card_side" value="front" />');

                var formData = new FormData($("#ins_company_form").get(0));

                var ajaxUrl = "process_ins_card_upload.php";

                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        // Handle progress
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = Math.round((evt.loaded / evt.total) * 100);
                                //console.log(percentComplete);
                                //console.log(percentComplete);
                                //console.log(percentComplete % 10);
                                //console.log(" ");
                                //Do something with upload progress
                                //if (percentComplete % 10 == 0) {
                                $('.progress-bar').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete);
                                $('#progresstext_front').text(percentComplete + '%');
                                //console.log(percentComplete);
                                //console.log(" ");
                                //echo "$i is a multiple of 10<br />";
                                //}
                                //console.log(percentComplete);
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                //console.log(percentComplete);
                            }
                        }, false);

                        return xhr;
                    },
                    complete: function () {
                        //console.log("Request finished.");
                    },
                    url: ajaxUrl,
                    type: "POST",
                    data: formData,
                    // both 'contentType' and 'processData' parameters are
                    // required so that all data are correctly transferred
                    contentType: false,
                    processData: false
                }).done(function (response) {

                    //var json_obj = $.parseJSON(response);//parse JSON
                    //debugger
                    //console.log(response);
                    $('#responsetext_front').text(response);
                    swal({
                        title: "Upload Response",
                        html: response,
                        type: "success",
                    }).then(function () {
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    });



                    //update the card front image

                    //alert(response);
                    //alert(response);
                    // In this callback you get the AJAX response to check
                    // if everything is right...
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("Status: " + textStatus); alert("Error: " + errorThrown);
                    $('#responsetext_front').text("Status: " + textStatus); alert("Error: " + errorThrown);
                    // Here you should treat the http errors (e.g., 403, 404)
                }).always(function () {
                    //alert("AJAX request finished!");
                });
            }
            //alert(selectedFile.name);
            //alert(selectedFile.size);
            //alert(selectedFile.type);

            function updateFrontCard() {
                $.ajax({
                    type: "POST",
                    url: "ws/get-ins-card-front-image.php",
                    data: values,
                    success: function (msg) {

                        alert("success");
                        $("#card-front").attr("src", msg);

                    },
                    error: function () {
                        alert("Error updating card image, please refresh the page");
                    },
                    complete: function () {
                    }
                });

            }
        });

        //back card image
        $(document).on('change', '#fileToUpload_back', function () {

            var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

            //input.trigger('fileselect', [numFiles, label]);
            $('#fileToUploadTextbox_back').val(label);
            $('#responsetext_back').text('');
            $('#progress_bar_back').addClass("hide");
            $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
            var selectedFile = document.getElementById('fileToUpload_back').files[0];

            //alert(selectedFile.name);
            //alert(selectedFile.size);
            //alert(selectedFile.type);
            //alert(selectedFile.lastModifiedDate);
            //alert(selectedFile.lastModified);

            var max_file_size = 5242880; // max size 5mb

            var file_ext_type = "";

            //only accept image formats
            switch (selectedFile.type) {
                case 'image/png':
                    file_ext_type = "png";
                    break;
                case 'image/jpg':
                    file_ext_type = "jpg";
                    break;
                case 'image/jpeg':
                    file_ext_type = "jpeg";
                    break;
                default:
                    file_ext_type = "???";
            }

            if (selectedFile.size > max_file_size) {
                $('#responsetext_back').text("the file size ( " + (selectedFile.size / 1000000).toFixed(2) + "MB ) exceeds the max file size of ( " + max_file_size / 1048576 + "MB ) and cannot be uploaded");
                $('#button-upload_back').prop('disabled', true);
                $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
                $('#progresstext_back').text(0 + '%');
            }
            else {
                if (file_ext_type == "???") {
                    $('#responsetext_back').html("The file type is not permitted and cannot be uploaded.<br />Please select jpg, jpeg, or png");
                    $('#button-upload_back').prop('disabled', true);
                    $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
                    $('#progresstext_back').text(0 + '%');
                }
                else {
                    $('#button-upload_back').prop('disabled', false);
                }
            }


            $("#button-upload_back").click(sendFormData);

            function sendFormData() {

                var fileName = $("#fileToUpload_back").val();

                if (fileName) { // returns true if the string is not empty
                    //alert(fileName + " was selected");
                } else { // no file was selected
                    $('#responsetext_back').text("no file selected");
                    return;
                }

                $('#progress_bar_back').removeClass("hide");

                var selectedFile = document.getElementById('fileToUpload_back').files[0];

                var file_ext_type = "";

                switch (selectedFile.type) {
                    case 'image/png':
                        file_ext_type = "png";
                        break;
                    case 'image/jpg':
                        file_ext_type = "jpg";
                        break;
                    case 'image/jpeg':
                        file_ext_type = "jpeg";
                        break;
                    default:
                        file_ext_type = "???";
                }

                $('#ins_company_form').append('<input type="hidden" id="file_size" name="file_size" value="' + selectedFile.size + '" />');
                $('#ins_company_form').append('<input type="hidden" id="file_ext_type" name="file_ext_type" value="' + file_ext_type + '" />');
                $('#ins_company_form').append('<input type="hidden" id="mime_type" name="mime_type" value="' + selectedFile.type + '" />');
                $('#ins_company_form').append('<input type="hidden" id="card_side" name="card_side" value="back" />');

                var formData = new FormData($("#ins_company_form").get(0));

                var ajaxUrl = "process_ins_card_upload.php";

                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        // Handle progress
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = Math.round((evt.loaded / evt.total) * 100);
                                //console.log(percentComplete);
                                //console.log(percentComplete);
                                //console.log(percentComplete % 10);
                                //console.log(" ");
                                //Do something with upload progress
                                //if (percentComplete % 10 == 0) {
                                $('.progress-bar').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete);
                                $('#progresstext_back').text(percentComplete + '%');
                                //console.log(percentComplete);
                                //console.log(" ");
                                //echo "$i is a multiple of 10<br />";
                                //}
                                //console.log(percentComplete);
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                //console.log(percentComplete);
                            }
                        }, false);

                        return xhr;
                    },
                    complete: function () {
                        //console.log("Request finished.");
                    },
                    url: ajaxUrl,
                    type: "POST",
                    data: formData,
                    // both 'contentType' and 'processData' parameters are
                    // required so that all data are correctly transferred
                    contentType: false,
                    processData: false
                }).done(function (response) {

                    //var json_obj = $.parseJSON(response);//parse JSON
                    //debugger
                    //console.log(response);
                    $('#responsetext_back').text(response);

                    setTimeout(function () {
                        location.reload();
                    }, 1000);

                    //update the card front image

                    //alert(response);
                    //alert(response);
                    // In this callback you get the AJAX response to check
                    // if everything is right...
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("Status: " + textStatus); alert("Error: " + errorThrown);
                    $('#responsetext_back').text("Status: " + textStatus); alert("Error: " + errorThrown);
                    // Here you should treat the http errors (e.g., 403, 404)
                }).always(function () {
                    //alert("AJAX request finished!");
                });
            }
            //alert(selectedFile.name);
            //alert(selectedFile.size);
            //alert(selectedFile.type);
        });



    </script>

</body>
</html>
