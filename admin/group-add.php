<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <title>Dashboard || EMTeLink Tracker Admin</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">

    <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">

    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">

    <link href="./css/pages/dashboard.css" rel="stylesheet">

    <link href="./css/custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php

    include("../include/incConfig.php");
    session_start();

    ?>
    <style>

ul, li {
    margin:0;
    padding:0px;
    list-style-type:none;
    text-align:left;
}

#pswd_info {
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
}

#pswd_info h5 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}


.invalid {
    background:url(../../images/redx.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    background:url(../../images/green-ck.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}


    </style>
</head>

<body>

    <?php 

    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];
    $admin_user = $_SESSION["admin_user"];
    $subType = $_SESSION["subType"];
    $subMaxUsers = $_SESSION["subMaxUsers"];
    $subId = $_SESSION["subId"];

    ?>

    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                </button>
                <a class="navbar-brand" href="./index.html">EMTeLink Tracker Admin</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu">
                            <!--<li><a href="./account.html">Account Settings</a></li>
                             <li><a href="javascript:;">Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:;">Help</a></li> -->
                        </ul>

                    </li>

                    <li><a href="../home.php" style="font-size: 16px"><i class="icon-home"></i>&nbsp;Home</a></li>
                    <?php if ($subType != "F") {  ?>
                    <li><a href="index.php" style="font-size: 16px"><i class="icon-dashboard"></i>&nbsp;Dashboard</a></li>
                    <?php }  ?>

                    <li class="dropdown">

                        <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px">
                            <i class="icon-user"></i>
                            <?php echo $firstname . " " . $lastname ?>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <!--  <li><a href="javascript:;">My Profile</a></li>
                            <li><a href="javascript:;">My Groups</a></li> 
                            <li class="divider"></li> -->
                            <li><a href="../Index.html" style="font-size: 14px">Logout</a></li>
                        </ul>

                    </li>
                </ul>

                <!-- <form class="navbar-form navbar-right" role="search">
                   <div class="form-group">
                     <input type="text" class="form-control input-sm search-query" placeholder="Search">
                   </div>
                 </form> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="subnavbar">

        <div class="subnavbar-inner">

            <div class="container">

                <!-- <a href="javascript:;" class="subnav-toggle" data-toggle="collapse" data-target=".subnav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>

                </a> -->


            </div>
            <!-- /container -->

        </div>
        <!-- /subnavbar-inner -->

    </div>


    <div class="main">

        <div class="container">
            <?php 
            $instruction_content = "Enter New Group Details below and then select \"Create\" to create the new group account.<br /><br />";
            $instruction_content = $instruction_content . "Select \"Done Adding Groups\" when complete.";
            ?>
               <div class="row">
                    <div class="col-md-2">

                </div>
                <div class="col-md-8 center-block">
                    <div class="panel-group accordion">
                        <div class="panel panel-warning open" style="border-color:black">

                            <div class="panel-heading" style="text-align:center">

                                <h4 class="panel-title">

                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                                        <span style="font-size: large;color:#525252;"><i class="icon-info-sign" style="color: #525252"></i>&nbsp;<u>Instructions</u></span><span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to Toggle)</span>
                                    </a>
                                </h4>

                            </div>

                            <div id="collapseOne33" class="panel-collapse collapse in">

                                <div class="panel-body">

                                    <p style="font-size: medium">
                                        <?php echo $instruction_content; ?>
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-2">

                </div>
            </div>
            <br />

            <div class="row">


                <div class="col-md-2">

                </div>
                <!-- /span4 -->

                <div class="col-md-8">

                    <div class="widget stacked ">

                        <div class="widget-header">
                            <i class="icon-group"></i>
                            <h3>Add Group</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">



                            <div class="tabbable">

                                <br>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <form id="editprofileform" class="editprofileform form-horizontal col-md-12">
                                            <fieldset>

                                                <div class="form-group">
                                                    <label for="groupname" class="col-md-3">Group Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="groupname" name="groupname" value="" placeholder="enter a group name">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->
                                                <br />

                                                <div class="form-group">

                                                    <div class="col-md-12 text-center btn-toolbar">
                                                        <button type="button" id="btn_add_group" class="btn btn-primary">Create</button>
                                                        <button class="btn btn-default" id="btn_cancel">Done Adding Groups</button>
                                                    </div>
                                                </div>
                                                <!-- /form-actions -->
                                            </fieldset>
                                            <input type="hidden" value="<?php echo $subId?>" id="subId" name="subId" />
                                        </form>
                                    </div>

                                </div>


                            </div>





                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="col-md-2">

               <!--     <div class="well">

                        <h4>Add User</h4>

                        <p>In order to add a new user, please enter the UserID, Firstname, Lastname and Password, then click the Create Button..</p>

                    </div> -->

                </div>
                <!-- /span4 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->

    <br />
    <div class="text-center">
        <br />
<br />
<br />
        <a href="../Home.php" class="btn btn-warning btn-group-md" style="color:black" role="button">Return To Home Screen</a>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />

    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?> 
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
       
 <!-- /footer -->



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script>

        $('#btn_add_group').on('click', function (e) {

            if ($('#groupname').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Group Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>Group Name</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            //alert("here")
            $.ajax({
                type: "POST",
                url: "../ws/put-group.php",
                data: $('form.editprofileform').serialize(),
                success: function (msg) {

                    alert(msg);
                    //$('#sug').val(msg);
                    //$("#thanks").html(msg)
                    //$("#form-content").modal('hide');
                    //window.location.href = "home.html";					
                },
                error: function () {
                    alert("unable to create group");
                }
            });
        })


        $('#btn_cancel').on('click', function (e) {
		    window.location.href = "group-manager.php";
		    return false;
        })

    </script>

</body>
</html>
