<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <title>Dashboard || EMTeLink MyEMRMyWay</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">

    <!-- <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet"> -->

    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">

    <link href="./css/pages/dashboard.css" rel="stylesheet">

    <link href="./css/custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="./css/bootstrap-toggle.min.css" rel="stylesheet">

    <?php

    include("../include/incConfig.php");
    session_start();

    ?>
    <style>

ul, li {
    margin:0;
    padding:0px;
    list-style-type:none;
    text-align:left;
}

.widget .widget-content {
    padding: 5px 15px 15px !important;
}
    </style>
</head>

<body>

    <?php 

    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];
    $admin_user = $_SESSION["admin_user"];
    $subType = $_SESSION["subType"];
    $subMaxUsers = $_SESSION["subMaxUsers"];
    $subId = $_SESSION["subId"];

    ?>

    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                </button>
                <a class="navbar-brand" href="../Index.html">EMTeLINK</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu">
                            <!--<li><a href="./account.html">Account Settings</a></li>
                             <li><a href="javascript:;">Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:;">Help</a></li> -->
                        </ul>

                    </li>

                    <li><a href="../home.php" style="font-size: 16px"><i class="icon-home"></i>&nbsp;Home</a></li>
                    <?php if ($subType != "F") {  ?>
                    <li><a href="index.php" style="font-size: 16px"><i class="icon-dashboard"></i>&nbsp;Dashboard</a></li>
                    <?php }  ?>

                    <li class="dropdown">

                        <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px">
                            <i class="icon-user"></i>
                            <?php echo  $_SESSION["loggedin_fname"] . " " .  $_SESSION["loggedin_lname"] ?>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <!--  <li><a href="javascript:;">My Profile</a></li>
                            <li><a href="javascript:;">My Groups</a></li> 
                            <li class="divider"></li> -->
                            <li><a href="../Index.html" style="font-size: 14px">Logout</a></li>
                        </ul>

                    </li>
                </ul>

                <!-- <form class="navbar-form navbar-right" role="search">
                   <div class="form-group">
                     <input type="text" class="form-control input-sm search-query" placeholder="Search">
                   </div>
                 </form> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="subnavbar">

        <div class="subnavbar-inner">

            <div class="container">

                <!-- <a href="javascript:;" class="subnav-toggle" data-toggle="collapse" data-target=".subnav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>

                </a> -->


            </div>
            <!-- /container -->

        </div>
        <!-- /subnavbar-inner -->

    </div>


    <div class="main">

        <div class="container">
            <?php 

            if ($subType != "F") {
                $instruction_content = "Enter additional users below.<br /><br />";
            }
            else {
                $instruction_content = "Use this screen to enter your providers.<br /><br />";
            }

            $instruction_content = $instruction_content . "Enter the Provider details below and then select \"Create\" to create the new provider record.<br /><br />";
            $instruction_content = $instruction_content . "Select \"Done Adding Providers\" when complete to return to the main Provider screen.";
            ?>
               <div class="row">
                    <div class="col-md-2">

                </div>
                <div class="col-md-8 center-block">
                    <div class="panel-group accordion">
                        <div class="panel panel-warning open" style="border-color:black">

                            <div class="panel-heading" style="text-align:center">

                                <h4 class="panel-title">

                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                                        <span style="font-size: large;color:#525252;"><i class="icon-info-sign" style="color: #525252"></i>&nbsp;<u>Instructions</u></span><span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to Toggle)</span>
                                    </a>
                                </h4>

                            </div>

                            <div id="collapseOne33" class="panel-collapse collapse">

                                <div class="panel-body">

                                    <p style="font-size: medium">
                                        <?php echo $instruction_content; ?>
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-2">

                </div>
            </div>
            <br />

            <div class="row">


                <div class="col-md-2">

                </div>
                <!-- /span4 -->

                <div class="col-md-8">

                    <div class="widget stacked ">

                        <div class="widget-header">
                            <h3><i class="icon-user-md"></i>&nbsp;&nbsp;Add Provider</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="tabbable">

                                <div class="tab-content">
                                    <div class="tab-pane active" id="provider">
                                        <form id="editproviderform" class="editproviderform form-horizontal col-md-12">
                                            <fieldset>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <!-- Name -->
                                                    <label for="providerName" class="control-label">Name</label>
                                                    <input type="text" class="form-control" id="providerName" name="providerName" style="margin-bottom:3px" placeholder="individual or business name" autofocus="autofocus">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <!-- Street 1 -->
                                                    <label for="providerAddress" class="control-label">Address</label>
                                                    <input type="text" class="form-control" id="providerAddress" name="providerAddress" placeholder="Street address, P.O. box, company name, c/o">
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <!-- City-->
                                                    <label for="providerCity" class="control-label">City</label>
                                                    <input type="text" class="form-control" id="providerCity" name="providerCity" placeholder="Enter city">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <!-- State Button -->
                                                    <label for="providerState" class="control-label">State</label>
                                                    <select class="form-control" id="providerState" name="providerState" placeholder="Select state">
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <!-- Zip Code-->
                                                    <label for="zip_id" class="control-label">Zip Code</label>
                                                    <input type="text" class="form-control" id="zip_id" name="zip" placeholder="#####">
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <!-- Phone Number -->
                                                    <label for="providerPhone" class="control-label">Phone #</label>
                                                    <input type="text" class="form-control" id="providerPhone" name="providerPhone" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <!-- Fax Number -->
                                                    <label for="providerFax" class="control-label">Fax #</label>
                                                    <input type="text" class="form-control" id="providerFax" name="providerFax">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <!-- Type/Speciality -->
                                                    <label for="providerType" class="control-label">Type/Speciality</label>
                                                    <input type="text" class="form-control" id="providerType" name="providerType" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <!-- Primary? -->
                                                    <label for="providerPrimary" class="control-label">Primary?</label>
                                                    <select class="form-control" id="providerPrimary" name="providerPrimary">
                                                        <option value="No">No</option>
                                                        <option value="Yes">Yes</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center btn-toolbar">
                                                    <button type="button" id="btn_add_provider" class="btn btn-primary" disabled>Create</button>
                                                    <button class="btn btn-default" id="btn_cancel">Done Adding Providers</button>
                                                </div>
                                            </div>
                                            </fieldset>
                                            <input type="hidden" value="<?php echo $subId?>" id="subId" name="subId" />
                                        </form>
                                    </div>

                                </div>


                            </div>





                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="col-md-2">

               <!--     <div class="well">

                        <h4>Add User</h4>

                        <p>In order to add a new user, please enter the UserID, Firstname, Lastname and Password, then click the Create Button..</p>

                    </div> -->

                </div>
                <!-- /span4 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->

    <br />
    <div class="text-center">
        <a href="../Home.php" class="btn btn-warning btn-group-md" style="color:black" role="button">Return To Home Screen</a>
    </div>
    <br />

    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?> 
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
       
 <!-- /footer -->



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="./js/libs/bootstrap-toggle.min.js"></script>
    
    <script>
	
	    $(document).ready(function() {
            /*
	        $('#user_type').multiselect({
	            buttonWidth: '100%',
	            maxHeight: 100
	        });
            */
	        //$(".chzn-select").chosen();

	    });

	    function toggleManageGroupDiv() {
	        if ($('#user_type').val() == "Admin") {
	            $('#manage_groups').show();
	        }
	        else {
	            $('#manage_groups').hide();
	        }

	        //alert(sel.value);
	        //if the user_type is admin, then add "Everyone" to the list
            //if the user_type is user, then remove "Everyone" from the list
	    }


        $('input[id=password1]').keyup(function () {
            // set password variable
            var pswd = $(this).val();

            //validate the length
            if (pswd.length < 8) {
                $('#length').removeClass('valid').addClass('invalid');
            } else {
                $('#length').removeClass('invalid').addClass('valid');
            }

            //validate letter
            if (pswd.match(/[A-z]/)) {
                $('#letter').removeClass('invalid').addClass('valid');
            } else {
                $('#letter').removeClass('valid').addClass('invalid');
            }

            //validate capital letter
            if (pswd.match(/[A-Z]/)) {
                $('#capital').removeClass('invalid').addClass('valid');
            } else {
                $('#capital').removeClass('valid').addClass('invalid');
            }

            //validate number
            if (pswd.match(/\d/)) {
                $('#number').removeClass('invalid').addClass('valid');
            } else {
                $('#number').removeClass('valid').addClass('invalid');
            }


            if ($("#number").hasClass('valid') && $("#letter").hasClass('valid') && $("#capital").hasClass('valid') && $("#length").hasClass('valid')) {

                //alert('number valid');
                $("#btn_add_user").prop('disabled', false);
            }
            else {

                $("#btn_add_user").prop('disabled', true);
            }
        })


        $('#btn_add_user').on('click', function (e) {


            if ($('#newuserid').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing UserID');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>UserID</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }


            if ($('#firstname').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing First Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>First Name</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }


            if ($('#lastname').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Last Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>Last Name</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            if ($('#email').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Email Address');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>Email Address</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            //alert("here")
            $.ajax({
                type: "POST",
                url: "../ws/put-provider.php",
                data: $('form.editproviderform').serialize(),
                success: function (msg) {

                    alert(msg);
                    //$('#sug').val(msg);
                    //$("#thanks").html(msg)
                    //$("#form-content").modal('hide');
                    //window.location.href = "home.html";					
                },
                error: function () {
                    alert("unable to create provider");
                }
            });
        })


        $('#btn_cancel').on('click', function (e) {
		    window.location.href = "providers.php";
		    return false;
        })

    </script>

</body>
</html>
