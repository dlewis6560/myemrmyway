<?php


include("../include/incConfig.php");
session_start();

$filter_text = "( not filtered )";

//make sure we have a valid sesion
if ($_SESSION["valid"] != "TRUE")
{
    header("Location: index.html");
};

$firstname = $_SESSION["firstname"];
$lastname = $_SESSION["lastname"];
$userid = $_SESSION["userid"];
$admin_user = $_SESSION["admin_user"];
$subId = $_SESSION["subId"];

if (isset($_POST["current_page"])) {
    $current_page = $_POST["current_page"];
}else{
    $current_page=1;
}


if (isset($_POST["page_filter"])) {
    $filter = $_POST["page_filter"];
}else{
    $filter="";
}

$where = "where dbo.[user].subID=" . $subId;

if (strlen($filter) > 0) {
    switch ($filter) {
        case 'a-d':
            $where = "where ((lastname like 'a%') or (lastname like 'b%') or (lastname like 'c%') or (lastname like 'd%')) AND dbo.[user].subID=" . $subId;
            $filter_text = "( last name filter applied - A thru D )";
            break;
        case 'e-h':
            $where = "where ((lastname like 'e%') or (lastname like 'f%') or (lastname like 'g%') or (lastname like 'h%')) AND dbo.[user].subID=" . $subId;
            $filter_text = "( last name filter applied - E thru H )";
            break;
        case 'i-l':
            $where = "where ((lastname like 'i%') or (lastname like 'j%') or (lastname like 'k%') or (lastname like 'l%')) AND dbo.[user].subID=" . $subId;
            $filter_text = "( last name filter applied - I thru L )";
            break;
        case 'm-p':
            $where = "where ((lastname like 'm%') or (lastname like 'n%') or (lastname like 'o%') or (lastname like 'p%')) AND dbo.[user].subID=" . $subId;
            $filter_text = "( last name filter applied - M thru P )";
            break;
        case 'q-t':
            $where = "where ((lastname like 'q%') or (lastname like 'r%') or (lastname like 's%') or (lastname like 't%')) AND dbo.[user].subID=" . $subId;
            $filter_text = "( last name filter applied - Q thru T )";
            break;
        case 'u-w':
            $where = "where ((lastname like 'u%') or (lastname like 'v%') or (lastname like 'w%')) AND dbo.[user].subID=" . $subId;
            $filter_text = "( last name filter applied - U thru W )";
            break;
        case 'x-z':
            $where = "where ((lastname like 'x%') or (lastname like 'y%') or (lastname like 'z%')) AND dbo.[user].subID=" . $subId;
            $filter_text = "( last name filter applied - X thru Z )";
            break;
        default:
            $where = "where ((lastname like '" . $filter . "%') ) AND dbo.[user].subID=" . $subId;
            $filter_text = "( last name filter applied - " . $filter . " )";
    }

}


$PageSize = 10;
$MaxPages = 0;

$strSQLCount = "SELECT Count(userid) as REC_COUNT ";
//$strSQLCount = $strSQLCount . "FROM user ";
$strSQLCount = $strSQLCount . "FROM [user] " . $where;

$recordset = $database->query($strSQLCount);
foreach($recordset as $data)
{
    $count = $data["REC_COUNT"];
}

$MaxPages = ceil($count / $PageSize);

$strSQL = "SELECT dbo.[user].uid, dbo.[user].userid, dbo.[user].firstname, dbo.[user].lastname, dbo.[user].student_email, dbo.groups.gname ";
$strSQL = $strSQL . "FROM dbo.groups INNER JOIN ";
$strSQL = $strSQL . "dbo.user_groups ON dbo.groups.gid = dbo.user_groups.gid RIGHT OUTER JOIN ";
$strSQL = $strSQL . "dbo.[user] ON dbo.user_groups.uid = dbo.[user].uid ";
$strSQL = $strSQL . " " . $where . " ";

//$strSQL = "SELECT uid,userid,firstname,lastname,student_email ";
//$strSQL = $strSQL . "FROM [user] " . $where . " ";
//$strSQL = $strSQL . "FROM user ";
$strSQL = $strSQL . "ORDER BY lastname ";
$strSQL = $strSQL . "OFFSET (".$PageSize." * (".$current_page." - 1)) ROWS ";
$strSQL = $strSQL . "FETCH NEXT ".$PageSize." ROWS ONLY;";



?>

                                <table class="table table-bordered table-hover table-striped table-responsive" id="user-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="font-size: 18px">Action</th>
                                            <th class="text-left" style="font-size: 18px">User ID</th>
                                            <th class="text-left" style="font-size: 18px">Name</th>
                                            <th class="text-left" style="font-size: 18px">Email</th>
                                            <th class="text-left hidden-xs" style="font-size: 18px">Group</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //echo $strSQL;
                                        $recordset = $database->query($strSQL);
                                        foreach($recordset as $data)
                                        {
                                            $uid = $data["uid"];
                                            $studentid = $data["userid"];
                                            $firstname = $data["firstname"];
                                            $lastname = $data["lastname"];
                                            $student_email = $data["student_email"];
                                            $groupname = $data["gname"];

                                            //echo "student - " . $studentid . "<br />";
                                            //echo "firstname - " . $firstname;
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default btn-md dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="../EmergencyInfo.php?report_user_id=<?php echo $studentid; ?>" target="_blank" style="font-size: 16px">View Report</a></li>
                                                        <li><a href="#" style="font-size: 16px" class="email_rpt" id="<?php echo $uid; ?>">Email Report</a></li>
                                                        <li><a href="#" style="font-size: 16px">View Details</a></li>
                                                        <li><a href="#" style="font-size: 16px" class="reset_pw" id="<?php echo $uid; ?>">Reset Password</a></li>
                                                        <li><a href="#" style="font-size: 16px" class="delete_user" id="<?php echo $uid; ?>">Delete User</a></li>
                                                    </ul>
                                                </div>
                                                <!-- <button type="button" class="btn-md btn-success">Manage</button>&nbsp;&nbsp; -->
                                            </td>
                                            <td class="text-left" style="font-size: 16px"><?php echo $studentid;?></td>
                                            <td class="text-left" style="font-size: 16px"><?php echo $firstname . " " . $lastname;?></td>
                                            <td class="text-left" style="font-size: 16px"><?php echo $student_email;?></td>
                                            <td class="text-left hidden-xs" style="font-size: 16px"><?php echo $groupname;?></td>
                                        </tr>
                                        <?php
                                        }   
                                        ?>
                                    </tbody>
                                </table>


<?php
