$(function () {

	var data = [];
	var series = 2;
	data[0] = { label: "Complete", data: Math.floor(0.38 * 100) + 1 }
	data[1] = { label: "Incomplete", data: Math.floor(0.62 * 100) + 1 }

	$.plot($("#donut-chart"), data,
	{
		colors: ["#F90", "#222", "#777", "#AAA"],
	        series: {
	            pie: { 
	                innerRadius: 0.5,
	                show: true
	            }
	        }
	});
	
});