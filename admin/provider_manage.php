<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>MyEMRMyWay :: Providers</title>
    <meta name="description" content="MyEMRMyWay App" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="./css/font-awesome.min.css" rel="stylesheet" />

    <!-- <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet"> -->

    <link href="./css/base-admin-3.css" rel="stylesheet" />
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet" />

    <link href="./css/pages/dashboard.css" rel="stylesheet" />

    <link href="./css/custom.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="./css/bootstrap-toggle.min.css" rel="stylesheet" />

    <link href="../components/datepicker/css/datepicker.css" rel="stylesheet" />
    <link href="../Content/sweetalert2.min.css" rel="stylesheet" />

    <?php

    include("../include/incConfig.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    session_start();
	include("../include/session.php");

    $id_value =  htmlspecialchars($_GET["id"]);
    $decrypted = Urlcrypt::decrypt($id_value);
    $mode = "";
    $starttime="";
    //echo "id_value = " . $id_value;
    //echo "decrypted = " . $decrypted;

    if ($id_value == "") {
        $mode = "insert";
        $WIDGET_HEADER_TITLE = "Add New Provider";
        $instruction_content = "Enter the new Provider info below and then tap the Save button.<br />";
    } else {
        list($providerid, $mode, $starttime) = explode("|", $decrypted);
    }

    //echo $mode;
    $endtime = time();
    $timediff = $endtime - $starttime;

    $Name = "";
    $Address = "";
    $City = "";
    $State = "";
    $Zip = "";
    $PhoneNumber = "";
    $FaxNumber = "";
    $Email = "";
    $Speciality = "";
    $Primary = "";
    $EditDate  = "";

    if ($mode == "edit") {

        $recordset = $database->select("user_providers", [
            "Name",
            "Address",
            "City",
            "State",
            "Zip",
            "PhoneNumber",
            "FaxNumber",
            "Email",
            "Specialty",
            "Primary",
            "EditDate"
        ], [
            "ProviderId" => $providerid
        ]);

        foreach($recordset as $data)
        {
            $Name = $data["Name"];
            $Address = $data["Address"];
            $City = $data["City"];
            $State = $data["State"];
            $Zip = $data["Zip"];
            $PhoneNumber = $data["PhoneNumber"];
            $FaxNumber = $data["FaxNumber"];
            $Email = $data["Email"];
            $Specialty = $data["Specialty"];

            if ($data["Primary"] == 1) {
                $Primary = "Yes";
            } else {
                $Primary = "No";
            }

            //$Primary = $data["Primary"];

            $EditDate  = $data["EditDate"];
        }



        $WIDGET_HEADER_TITLE = "Editing " . $Name . " record.";

        $instruction_content = "Make changes and then tap the Save button.<br />";
    }

    ?>
    <style>
        ul, li {
            margin: 0;
            padding: 0px;
            list-style-type: none;
            text-align: left;
        }

        #pswd_info {
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_info h5 {
                margin: 0 0 10px 0;
                padding: 0;
                font-weight: normal;
            }


        .invalid {
            background: url(../../images/redx.jpg) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #ec3f41;
        }

        .valid {
            background: url(../../images/green-ck.jpg) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #3a7d34;
        }

        .form-control {
            color: #000000 !important;
            background-color: #F7F7F7 !important;
            font-weight: 600 !important;
        }


        @media screen and (max-width: 767px) {

            .widget .widget-content {
                padding: 0px 0px 0px !important;
                background: #fff;
                border: 1px solid #D5D5D5;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
            }
        }
    </style>
</head>

<body>

    <?php

	include("../include/navbar.php");

    ?>

    <div class="main">
        <br />
        <br />
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h3>
                        <strong>Provider</strong>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 hidden-xs"></div>
                <div class="col-md-10 col-xs-12 center-block">
                    <div class="panel-group accordion">
                        <div class="panel panel-warning open" style="border-color:black">

                            <div class="panel-heading" style="text-align:center">

                                <h4 class="panel-title">

                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                                        <span style="font-size: large;color:#525252;">
                                            <i class="icon-info-sign" style="color: #525252"></i>&nbsp;
                                            <u>Instructions</u>
                                        </span>
                                        <span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to Toggle)</span>
                                    </a>
                                </h4>

                            </div>

                            <div id="collapseOne33" class="panel-collapse collapse in">

                                <div class="panel-body">

                                    <p style="font-size: medium">
                                        <?php echo $instruction_content; ?>
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-1 hidden-xs"></div>
            </div>
            <br />

            <div class="row">


                <div class="col-md-1 hidden-xs"></div>
                <!-- /span4 -->

                <div class="col-md-10 col-xs-12">

                    <div class="widget stacked ">

                        <div class="widget-header">
                            <i class="icon-user-md"></i>
                            <h3>
                                <strong>
                                    <?php echo $WIDGET_HEADER_TITLE ?>
                                </strong>
                            </h3>
                        </div>
                        <!-- /widget-header -->
                        <div class="widget-content">
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile">
                                    <form id="provider_form" class="provider_form form-horizontal col-md-12">
                                        <fieldset>
                                            <div class="form-group">
                                                <label for="prov_name" class="col-md-3">Provider Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="prov_name" name="prov_name" placeholder="enter provider name" value="<?php echo $Name ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="prov_address" class="col-md-3">Address</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="prov_address" name="prov_address" placeholder="enter provider address" value="<?php echo $Address ?>" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="prov_city">City</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="prov_city" name="prov_city" placeholder="enter city" value="<?php echo $City ?>" />
                                                </div>

                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3" for="prov_state">State</label>
                                                <div class="col-md-9">
                                                    <?php $SelectedState = $State; ?>
                                                    <select class="form-control" id="prov_state" name="prov_state">
                                                        <?php include("../include/incStateListOptions.php"); ?>
                                                    </select>
                                                </div>

                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3" for="prov_zip">Zip</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="prov_zip" name="prov_zip" placeholder="enter zip" value="<?php echo $Zip ?>" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="prov_phone">Phone Number</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="prov_phone" name="prov_phone" placeholder="enter phone number" value="<?php echo $PhoneNumber ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="prov_fax">Fax Number</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="prov_fax" name="prov_fax" placeholder="enter fax number" value="<?php echo $FaxNumber ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="prov_email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="prov_email" name="prov_email" placeholder="enter provider email" value="<?php echo $Email ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="prov_speciality">Speciality</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="prov_speciality" name="prov_speciality" placeholder="enter the providers speciality" value="<?php echo $Specialty ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3" for="prov_primary">Primary</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" id="prov_primary" name="prov_primary" value="<?php echo $Primary ?>">
                                                        <option value="1"  <?php if ($mode=='edit') { if ($Primary == 'Yes') {echo 'selected';}} ?> >Yes</option>
                                                        <option value="0" <?php if ($mode=='edit') { if ($Primary == 'No') {echo 'selected';}} ?>>No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <hr />
                                            <div class="form-group">
                                                <div class="col-xs-6 text-right">
                                                    <button type="button" class="btn btn-warning" id="btn_cancel" style="color:black">
                                                        <i class="icon-arrow-left"></i>&nbsp;Back To Providers
                                                    </button>
                                                </div>
                                                <div class="col-xs-6 text-left">
                                                    <button type="button" class="btn btn-success" id="btn_add_update_provider">Save</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <input type="hidden" value="<?php echo $id_value ?>" id="hidden" name="hidden" />
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="col-md-1 hidden-xs">

                    <!--     <div class="well">

                        <h4>Add User</h4>

                        <p>In order to add a new user, please enter the UserID, Firstname, Lastname and Password, then click the Create Company Button..</p>

                    </div> -->

                </div>
                <!-- /span4 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->
    <div class="text-center">
        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">
            <i class="icon-home" style="color: #ffffff;"></i>Return Home
        </a>
    </div>
    <br />

    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- /footer -->



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="./js/libs/bootstrap-toggle.min.js"></script>

    <script src="../components/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../Scripts/sweetalert2.min.js"></script>

    <script>

        $(document).ready(function () {


            //$('.effective_date_picker').datepicker();
            /*
	        $('#user_type').multiselect({
	            buttonWidth: '100%',
	            maxHeight: 100
	        });
            */
            //$(".chzn-select").chosen();

        });


        $('#btn_add_update_provider').on('click', function (e) {

            if ($('#prov_name').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Provider Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h4>Please enter a <u>Provider Name</u>.</h4></span>');
                $('#myModal').modal('show');
                return;
            }

            //alert("here")
            <?php
            if ($mode == "edit") {
                $url = "../ws/edit-provider.php";
            }
            else {
                $url = "../ws/put-provider.php";
            }
            ?>

            $.ajax({
                type: "POST",
                url: "<?php echo $url; ?>",
                data: $('form.provider_form').serialize(),
                success: function (msg) {
                    //alert(msg);

                    var msg_type = "error";
                    var msg_title = "Not Saved";

                    if (msg.includes("success")) {
                        msg_type = "success";
                        msg_title = "Successful";
                    }

                    swal({
                        title: msg_title,
                        html: msg,
                        type: msg_type,
                    }).then(function () {
                        if (msg_type == "success") {
                            window.location.href = "../providers.php";
                        }
                    });
                },
                error: function () {
                    alert("unable to create provider");
                }

            });
        })

        $('#btn_cancel').on('click', function (e) {
            e.preventDefault();
            window.location.href = "../providers.php";
        })

    </script>

    <?php include("../include/incUpload.php"); ?>
</body>
</html>
