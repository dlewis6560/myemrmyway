<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <title>MyEMRMyWay :: User Add</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="description" content="MyEMRMyWay App" />


    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">

    <!-- <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet"> -->

    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">

    <link href="./css/pages/dashboard.css" rel="stylesheet">

    <link href="./css/custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="./css/bootstrap-toggle.min.css" rel="stylesheet">

    <?php

    include("../include/incConfig.php");
    session_start();

    include("../include/session.php");

    //make sure there are available user slots to add
    $count = $database->count("user", [
	"userid" => $userid
    ]);

    $can_add_more_users = true;

    if ($count >= $subMaxUsers)
    {
        $can_add_more_users = false;
    }

    $stroutput = "count = " . $count . "<br />";
    $stroutput = $stroutput . "subMaxUsers = " . $subMaxUsers . "<br />";
    $stroutput = $stroutput . "can_add_more_users = " . $can_add_more_users;

    ?>
    <style>

ul, li {
    margin:0;
    padding:0px;
    list-style-type:none;
    text-align:left;
}

#pswd_info {
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
}

#pswd_info h5 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}


.invalid {
    background:url(../../images/redx.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    background:url(../../images/green-ck.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}


    </style>
</head>

<body>

    <?php 

    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];
    $admin_user = $_SESSION["admin_user"];
    $subType = $_SESSION["subType"];
    $subMaxUsers = $_SESSION["subMaxUsers"];
    $subId = $_SESSION["subId"];

    ?>

    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                </button>
                <a class="navbar-brand" href="./index.html">EMTeLink Tracker Admin</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu">
                            <!--<li><a href="./account.html">Account Settings</a></li>
                             <li><a href="javascript:;">Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:;">Help</a></li> -->
                        </ul>

                    </li>

                    <li><a href="../home.php" style="font-size: 16px"><i class="icon-home"></i>&nbsp;Home</a></li>
                    <?php if ($subType != "F") {  ?>
                    <li><a href="index.php" style="font-size: 16px"><i class="icon-dashboard"></i>&nbsp;Dashboard</a></li>
                    <?php }  ?>

                    <li class="dropdown">

                        <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px">
                            <i class="icon-user"></i>
                            <?php echo  $_SESSION["loggedin_fname"] . " " .  $_SESSION["loggedin_lname"] ?>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <!--  <li><a href="javascript:;">My Profile</a></li>
                            <li><a href="javascript:;">My Groups</a></li> 
                            <li class="divider"></li> -->
                            <li><a href="../Index.html" style="font-size: 14px">Logout</a></li>
                        </ul>

                    </li>
                </ul>

                <!-- <form class="navbar-form navbar-right" role="search">
                   <div class="form-group">
                     <input type="text" class="form-control input-sm search-query" placeholder="Search">
                   </div>
                 </form> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="subnavbar">

        <div class="subnavbar-inner">

            <div class="container">

                <!-- <a href="javascript:;" class="subnav-toggle" data-toggle="collapse" data-target=".subnav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>

                </a> -->


            </div>
            <!-- /container -->

        </div>
        <!-- /subnavbar-inner -->

    </div>


    <div class="main">

        <div class="container">
            <?php 

            if ($subType != "F") {
                $instruction_content = "Enter additional users below.<br /><br />";
            }
            else {
                $instruction_content = "Enter additional family members below.<br /><br />";
            }

            $instruction_content = $instruction_content . "Enter New User Details below and then select \"Create\" to create the new user account.<br /><br />";
            $instruction_content = $instruction_content . "The User Type can either be \"User\" or \"Admin\".  Only Admin users can create and delete accounts and reset user passwords.<br /><br />";
            $instruction_content = $instruction_content . "Select \"Done Adding Users\" when complete.";
            ?>
               <div class="row">
                    <div class="col-md-2">

                </div>
                <div class="col-md-8 center-block">
                    <div class="panel-group accordion">
                        <div class="panel panel-warning open" style="border-color:black">

                            <div class="panel-heading" style="text-align:center">

                                <h4 class="panel-title">

                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                                        <span style="font-size: large;color:#525252;"><i class="icon-info-sign" style="color: #525252"></i>&nbsp;<u>Instructions</u></span><span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to Toggle)</span>
                                    </a>
                                </h4>

                            </div>

                            <div id="collapseOne33" class="panel-collapse collapse in">

                                <div class="panel-body">

                                    <p style="font-size: medium">
                                        <?php echo $instruction_content; ?>
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-2">

                </div>
            </div>
            <br />

            <div class="row">


                <div class="col-md-2">

                </div>
                <!-- /span4 -->

                <div class="col-md-8">

                    <div class="widget stacked ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Add User</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">



                            <div class="tabbable">
                                <?php //echo $stroutput; ?>

                                <?php if ($can_add_more_users == false) {?>
                                  <span style="color: #fafafa;font-size: 14px;font-weight: 600;background-color: red;padding: 5px;">
                                     <u>You have reached the maximum number of users for your account and cannot add anymore.</u>
                                  </span>
                                <?php } ?>
                                <br>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <form id="editprofileform" class="editprofileform form-horizontal col-md-12">
                                            <fieldset>

                                                <div class="form-group">
                                                    <label for="newuserid" class="col-md-3">UserID</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="newuserid" name="newuserid" value="" placeholder="enter a userid">
                                                        <p class="help-block" style="color:red;">This is the value the user will use to logon.</p>
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->


                                                <div class="form-group">
                                                    <label for="firstname" class="col-md-3">First Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="firstname" name="firstname" value="" placeholder="enter a first name">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->


                                                <div class="form-group">
                                                    <label class="col-md-3" for="lastname">Last Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="lastname" name="lastname" value="" placeholder="enter a last name">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->


                                                <div class="form-group">
                                                    <label class="col-md-3" for="email">Email Address</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="email" name="email" value="" placeholder="enter an email address">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->

                                                <div class="form-group">
                                                    <label class="col-md-3" for="user_type">User Type</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" id="user_type" name="user_type" style="width:100%" 
                                                                <?php if ($subType != "F") {  ?>
                                                                onchange="toggleManageGroupDiv()"
                                                                <?php } ?>
                                                                 >
                                                            <option>User</option>
                                                            <option>Admin</option>
                                                        </select>
                                                            <p class="help-block" style="color:red;">Only Admin user's can add and delete user accounts and reset passwords.</p>
                                                    </div>
                                                    <!-- /controls -->
                                                </div>

                                                <?php if ($subType != "F") {  ?>
                                                <div class="form-group">
                                                    <label class="col-md-3" for="groups">Group</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" id="user_group" name="user_group" data-placeholder="Select group user will be a member of...">
                                                            <option value="none">&nbsp;</option>
                                                            <?php
                                                            $strSQL = "SELECT gid, gname, gmaxusers, subId FROM groups WHERE (subId =" . $subId . ") order by gname";
                                                           
                                                            $recordset = $database->query($strSQL);

                                                            foreach($recordset as $data)
                                                            {
                                                                echo ("<option value=\"". $data["gid"] . "\">". $data["gname"] . "</option>");
                                                            }
                                                            ?>
                                                        </select>
                                                        <p class="help-block" style="color:red;">Admininstators can only edit user accounts that share the same groups.</p>
                                                        <div id="manage_groups" style="display: none;">
                                                          <input type="checkbox" data-toggle="toggle" id="manage_all_groups" name="manage_all_groups" data-on="Yes" data-off="No" data-size="mini" data-onstyle="success" data-offstyle="danger" />
                                                            &nbsp;<label style="color:red;font-weight:600" for="manage_all_groups">This administrator can manage users in all groups?</label>
                                                        </div>
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <?php } ?>

                                                <hr />

                                                <div class="form-group">
                                                    <label class="col-md-3" for="password1">Password</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="password1" name="password1" value="" placeholder="enter a password">
                                                        <br />
                                                            <div id="pswd_info">
                                                                <h5 style="text-align:left">New Password must meet the following requirements:</h5>
                                                                <ul>
                                                                    <li id="letter" class="invalid">
                                                                        At least
                                                                        <strong>one letter</strong>
                                                                    </li>
                                                                    <li id="capital" class="invalid">
                                                                        At least
                                                                        <strong>one capital letter</strong>
                                                                    </li>
                                                                    <li id="number" class="invalid">
                                                                        At least
                                                                        <strong>one number</strong>
                                                                    </li>
                                                                    <li id="length" class="invalid">
                                                                        Be at least
                                                                        <strong>8 characters</strong>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->


                                                <div class="form-group">
                                                    <label class="col-md-3" for="password2">Confirm</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="password2" name="password2" value="" placeholder="confirm password">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->

                                                <br />


                                                <div class="form-group">

                                                    <div class="col-md-12 text-center btn-toolbar">
                                                        <?php if ($can_add_more_users == true) {?>
                                                          <button type="button" id="btn_add_user" class="btn btn-primary" disabled>Create</button>
                                                        <?php } ?>
                                                        <button class="btn btn-default" id="btn_cancel">Done Adding Users</button>
                                                    </div>
                                                </div>
                                                <!-- /form-actions -->
                                            </fieldset>
                                            <input type="hidden" value="<?php echo $subId?>" id="subId" name="subId" />
                                        </form>
                                    </div>

                                </div>


                            </div>





                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="col-md-2">

               <!--     <div class="well">

                        <h4>Add User</h4>

                        <p>In order to add a new user, please enter the UserID, Firstname, Lastname and Password, then click the Create Button..</p>

                    </div> -->

                </div>
                <!-- /span4 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->

    <br />
    <div class="text-center">
        <a href="../Home.php" class="btn btn-warning btn-group-md" style="color:black" role="button">Return To Home Screen</a>
    </div>
    <br />

    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?> 
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
       
 <!-- /footer -->



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="./js/libs/bootstrap-toggle.min.js"></script>
    
    <script>
	
	    $(document).ready(function() {
            /*
	        $('#user_type').multiselect({
	            buttonWidth: '100%',
	            maxHeight: 100
	        });
            */
	        //$(".chzn-select").chosen();

	    });

	    function toggleManageGroupDiv() {
	        if ($('#user_type').val() == "Admin") {
	            $('#manage_groups').show();
	        }
	        else {
	            $('#manage_groups').hide();
	        }

	        //alert(sel.value);
	        //if the user_type is admin, then add "Everyone" to the list
            //if the user_type is user, then remove "Everyone" from the list
	    }


        $('input[id=password1]').keyup(function () {
            // set password variable
            var pswd = $(this).val();

            //validate the length
            if (pswd.length < 8) {
                $('#length').removeClass('valid').addClass('invalid');
            } else {
                $('#length').removeClass('invalid').addClass('valid');
            }

            //validate letter
            if (pswd.match(/[A-z]/)) {
                $('#letter').removeClass('invalid').addClass('valid');
            } else {
                $('#letter').removeClass('valid').addClass('invalid');
            }

            //validate capital letter
            if (pswd.match(/[A-Z]/)) {
                $('#capital').removeClass('invalid').addClass('valid');
            } else {
                $('#capital').removeClass('valid').addClass('invalid');
            }

            //validate number
            if (pswd.match(/\d/)) {
                $('#number').removeClass('invalid').addClass('valid');
            } else {
                $('#number').removeClass('valid').addClass('invalid');
            }


            if ($("#number").hasClass('valid') && $("#letter").hasClass('valid') && $("#capital").hasClass('valid') && $("#length").hasClass('valid')) {

                //alert('number valid');
                $("#btn_add_user").prop('disabled', false);
            }
            else {

                $("#btn_add_user").prop('disabled', true);
            }
        })


        $('#btn_add_user').on('click', function (e) {


            if ($('#newuserid').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing UserID');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>UserID</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }


            if ($('#firstname').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing First Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>First Name</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }


            if ($('#lastname').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Last Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>Last Name</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            if ($('#email').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Email Address');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>Email Address</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }


            if ($('#password1').val() == '') {
                var modal = $('#myModal');

                modal.find('.modal-title').text('Missing Password');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>password</u> for the user.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            if ($('#password2').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Confirmation');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please <u>confirm</u> the new password.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            if ($('#password1').val() != $('#password2').val()) {
                var modal = $('#myModal');

                modal.find('.modal-title').text('Password and Confirmation don\'t match');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please make sure your <u>password</u> and <u>confirm</u> password match.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            //alert("here")
            $.ajax({
                type: "POST",
                url: "../ws/put-user.php",
                data: $('form.editprofileform').serialize(),
                success: function (msg) {

                    alert(msg);
                    //$('#sug').val(msg);
                    //$("#thanks").html(msg)
                    //$("#form-content").modal('hide');
                    //window.location.href = "home.html";					
                },
                error: function () {
                    alert("unable to create user");
                }
            });
        })


        $('#btn_cancel').on('click', function (e) {
		    window.location.href = "user-manager.php";
		    return false;
        })

    </script>

</body>
</html>
