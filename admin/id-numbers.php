<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8" />
    <title>MyEMRMyWay :: ID Numbers</title>
    <meta name="description" content="MyEMRMyWay App">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    
    <link href="css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
    
    <link href="css/base-admin-3.css" rel="stylesheet">
    <link href="css/base-admin-3-responsive.css" rel="stylesheet">


    <link href="../Content/bootstrap-dialog.min.css" rel="stylesheet" />

    <link href="css/pages/dashboard.css" rel="stylesheet">

    <link href="css/custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php


    include("../include/incConfig.php");
    session_start();

    //make sure we have a valid sesion	
	include("../include/session.php");
	
    $filter_text = "( not filtered )";

    if (isset($_POST["current_page"])) {
        $current_page = $_POST["current_page"];
    }else{  
        $current_page=1;
    }
    

    if (isset($_POST["page_filter"])) {
        $filter = $_POST["page_filter"];
    }else{  
        $filter="";
    }

    $where = "where uid=" . $uid;
    
    ?>
	
<style>
	@media (max-width: 768px) { /* use the max to specify at each container level */
		.widget .widget-content {
			padding: 10px 10px;
			background: #FFF;
			border: 1px solid #D5D5D5;
			-moz-border-radius: 5px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
		}
		
		.col-md-12 {
			padding-right:1px !important;
			padding-left:1px !important;		
		}

		.col-xs-12 {
			padding-right:4px !important;
			padding-left:4px !important;		
		}
		
		.well-sm {
			padding-right:4px !important;
			padding-left:4px !important;	
		}	
		
		h2, .h2 {
			font-size: 26px  !important;
		}		
	}
</style>

</head>

<body>

    <?php 

    $PageSize = 10;

    $MaxPages = 0;

    $strSQLCount = "SELECT Count(uid) as REC_COUNT ";
    $strSQLCount = $strSQLCount . "FROM [user_ids] " . $where;

    $recordset = $database->query($strSQLCount);
    foreach($recordset as $data)
    {
        $count = $data["REC_COUNT"];
    }

    $MaxPages = ceil($count / $PageSize);

    $strSQL = "SELECT uid,idnumber,description ";
    $strSQL = $strSQL . "FROM [user_ids] " . $where . " ";
    $strSQL = $strSQL . "ORDER BY description ";

	//navbar
	include("../include/navbar.php");

    ?>

    <div class="well well-sm">
        <div class="text-center">
            <h2>
                <img src="../img/drivers-license-circle.png" class="img-circle" alt="profile" width="64" height="64" style="border: solid; border-width: medium">
                ID Numbers for <?php echo $firstname . " " . $lastname ?></h2>
        </div>
        <!-- End container -->
    </div>

    <div class="main">

        <div class="container">
            <?php 
            $instruction_content = "Enter your Identification Numbers below.<br /><br />";
            $instruction_content = $instruction_content . "Primarily, you need to enter your driver's license number or State id card number.<br /><br />";
            $instruction_content = $instruction_content . "If you don&apos;t have a driver's license or state id card, then you may want to use a medicaid id or other health card id number.<br /><br />";
            $instruction_content = $instruction_content . "1st responders can use this information to locate your emergency medical information and emergency contacts.";
            include("../include/incInstructions.php");
            ?>

            <div class="row">

                <div class="col-md-12">

                    <!--    <div class="well">

                        <h4>Manage Users</h4>

                        <p>Select an Action ...</p>

                    </div> -->

                    <div class="widget stacked ">

                        <div class="widget-header" style="vertical-align: middle">
                            <h3>ID Numbers</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content text-center">
                            <div class="text-center" style="margin: 0 auto; font-size: 16px" id="user-div">
                                <table class="table table-bordered table-hover table-striped table-responsive" id="user-table">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center" style="font-size: 18px">Current</th> -->
                                            <th class="text-center" style="font-size: 18px">Action</th>
                                            <th class="text-left" style="font-size: 18px">ID Number</th>
                                            <th class="text-left" style="font-size: 18px">Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //echo $strSQL;
                                        $recordset = $database->query($strSQL);
                                        foreach($recordset as $data)
                                        {
                                            $uid = $data["uid"];
                                            $idnumber = $data["idnumber"];
                                            $desc = $data["description"];

                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default btn-md dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" style="font-size: 16px" class="delete_id" id="<?php echo $idnumber; ?>">Delete ID</a></li>
                                                        <li><a href="#" style="font-size: 16px" class="qrcode_id" id="<?php echo 'qr'.$idnumber; ?>">Print QR Codes</a></li>														
                                                    </ul>
                                                </div>
                                                <!-- <button type="button" class="btn-md btn-success">Manage</button>&nbsp;&nbsp; -->
                                            </td>
                                            <td class="text-left" style="font-size: 16px"><?php echo $idnumber;?></td>
                                            <td class="text-left" style="font-size: 16px"><?php echo $desc;?></td>
                                        </tr>
                                        <?php
                                        }   
                                        ?>
										<tr>
										  <td colspan="3">
											<div class="btn-toolbar" style="margin: 0 auto;">
												<button id="btn_add_idnumber" class="btn btn-md btn-success <?php if ($subType != "F") { ?>pull-left<?php } ?>">Add ID Number</button>
											</div>
										  </td>
										</tr>										
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>


            </div>

            <div class="row hidden-xs">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 center-block">

                    <div class="well">
                        <h2>Register Your ID Cards!</h2>
                        <p>You should register your driver's license or other government issued identification card, which will enable Emergency Responders to access your Emergency Information.</p>

                        <p>You or another authorized family member can use the "Secure Link" technology to email your information to a provider, which provides a link that expires in 24-hours.</p>
                        <br />
                        <center>
                        <img src="../img/id-cards.jpg" class="img-responsive img-rounded" alt="id cards" border="5">
                            </center>
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>

        </div>
        <div id="addID" class="modal fade" role="dialog">
            <form id="add_id_form" class="add_id_form">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="ch_id_modal_title">Add ID</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="id_number">ID Number</label>
                                <input type="text" class="form-control input-md" id="id_number" name="id_number" placeholder="enter id number" value="" />
                                <small id="emailHelp" class="form-text text-muted">examples:<br />-Government Issued Id
                                                                                            <br />-Driver's license
                                                                                            <br />-Healthcard Provider</small>
                            </div>

                            <div class="form-group">
                                <label for="id_description">Description / ID Type</label>
                                <input type="text" class="form-control input-md" id="id_description" name="id_description" placeholder="enter a description for this id card" value="" />
                                <small id="emailHelp" class="form-text text-muted">
                                    examples:
                                    <br />-Veterans Id
                                    <br />-Florida Driver's License
                                    <br />-Medicaid Id
                                </small>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" id="btn_save">Save</button>
                            <br />
                            <br />
                            <div>
                                <div class="alert alert-success hide text-center" role="alert" id="id-success">ID added successfully.</div>
                                <div class="alert alert-danger hide text-center" role="alert" id="id-number-required">An ID number is required.</div>
                                <div class="alert alert-danger hide text-center" role="alert" id="id-description-required">A description is required.</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <input type="hidden" id="id_uid" name="id_uid" value="<?php echo $uid ?>" />
                    </div>
                </div>
            </form>
        </div>


    </div>
    <div class="text-center">
        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">Return To Home Screen</a>
    </div>
    <br />
    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?>



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="../Scripts/bootstrap-dialog.min.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $.wait = function (callback, seconds) {
            return window.setTimeout(callback, seconds * 1000);
        }

        $(document).ready(function () {

            //####################################################### Add ID Number button  ##################################
            $('#btn_add_idnumber').on('click', function (e) {
                e.preventDefault();
                //alert("test");
                // alert($(this).attr('id'));
                //var user_id_to_change = $(this).attr('id');
                //$("#id_uid").val(user_id_to_change);
                //alert($("#id_uid").val());

                //alert($(this).attr('id'));
                $("#id_number").val("");
                $("#id_description").val("");

                $('#addID').modal('show');

            })

/*

                $.ajax({
                    type: "POST",
                    url: "process_id_request.php",
                    data: $('form.signin-form').serialize(),
                    success: function (msg) {
                        //alert("Message: " + msg);
                        //alert(msg);
                        //$('#sug').val(msg);
                        //$("#thanks").html(msg)
                        //$("#form-content").modal('hide');
                        if (msg != "") {
                            //alert("Report Found.  Click OK to view it.");
                            window.location.href = "https://emtelink.com/tracker/SecureEmergencyInfo.php?id=" + msg;
                        }
                        else {
							//alert("A Report for the ID you entered was not found, please try again or contact our support line at 888-000-3333.")
                            $('#id-notfound').removeClass('hide');
                            //$.wait(function () { $('#id-notfound').addClass('hide') }, 2);
                        }
                    },
                    error: function (msg) {
					    alert(msg);
                        alert("An error occured processing your report attempt.\nPlease try again.");
                    }
                });
            });
        });

*/
            //################################################### Delete button ################################################
            $(document).on("click", ".delete_id", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var post_data = $(this).attr('id'); //id number to delete
                //alert(post_data);
                //exit;

                BootstrapDialog.confirm({
                    title: 'Confirmation',
                    message: "Are you sure you want to delete this id number?",
                    type: BootstrapDialog.TYPE_SUCCESS, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                    closable: false, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelLabel: 'No', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Yes', // <-- Default value is 'OK',
                    btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                    btnCancelClass: 'btn-danger',
                    animate: false,

                    callback: function (result) {
                        // result will be true if button was click, while it will be false if users close the dialog directly.
                        if (result) {
                            $.ajax({
                                type: "POST",
                                url: "../ws/delete-id-number.php",
                                data: { "idnumber": post_data },
                                success: function (msg) {
                                    //alert(msg);
                                    //alert(strcmp(msg,"success"));
                                    if (strcmp(msg, "success")) {
                                        BootstrapDialog.alert('ID deleted successfully.', function () {
                                            location.reload();
                                        });
                                    }
                                    else {
                                        alert(msg);
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    alert("An processing error occured.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                                }
                            });
                        } else {
                            //alert('Nope.');
                        }
                    }
                });


            });
			

            function strcmp(str1, str2) {
                return str1.localeCompare(str2) / Math.abs(str1.localeCompare(str2));
            }

            //################################################### qrcode button ################################################
            $(document).on("click", ".qrcode_id", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var post_data = $(this).attr('id').substring(2); //id number to use to generate qr code
                //alert(post_data);
                //exit;

				$.ajax({
                    type: "POST",
                    url: "../../mobile/process_id_request_number.php",
                    data: { "idnumber": post_data },
                    success: function (msg) {
                        //alert("Message: " + msg);
                        if (msg != "") {
                            //alert("Report Found.  Click OK to view it.");
                            window.open("https://emtelink.com/tracker/printqr.php?idnum=" + msg);
                        }
                        else {
                            alert("Message: Not Found");
                        }
                    },
                    error: function (msg) {
					    alert(msg);
                        alert("An error occured processing your report attempt.\nPlease try again.");
                    }
                });
            });
	

            //####################################################### Change Password Button #######################################
            $(document).on("click", ".reset_pw", function (e) {
                e.preventDefault();
                alert($(this).attr('id'));
                var user_id_to_change = $(this).attr('id');
                $("#chg_pw_userid").val(user_id_to_change);

                //alert($(this).attr('id'));
                $("#new_pw").val("");
                $("#confirm_pw").val("");

                $('#chgPassword').modal('show');

            })

            $('#btn_save').on('click', function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));  //make sure the id number is not empty
                if ($("#id_number").val().length < 1) {
                    $('#id-number-required').removeClass('hide');
                    $('#id_number').focus();
                    $.wait(function () { $('#id-number-required').addClass('hide') }, 3);
                    //alert("Please enter a number.");
                    exit;
                }
                if ($("#id_description").val().length < 1) {  // check for empty description
                    $('#id-description-required').removeClass('hide');
                    $('#id_description').focus();
                    $.wait(function () { $('#id-description-required').addClass('hide') }, 3);
                    //alert("Please enter a description.");
                    exit;
                }
                else {
                    //alert("values Not empty");
                    var new_id = $("#id_number").val();
                    var new_desc = $("#id_description").val();
                    var id_uid = $("#id_uid").val();

                    $.ajax({
                        type: "POST",
                        url: "../ws/put-id-number.php",
                        data: { "uid": id_uid, "id_number": new_id, "id_description": new_desc },
                        success: function (msg) {

                            if (msg == "success") {
                                //alert(msg);
                                $('#id-success').text("new id created successfully.");
                                $('#id-success').removeClass('hide');
                                $.wait(function () { $('#id-success').addClass('hide'); $('#addID').modal('hide'); }, 2);
                                location.reload();
                            }
                            else {
                                //alert(msg);
                                $('#id-success').html(msg);
                                $('#id-success').removeClass('alert-success');
                                $('#id-success').addClass('alert-danger');
                                $('#id-success').removeClass('hide');
                                $.wait(function () { $('#id-success').addClass('hide'); }, 4);
                                $('#id-success').removeClass('alert-danger');
                                $('#id-success').addClass('alert-success');
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("An processing error occured.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                        }
                    });
                }
            })


            //####################################################### Mail Report #######################################
            $(document).on("click", ".email_rpt", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var user_id_to_email = $(this).attr('id');
                $("#email_userid").val(user_id_to_email);

                //alert($(this).attr('id'));
                //blank out the fields
                $("#recp_name").val("");
                $("#recp_email").val("");
                $("#email_subject").val("");
                $("#email_message").val("");

                //alert($("#email_userid").val());

                $('#mailReport').modal('show');

            })

            $('#btn_send_email').on('click', function (e) {
                e.preventDefault();

                //alert($("#recp_email").val().indexOf("@"));
                //return;

                if ($('#recp_email').val() == '') {
                    $('#email-fail').removeClass('hide')
                    $.wait(function () { $('#email-fail').addClass('hide'); }, 2);
                    return;
                }

                //make sure email address has @ in it

                if ($("#recp_email").val().indexOf("@") == -1) {
                    $('#email-address-not-valid').removeClass('hide')
                    $.wait(function () { $('#email-address-not-valid').addClass('hide'); }, 2);
                    return;
                }

                $('#busy').removeClass('hide');
                //alert("about to send email");
                var user_id_to_email = $("#email_userid").val();
                $.ajax({
                    type: "POST",
                    url: "email.php",
                    //data: { "uid": user_id_to_email },
                    data: $('form.mail_report_form').serialize(),
                    success: function (msg) {
                        //alert(msg);
                        $('#busy').addClass('hide');
                        $('#email-success').removeClass('hide');
                        $.wait(function () { $('#email-success').addClass('hide'); $('#mailReport').modal('hide'); }, 2);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An processing error occured while attempting to email report.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                    }
                });



            })


            //####################################################### Notify Contacts #######################################
            $(document).on("click", ".notify_contacts", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var user_id_to_notify_about = $(this).attr('id');
                $("#notify_userid").val(user_id_to_notify_about);

                //alert($(this).attr('id'));
                //blank out the fields
                //$("#notify_message").val("");

                //alert($("#email_userid").val());

                $('#mailContact').modal('show');

            })

            $('#btn_send_notify').on('click', function (e) {
                e.preventDefault();

                //alert($("#recp_email").val().indexOf("@"));
                //return;

                //if ($('#recp_email').val() == '') {
                //    $('#email-fail').removeClass('hide')
                //    $.wait(function () { $('#email-fail').addClass('hide'); }, 2);
                //    return;
                //}

                //make sure email address has @ in it

                //if ($("#recp_email").val().indexOf("@") == -1) {
                //    $('#email-address-not-valid').removeClass('hide')
                //    $.wait(function () { $('#email-address-not-valid').addClass('hide'); }, 2);
                //    return;
                // }

                $('#busy_notify').removeClass('hide');
                //alert("about to send email");
                var user_id_to_email = $("#email_userid").val();
                $.ajax({
                    type: "POST",
                    url: "ws-voicecall.php",
                    //data: { "uid": user_id_to_email },
                    data: $('form.mail_contact_form').serialize(),
                    success: function (msg) {
                        //alert(msg);
                        $('#busy_notify').addClass('hide');
                        $('#notify-success').removeClass('hide');
                        $.wait(function () { $('#notify-success').addClass('hide'); }, 2);
                        //$.wait(function () { $('#notify-success').addClass('hide'); $('#mailContact').modal('hide'); }, 2);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#busy_notify').addClass('hide');
                        $('#notify-fail').removeClass('hide')
                        $.wait(function () { $('#notify-fail').addClass('hide'); }, 2);
                        alert("An processing error occured while attempting to notify contacts.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                    }
                });



            })


        }) //document ready


    </script>
</body>
</html>
