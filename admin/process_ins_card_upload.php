<?php

include("../include/incConfig.php");
include("../include/incFunctions.php");

use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';
Urlcrypt::$key = $mykey;

session_start();

//make sure we have a valid sesion
include("../include/session.php");

$id_value =  htmlspecialchars($_POST["hidden"]);
$decrypted = Urlcrypt::decrypt($id_value);
$mode = "";
$card_side =  htmlspecialchars($_POST["card_side"]);

list($companyid, $mode, $starttime) = explode("|", $decrypted);

//make sure the upload directory exists
makeDir ("C:\\inetpub\\wwwroot\\emtelink\\new\\Uploads\\" . $subId . "\\");

$target_dir = "C:\\inetpub\\wwwroot\\emtelink\\new\\Uploads\\" . $subId . "\\";

//var_dump($_FILES);
//var_dump($_REQUEST);

$file_size = $_POST["file_size"];
$file_ext_type = $_POST["file_ext_type"];
$mime_type = $_POST["mime_type"];

if ($card_side == "front") {

    $target_file = $target_dir . basename($_FILES["fileToUpload_front"]["name"]);
    $target_filename_only = basename($_FILES["fileToUpload_front"]["name"]);
    $file_name = basename( $_FILES["fileToUpload_front"]["name"]);

    //echo $target_filename_only;
    $uploadOk = 1;
    $imageFileType = $file_ext_type;
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload_front"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        //echo "\n\nDeleting existing file.";
        unlink($target_file);
        $uploadOk = 1;
    }
    // Check file size, must be less than 5mb
    if ($_FILES["fileToUpload_front"]["size"] > 5242880) {
        echo "\n\nSorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        echo "\n\nSorry, only, jpg, jpeg, png, gif files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        //echo "\n\nSorry, your file was not uploaded.";
        // if everything is ok, try to upload file
    } else {


        switch(strtolower($_FILES['fileToUpload_front']['type']))
        {
            case 'image/jpg':
                $image = imagecreatefromjpeg($_FILES['fileToUpload_front']['tmp_name']);
                break;
            case 'image/jpeg':
                $image = imagecreatefromjpeg($_FILES['fileToUpload_front']['tmp_name']);
                break;
            case 'image/png':
                $image = imagecreatefrompng($_FILES['fileToUpload_front']['tmp_name']);
                break;
            case 'image/gif':
                $image = imagecreatefromgif($_FILES['fileToUpload_front']['tmp_name']);
                break;
            default:
                exit('Unsupported type: '.$_FILES['fileToUpload_front']['type']);
        }

        // Target dimensions
        $max_width = 312;
        $max_height = 194;

        // Get current dimensions
        $old_width  = imagesx($image);
        $old_height = imagesy($image);

        // Calculate the scaling we need to do to fit the image inside our frame
        $scale      = min($max_width/$old_width, $max_height/$old_height);

        // Get the new dimensions
        $new_width  = ceil($scale*$old_width);
        $new_height = ceil($scale*$old_height);

        // Create new empty image
        $new = imagecreatetruecolor($new_width, $new_height);

        // Resize old image into new
        imagecopyresampled($new, $image,
            0, 0, 0, 0,
            $new_width, $new_height, $old_width, $old_height);

        $base_file_name_to_save = $target_dir . "ins_card_" . $companyid . "_front." . $file_ext_type;
        //echo "filenametosave:" . $base_file_name_to_save;
        //exit;

        unlink($target_dir . "ins_card_" . $companyid . "_front.jpg");
        unlink($target_dir . "ins_card_" . $companyid . "_front.jpeg");
        unlink($target_dir . "ins_card_" . $companyid . "_front.png");
        unlink($target_dir . "ins_card_" . $companyid . "_front.gif");

        imagejpeg($new, $base_file_name_to_save);

        // Catch the imagedata
        //ob_start();
        //imagejpeg($new, NULL, 90);
        //$data = ob_get_clean();

        // Destroy resources
        imagedestroy($image);
        imagedestroy($new);

        echo "The file has been uploaded.";

        //if (move_uploaded_file($new, $base_file_name_to_save)) {
        //    //insert document record into the database
        //    //$database->insert("user_docs", [
        //    //   "uid" => $uid,
        //    //   "doc_name" => $docnameInput,
        //    //   "doc_description" => $docdescInput,
        //    //   "file_name" => $file_name,
        //    //   "file_size" => $file_size,
        //    //   "file_type" => $file_ext_type,
        //    //   "mime_type" => $mime_type,
        //    //  "status" => "uploaded by user"
        //    // //"createdate" => "uploaded by user"
        //    //]);
        //    //echo "The file ". basename( $_FILES["fileToUpload_front"]["name"]). " has been uploaded." . " - file_ext_type:" . $file_ext_type;
        //    echo "The file has been uploaded.";

        //} else {
        //    echo "Sorry, there was an error uploading your file.";
        //}
    }
}


if ($card_side == "back") {

    $target_file = $target_dir . basename($_FILES["fileToUpload_back"]["name"]);
    $target_filename_only = basename($_FILES["fileToUpload_back"]["name"]);
    $file_name = basename( $_FILES["fileToUpload_back"]["name"]);

    //echo $target_filename_only;
    $uploadOk = 1;
    $imageFileType = $file_ext_type;
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload_back"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        //echo "\n\nDeleting existing file.";
        unlink($target_file);
        $uploadOk = 1;
    }
    // Check file size, must be less than 5mb
    if ($_FILES["fileToUpload_back"]["size"] > 5242880) {
        echo "\n\nSorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        echo "\n\nSorry, only, jpg, jpeg, png, gif files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        //echo "\n\nSorry, your file was not uploaded.";
        // if everything is ok, try to upload file
    } else {


        switch(strtolower($_FILES['fileToUpload_back']['type']))
        {
            case 'image/jpg':
                $image = imagecreatefromjpeg($_FILES['fileToUpload_back']['tmp_name']);
                break;
            case 'image/jpeg':
                $image = imagecreatefromjpeg($_FILES['fileToUpload_back']['tmp_name']);
                break;
            case 'image/png':
                $image = imagecreatefrompng($_FILES['fileToUpload_back']['tmp_name']);
                break;
            case 'image/gif':
                $image = imagecreatefromgif($_FILES['fileToUpload_back']['tmp_name']);
                break;
            default:
                exit('Unsupported type: '.$_FILES['fileToUpload_back']['type']);
        }

        // Target dimensions
        $max_width = 312;
        $max_height = 194;

        // Get current dimensions
        $old_width  = imagesx($image);
        $old_height = imagesy($image);

        // Calculate the scaling we need to do to fit the image inside our frame
        $scale      = min($max_width/$old_width, $max_height/$old_height);

        // Get the new dimensions
        $new_width  = ceil($scale*$old_width);
        $new_height = ceil($scale*$old_height);

        // Create new empty image
        $new = imagecreatetruecolor($new_width, $new_height);

        // Resize old image into new
        imagecopyresampled($new, $image,
            0, 0, 0, 0,
            $new_width, $new_height, $old_width, $old_height);

        $base_file_name_to_save = $target_dir . "ins_card_" . $companyid . "_back." . $file_ext_type;
        //echo "filenametosave:" . $base_file_name_to_save;
        //exit;

        unlink($target_dir . "ins_card_" . $companyid . "_back.jpg");
        unlink($target_dir . "ins_card_" . $companyid . "_back.jpeg");
        unlink($target_dir . "ins_card_" . $companyid . "_back.png");
        unlink($target_dir . "ins_card_" . $companyid . "_back.gif");

        imagejpeg($new, $base_file_name_to_save);

        // Catch the imagedata
        //ob_start();
        //imagejpeg($new, NULL, 90);
        //$data = ob_get_clean();

        // Destroy resources
        imagedestroy($image);
        imagedestroy($new);

        echo "The file has been uploaded.";

    }
}

?>