<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8" />
    <title>MyEMRMyWay :: Emergency Contacts</title>
    <meta name="description" content="MyEMRMyWay App">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">

    <!-- <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet"> -->

    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">

    <link href="./css/pages/dashboard.css" rel="stylesheet">

    <link href="./css/custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="./css/bootstrap-toggle.min.css" rel="stylesheet">

    <?php

    include("../include/incConfig.php");
    session_start();

    ?>
    <style>

ul, li {
    margin:0;
    padding:0px;
    list-style-type:none;
    text-align:left;
}

#pswd_info {
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
}

#pswd_info h5 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}


.invalid {
    background:url(../../images/redx.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    background:url(../../images/green-ck.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}


    </style>
</head>

<body>

    <?php 

    //make sure we have a valid sesion
	include("include/session.php");
	
	include("../include/navbar.php");	

    ?>

    <div class="main">
        <br /><br />
        <div class="container">
            <?php 

            $instruction_content = "Enter additional emergency contacts below.<br /><br />";
            $instruction_content = $instruction_content . "Enter New Emergency Contact Details below and then select \"Create\" to create the new emergency contact.<br /><br />";
            $instruction_content = $instruction_content . "Select \"Return to Profile\" when complete.";
            ?>
               <div class="row">
                    <div class="col-md-2">

                </div>
                <div class="col-md-8 center-block">
                    <div class="panel-group accordion">
                        <div class="panel panel-warning" style="border-color:black">

                            <div class="panel-heading" style="text-align:center">

                                <h4 class="panel-title">

                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                                        <span style="font-size: large;color:#525252;"><i class="icon-info-sign" style="color: #525252"></i>&nbsp;<u>Instructions</u></span><span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to Toggle)</span>
                                    </a>
                                </h4>

                            </div>

                            <div id="collapseOne33" class="panel-collapse collapse">

                                <div class="panel-body">

                                    <p style="font-size: medium">
                                        <?php echo $instruction_content; ?>
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-2">

                </div>
            </div>
            <br />

            <div class="row">


                <div class="col-md-2">

                </div>
                <!-- /span4 -->

                <div class="col-md-8">

                    <div class="widget stacked ">

                        <div class="widget-header">
                            <i class="icon-book"></i>
                            <h3>Add Emergency Contact</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">



                            <div class="tabbable">

                                <br>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <form id="editprofileform" class="editprofileform form-horizontal col-md-12">
                                            <fieldset>
                                                <!-- name -->
                                                <div class="form-group">
                                                    <label for="contacts_full_name" class="col-md-3">Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="contacts_full_name" name="contacts_full_name" placeholder="enter contacts full name">
                                                    </div>
                                                </div>

                                                <!-- address -->
                                                <div class="form-group">
                                                    <label for="contacts_address" class="col-md-3">Address</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="contacts_address" name="contacts_address" placeholder="enter street address or p.o. box">
                                                    </div>
                                                </div>
												
                                                <!-- city -->
                                                <div class="form-group">
                                                    <label for="contacts_city" class="col-md-3">City</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="contacts_city" name="contacts_city" placeholder="enter city">
                                                    </div>
                                                </div>

                                                <!-- state -->
                                                <div class="form-group">
                                                    <label for="contacts_state" class="col-md-3">State</label>
                                                    <div class="col-md-9">
														  <select class="form-control" id="contacts_state" name="contacts_state" placeholder="Select state">
															<option value="">Select a state</option>
															<option value="AL">Alabama</option>
															<option value="AK">Alaska</option>
															<option value="AZ">Arizona</option>
															<option value="AR">Arkansas</option>
															<option value="CA">California</option>
															<option value="CO">Colorado</option>
															<option value="CT">Connecticut</option>
															<option value="DE">Delaware</option>
															<option value="DC">District Of Columbia</option>
															<option value="FL">Florida</option>
															<option value="GA">Georgia</option>
															<option value="HI">Hawaii</option>
															<option value="ID">Idaho</option>
															<option value="IL">Illinois</option>
															<option value="IN">Indiana</option>
															<option value="IA">Iowa</option>
															<option value="KS">Kansas</option>
															<option value="KY">Kentucky</option>
															<option value="LA">Louisiana</option>
															<option value="ME">Maine</option>
															<option value="MD">Maryland</option>
															<option value="MA">Massachusetts</option>
															<option value="MI">Michigan</option>
															<option value="MN">Minnesota</option>
															<option value="MS">Mississippi</option>
															<option value="MO">Missouri</option>
															<option value="MT">Montana</option>
															<option value="NE">Nebraska</option>
															<option value="NV">Nevada</option>
															<option value="NH">New Hampshire</option>
															<option value="NJ">New Jersey</option>
															<option value="NM">New Mexico</option>
															<option value="NY">New York</option>
															<option value="NC">North Carolina</option>
															<option value="ND">North Dakota</option>
															<option value="OH">Ohio</option>
															<option value="OK">Oklahoma</option>
															<option value="OR">Oregon</option>
															<option value="PA">Pennsylvania</option>
															<option value="RI">Rhode Island</option>
															<option value="SC">South Carolina</option>
															<option value="SD">South Dakota</option>
															<option value="TN">Tennessee</option>
															<option value="TX">Texas</option>
															<option value="UT">Utah</option>
															<option value="VT">Vermont</option>
															<option value="VA">Virginia</option>
															<option value="WA">Washington</option>
															<option value="WV">West Virginia</option>
															<option value="WI">Wisconsin</option>
															<option value="WY">Wyoming</option>
														</select>													
                                                    </div>
                                                </div>

												<!-- zip -->
                                                <div class="form-group">
                                                    <label for="contacts_address" class="col-md-3">Zip</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="contacts_zip" name="contacts_zip" placeholder="enter zip code">
                                                    </div>
                                                </div>

									            <!-- phone -->
                                                <div class="form-group">
                                                    <label class="col-md-3" for="contacts_phone">Phone Number</label>
                                                    <div class="col-md-9">
                                                      <input type="text" class="form-control" id="contacts_phone" name="contacts_phone" placeholder="enter a phone number">
                                                    </div>
                                                </div>	
												
									            <!-- email -->
                                                <div class="form-group">
                                                    <label class="col-md-3" for="contacts_email">Email</label>
                                                    <div class="col-md-9">
                                                      <input type="text" class="form-control" id="contacts_email" name="contacts_email" placeholder="enter an email address">
                                                    </div>
                                                </div>													

                                                <div class="form-group">

                                                    <div class="col-md-12 text-center btn-toolbar">
                                                        <button type="button" id="btn_add_contact" class="btn btn-primary" disabled>Create</button>
                                                        <button class="btn btn-default" id="btn_cancel">Return to Profile</button>
                                                    </div>
                                                </div>
                                                <!-- /form-actions -->
                                            </fieldset>
                                            <input type="hidden" value="<?php echo $subId?>" id="subId" name="subId" />
                                        </form>
                                    </div>

                                </div>


                            </div>





                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="col-md-2">

               <!--     <div class="well">

                        <h4>Add User</h4>

                        <p>In order to add a new user, please enter the UserID, Firstname, Lastname and Password, then click the Create Button..</p>

                    </div> -->

                </div>
                <!-- /span4 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->

    <br />
    <div class="text-center">
        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">Return To Home Screen</a>
    </div>
    <br />

    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?> 
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
       
 <!-- /footer -->



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="./js/libs/bootstrap-toggle.min.js"></script>
    
    <script>
	
	    $(document).ready(function() {
            /*
	        $('#user_type').multiselect({
	            buttonWidth: '100%',
	            maxHeight: 100
	        });
            */
	        //$(".chzn-select").chosen();

	    });

	    function toggleManageGroupDiv() {
	        if ($('#user_type').val() == "Admin") {
	            $('#manage_groups').show();
	        }
	        else {
	            $('#manage_groups').hide();
	        }

	        //alert(sel.value);
	        //if the user_type is admin, then add "Everyone" to the list
            //if the user_type is user, then remove "Everyone" from the list
	    }


        $('input[id=password1]').keyup(function () {
            // set password variable
            var pswd = $(this).val();

            //validate the length
            if (pswd.length < 8) {
                $('#length').removeClass('valid').addClass('invalid');
            } else {
                $('#length').removeClass('invalid').addClass('valid');
            }

            //validate letter
            if (pswd.match(/[A-z]/)) {
                $('#letter').removeClass('invalid').addClass('valid');
            } else {
                $('#letter').removeClass('valid').addClass('invalid');
            }

            //validate capital letter
            if (pswd.match(/[A-Z]/)) {
                $('#capital').removeClass('invalid').addClass('valid');
            } else {
                $('#capital').removeClass('valid').addClass('invalid');
            }

            //validate number
            if (pswd.match(/\d/)) {
                $('#number').removeClass('invalid').addClass('valid');
            } else {
                $('#number').removeClass('valid').addClass('invalid');
            }


            if ($("#number").hasClass('valid') && $("#letter").hasClass('valid') && $("#capital").hasClass('valid') && $("#length").hasClass('valid')) {

                //alert('number valid');
                $("#btn_add_contact").prop('disabled', false);
            }
            else {

                $("#btn_add_contact").prop('disabled', true);
            }
        })


        $('#btn_add_contact').on('click', function (e) {


            if ($('#newuserid').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing UserID');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>UserID</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }


            if ($('#firstname').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing First Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>First Name</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }


            if ($('#lastname').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Last Name');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>Last Name</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            if ($('#email').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Email Address');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>Email Address</u>.</h5></span>');
                $('#myModal').modal('show');
                return;
            }


            if ($('#password1').val() == '') {
                var modal = $('#myModal');

                modal.find('.modal-title').text('Missing Password');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter a <u>password</u> for the user.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            if ($('#password2').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing Confirmation');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please <u>confirm</u> the new password.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            if ($('#password1').val() != $('#password2').val()) {
                var modal = $('#myModal');

                modal.find('.modal-title').text('Password and Confirmation don\'t match');
                modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please make sure your <u>password</u> and <u>confirm</u> password match.</h5></span>');
                $('#myModal').modal('show');
                return;
            }

            //alert("here")
            $.ajax({
                type: "POST",
                url: "../ws/put-user.php",
                data: $('form.editprofileform').serialize(),
                success: function (msg) {

                    alert(msg);
                    //$('#sug').val(msg);
                    //$("#thanks").html(msg)
                    //$("#form-content").modal('hide');
                    //window.location.href = "home.html";					
                },
                error: function () {
                    alert("unable to create user");
                }
            });
        })


        $('#btn_cancel').on('click', function (e) {
             e.preventDefault();
		     window.location.href = "../info.php#emergency_contacts";
        })

    </script>

</body>
</html>
