<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <title>EMTeLink Tracker :: Group Manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="description" content="EMTeLink Tracker App">

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">
    
    <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
    
    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">


    <link href="../Content/bootstrap-dialog.min.css" rel="stylesheet" />

    <link href="./css/pages/dashboard.css" rel="stylesheet">

    <link href="./css/custom.css" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php


    include("../include/incConfig.php");
    session_start();

    $filter_text = "( not filtered )";

    //make sure we have a valid sesion
    if ($_SESSION["valid"] != "TRUE")
    {
        header("Location: index.html");
    };

    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];
    $admin_user = $_SESSION["admin_user"];
    $subType = $_SESSION["subType"];
    $subMaxUsers = $_SESSION["subMaxUsers"];
    $subId = $_SESSION["subId"];

    $current_page=1;
  
   
    ?>

</head>

<body>

    <?php

    if ($subType == "F") {
      $PageSize = 10;
    }
    else {
      $PageSize = 10;
    }

    $MaxPages = 0;

    $where = "where subID=" . $subId;


    $strSQL = "SELECT gid,gname ";
    $strSQL = $strSQL . "FROM [groups] where subID=" . $subId . " ";
    $strSQL = $strSQL . "ORDER BY gname ";

    ?>

    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                </button>
                <a class="navbar-brand" href="./index.html">EMTeLink Tracker</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu">
                            <!--<li><a href="./account.html">Account Settings</a></li>
                             <li><a href="javascript:;">Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:;">Help</a></li> -->
                        </ul>

                    </li>

                    <li><a href="../home.php" style="font-size: 16px"><i class="icon-home"></i>&nbsp;Home <?php //echo $strSQL ?></a></li>
                    <?php if ($subType != "F") {  ?>
                    <li><a href="index.php" style="font-size: 16px"><i class="icon-dashboard"></i>&nbsp;Dashboard</a></li>
                    <?php }  ?>

                    <li class="dropdown">

                        <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px">
                            <i class="icon-user"></i>
                            <?php echo  $_SESSION["loggedin_fname"] . " " .  $_SESSION["loggedin_lname"] ?>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <!--  <li><a href="javascript:;">My Profile</a></li>
                            <li><a href="javascript:;">My Groups</a></li> 
                            <li class="divider"></li> -->
                            <li><a href="../Index.html" style="font-size: 14px">Logout</a></li>
                        </ul>

                    </li>
                </ul>

                <!-- <form class="navbar-form navbar-right" role="search">
                   <div class="form-group">
                     <input type="text" class="form-control input-sm search-query" placeholder="Search">
                   </div>
                 </form> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    
    <!-- /subnavbar -->

        <div class="well well-sm">
        <div class="text-center">
          <h2><i class="icon-group" style="color:#F99500;"></i> Manage Groups</h2>
        </div>
        <!-- End container -->
    </div>



    <div class="main">

        <div class="container">
            <?php 
            $instruction_content = "Manage your groups below.<br /><br />";
            $instruction_content = $instruction_content . "Use the \"Add New Group\" button to add groups.<br /><br />";
            $instruction_content = $instruction_content . "Click on the Action button to see additional actions : Modify, Delete";
            include("../include/incInstructions.php");
            ?>
            <br />
            <div class="row">

                <div class="col-md-12">

                    <!--    <div class="well">

                        <h4>Manage Users</h4>

                        <p>Select an Action ...</p>

                    </div> -->

                    <div class="widget stacked ">

                        <div class="widget-header" style="vertical-align: middle">
                            <i class="icon-group" style="color:#F99500;"></i>
                              <h3>Manage Groups</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content text-center">


                            <div class="table-responsive text-center" style="width: 95%; margin: 0 auto; font-size: 16px" id="user-div">
                                <table class="table table-bordered table-hover table-striped table-responsive" id="user-table">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center" style="font-size: 18px">Current</th> -->
                                            <th class="text-center" style="font-size: 18px;width:15%;">Action</th>
                                            <th class="text-left" style="display:none;">Group ID</th>
                                            <th class="text-left" style="font-size: 18px">Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //echo $strSQL;
                                        $recordset = $database->query($strSQL);
                                        foreach($recordset as $data)
                                        {
                                            $groupid = $data["gid"];
                                            $groupname = $data["gname"];

                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <button class="btn btn-default btn-md dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="#" style="font-size: 16px" class="delete_group" id="<?php echo $groupid; ?>">Delete Group</a></li>
                                                        <li>
                                                            <a href="#" style="font-size: 16px" class="modify_group" id="<?php echo $groupid; ?>">Modify Group</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- <button type="button" class="btn-md btn-success">Manage</button>&nbsp;&nbsp; -->
                                            </td>
                                            <td class="text-left" style="display:none;">
                                                <?php echo $groupid;?>
                                            </td>
                                            <td class="text-left" style="font-size: 16px"><?php echo $groupname;?></td>
                                        </tr>
                                        <?php
                                        }   
                                        ?>
                                    </tbody>
                                </table>

                                <!-- <ul class="pagination">
                                    <li><a href="javascript:;">&laquo;</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="javascript:;">2</a></li>
                                    <li><a href="javascript:;">3</a></li>
                                    <li><a href="javascript:;">4</a></li>
                                    <li><a href="javascript:;">5</a></li>
                                    <li><a href="javascript:;">&raquo;</a></li>
                                </ul> -->
                                <div class="well well-sm">
                                    <div class="btn-toolbar" style="margin: 0 auto;">
                                        <?php if ($_SESSION["admin_user"] == 1) {  //don't show add new user button for non-admin users ?>
                                        <button id="btn_add_group" class="btn btn-md btn-primary">Add New Group</button>
                                        <?php } ?>

                                    </div>
                                </div>
                                <div>
                                    <form id="user_form" class="user_form" method="post" action="user-manager.php">
                                        <input type="hidden" value="<?php echo $current_page?>" id="current_page" name="current_page" />
                                        <input type="hidden" value="<?php echo $filter?>" id="page_filter" name="page_filter" />
                                        <input type="hidden" value="<?php echo $MaxPages?>" id="MaxPages" name="MaxPages" />

                                    </form>
                                </div>

                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>


            </div>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 center-block">
                    <div class="well">
                        <h2>Register Your Groups</h2>
                        <p>
                            Use this section to register your groups.
                            <br />
                            <br />
                            Using groups, you can limit administrators access to only those accounts that share the same group(s).
                        </p>
                        <br />
                        <center>
                            <img src="../img/group-members2.jpg" class="img-responsive img-rounded hidden-xs" alt="groups image" />
                        </center>
                    </div>

                </div>
                <div class="col-sm-1"></div>
            </div>

        </div>

    </div>
    <br />
    <div class="text-center">
        <a href="../Home.php" class="btn btn-warning btn-group-md" style="color:black" role="button">Return To Home Screen</a>
    </div>
    <br />
    <footer>
        <div class="extra">
        <div class="container text-center">
            <p>&copy; Copyright @ 2016 EMTeLink</p>
        </div>
        </div>
        <!-- end Container-->
    </footer>



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="../Scripts/bootstrap-dialog.min.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $.wait = function (callback, seconds) {
            return window.setTimeout(callback, seconds * 1000);
        }

        $(document).ready(function () {



            //####################################################### Add Group  ##################################
            $('#btn_add_group').on('click', function (e) {
                e.preventDefault();
                //alert("test");
                window.location = "group-add.php";
            })


            //################################################### Delete button ################################################
            $(document).on("click", ".delete_user", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var post_data = $(this).attr('id');
                //alert(post_data);

                BootstrapDialog.confirm({
                    title: 'Confirmation',
                    message: "Are you sure you want to delete this user?",
                    type: BootstrapDialog.TYPE_SUCCESS, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                    closable: false, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelLabel: 'No', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Yes', // <-- Default value is 'OK',
                    btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                    btnCancelClass: 'btn-danger',
                    animate: false,

                    callback: function (result) {
                        // result will be true if button was click, while it will be false if users close the dialog directly.
                        if (result) {
                            $.ajax({
                                type: "POST",
                                url: "../ws/delete-user.php",
                                data: { "uid": post_data },
                                success: function (msg) {
                                    //alert(msg);
                                    //alert(strcmp(msg,"success"));
                                    if (strcmp(msg, "success")) {
                                        BootstrapDialog.alert('User deleted successfully.', function () {
                                            location.reload();
                                        });
                                    }
                                    else {
                                        alert(msg);
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    alert("An processing error occured.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                                }
                            });
                        } else {
                            //alert('Nope.');
                        }
                    }
                });


            });

            function strcmp(str1, str2) {
                return str1.localeCompare(str2) / Math.abs(str1.localeCompare(str2));
            }


            //####################################################### Change Password Button #######################################
            $(document).on("click", ".reset_pw", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var user_id_to_change = $(this).attr('id');
                $("#chg_pw_userid").val(user_id_to_change);

                //alert($(this).attr('id'));
                $("#new_pw").val("");
                $("#confirm_pw").val("");

                $('#chgPassword').modal('show');

            })

            $('#btn_save').on('click', function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                if ($("#new_pw").val() != $("#confirm_pw").val()) {
                    //alert("values do NOT match");
                    $('#pw-chg-nomatch').removeClass('hide');
                    $.wait(function () { $('#pw-chg-nomatch').addClass('hide') }, 3);
                    //more processing here
                }
                else {
                    //alert("values DO match");
                    var new_pw = $("#new_pw").val();
                    var user_id_to_change = $("#chg_pw_userid").val();
                    //alert(new_pw);
                    $.ajax({
                        type: "POST",
                        url: "ws-chg-pw.php",
                        data: { "uid": user_id_to_change, "npw": new_pw },
                        success: function (msg) {
                            //alert(msg);
                            $('#pw-chg-success').removeClass('hide');
                            $.wait(function () { $('#pw-chg-success').addClass('hide'); $('#chgPassword').modal('hide'); }, 2);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("An processing error occured.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                        }
                    });
                }
            })


            //####################################################### Mail Report #######################################
            $(document).on("click", ".email_rpt", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var user_id_to_email = $(this).attr('id');
                $("#email_userid").val(user_id_to_email);

                //alert($(this).attr('id'));
                //blank out the fields
                $("#recp_name").val("");
                $("#recp_email").val("");
                $("#email_subject").val("");
                $("#email_message").val("");

                //alert($("#email_userid").val());

                $('#mailReport').modal('show');

            })

            $('#btn_send_email').on('click', function (e) {
                e.preventDefault();

                //alert($("#recp_email").val().indexOf("@"));
                //return;

                if ($('#recp_email').val() == '') {
                    $('#email-fail').removeClass('hide')
                    $.wait(function () { $('#email-fail').addClass('hide'); }, 2);
                    return;
                }

                //make sure email address has @ in it

                if ($("#recp_email").val().indexOf("@") == -1) {
                    $('#email-address-not-valid').removeClass('hide')
                    $.wait(function () { $('#email-address-not-valid').addClass('hide'); }, 2);
                    return;
                }

                $('#busy').removeClass('hide');
                //alert("about to send email");
                var user_id_to_email = $("#email_userid").val();
                $.ajax({
                    type: "POST",
                    url: "email.php",
                    //data: { "uid": user_id_to_email },
                    data: $('form.mail_report_form').serialize(),
                    success: function (msg) {
                        //alert(msg);
                        $('#busy').addClass('hide');
                        $('#email-success').removeClass('hide');
                        $.wait(function () { $('#email-success').addClass('hide'); $('#mailReport').modal('hide'); }, 2);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("An processing error occured while attempting to email report.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                    }
                });



            })


            //####################################################### Notify Contacts #######################################
            $(document).on("click", ".notify_contacts", function (e) {
                e.preventDefault();
                //alert($(this).attr('id'));
                var user_id_to_notify_about = $(this).attr('id');
                $("#notify_userid").val(user_id_to_notify_about);

                //alert($(this).attr('id'));
                //blank out the fields
                //$("#notify_message").val("");

                //alert($("#email_userid").val());

                $('#mailContact').modal('show');

            })

            $('#btn_send_notify').on('click', function (e) {
                e.preventDefault();

                //alert($("#recp_email").val().indexOf("@"));
                //return;

                //if ($('#recp_email').val() == '') {
                //    $('#email-fail').removeClass('hide')
                //    $.wait(function () { $('#email-fail').addClass('hide'); }, 2);
                //    return;
                //}

                //make sure email address has @ in it

                //if ($("#recp_email").val().indexOf("@") == -1) {
                //    $('#email-address-not-valid').removeClass('hide')
                //    $.wait(function () { $('#email-address-not-valid').addClass('hide'); }, 2);
                //    return;
                 // }

                $('#busy_notify').removeClass('hide');
                //alert("about to send email");
                var user_id_to_email = $("#email_userid").val();
                $.ajax({
                    type: "POST",
                    url: "ws-voicecall.php",
                    //data: { "uid": user_id_to_email },
                    data: $('form.mail_contact_form').serialize(),
                    success: function (msg) {
                        //alert(msg);
                        $('#busy_notify').addClass('hide');
                        $('#notify-success').removeClass('hide');
                        $.wait(function () { $('#notify-success').addClass('hide');}, 2);
                        //$.wait(function () { $('#notify-success').addClass('hide'); $('#mailContact').modal('hide'); }, 2);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#busy_notify').addClass('hide');
                        $('#notify-fail').removeClass('hide')
                        $.wait(function () { $('#notify-fail').addClass('hide'); }, 2);
                        alert("An processing error occured while attempting to notify contacts.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                    }
                });



            })

            //if there is only one page
            if ($("#current_page").val() == $("#MaxPages").val()) {
                $("#btn_prev").prop('disabled', true);
                $("#btn_next").prop('disabled', true);
                $("#btn_first").prop('disabled', true);
                $("#btn_last").prop('disabled', true);
            }

        }) //document ready


    </script>
</body>
</html>
