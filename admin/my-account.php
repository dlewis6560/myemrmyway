<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <title>MyEMRMyWay :: My Account</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="MyEMRMyWay App">
	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">

    <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">

    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">

    <link href="./css/pages/dashboard.css" rel="stylesheet">

    <link href="./css/custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php

    include("../include/incConfig.php");
    session_start();
	
	include("../include/session.php");

    ?>
	
	<style>
	
	@media only screen and (max-width: 768px) {
			widget .widget-content {
				padding: 0px !important;
				background: #FFF;
				border: 1px solid #D5D5D5;
				-moz-border-radius: 5px;
				-webkit-border-radius: 5px;
				border-radius: 5px;
			}
	}	
	
	</style>

</head>

<body>

    <?php 

    $recordset = $database->select("Subscriptions", [
        "subName",
        "subAddress1",
        "subAddress2",
        "subCity",
        "subState",
        "subZip"
    ], [
        "subId" => "$subId"
    ]);

    foreach($recordset as $data)
    {
        $subName = $data["subName"];
        $subAddress1 = $data["subAddress1"];
        $subAddress2 = $data["subAddress2"];
        $subCity = $data["subCity"];
        $subState = $data["subState"];
        $subZip = $data["subZip"];
    }   

	//Navbar
	include("../include/navbar.php");
    ?>

    <div class="main">

        <div class="container">
            <br />
           <?php 
            $instruction_content = "Manage account information below.<br /><br />";
            $instruction_content = $instruction_content . "Make any desired changes and then Tap the Update button.<br /><br />";
            $instruction_content = $instruction_content . "Tap the \"Home\" button to return to the Home Screen.";
            ?>
               <div class="row">
                    <div class="col-xs-1">

                </div>
                <div class="col-xs-10 center-block">
                    <div class="panel-group accordion">
                        <div class="panel panel-warning open" style="border-color:black">

                            <div class="panel-heading" style="text-align:center">

                                <h4 class="panel-title">

                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                                        <span style="font-size: large;color:#525252;"><i class="icon-info-sign" style="color: #525252"></i>&nbsp;<u>Instructions</u></span><span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to Toggle)</span>
                                    </a>
                                </h4>

                            </div>

                            <div id="collapseOne33" class="panel-collapse collapse in">

                                <div class="panel-body collapse">

                                    <p style="font-size: medium">
                                        <?php echo $instruction_content; ?>
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-xs-1">

                </div>
            </div>
            <br />

            <div class="row">


                <div class="col-sm-1 hidden-xs">

                </div>
                <!-- /span4 -->

                <div class="col-xs-12 col-sm-10">
                    <div class="widget stacked ">

                        <div class="widget-header">
                            <i class="icon-sign-blank"></i>
                            <h3>My Account Information</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">



                            <div class="tabbable">

                                <br>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <form id="editprofileform" class="editprofileform form-horizontal col-md-12">
                                            <fieldset>

                                                <div class="form-group">
                                                    <label for="subName" class="col-md-3">Subscription Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="subName" name="subName" value="<?php echo $subName ?>" placeholder="subscription name">
                                                        <!-- <p class="help-block">This is the value the user will use to logon.</p> -->
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->


                                                <div class="form-group">
                                                    <label for="address1" class="col-md-3">Address 1</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $subAddress1 ?>" placeholder="address 1">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->


                                                <div class="form-group">
                                                    <label class="col-md-3" for="address2">Address 2</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $subAddress2 ?>" placeholder="address 2">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->


                                                <div class="form-group">
                                                    <label class="col-md-3" for="city">City</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $subCity ?>" placeholder="city">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->

                                                <div class="form-group">
                                                    <label class="col-md-3" for="state">State</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="state" name="state" value="<?php echo $subState ?>" placeholder="state">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->

                                                <div class="form-group">
                                                    <label class="col-md-3" for="zip">Zip</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="zip" name="zip" value="<?php echo $subZip ?>" placeholder="zip">
                                                    </div>
                                                    <!-- /controls -->
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3" for="subType">Subscription Type</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="subType" name="subType" value="<?php 
                                                                                                                                   if ($subType == "F") {echo "Family";}
                                                                                                                                   if ($subType == "S") {echo "School";}
                                                                                                                                   if ($subType == "G") {echo "Group";}
                                                                                                                                   ?>" readonly>
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3" for="maxUsers">Max Users</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="maxUsers" name="maxUsers" value="<?php echo $subMaxUsers ?>" readonly>
                                                    </div>
                                                    <!-- /controls -->
                                                </div>
                                                <!-- /control-group -->

                                                <br />


                                                <div class="form-group">

                                                    <div class="col-md-12 text-center btn-toolbar">
                                                        <button type="button" id="btn_update_account" style="background-color:#3881C0;color:white;" class="btn">Update</button>
                                                        <button class="btn" style="color:white;background-color:#214D8C" id="btn_cancel">Return To Home Screen</button>
                                                    </div>
                                                </div>
                                                <!-- /form-actions -->
                                            </fieldset>
                                            <input type="hidden" value="<?php echo $subId?>" id="subId" name="subId" />
                                        </form>
                                    </div>

                                </div>


                            </div>





                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="col-sm-1 hidden-xs">

               <!--     <div class="well">

                        <h4>Add User</h4>

                        <p>In order to add a new user, please enter the UserID, Firstname, Lastname and Password, then click the Create Button..</p>

                    </div> -->

                </div>
                <!-- /span4 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->
 <!-- /footer -->

<?php  include("../include/incFooter-sm.php"); ?>

    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script>



        $('#btn_update_account').on('click', function (e) {

            //alert("here")
            $.ajax({
                type: "POST",
                url: "../ws/process-account.php",
                data: $('form.editprofileform').serialize(),
                success: function (msg) {

                    alert(msg);
                    //$('#sug').val(msg);
                    //$("#thanks").html(msg)
                    //$("#form-content").modal('hide');
                    //window.location.href = "home.html";					
                },
                error: function () {
                    alert("unable to update account, try again.");
                }
            });
        })


        $('#btn_cancel').on('click', function (e) {
		    window.location.href = "../home.php";
		    return false;
        })

    </script>

</body>
</html>
