<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8" />
    <title>MyEMRMyWay :: My Documents</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="MyEMRMyWay App" />

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet" />



    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />

    <link href="css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" />

    <link href="css/base-admin-3.css" rel="stylesheet" />
    <link href="css/base-admin-3-responsive.css" rel="stylesheet" />


    <link href="../Content/bootstrap-dialog.min.css" rel="stylesheet" />

    <link href="css/pages/dashboard.css" rel="stylesheet" />
    <link href="css/plugins/easyWizard/easyWizard.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="../Content/sweetalert2.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-datepicker3.min.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php


    include("../include/incConfig.php");
    session_start();

	//make sure we have a valid sesion
	include("../include/session.php");

    use Urlcrypt\Urlcrypt;
    require_once '../../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    $filter_text = "( not filtered )";

    if (isset($_POST["current_page"])) {
        $current_page = $_POST["current_page"];
    }else{
        $current_page=1;
    }


    if (isset($_POST["page_filter"])) {
        $filter = $_POST["page_filter"];
    }else{
        $filter="";
    }

    $where = "where uid=" . $uid;

    ?>

    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }

            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }

        @media (max-width: 768px) {
            .table-responsive {
                width: 100%;
                margin-bottom: 15px;
                /* overflow-x: scroll; */
                overflow-y: -webkit-paged-x;
                border: 1px solid #ddd;
            }
        }

        .form-group {
            margin-bottom: 20px !important;
        }

        .error_text {
            background-color: #FFE1E1;
            color: black;
            padding: 5px;
            font-size: 14px;
            font-weight:600;
        }

        textarea {max-width:95%;}

    </style>


</head>

<body>

    <?php

    $PageSize = 10;
    $MaxPages = 0;

    //get user info for 2nd step in wizard
    $recordset = $database->select("user", [
        "address",
        "firstname",
        "middleinitial",
        "lastname",
        "dob",
        "gender",
        "city",
        "st",
        "zip",
        "student_email",
    ], [
        "userid" => "$userid"
    ]);


    foreach($recordset as $data)
    {
        //demographic data ########################################################
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $middleinitial = $data["middleinitial"];
        $address = $data["address"];
        $city = $data["city"];
        $state = $data["st"];
        $zip = $data["zip"];
        $gender = $data["gender"];
        $student_email = $data["student_email"];

        $dob = "";
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data["dob"]);
        if(!$myDateTime == ''){
            $dob = $myDateTime->format('m-d-Y');
        }

        $request_name = $firstname . " " . $middleinitial . " " . $lastname;
        $request_dob = $dob;
        $request_address = $address . " " . $city . "," . $state . " " . $zip;
        $request_email = $student_email;
        $request_phone = "";


        //echo "<tr><td>" . $userid . "</td><td>" . $firstname . " " . $lastname . "</td></tr>";
        //########################################################################
    }


    //$strSQLCount = "SELECT Count(uid) as REC_COUNT ";
    //$strSQLCount = $strSQLCount . "FROM [user_docs] " . $where;

    //$recordset = $database->query($strSQLCount);
    //foreach($recordset as $data)
    //{
    //    $count = $data["REC_COUNT"];
    // }

    //$MaxPages = ceil($count / $PageSize);

    $strSQL = "SELECT uniqueid,doc_name,doc_description,status,file_name ";
    $strSQL = $strSQL . "FROM [user_docs] " . $where . " ";
    $strSQL = $strSQL . "ORDER BY doc_name ";


	//Navbar
	include("../include/navbar.php");

    ?>


    <div class="well well-sm">
        <div class="text-center">
            <h2>
                <img src="../img/docs-icon.png" class="img-circle" alt="profile" width="64" height="64" style="border: solid; border-width: medium" />
                Documents for <?php echo $firstname . " " . $lastname ?>
            </h2>
        </div>
        <!-- End container -->
    </div>

    <div class="main">

        <div class="container">
            <?php
            $instruction_content = "Manage your Documents below.<br /><br />";
            $instruction_content = $instruction_content . "Use the action menu to View, Print, Email, or Delete your documents.<br /><br />";
            $instruction_content = $instruction_content . "Click the Upload button to upload a new document.<br /><br />";
            $instruction_content = $instruction_content . "Click the Request button to fax a request for a document.";
            include("../include/incInstructions.php");
            ?>

            <div class="row">

                <div class="col-md-12">

                    <!--    <div class="well">

                        <h4>Manage Users</h4>

                        <p>Select an Action ...</p>

                    </div> -->

                    <div class="widget stacked ">

                        <div class="widget-header" style="vertical-align: middle">
                            <h3>My Document List</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content text-center">
                            <div class="table-responsive text-center" style="margin: 0 auto; font-size: 16px" id="documents-div">
                                <div class="table-responsive text-center" style="margin: 0 auto; font-size: 16px" id="documents-div">
                                    <table class="table table-bordered table-hover table-striped table-responsive" id="documents-table">
                                        <thead>
                                            <tr>
                                                <!-- <th class="text-center" style="font-size: 18px">Current</th> -->
                                                <th class="text-center" style="font-size: 18px">Action</th>
                                                <th class="text-left" style="font-size: 18px">Name</th>
                                                <th class="text-left" style="font-size: 18px">Description</th>
                                                <th class="text-left" style="font-size: 18px">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            //echo $strSQL;

                                            //$strSQL = "SELECT uniqueid,doc_name,doc_description,status,file_name ";

                                            $recordset = $database->query($strSQL);
                                            foreach($recordset as $data)
                                            {
                                                $uniqueid = $data["uniqueid"];
                                                $doc_name = $data["doc_name"];
                                                $doc_description = $data["doc_description"];
                                                $status = $data["status"];
                                                $file_name = $data["file_name"];

                                                $encrypted = Urlcrypt::encrypt($uniqueid . "|" . "view". "|" . time());

                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <button class="btn btn-default btn-md dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a href="mydocument_viewer.php?doc_id=<?php echo $encrypted; ?>" target="_self" style="font-size: 16px">View Document</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" style="font-size: 16px" class="deleteDocumentButton" id="<?php echo $encrypted; ?>">Delete Document</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <!-- <button type="button" class="btn-md btn-success">Manage</button>&nbsp;&nbsp; -->
                                                </td>
                                                <td class="text-left" style="font-size: 16px">
                                                    <?php echo $doc_name;?>
                                                </td>
                                                <td class="text-left" style="font-size: 16px">
                                                    <?php echo $doc_description?>
                                                </td>
                                                <td class="text-left hidden-xs" style="font-size: 16px">
                                                    <?php echo $status;?>
                                                </td>
                                                <?php if ($subType != "F") {  ?>
                                                <td class="text-left hidden-xs" style="font-size: 16px">
                                                    <?php echo $groupname;?>
                                                </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>

                                </div>

                                <div id="dialog" style="display: none"></div>
                                <div class="well well-sm">
                                    <div class="btn-toolbar" style="margin: 0 auto;">
                                        <!-- Button trigger modal -->
                                        <button id="btn_add_document" class="btn btn-md btn-success" data-toggle="modal" data-target="#uploadModal"  data-backdrop="static" data-keyboard="false">
                                            <i class="icon-upload" aria-hidden="true"></i>&nbsp;Upload Document
                                        </button>
                                        <button id="btn_request_document" class="btn btn-md btn-primary" data-toggle="modal" data-target="#requestModal"  data-backdrop="static" data-keyboard="false">
                                            <i class="icon-mail-forward" aria-hidden="true"></i>&nbsp;Request Document
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>


                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>


            </div>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 center-block">

                    <div class="well">
                        <h2>Manage Your Documents</h2>
                        <p>
                            You can use this area to request, upload, store, share, and manage your documents.
                            <br />
                        </p>
                        <p>
                            Providing you with easy access to your health information empowers you to be more in control of decisions regarding
						your health and well-being.  It enables you to more easily monitor any chronic conditions, adhere to treatment plans,
						find and fix errors in your health records, and track progress in wellness or disease management programs.  Using
                        MyEMRMyWay, you have ever expanding and innovative opportunities to access your health information electronically,
						more quickly and easily, in real time and on demand. MyEMRMyWay puts you "in the driver's seat" with respect to your health.
                        </p>
                        <br />
                        <p>
                            The Privacy Rule gives you, with few exceptions, the right to inspect, review, and receive a copy of your medical records
						and billing records that are held by health plans and health care providers covered by the Privacy Rule.
                            <br />
                            HIPAA gives you important rights to access - PDF your medical record and to keep your information private.
                            <br />
                            <br />
                            See the following link for more information
                            <a href="https://www.hhs.gov/hipaa/for-individuals/index.html" target="blank">HIPAA for Individuals</a>
                        </p>
                        <br />
                        <center>
                            <img src="../img/docs-sm.png" class="img-responsive img-rounded" alt="image of documents" />
                        </center>
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>

        </div>
        <div id="addID" class="modal fade" role="dialog">
            <form id="add_id_form" class="add_id_form">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="ch_id_modal_title">Add Document</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                                <div class="col-xs-3">
                                    <label style="font-size: 16px; color: black; font-weight: 500" for="new_id">Document Number</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control input-md" id="id_number" name="id_number" placeholder="enter id number" value="" />
                                </div>
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                                <div class="col-xs-3">
                                    <label style="font-size: 16px; color: black; font-weight: 500" for="confirm_pw">Description</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control input-md" id="id_description" name="id_description" placeholder="description" value="" />
                                </div>
                                <div class="col-xs-1">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" id="btn_save">Save</button>
                            <br />
                            <br />
                            <div>
                                <div class="alert alert-success hide text-center" role="alert" id="id-success">ID added successfully.</div>
                                <div class="alert alert-danger hide text-center" role="alert" id="id-number-required">An ID number is required.</div>
                                <div class="alert alert-danger hide text-center" role="alert" id="id-description-required">A description is required.</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <input type="hidden" id="id_uid" name="id_uid" value="<?php echo $uid ?>" />
                    </div>
                </div>
            </form>
        </div>

        <!-- Upload Document Modal -->
        <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="uploadModalLabel">
                            Add Document
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-10 col-sm-push-1 col-xs-12" style="margin: 0 auto; border-bottom-color: gainsboro; border: none; border-width: 1px; padding: 2px;">
                                <div>
                                    <form id="form-upload" onsubmit="return false">

                                        <div class="form-group" style="margin-bottom:10px;">
                                            <label>Select the file to Upload (<u>pdf files only</u>)</label>
                                            <div class="input-group">
                                                <input type="text" id="fileToUploadTextbox" class="form-control" readonly />
                                                <label class="input-group-btn">
                                                    <span class="btn btn-primary">
                                                        Browse&hellip;
                                                        <input type="file" style="display: none;" id="fileToUpload" name="fileToUpload" />
                                                    </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="progress hide" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;" id="progress_bar">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                <span id="progresstext">0% complete</span>
                                            </div>
                                        </div>


                                        <div class="form-group" style="margin-bottom:10px;">
                                            <label for="docnameInput">Document Name</label>
                                            <input type="text" id="docnameInput" name="docnameInput" class="form-control"
                                                placeholder="Enter document name" />
                                        </div>
                                        <div class="form-group" style="margin-bottom:10px;">
                                            <label for="docdescInput">Document Description</label>
                                            <div class="input-group">
                                                <textarea rows="4" cols="75" class="form-control" id="docdescInput" name="docdescInput" placeholder="Enter document description" maxlength="512"></textarea>
                                                <h6 class="pull-right" id="count_message"></h6>
                                            </div>
                                        </div>

                                        <div style="margin-top: 10px; margin-left: 5px; margin-right: 5px; background-color: cornsilk">
                                            <span id="responsetext"></span>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="button-upload">Upload</button>
                        <button type="button" class="btn btn-secondary" id="button-close" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Request Documents  -->
        <div class="modal fade" id="requestModal" role="dialog" aria-labelledby="requestModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Request Records</h4>
                    </div>
                    <div class="modal-body wizard-content">
                        <form id="records_request_form">
                            <!-- Step 1 Patient Information--->
                            <div class="wizard-step">
                                <div class="well">
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <h4>
                                                <strong>Please enter your patient information.</strong>
                                                <br />
                                                <small>
                                                    <i>(info from your profile loaded by default)</i>
                                                </small>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <!-- Name -->
                                            <label for="requestName" class="control-label">*Name</label>
                                            <input type="text" class="form-control" id="requestName" name="requestName" style="margin-bottom:3px" placeholder="enter your name" value="<?php echo $request_name ?>" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <!-- DOB -->
                                            <label for="requestDOB" class="control-label">*DOB</label>
                                            <input type="text" class="form-control" id="requestDOB" name="requestDOB" style="margin-bottom:3px" placeholder="enter your date of birth" value="<?php echo $request_dob ?>" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12 col-sm-6">
                                            <!-- Phone -->
                                            <label for="requestPhone" class="control-label">*Phone</label>
                                            <input type="text" class="form-control" id="requestPhone" name="requestPhone" style="margin-bottom:3px" placeholder="enter your phone number" value="<?php echo $request_phone ?>" />
                                        </div>
                                        <div class="form-group  col-xs-12 col-sm-6">
                                            <!-- Email -->
                                            <label for="requestEmail" class="control-label">*Email</label>
                                            <input type="text" class="form-control" id="requestEmail" name="requestEmail" style="margin-bottom:3px" placeholder="enter your email address" value="<?php echo $request_email ?>" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <!-- Address -->
                                            <label for="requestAddress" class="control-label">Address</label>
                                            <input type="text" class="form-control" id="requestAddress" name="requestAddress" style="margin-bottom:3px" placeholder="enter your street address" value="<?php echo $request_address ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="error_text hide" id="step1_error">Error Display</div>
                            </div>

                            <!-- Step 2 Who records request is from --->
                            <div class="wizard-step">
                                <div class="well">
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <h4>
                                                <strong>Who are you requesting records from?</strong>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <!-- requestPersonOrFacilityName -->
                                            <label for="requestPersonOrFacilityName" class="control-label">*Person or Facility Name</label>
                                            <input type="text" class="form-control" id="requestPersonOrFacilityName" name="requestPersonOrFacilityName" style="margin-bottom:3px" placeholder="enter person or facility name" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <!-- requestPersonOrFacilityAddress -->
                                            <label for="requestPersonOrFacilityAddress" class="control-label">Street Address <i>(optional)</i></label>
                                            <input type="text" class="form-control" id="requestPersonOrFacilityAddress" name="requestPersonOrFacilityAddress" style="margin-bottom:3px" placeholder="enter person or facility street address" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12 col-sm-6">
                                            <!-- requestMethod -->
                                            <label for="requestMethod" class="control-label">*Send Request via Email or Fax?</label>
                                            <select class="form-control" id="requestMethod">
                                                <option>Email</option>
                                                <option>Fax</option>
                                            </select>
                                        </div>
                                        <div class="form-group  col-xs-12 col-sm-6">
                                            <!-- requestMethodValue -->
                                            <label for="requestMethodValue" id="requestMethodValueLabel" class="control-label">*Email Address</label>
                                            <input type="text" class="form-control" id="requestMethodValue" name="requestMethodValue" style="margin-bottom:3px" placeholder="enter email address" />
                                        </div>
                                    </div>
                                </div>

                                <div class="error_text hide" id="step2_error"></div>
                            </div>

                            <!-- Step  3 What records --->
                            <div class="wizard-step">
                                <div class="well">
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <h4>
                                                <strong>What records are you requesting?</strong>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            What are the Date(s) of Service for this request?<br />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12 col-sm-6">
                                            <!-- From Date -->
                                            <label for="" class="control-label">From Date</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control" id="request_from_date" data-provide="datepicker" />
                                                <div class="input-group-addon">
                                                    <i class="icon-calendar" id="request_from_date_calendar"></i>
                                                </div>
                                            </div>
                                            <!--<input type="text" class="form-control" id="requestFromDate" name="requestFromDate" style="margin-bottom:3px" placeholder="enter date" autofocus="autofocus" />-->
                                        </div>
                                        <div class="form-group  col-xs-12  col-sm-6">
                                            <!-- From Date -->
                                            <label for="" class="control-label">To Date</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control" id="request_to_date" data-provide="datepicker" />
                                                <div class="input-group-addon">
                                                    <i class="icon-calendar" id="request_to_date_calendar"></i>
                                                </div>
                                            </div>
                                            <!--<input type="text" class="form-control" id="requestFromDate" name="requestFromDate" style="margin-bottom:3px" placeholder="enter date" autofocus="autofocus" />-->
                                        </div>
                                    </div>
                                    <div class="row" style="padding-bottom:10px">
                                        <div class="col-xs-12">
                                            <!-- Notes -->
                                            <label for="requestNotes" class="control-label">*I am requesting the following records (see <u>examples</u>)</label>
                                            <textarea rows="4" cols="60" id="requestNotes" name="requestNotes"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12" style="color:#808080 !important">
                                            <u>examples</u>
                                            <ul>
                                                <li>Physician Practice Records</li>
                                                <li>Billing Records</li>
                                                <li>Discharge Summary</li>
                                                <li>Emergency Room Records</li>
                                                <li>Operative/Procedure Reports</li>
                                                <li>Test Results (X-Rays, Lab/Pathology Results)</li>
                                                <li>Other (Immunization Records, Medication Lists)</li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="error_text hide" id="step3_error"></div>
                            </div>

                            <!-- Step zzz Where records request is from --->
                            <!--<div class="wizard-step">
            <div class="well">
                <div class="row">
                    <div class="form-group  col-xs-12">
                        <h4>
                            Verify Delivery Settings
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-xs-12">
                        <label for="deliveryEmailAddress" class="control-label">Email To Send Records To</label>
                        <input type="text" class="form-control" id="deliveryEmailAddress" name="deliveryEmailAddress" style="margin-bottom:3px"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-xs-12">
                        <label for="deliveryFax" class="control-label">Fax To Send Records To</label>
                        <input type="text" class="form-control" id="deliveryFax" name="deliveryFax" style="margin-bottom:3px"/>
                    </div>
                </div>
            </div>
        </div>-->
                            <!-- Step 4 Signature -->
                            <div class="wizard-step">
                                <div class="well">
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <h4>
                                                <strong>Please review the following and indicate your acceptance by signing below.</strong>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <h4>
                                                I 
                                                <strong><u><span id="step4_name"></span></u></strong>, residing at 
                                                <strong>
                                                    <u><span id="step4_address"></span></u>
                                                </strong>
                                                , am requesting my records from 
                                                <strong>
                                                    <u><span id="step4_facility"></span></u>
                                                </strong>
                                            </h4>
                                            <br />
                                            <h4 style="color:#808080 !important">
                                                <i>I understand that the Health Insurance Portability and Accountability Act (HIPAA) gives me the right to access the medical and health information about me and the right to have that information transmitted directly to a person designated by me.</i>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <canvas id="signature" style="height: 150px; border-width: 1px; border-style: dotted;"></canvas>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button type="button" id="btn_clear_signature" class="btn btn-xs btn-danger">Clear Signature</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="error_text hide" id="step4_error"></div>
                            </div>

                            <!-- Step 5 Signature -->
                            <div class="wizard-step">
                                <div class="well">
                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <h4>
                                                <strong>Please review following and then click the Send Request button.</strong>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group  col-xs-12">
                                            <h4>
                                                1. You are requesting records from:
                                                <br />
                                                <br />
                                                <strong><u><span id="step5_facility"></span></u></strong>
                                                <br />
                                                <br />
                                                <br />
                                                2. You are requesting the following records:
                                                <br />
                                                <br />
                                                From: <strong><u><span id="step5_from_date"></span></u></strong> To: <strong><u><span id="step5_to_date"></span></u></strong>
                                                <br />
                                                <br />
                                                <br />
                                                Records Requested: <strong><u><span id="step5_notes"></span></u></strong>
                                                <br />
                                                <br />
                                                <br />
                                                3. Request will be sent via <strong><u><span id="step5_requestMethod"></span></u></strong> to <strong><u><span id="step5_requestMethodValue"></span></u></strong>
                                                
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer wizard-buttons">
                        <!-- The wizard button will be inserted here. -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center">
        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">Return To Home Screen</a>
    </div>
    <br />
    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?>



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>

    <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="../Scripts/bootstrap-dialog.min.js"></script>

    <script src="./js/plugins/easyWizard/easyWizard.js"></script>
    <script src="../Scripts/signatureCapture.js"></script>
    <script src="../Scripts/modernizr.custom.34982.js"></script>
    <script src="../Scripts/sweetalert2.min.js"></script>
    <script src="../Scripts/bootstrap-datepicker.min.js"></script>
   


    <script type="text/javascript">
        // When the document is ready
        $.wait = function (callback, seconds) {
            return window.setTimeout(callback, seconds * 1000);
        }

        function fitToContainer(canvas) {
            // Make it visually fill the positioned parent
            canvas.style.width = '100%';
            canvas.style.height = '100%';
            // ...then set the internal size to match
            canvas.width = canvas.offsetWidth;
            canvas.height = canvas.offsetHeight;
        }

        $(document).ready(function () {



            //################################################## document delete button ###############################################
            $('.deleteDocumentButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {
                    swal({
                        title: 'Delete Document?',
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#41A24D',
                        cancelButtonColor: '#D54F49',
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {

                            var values = {
                                'itemno': $(this).attr("id"),
                            };

                            $.ajax({
                                type: "POST",
                                url: "../ws/delete-document.php",
                                data: values,
                                success: function (msg) {
                                    swal({
                                        title: "Deleted",
                                        text: "Document has been deleted." + msg,
                                        type: "success"
                                    }).then(function () {
                                        location.reload();
                                    });

                                    //$('#demo-success').removeClass('hide');
                                    //$.wait(function () { $('#demo-success').addClass('hide') }, 2);

                                    /* BootstrapDialog.show({
										message: msg,
										buttons: [{
											label: 'Close',
											action: function(dialogItself){
												dialogItself.close();
											}
										}]
									});
									*/
                                    //alert(msg);
                                    //$('#sug').val(msg);
                                    //$("#thanks").html(msg)
                                    //$("#form-content").modal('hide');
                                    //window.location.href = "home.html";					
                                },
                                error: function () {
                                    swal({
                                        title: "Error",
                                        text: "Error deleting document." + msg,
                                        type: "error"
                                    }).then(function () {
                                        location.reload();
                                    });
                                },
                                complete: function () {
                                }
                            });

                            //$('#med-deleted').removeClass('hide');
                        }
                    })
                });


            //################################################### Delete button ################################################
            //$(document).on("click", ".deleteDocumentButton", function (e) {
            //    e.preventDefault();
            //    //alert($(this).attr('id'));
            //    var post_data = $(this).attr('id'); //document name to delete
            //    //alert(post_data);
            //    //exit;

            //    BootstrapDialog.confirm({
            //        title: 'Confirmation',
            //        message: "Are you sure you want to delete this document?",
            //        type: BootstrapDialog.TYPE_SUCCESS, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            //        closable: false, // <-- Default value is false
            //        draggable: true, // <-- Default value is false
            //        btnCancelLabel: 'No', // <-- Default value is 'Cancel',
            //        btnOKLabel: 'Yes', // <-- Default value is 'OK',
            //        btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
            //        btnCancelClass: 'btn-danger',
            //        animate: false,

            //        callback: function (result) {
            //            // result will be true if button was click, while it will be false if users close the dialog directly.
            //            if (result) {
            //                $.ajax({
            //                    type: "POST",
            //                    url: "../ws/delete-document.php",
            //                    data: { "docnametodelete": post_data },
            //                    success: function (msg) {
            //                        //alert(msg);
            //                        //alert(strcmp(msg,"success"));
            //                        if (strcmp(msg, "success")) {
            //                            BootstrapDialog.alert('Document deleted successfully.', function () {
            //                                location.reload();
            //                            });
            //                        }
            //                        else {
            //                            alert(msg);
            //                        }
            //                    },
            //                    error: function (jqXHR, textStatus, errorThrown) {
            //                        alert("An processing error occured.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
            //                    }
            //                });
            //            } else {
            //                //alert('Nope.');
            //            }
            //        }
            //    });


            //});

            function strcmp(str1, str2) {
                return str1.localeCompare(str2) / Math.abs(str1.localeCompare(str2));
            }


            $("#button-close").click(
                reLoadMe
             );

            function reLoadMe() {
                location.reload();
            }


            $("#button-upload").click(sendFormData);

            function sendFormData() {

                var fileName = $("#fileToUpload").val();

                if (fileName) { // returns true if the string is not empty
                    //alert(fileName + " was selected");
                } else { // no file was selected
                    $('#responsetext').text("no file selected");
                    return;
                }

                $('#progress_bar').removeClass("hide");

                var selectedFile = document.getElementById('fileToUpload').files[0];

                var file_ext_type = "";

                switch (selectedFile.type) {
                    case 'application/pdf':
                        file_ext_type = "pdf";
                        break;
                    //case 'image/png':
                    //    file_ext_type = "png";
                    //    break;
                    //case 'image/jpg':
                    //    file_ext_type = "jpg";
                    //    break;
                    //case 'image/jpeg':
                    //    file_ext_type = "jpeg";
                    //    break;
                    //case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                    //    file_ext_type = "docx";
                    //    break;
                    default:
                        file_ext_type = "???";
                }

                $('#form-upload').append('<input type="hidden" id="file_size" name="file_size" value="' + selectedFile.size + '" />');
                $('#form-upload').append('<input type="hidden" id="file_ext_type" name="file_ext_type" value="' + file_ext_type + '" />');
                $('#form-upload').append('<input type="hidden" id="mime_type" name="mime_type" value="' + selectedFile.type + '" />');

                $('#form-upload').append('<input type="hidden" id="uid" name="uid" value="' + <?php echo $uid ?>+ '" />');

                var formData = new FormData($("#form-upload").get(0));

                var ajaxUrl = "process_upload.php";

                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        // Handle progress
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = Math.round((evt.loaded / evt.total) * 100);
                                //console.log(percentComplete);
                                //console.log(percentComplete);
                                //console.log(percentComplete % 10);
                                //console.log(" ");
                                //Do something with upload progress
                                //if (percentComplete % 10 == 0) {
                                $('.progress-bar').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete);
                                $('#progresstext').text(percentComplete + '%');
                                //console.log(percentComplete);
                                //console.log(" ");
                                //echo "$i is a multiple of 10<br />";
                                //}
                                //console.log(percentComplete);
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                //console.log(percentComplete);
                            }
                        }, false);

                        return xhr;
                    },
                    complete: function () {
                        //console.log("Request finished.");
                    },
                    url: ajaxUrl,
                    type: "POST",
                    data: formData,
                    // both 'contentType' and 'processData' parameters are
                    // required so that all data are correctly transferred
                    contentType: false,
                    processData: false
                }).done(function (response) {

                    //var json_obj = $.parseJSON(response);//parse JSON
                    //debugger
                    //console.log(response);
                    $('#responsetext').text(response);

                    //alert(response);
                    //alert(response);
                    // In this callback you get the AJAX response to check
                    // if everything is right...
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("Status: " + textStatus); alert("Error: " + errorThrown);
                    $('#responsetext').text("Status: " + textStatus); alert("Error: " + errorThrown);
                    // Here you should treat the http errors (e.g., 403, 404)
                }).always(function () {
                    //alert("AJAX request finished!");
                });
            }

            // clears modal
            $('#uploadModal').on('hidden.bs.modal', function (e) {
                $(":input").val('');
                $("select").change();
                $('#responsetext').text('');
                $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
                $('#progress_bar').addClass("hide");
                $('#count_message').html('512 remaining');
            })

            var text_max = 512;
            $('#count_message').html(text_max + ' remaining');

            $('#docdescInput').keyup(function () {
                var text_length = $('#docdescInput').val().length;
                var text_remaining = text_max - text_length;

                $('#count_message').html(text_remaining + ' remaining');
            });

            $("#requestModal").wizard({
                exitText: 'exit',
                onnext: function (step) {
                    //alert(step);
                    var step1_error_element = document.getElementById("step1_error");
                    var step2_error_element = document.getElementById("step2_error");
                    var step3_error_element = document.getElementById("step3_error");
                    var step4_error_element = document.getElementById("step4_error");

                        var step_error_text = "";

                        //step 1
                        if (step == 1 ) {
                                if ($("#requestName").val() == "") {
                                    step_error_text = step_error_text + "Name is required."
                                }

                                if ($("#requestDOB").val() == "") {
                                    if (step_error_text != "") { step_error_text = step_error_text + "<br />" };
                                    step_error_text = step_error_text + "DOB is required."
                                }

                                if (($("#requestPhone").val() == "") && ($("#requestEmail").val() == "")) {
                                    if (step_error_text != "") { step_error_text = step_error_text + "<br />" };
                                    step_error_text = step_error_text + "Phone or Email is required."
                                }

                                if (step_error_text != "") {
                                    $("#step1_error").html(step_error_text);
                                    //$("#step1_error").removeclass("hide");
                                    step1_error_element.className = step1_error_element.className.replace(/\bhide\b/g, "");
                                    setTimeout(function () { step1_error_element.classList.add("hide");}, 3000);
                                    return "error";
                                }
                        }

                    //step 2
                        var step_error_text = "";
                        if (step == 2) {
                            if ($("#requestPersonOrFacilityName").val() == "") {
                                step_error_text = step_error_text + "Person or Facility Name is required."
                            }

                            if ($("#requestMethodValue").val() == "") {
                                if (step_error_text != "") { step_error_text = step_error_text + "<br />" };
                                if ($("#requestMethod").val() == "Email") {
                                    step_error_text = step_error_text + "Email Address is required."
                                }
                                else {
                                    step_error_text = step_error_text + "Fax Number is required.";
                                }
                            }


                            if (step_error_text != "") {
                                $("#step2_error").html(step_error_text);
                                //$("#step1_error").removeclass("hide");
                                step2_error_element.className = step2_error_element.className.replace(/\bhide\b/g, "");
                                setTimeout(function () { step2_error_element.classList.add("hide"); }, 3000);
                                return "error";
                            }
                        }

                    //step 3
                        var step_error_text = "";
                        if (step == 3) {
                            if ($('#requestNotes').val() == "") {
                                step_error_text = step_error_text + "Please enter the records you are requesting."
                            }

                            if (step_error_text != "") {
                                $("#step3_error").html(step_error_text);
                                step3_error_element.className = step3_error_element.className.replace(/\bhide\b/g, "");
                                setTimeout(function () { step3_error_element.classList.add("hide"); }, 3000);
                                return "error";
                            }

                            document.getElementById("step4_name").textContent = document.getElementById("requestName").value;
                            document.getElementById("step4_address").textContent = document.getElementById("requestAddress").value;
                            document.getElementById("step4_facility").textContent = document.getElementById("requestPersonOrFacilityName").value;
                        }

                    //step 4 / require signature

                        var step_error_text = "";
                        if (step == 4) {
                            //check to see if signature is blank
                            if (signature.toDataURL() == blank_signature) {
                                step_error_text = step_error_text + "Please add your signature."
                            }

                            if (step_error_text != "") {
                                $("#step4_error").html(step_error_text);
                                step4_error_element.className = step4_error_element.className.replace(/\bhide\b/g, "");
                                setTimeout(function () { step4_error_element.classList.add("hide"); }, 3000);
                                return "error";
                            }

                            document.getElementById("step5_facility").textContent = document.getElementById("requestPersonOrFacilityName").value;
                            document.getElementById("step5_from_date").textContent = document.getElementById("request_from_date").value;
                            document.getElementById("step5_to_date").textContent = document.getElementById("request_to_date").value;
                            document.getElementById("step5_notes").textContent = document.getElementById("requestNotes").value;
                            document.getElementById("step5_requestMethod").textContent = document.getElementById("requestMethod").value;
                            document.getElementById("step5_requestMethodValue").textContent = document.getElementById("requestMethodValue").value;
                        }


                    return "success";


                },

                onfinish: function () {
                    //your records request has been

                    var dataURL=document.getElementById('signature').toDataURL('image/png');

                    $.ajax({
                        type: "POST",
                        url: "../ws/email-request-records.php",
                        data: {
                                "name": $("#requestName").val(),
                                "dob": $("#requestDOB").val(),
                                "phone": $("#requestPhone").val(),
                                "email": $("#requestEmail").val(),
                                "address": $("#requestAddress").val(),
                                "requestPersonOrFacilityName": $("#requestPersonOrFacilityName").val(),
                                "requestPersonOrFacilityAddress": $("#requestPersonOrFacilityAddress").val(),
                                "requestMethod": $("#requestMethod").val(),
                                "requestMethodValue": $("#requestMethodValue").val(),
                                "request_from_date": $("#request_from_date").val(),
                                "request_to_date": $("#request_to_date").val(),
                                "requestNotes": $("#requestNotes").val(),


                                "signatureimageDataURL": dataURL
                        },

                        success: function (msg) {
                            //alert(msg);
                            console.log(msg);

                            //if (msg.substring(0, 7) == "success") {
                            //    $('#email-sent-success').removeClass('hide');
                            //    //$.wait(function () { $('#new-account-success').addClass('hide'); $('#newaccount').modal('hide'); }, 8);
                            //    return;
                            swal(
                            'Good job!',
                            'Your records have been requested.',
                            'success'
                            );


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("An processing error occured.\n\nStatus: " + textStatus + "\n\nmessage: " + errorThrown);
                        }
                    });


                }
            });

            var sigCapture = null;
            sigCapture = new SignatureCapture("signature");

            $("#btn_clear_signature").click(function () {

                //alert("Signature Cleared.");
                sigCapture.clear();

            });

            $('#request_from_date').datepicker({
                format: 'mm-dd-yyyy',
                autoclose: true
            });

            $('#request_to_date').datepicker({
                format: 'mm-dd-yyyy',
                autoclose: true
            });

            $('#request_from_date_calendar').click(function () {
                $("#request_from_date").focus();
            });

            $('#request_to_date_calendar').click(function () {
                $("#request_to_date").focus();
            });

            $('#requestMethod').on('change', function(){
                var selected = $(this).find("option:selected").val();

                if (selected == "Email") {
                    console.log("requestMethod:email");
                    $('#requestMethodValueLabel').text("*Email Address");
                    $("#requestMethodValue").attr("placeholder", "enter email address");
                } else {
                    console.log("requestMethod:fax");
                    $('#requestMethodValueLabel').text("*Fax Number");
                    $("#requestMethodValue").attr("placeholder", "enter fax number");
                }


            });

            var blank_signature = signature.toDataURL();

        }) //document ready

        $(document).on('change', ':file', function () {

            var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

            //input.trigger('fileselect', [numFiles, label]);
            $('#fileToUploadTextbox').val(label);
            $('#responsetext').text('');
            $('#progress_bar').addClass("hide");
            $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
            var selectedFile = document.getElementById('fileToUpload').files[0];

            //alert(selectedFile.name);
            //alert(selectedFile.size);
            //alert(selectedFile.type);
            //alert(selectedFile.lastModifiedDate);
            //alert(selectedFile.lastModified);

            var max_file_size = 25000000; // max size 25mb

            var file_ext_type = "";

            switch (selectedFile.type) {
                case 'application/pdf':
                    file_ext_type = "pdf";
                    break;
                //case 'image/png':
                //    file_ext_type = "png";
                //    break;
                //case 'image/jpg':
                //    file_ext_type = "jpg";
                //    break;
                //case 'image/jpeg':
                //    file_ext_type = "jpeg";
                //    break;
                //case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                //    file_ext_type = "docx";
                //    break;
                default:
                    file_ext_type = "???";
            }

            if (selectedFile.size > max_file_size) {
                $('#responsetext').text("the file size ( " + (selectedFile.size / 1000000).toFixed(2) + "MB ) exceeds the max file size of ( " + max_file_size / 1000000 + "MB ) and cannot be uploaded");
                $('#button-upload').prop('disabled', true);
                $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
                $('#progresstext').text(0 + '%');
            }
            else {
                if (file_ext_type == "???") {
                    $('#responsetext').text("the file type ( " + selectedFile.type + " ) is not permitted and cannot be uploaded");
                    $('#button-upload').prop('disabled', true);
                    $('.progress-bar').css('width', 0 + '%').attr('aria-valuenow', 0);
                    $('#progresstext').text(0 + '%');
                }
                else {
                    $('#button-upload').prop('disabled', false);
                }
            }


            //alert(selectedFile.name);
            //alert(selectedFile.size);
            //alert(selectedFile.type);
        });


    </script>
</body>
</html>