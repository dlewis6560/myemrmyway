<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Mail Form</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet" />
</head>
<body>

    <form class="mailform" id="mailgun">
        <label for="name">Name</label>
        <input type="text" id="name" name="name" placeholder="Name" required>

        <label for="email">Email</label>
        <input type="email" id="email" name="email" placeholder="Email" required>

        <label for="subject">Subject</label>
        <input type="text" id="subject" name="subject" placeholder="Subject" required>

        <label for="message">Message</label>
        <textarea id="message" name="message" rows="6" placeholder="Message" required></textarea>

        <button type="button" id="btn_mail" class="btn btn-success">Send</button>
    </form>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {


            $('#btn_mail').on('click', function (e) {

                alert('here');

            })


        });
    </script>



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../Scripts/jquery-1.9.0.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
</body>
</html>
