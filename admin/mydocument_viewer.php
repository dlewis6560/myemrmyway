<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>MyEMRMyWay :: My Document Viewer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="MyEMRMyWay App" />

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet" />


    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />

    <link href="css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" />

    <link href="css/base-admin-3.css" rel="stylesheet" />
    <link href="css/base-admin-3-responsive.css" rel="stylesheet" />


    <link href="../Content/bootstrap-dialog.min.css" rel="stylesheet" />

    <link href="css/pages/dashboard.css" rel="stylesheet" />
    <link href="css/plugins/easyWizard/easyWizard.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="../Content/sweetalert2.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-datepicker3.min.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php


    include("../include/incConfig.php");
    session_start();

	//make sure we have a valid sesion
	include("../include/session.php");

    use Urlcrypt\Urlcrypt;
    require_once '../../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    $pdf_directory = "c:\\inetpub\\wwwroot\\emtelink\\new\\Uploads\\" . $subId . "\\";

    $encrypted = htmlspecialchars($_GET["doc_id"]);
    $decrypted = Urlcrypt::decrypt($encrypted);
    $mode = "";

    list($docid, $mode, $starttime) = explode("|", $decrypted);

    $strSQL = "SELECT file_name ";
    $strSQL = $strSQL . "FROM [user_docs] where uniqueId = " . $docid . " ";

    $recordset = $database->query($strSQL);
    foreach($recordset as $data)
    {
        $file_name = $data["file_name"];
    }


    $pdf_fileName = $pdf_directory . htmlspecialchars($file_name);


    $tmp_directory = "c:\\inetpub\\wwwroot\\emtelink\\new\\newsite\\MyEMRMyWay\\tmp\\" . $subId . "\\";
    $tmp_filename = $tmp_directory . "mydoc.pdf";

    if(!is_dir($tmp_directory)){
        mkdir($tmp_directory);
    }

    copy($pdf_fileName,$tmp_filename);


    //$pdf_fileName = $pdf_directory . "hand.pdf";
    //$pdf_fileName = $pdf_directory . "records_request.pdf";
    //echo $pdf_fileName;

 //   $pdf_base64_data = base64_encode(file_get_contents($pdf_fileName));



    ?>

    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }

            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }

        @media (max-width: 768px) {
            .table-responsive {
                width: 100%;
                margin-bottom: 15px;
                /* overflow-x: scroll; */
                overflow-y: -webkit-paged-x;
                border: 1px solid #ddd;
            }
        }

        .form-group {
            margin-bottom: 20px !important;
        }

        .error_text {
            background-color: #FFE1E1;
            color: black;
            padding: 5px;
            font-size: 14px;
            font-weight:600;
        }

        textarea {max-width:95%;}

        .main {

            height: calc(100vh - 174px);
        }

        .widget-content {
            height: calc(100vh - 230px) !important;
        }

    </style>


</head>

<body>

    <?php

    $PageSize = 10;
    $MaxPages = 0;

    //get user info for 2nd step in wizard
    $recordset = $database->select("user", [
        "address",
        "firstname",
        "middleinitial",
        "lastname",
        "dob",
        "gender",
        "city",
        "st",
        "zip",
        "student_email",
    ], [
        "userid" => "$userid"
    ]);


    foreach($recordset as $data)
    {
        //demographic data ########################################################
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $middleinitial = $data["middleinitial"];
        $address = $data["address"];
        $city = $data["city"];
        $state = $data["st"];
        $zip = $data["zip"];
        $gender = $data["gender"];
        $student_email = $data["student_email"];

        $dob = "";
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data["dob"]);
        if(!$myDateTime == ''){
            $dob = $myDateTime->format('m-d-Y');
        }

        $request_name = $firstname . " " . $middleinitial . " " . $lastname;
        $request_dob = $dob;
        $request_address = $address . " " . $city . "," . $state . " " . $zip;
        $request_email = $student_email;
        $request_phone = "";


        //echo "<tr><td>" . $userid . "</td><td>" . $firstname . " " . $lastname . "</td></tr>";
        //########################################################################
    }


    //$strSQLCount = "SELECT Count(uid) as REC_COUNT ";
    //$strSQLCount = $strSQLCount . "FROM [user_docs] " . $where;

    //$recordset = $database->query($strSQLCount);
    //foreach($recordset as $data)
    //{
    //    $count = $data["REC_COUNT"];
    // }

    //$MaxPages = ceil($count / $PageSize);

    //$strSQL = "SELECT uniqueid,doc_name,doc_description,status,file_name ";
    //$strSQL = $strSQL . "FROM [user_docs] " . $where . " ";
    //$strSQL = $strSQL . "ORDER BY doc_name ";


	//Navbar
	include("../include/navbar.php");

    ?>

    <div class="main">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <!--    <div class="well">

                        <h4>Manage Users</h4>

                        <p>Select an Action ...</p>

                    </div> -->

                    <div class="widget stacked " style="padding-top:5px;">

                        <div class="widget-header" style="vertical-align: middle">
                            <h3>Document Viewer - 
<b>
    <?php echo htmlspecialchars($file_name); ?>
</b></h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content text-center" id="widget_content">

                            <!-- 16:9 aspect ratio -->
                            <div class="">
                                <iframe id="doc_iframe" style="width:100%;min-height:300px" class="embed-responsive-item" src="https://emtelink.com/myemrmyway/components/pdfjs-viewer/web/viewer.html?file=..%2F..%2F..%2Ftmp%2F<?php echo($subId); ?>%2Fmydoc.pdf" allowfullscreen webkitallowfullscreen></iframe>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>


            </div>



        </div>


    </div>
    <div class="text-center">
        <a href="javascript:history.back()" class="btn btn-group-md btn-warning" style="color:black;" role="button">
<i class="icon-arrow-left"></i>&nbsp;Back To Documents</a>

        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">Return To Home Screen</a>
    </div>
    <br />
    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?>



    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>

    <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="../Scripts/bootstrap-dialog.min.js"></script>

    <script src="./js/plugins/easyWizard/easyWizard.js"></script>
    <script src="../Scripts/sweetalert2.min.js"></script>
    <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $.wait = function (callback, seconds) {
            return window.setTimeout(callback, seconds * 1000);
        }


        $(document).ready(function () {

            $("#doc_iframe").height($("#widget_content").height());

//        function renderPage(num) {
//            pageRendering = true;
//            // Using promise to fetch the page
//            pdfDoc.getPage(num).then(function(page) {
//            var viewport = page.getViewport(scale);
//            canvas.height = viewport.height;
//            canvas.width = viewport.width;

//        // Render PDF page into canvas context
//    var renderContext = {
//      canvasContext: ctx,
//      viewport: viewport
//    };

//    var renderTask = page.render(renderContext);

//    // Wait for rendering to finish
//    renderTask.promise.then(function() {
//      pageRendering = false;
//      if (pageNumPending !== null) {
//        // New page rendering is pending
//        renderPage(pageNumPending);
//        pageNumPending = null;
//      }
//    });

//  });

//  // Update page counters
//  document.getElementById('page_num').textContent = num;
//}




            //// atob() is used to convert base64 encoded PDF to binary-like data.
            //// (See also https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/
            //// Base64_encoding_and_decoding.)
            ////var pdfData = atob('<?php //echo $pdf_base64_data;?>');

            //// Loaded via <script> tag, create shortcut to access PDF.js exports.
            //var pdfjs = window['pdfjs-dist/build/pdf'];

            //// The workerSrc property shall be specified.
            //pdfjs.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

            //// Using DocumentInitParameters object to load binary data.
            //var loadingTask = pdfjs.getDocument({ data: pdfData });
            //loadingTask.promise.then(function (pdf) {
            //    console.log('PDF loaded');

            //    var options = options || { scale: 1.25 };
            //    var canvasContainer = document.getElementById('canvas-container');

            //    function renderPage(page) {
            //        var viewport = page.getViewport(options.scale);
            //        var canvas = document.createElement('canvas');
            //        var ctx = canvas.getContext('2d');
            //        var renderContext = {
            //            canvasContext: ctx,
            //            viewport: viewport
            //        };

            //        canvas.height = viewport.height;
            //        canvas.width = viewport.width;
            //        canvasContainer.appendChild(canvas);

            //        page.render(renderContext);
            //    }

            //    for (var num = 1; num <= pdf.numPages; num++)
            //        pdf.getPage(num).then(renderPage);

            //}, function (reason) {
            //    // PDF loading error
            //    console.error(reason);
            //});



        }) //document ready

        $(window).resize(function () {
            $("#doc_iframe").height($("#widget_content").height());
        });


    </script>
</body>
</html>