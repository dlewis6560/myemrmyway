<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>MyEMRMyWay :: ID Cards</title>
    <meta name="description" content="MyEMRMyWay App" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="./css/font-awesome.min.css" rel="stylesheet" />

    <!-- <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet"> -->

    <link href="./css/base-admin-3.css" rel="stylesheet" />
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet" />

    <link href="./css/pages/dashboard.css" rel="stylesheet" />

    <link href="./css/custom.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="./css/bootstrap-toggle.min.css" rel="stylesheet" />

    <link href="../components/datepicker/css/datepicker.css" rel="stylesheet" />
    <link href="../Content/sweetalert2.min.css" rel="stylesheet" />

    <?php

    include("../include/incConfig.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    session_start();
	include("../include/session.php");

    $id_value =  htmlspecialchars($_GET["id"]);
    $decrypted = Urlcrypt::decrypt($id_value);
    $mode = "";
    $starttime="";
    //echo "id_value = " . $id_value;
    //echo "decrypted = " . $decrypted;

    if ($id_value == "") {
        $mode = "insert";
        $WIDGET_HEADER_TITLE = "Add New ID Card";
        $instruction_content = "Enter the new ID Card info below and then tap the Save button.<br />";
    } else {
        list($cardid, $mode, $starttime) = explode("|", $decrypted);
    }

    //echo $mode;
    $endtime = time();
    $timediff = $endtime - $starttime;

    $IDNumber = "";
    $Description = "";
    $EditDate  = "";

    if ($mode == "edit") {

        $recordset = $database->select("user_ids", [
            "idnumber",
            "description",
            "EditDate"
        ], [
            "uniqueid" => $cardid
        ]);

        foreach($recordset as $data)
        {
            $IDNumber = $data["idnumber"];
            $Description = $data["description"];
            $EditDate = $data["EditDate"];
        }

        $WIDGET_HEADER_TITLE = "Editing " . $IDNumber . " record.";

        $instruction_content = "Make changes and then tap the Save button.<br />";
    }

    ?>
    <style>
        ul, li {
            margin: 0;
            padding: 0px;
            list-style-type: none;
            text-align: left;
        }

        #pswd_info {
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_info h5 {
                margin: 0 0 10px 0;
                padding: 0;
                font-weight: normal;
            }


        .invalid {
            background: url(../../images/redx.jpg) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #ec3f41;
        }

        .valid {
            background: url(../../images/green-ck.jpg) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #3a7d34;
        }


        @media screen and (max-width: 767px) {

            .widget .widget-content {
                padding: 0px 0px 0px !important;
                background: #fff;
                border: 1px solid #D5D5D5;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
            }
        }

        .form-control {
            color: #000000 !important;
            background-color: #F7F7F7 !important;
            font-weight: 600 !important;
        }

    </style>
</head>

<body>

    <?php

	include("../include/navbar.php");

    ?>

    <div class="main">
        <br />
        <br />
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h3>
                        <strong>ID Card</strong>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 hidden-xs"></div>
                <div class="col-md-10 col-xs-12 center-block">
                    <div class="panel-group accordion">
                        <div class="panel panel-warning open" style="border-color:black">

                            <div class="panel-heading" style="text-align:center">

                                <h4 class="panel-title">

                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                                        <span style="font-size: large;color:#525252;">
                                            <i class="icon-info-sign" style="color: #525252"></i>&nbsp;
                                            <u>Instructions</u>
                                        </span>
                                        <span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to Toggle)</span>
                                    </a>
                                </h4>

                            </div>

                            <div id="collapseOne33" class="panel-collapse collapse in">

                                <div class="panel-body">

                                    <p style="font-size: medium">
                                        <?php echo $instruction_content; ?>
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-1 hidden-xs"></div>
            </div>
            <br />

            <div class="row">


                <div class="col-md-1 hidden-xs"></div>
                <!-- /span4 -->

                <div class="col-md-10 col-xs-12">

                    <div class="widget stacked ">

                        <div class="widget-header">
                            <i class="icon-credit-card"></i>
                            <h3>
                                <strong>
                                    <?php echo $WIDGET_HEADER_TITLE ?>
                                </strong>
                            </h3>
                        </div>
                        <!-- /widget-header -->
                        <div class="widget-content">
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile">
                                    <form id="id_card_form" class="id_card_form form-horizontal col-md-12">
                                        <fieldset>
                                            <div class="form-group">
                                                <label for="id_number">ID Number</label>
                                                <input type="text" class="form-control input-md" id="id_number" name="id_number" placeholder="enter id number" value="<?php echo $IDNumber ?>" />
                                                <small id="emailHelp" class="form-text text-muted">
                                                    examples:
                                                    <br />-Government Issued Id
                                                    <br />-Driver's license
                                                    <br />-Healthcare Provider
                                                </small>
                                            </div>

                                            <div class="form-group">
                                                <label for="id_description">Description / ID Type</label>
                                                <input type="text" class="form-control input-md" id="id_description" name="id_description" placeholder="enter a description for this id card" value="<?php echo $Description ?>" />
                                                <small id="emailHelp" class="form-text text-muted">
                                                    examples:
                                                    <br />-Veterans Id
                                                    <br />-Florida Driver's License
                                                    <br />-Medicaid Id
                                                </small>
                                            </div>
                                            <hr />
                                            <div class="form-group">
                                                <div class="col-xs-6 text-right">
                                                    <button type="button" class="btn btn-warning" id="btn_cancel" style="color:black">
                                                        <i class="icon-arrow-left"></i>&nbsp;Back To ID Cards
                                                    </button>
                                                </div>
                                                <div class="col-xs-6 text-left">
                                                    <button type="button" class="btn btn-success" id="btn_add_update_id_card">Save</button>
                                                </div>
                                            </div>
                                            <!--
                                                <hr style="border-top: 1px solid #8c8b8b;margin: 1px 0px 1px 0px;" />
                                                <div style="background-color:#EBEBEB;height:24px;" class="text-center">
                                                    <h4>
                                                        <strong>
                                                            <u>ID Card Pictures</u>
                                                        </strong>
                                                    </h4>
                                                </div>

                                                <div class="form-group" style="padding-top:10px;">
                                                    <div class="col-md-12 text-center">
                                                        <h5>
                                                            <strong>
                                                                <u>Card Front</u>
                                                            </strong>
                                                        </h5>
                                                        <img id="card-front" width="312" height="194" class="img-round img-responsive center-block" style="border-width:thin;border-style:solid;border-color:black;margin: 0 auto;"
                                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATgAAADNCAIAAACjNfsjAAAALHRFWHRDcmVhdGlvbiBUaW1lAFNhdCAyMCBKYW4gMjAxOCAyMjowMzoyOCAtMDUwMJyJA54AAAAHdElNRQfiARUDCCVMZH4iAAAACXBIWXMAAArwAAAK8AFCrDSYAAAABGdBTUEAALGPC/xhBQAAGgdJREFUeNrs3Xl4G+WdB/C5pJFGo1uj+7BkyZd85PCRTQgQAgHCkhISAgTIcl9bIAQaylFueDjD2fBsFtgCu5SFlm36sBxLoYQt2+4CpTkI5CCJEye2Yzu2Y8uWde4khtRNHFszGmkO/T7kj+CM5ZGlr97fO/Me6IHESOvwEAIAkDDidz37l238XOzTAABMhEhnsyOZjNinAQCYCCb2CQAAJgdBBUAGIKgAyAAEFQAZgKACIAMQVABkAIIKgAxAUAGQAQgqADIAQQVAomZT+iN/h6ACIEWNFP1AoOLI/xJinw8A4GgmnHg6XEsj6JGvQIsKgLSwmXw+WFWvNx31RQCAVLBt6HUO79/bPUd9HYIKgIScrDf9rKwKQ9Gjvg5BBUAqIqTmhYp6ihjnypFULiaxHxhmnPCoyXotHdDRVWqNXa0xqtQGtfrIMYl0eiCV7E2MtCbiW4aHdg4Nro/HDiQTQ9ms2KcPQL5oDFsTqXdrqHH/VeSgsvms11BzzcyJJms1RTOkFj+m0Z/AcCq1Kx776mDfR/09v+vr6c2kxX06APCjQtBH/RXTDObjHSBaUGs11AKL4wK7O0DRKJdwjqUliGrayP5Z6g6woV3X2/VWd/sHfT0HIbFAVm5y+S92ByYIQrGDSqLobL3pGlfgBJNVR6gEfGQ2tGcwrnk259748Fv7217bv++7RLzIzw4AHhaZmZ8EIhM3V8ULKlvlnqI3rfCVzzBaCaxQF7EwFPVpqRWBistdgdc79zy7r3VfKlmIH8R+xtSQ2iBF61VqFOFZEQCJG06n2uOxL4cGhwt2HaRBq3s6UqfFJ0likYI6Rau7yx9hm7tj/2lLa+eu9gP5/4gTGkI6LXnkf01q8npfeKnD91Tbd2s62mJZwVZaNGP4tWyhYvf6KF1RfnlAZAeTid92ta/au3O70DVaQE2+WjXVqFJPemTBg0pj2HJX4DpvSH+cQvfjv+x465MN+f+gmoBjbFBHsXG9N1i90Oa6c+e3/z3Yn/9PmUHpn4/URXR63v1qIDsGlZrtQJ5pc9yx49s3ezqEuv5hx4mXI/VsUZbLwYW9j1qroX5TPX1lWaVe0O4oJ2yiphjMb0ab7vYEyfzS1UzpX6ueVkEbIKUlyKrWPBupW8a4BXk0toV8vjzaaLTkeHyhgsq+kS+xOt+vn9FsshboR3CiI4hbyyp/Wz2tTE3yewQDhv+icopDoxX7qQDRkDj+WHlNi86Q5+MQCLoqWHW6zZn7J35Bgso2XPd4y5+uqDfkUHwX09+ZmbXRppm8ftE3ugIeLcXjG4GSkDhxbyCST2nGtqU/dZctcwU41WXCB5VC0dWhmuX+sKpgl3bzwXYJ/j3aON+Qa8kxyojh59vdUPECVrPROiW3juW4rrV7bg5EMI7vJYGzZMCwVyL1ix1erudRTEaV+uXqaeeZmdy/pV6rC+Tx2gAlYVug2UaeHbrzLfYHwlEebZiQQWVTujoUPZ1xSb/loQjimYr6xTlnNQx3YsAYDbw+tc8y2Z6K1OEon9AJFlQcQR4vqzrbLsw1sSKgCdWzFfVzaGMuB6t4/XKBUqm5N0Un600/j9TRfG9/CPb+u90TPN/pk35bOhb7W1td2VCpmfwSUVK48RJAAYY47lQ6kza8XjPdwveOAyJUUM8x2272c+4fS4FHQ/1zuNaC4xMftnUoloXJdOAHG4cGcz+4hdL/onIq77Z0lABBrSS1j4X49I8losFgvtcXmfgzZlM81hYfEvtMgSQkM+l1/bkOem2k6Ferpzrzvv2eb7o0KLqqvCb/8xARW64vcwcWmGwTHNOfTv+ysw0aVcD6n97ujUMDuRzZqKXfqml05dC3mlS+Qb3S7p014VtcFtii/aFQtYuYaHjGc+27d3IpeIAixVLJ+3ZvT+RwZDOlZ/ulVlIjyM/NK6helfo27rdupcmv1d3mDU5wQH8mfdW2DV0wx7WEJTOZe3Z880UOzalQFe8R/IPKfuedvnAuM3TkYqnT1zJmE4FjfR4bWLjpi28G+qEGLkFdI/Ebtqxf07Vv0iNP0pvW1ja7hah4j+Af1Cad/lz53DXNhQYnfuoPT1webBgenLfp/x7c+e222EAiDQu+KF8qk2mPD7+8d+e8DX98/UDnpMfPNVherZqqF7oB4z8f9SZ32aTT0mVnjsXerDP8b+zgBMewNfDj7a2rO/aEtFRYrdHJ9nI3mFQmm92SGNkVH+pJp3I5foGZeaGivhCTOnkmbZpWN9fqKOjvSBRsf/tWT/CCresnbStj2czGoUFO99OAgrGf1tcw7gfCUTWG5/9o4z4+H8scPuU1p6NOtDA1gvYugOIRCLrSFXgwXFuglCL8gqrHsMXK6p2OxX4ALWFcYp8FkA0Vgj7kD68sqyzomB8+D73QYpfajHBhLWTcOhiFD3JgxvCXwrXXekOFW1hzFJ95cefYFN7guEltS26zakAp86rUb1RN/VFRVhTg3M90qNSNP+zcuLOt68lf/SHPM+joFeZ6zP3/+hGpyqvbXONjrl90wqFfCoadYWE+Hujl9zjK7LsrSxb5fi3m9OG/81CjoV6pmlKZ9/pJOeL8ppqhM5h+mK0zHE9ubt1fnBOd1Hf78l0cWD8m5/PNzG2tW3m8hHYMf7t6mkG8VRdBLkYyGfJwsfrI7m2v93Zx/fazjNafV9TnM22NK85BnS2NVQULzUvR5Sr19mQugzr/BoGiPoo2F/ElBPnges+TDcwVDu+9ZZXCbsgyKc591Ja/3bFcqTAUbdCb838coCQkij7ijzxSXlPklCJcW1QHTni1pbJ60Gy98dc5DBk7ShbJxlJJQhETFRQsnk5rDq8WkMx5rQYnoXquPHr6eNuyFAG3oHpJrW6yxRAUo4zXAlad6fSsDX+CmEpcBslihy8nDeW2Q2etVvcvFfUVxbp0dCxuQS3TaAs39kJq/KSGRrFBjqslmTHsyWAVrdBhW4oxlE5Rh1+jl9pb3zs4yeX9+UbLM5E6Bynm6gjc3k9hUc+1yKwqNVs+DKa4BZVEsTlmBi4mycWHB/YjyHGDih3aIcF/Z6CSFLuQ5BZUaym9/3SEijxUPhRke1UgfQYMf6Ks4jynj99KvMLiFlSDqoSCyhb5NPfP0ezh60kZmFkubexrNLr99PHWAPCryNXh6IkWu9hn+j1uQVVhpXWVxIsTmzl+y4F0etm3XxES+AwGEzhy1XfbcOzYf51FG16smCKpPcE4BJXNaAAG3ExmBMl+OiDAjslAFGx2L2fc94aqRdzRd1zcWlRoJoCCqVH0fn/4KndQgotUcwgqW8u3pVKNYp8xAIXgVqlfDNfNMtukuS0LtxY1XmLXSLpzuxsO5O5k2vhMpC4o4Z01uQV1IDki9gkXTzqb6YF1BpWO7ZReYnM9GKqW+FoI3IJ6IFFCQY0lkwloURWNxrCH/ZF/cJdJs9wdi1tQ20ZKaJ34/lRyGFpU5QqTmn8K1zUaLdJPKcI1qFtHhlOZTKGXh5GI1kT8ILSoCnWqwfxcpM4jn+UmuQV1X3xoKJ0yYJKu5oWyIzYAuxcrD4mgN7r8KwMVog/f5YRbUHenkp2JEYl3u4Uy8Xr5QI4YQvV8qHqezYXLodwdi1sRm0WQLyebE6QM2Wx2EwwwUpYmSv9etOlMxi27lCI8xhr94WC+a4jJQvdIfD1sMa4UbC6vsXvW1jVX0KLN/M4T5/nN6w72xVLJ0TVjVATuMOa7MstAPDE0IsBUMqtem+dVLhP919m2H/d1QwdVGRicuC8QuVAas9V44xzUfYn4+oH+meZDu4yHfMyLKxfneQYvvfvFbz77Ov9n8tBlp3scea2ajf+weEUmm/0v7ktIAgmq11CrI3X1BtmvU8c5qGzb905P52hQcRzTUfnufK5SCXPxTatR5X8yo7oTI+v6S6LCVzD2nX2+1fFoeVQZ1z75FANv93TEc9suUqY+6Oncr+gnqHgWnHg6WPVsRYMyUorwC2p7KvlhD+d1NOUilcm83rVX7LMA/NVqde/UNF7iCkhwthpvfJ5Jlu1YduzJfUFUefm8/8AXsQGxzwLwgR2a9u16v66l1mCSxcBATk+Nj08G+v7U3yP2yRfE6vZdIyU2m08ZzDjxYnn0iXCdYsrdsXgGlW1Mn2rbmVJco7r+YO97vcr8AFK2Zp3+3WjTYqdPqQPR+T+rTw/2rlPWPQy2mH909/YEz234gDgIBFnh9K+tbYnqlbylLf+gsm/o+1u3jihoItjvD3S+q9B6Xql8avKNyoa7Q9U6QuFbE+RVJ3w1HHt5366sInp0fcmRu1u3Ka2UVy7s0Callndrm+fZXJiyrhsd7/nm5ZG9O7YPKeEa6ZOt2zfD4F6Z0GHYI/7IazWNgZLZWzDfoPam0zdt/zqWkve+D7/v6Vzd2Sb2WYCcTKfoD6JN13hDSr1uNC4Bnupng/1PtG6TbwHcNhxbvmNzEq4hSR6JotfZPWtrm+sNZoXdJp2UAF1w9g3+VMfuOtqw0O6R3a+PrQWu37ZxZykt2iZTARX5ZKjqNKtTdu8xQQhzrSyDIMt3fONQkbMsjNjPiINkJrNy+9frBvrEPhEwERxBzjEzj5XX2NSa0kwpIuAmFX2Z9JXbN26Sz5s+lck8sPPb13o6oOSVMhtOPBesXlM1lSG1JZtSRNjdZPYmE4u++fPmQRmsYMKmdNWe7c927Bb7RMBxsaGcqzd9VD/jYreihtfzI/Dzb08mFm/+8+d9kh42kMyk79yx+eG2HXDXVLIsOP6YP/LLaJOUt5koJuE/qNqSI0u3/OXD7g5pXgceTCVXbN34QmcbpFSa2Ia0Raf/z2jT1d6QRlYrehZUQSqKzlTygi3rV7ftkNpUuF1Dg+d//cUrPR1inwgYnw7F7vKG3qmbEdUrbZ5ango1QjKJZO/cve3Lgb6HQtUuCaxHns5mPujuvGXH5r0yH5uhVGwoGyn9qvKaOr2pFIYEclXAocxsY/qr3q4vNw48Hqyca3WIuAbcgcTIw61bX+naBxNNpYltSG9yBf7RF5LaPt/SUfA5BzsT8SVb1i82228PRMp1+mMPqPExZzdV5P+DdBR57BdTmczarn337d62C4Y0SNV0in6qPNoAte6EijE5iG1a3+zd/0F/zxV2z+XugE9DjX1JTpoWZv8I/kPZ7vFnfd2P7vnuj4P90uoogx8YMXy5K3CtN6iDhnQyxZvF159Jr+rY/Vp3+1Kb6yqX36vVFagrMpxOfdrbvaa99ZODvdAflSb08AYTz4SjNbQRGtJcFHu6bVcq+UzH7jWde+abbOcx7pPMNqE+TbPZ7J7h2Nvd7W90tcOENSljG9I7vKHL3AENrvDZ3gIS5zc1nM3+urfrP3q7GEJ1qtE6z2ybbjC7SI0K43zfrD8xsj0+xFa57x/o+nJocDgLda50YQhyisH8RKgmSNHQkHIi5kda5vAd13/r6WD/sJ+yAY12upYupw0NGsqjoSykRoNh6KEx2Sj7XzbLHp9NZZGBVKJ7JP5dfGhjbGBTbGDzcGxvckQ568Eol5NQ/cwXXuLwymtjUomQSu3B9mA3DA2yf5AxoxGcOK5BMQNOsJ++I5nMcCbdnU7F4BaL3LC5XGyxPxisdmi0+T9aaZJKUMfVcWjltDQCQxTkLKqh7glUnGa1y3ozNdFJOqhA1igUu9rhucUfMSpxRewig6CCgjiZNt4XrJoq//0OJQKCCgTmUalv8wSXuvxq7tfwwfFAUIFgVCi61OpY6Y/4SmYVz6KBoAJhNFL6BwIVM802uEFaCBBUkC+GUP3EXbbMHaBgpFHBwG8W8MfWuheyta4vzNa60JAWFAQV8NTM1rplFS0mG8zzLgIIKuCsTE3e6g4ucfpgTaOigaACDnQYdinjXu4LO0iN2OdSWiCoIFdnm6x3+CNRvUnsEylFEFQwCbYDOoWi7/KF51jsJbWBmqRAUMFEPITqFm/oAoeXhtVSRAVBBePTY9jVdu+PvSErdEclAIIKjkah2AILw3ZHA3B3VDIgqOCv2HfDiQbz7b7yRqMFpo9KCgQVfG+qlmYjeprNiUMrKj0QVIBESO1KT3CB3a2FwbpSBS9MSXMQqhtcgUtcfjMswiBtENQSZcbwS+2eG7whG1zUlQMIasmhMWypzXWjN3TU3iJAyiCoJUSNoj8yMzd7Q1HYSEJuIKglAUOQs4zWFb7yqQYzzEqTIwiqwuEIcqrBcrM32GKywq1R+YKgKhb70jbpDLd4Q3MsDI9NfYCkQFCVqUGru9MXPtXqgPkuygBBVRT00Abe+hvdZfMZpwrF4IqRYkBQFYJtN+u0uhXe0BlWBwwwUh54RZWALXRXeELzbU7Y0VCpIKgyNlro3uAOnMW4oNBVNgiqLI0Wujezha7FQRHwIiofvMby00jRP3aXnWVzQaFbOiCossGGciZtvMlTNsfiOLQHOxS6pQSCKgNqFD2BNq7whlqMVmhFSxMEVdLYl2e+yXajJzjdYMZh6EIJg6BKlAHDzzEz13uCVbQBhtEDCKrk2HDiQpvzMlegnKKhIwpGQVAlxEuornD6L3Z4HRqt2OcCpAWCKgnTKfoyh/dcu0eHE9CKgmNBUMVEougJetNVTv8pFkYDA3TB8cGbQxwUii2y2q90+htgyQWQAwhqsQXU5KWM+yKn30FqoMoFOYKgFokKQafr9Fc6fadZ7GY1KfbpAJmBoBacDsXOtdgvcnhbTLChC+AJgloobFFbpaEusDkvdHjtpBY6oiAfEFTh6TBsNm260uWbabLB/r9AEBBUIXkJ9VK7ewnjjuj0cKEICAiCKgAaxU4xWi6ye04yM1och4gCwUFQ+cMRpILULmFcC2zOEKWHbUVB4UBQ+TBi2AKzfbHdPctoVcMEUVB4EFQO2BK3mTYsZlxn21wGQgUlLigaCOrk1CjKlrjnWp1siRukaBVM4AZFB0GdSFBFzrcwixh3vd6ohu1bgHggqOMIqMm5Rssim6vJaCUxWC8XiA+C+j22nGUIFZvP82yuaQYzDMcFkgJBRdyE6gwzc6bFPtNo0avUYp8OAOMo0aCyT7tCQ51ssCy0ORoMZhKDUQpA0korqDSGRTW6U822M6yOiJaGzSCAXJTEO5XB8ZkGy+lm22yj1afVwUQWIDuKDaoBxSop3UkGy2lmG1vcaqG4BXKmqKD+f3v3+9LGHccBPPf71/cuiQZrqiZzJvG3rdZugi3V1UHXgc86usFw0D2ug479Fe3f0Ee1uD4T9kD7ZJ0r06wrWIYN1l/U6pCmYmpdzCJmX3oQdStbTmMuF9+vB8fdcRe/B775fO/X9xSG8QvSecP7kae8Q/eelGUBNz/hXW7duvnvlTdufHukf/Thw59jsdi1a18fYN9SCOr7gviB4T1vlJ3VPXUaQTihOE1MTBx4X0cGVWfY92TljKZ3ucvO6V6/ouHrZnAAR11C88gZQRVdjMHzjZLSaXi7dU8DMSpECQPhwlEwe8XXrw8+ePDj9PR0JpMZHPyGrkmn07TvOjMzs7GxIUlSMBi8cKHHMIy9e9HkR6PRqamp168ThuFub2/v7Ox07e9pZ7e01Kri/V8XXa5mWW0i7g+J0aa7Qwox8DQCFApNKc1bdpGmdGjoTjweNxdTqRRN7PLy8sDAV4qy+/2R8fHxaHTSnKdZpT8iikJb26nDt6eIguphuWpJblW0FuI+SwxaNnVeZF0u9Gmh8GKxWH9/fygUNv/9Hj/+jabU4/Fcvvyp3+9/9So+MjKytrY2OTnR09Ob3evp0+krVz6rqanZ3NwcHR1dXFygaadBNevnwWqpybag0t4s4bhaUW5USTsxWokRkLVyQZDQoYVC+ce1370Rot3acDiSXaS5pdOLF/toSulMebmPzt+79/3c3NzeoF669EkgEKAzhJDe3t7btxeyRfiQCpqKKl6oV7Q2TW/S9Iiq10qKV5IL2QCAHEUikb2LtHjSKS2V2TXm/Pr6+t7NzJSaysrK6HRnZycv7TnCoAYE0SdKrbIW0kinatRpxCfKLMPgwSAoEv/RC5Wkfa9PmXlj7RszID9BlVyMxnF+QWyU1bBmtKskoJKTokR4gcd4COB89OyUFs+lpaVszVxZWaFTn89XmAYcMKiCy1UvyUGFnFJJnaqFFRKUFI8o4cIPlKRQKPzo0a/3749lLyaNjY3S9Q0NDVZ/6sWLperqGqt75RTUcpY7IUo0ii2qVqfqp1USVInM8bgkC8dEV1fX/PwcPVMdGrqTXUnLaUfHmdx/hG4fj8eHh4ddh7+PSrOnsxzh+DZZrdV02okNqaRKVtycIGNoaTiu6Cnr1aufj4//NDs7m0wmdV2PRCLd3ecEwcInS/r6PqZ1mKbdMNxWG8DcXXn+xZMonfPzwndVtXWaHpFVnyRjLC8Ae81vJE4/+cWc362olaL0pT+I4aQBipCFi0nb29tbqZTdDQYoEfQ0UlGUHG/5WAjqxps3z57N2n10ACWC47jWlmZRzOkJdtzkBHAABBXAASx0fXmeNwzd7gYD5AU9Q8zY2wLWykcYLASVaFr9/ieVAaAwLAQVTzsA2MVCUP9MJl++zM/LdQD2okUnY3PPl3Z9GX9lJZ/bKPAWgppKpVZXV20+OIBSwXHciYqKHDfGVV8AB0BQARzA2lXfSDhkd4MB8oBh2EwmP4OkHKYROZ6guiwFVRAEj8dj87EBHEvo+gI4gIWKmkgk5hcW7W4wQB4wDJOx+/4My7KNDfU5PpRvIag7mUw6nbb32ABKBmfl3W90fQEcwEJFZRk2xzINAP/r7SvjR/BQvq6TluYmu48OoHTk3vu1UlHfsvvQAI4jBA/AARBUAAdAUAEcYPccNbG9/UP8D3zTCaBIrG4ls/O7QZ3/a2tg9ne72wYA74D6CeAACCqAAyCoAA6AoAI4wN+dOzL8xVQB3gAAAABJRU5ErkJggg==" />
                                                        <br />
                                                        <a href="#" class="btn btn-success" role="button">
                                                            <i class="icon-upload" style="color: #ffffff;"></i>Upload
                                                        </a>
                                                    </div>
                                                </div>
                                                <hr style="border-top: 1px dashed #8c8b8b;margin: 1px 0px 15px 0px;" />
                                                <div class="form-group" style="padding-top:10px;">
                                                    <div class="col-md-12 text-center">
                                                        <h5>
                                                            <strong>
                                                                <u>Card Back</u>
                                                            </strong>
                                                        </h5>
                                                        <img id="card-back" width="312" height="194" class="img-round img-responsive center-block" style="border-width:thin;border-style:solid;border-color:black;margin: 0 auto;"
                                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATgAAADNCAIAAACjNfsjAAAALHRFWHRDcmVhdGlvbiBUaW1lAFNhdCAyMCBKYW4gMjAxOCAyMjowMzoyOCAtMDUwMJyJA54AAAAHdElNRQfiARUDBx8N8LtfAAAACXBIWXMAAAsSAAALEgHS3X78AAAABGdBTUEAALGPC/xhBQAAErFJREFUeNrt3Xlwm+WdB/D3fqX30n3LkiVL8iHLjolzEAjkhJBASAIFAuUqx7K7JZRzt7Adzu60Xa4sAWaZXtudKcdudwPbnU45trMtLUzDTgkEKCE4gS0kwUmoc9jxqX1iZxxP4sSvZEnv+0rfzx8eWXn95pGlr5/f+77P+zz0voH+T/p6KQAwMe7VvV9c9e4mo5sBAKfCDefz/SMjRjcDAE6FMboBADA1BBXAAhBUAAtAUAEsAEEFsAAEFcACEFQAC0BQASwAQQWwAAQVwKTmS+r4YwQVwIw6JeXBeGb8W87o9gDA8Zws93iqVaHo8WfQowKYC8nkhkRTm+o87kkAMAvSh/5lIHq+P3Lc8wgqgIksUJ3fqm9iaPq45xFUALNIi7anM20SN8mZIwQVwBQUhnkm3Ra2SZP+K4IKYDyeor8by5ymuU62AYIKYLxbQrGvhuP0CYem4xBUAINd5PLdGU+fIqUUBjwUjaeoFtGekBSVF2iKnv4OwYT6hod2Hj70v70H+/L5Mv0X7Xb58XTOzk6RRAS1YC6GvYkUKv5onSQb3RaohP2DAy9173z0s+3bBg6Xds9xQfxJU4eDF6bcEkEtzFxJ3ZDOpWX11IUKVBONF8gB5HnewN1df3xh767hEu3Wz3I/TLeRokzPxghqAWZL6r80nxaw2Y1uCBjAI9j+MZ2zMcyPuj+f/t5I8DY0ZDsdbp3b42SSXhrD/rhxBlJay0SW/V5DyxxZm+Z+OIp+NNF0rjeovy5DUPVaF4pH7NL09wOWJrLcffG0OI0DH9KX/m24/qpQvKCjJwRVF4GmL/WHcVwKxGyHZ4a+A8tJ3eSP3BpPMwV+lhBUXVwsF5/GewPVhGeY+Q5PcT97qdv/YCpL9lDoDyKoumgcb3QTwETai/qrvcLpfSydY+liQoeg6oJfE0wkFH4QtEB1PpnOKcX+xcflGV2wLiVM1FvgSqXzFO2nLTOLTimFrkKnnqHBfNkGkYHlvNt7UP/GcyT1x40d00kphaDq9OfhoT8d7jW6FWAKgyPD/9OzT+fGnZLyk+aO4LQvvyOougzk88/u/hM6VSB+9+Wed3sP6Nmy0678a0tnyFaCy+8Iql5P7Px0eyEFD1SlQ0OD93+6bUDHlrMllRyXekRbSf5fBFWvnpHhGz56p7vU90+AhQyOjNzb9cFbOrrTUlW84xDUAmw6dGD1lrc+ONCDGrgGdfcfvvnDzc/oGJF/tup8sXV2uBQV7zhcninMO30Hz9ny+xsD0csC0bhNEljW6BZBeQ2NjHQP9P9i764nPt/x8UD/lNsv1tw/bGxXddxiWhAEtWCkBv6HnZ88tev/knYpJdjkwoeDgVWM5PMfDvTvONy7d3hIz/YrXb6nM21qGcaxIahFOpQfebf3YEHX06CKkb/Wf+ELP5jKCkxZiiwEFWC6OIq+IxS7s76xiNH2uv8LAJgGnqIfiqVuiCS4ch4EIagAxXMx7Ppky4Xlv1cZQQUoUpQXfpBum+v0VGBGAQS19PA7Nb88dXQu5uHRx0VosUn/3DSjcdrzJ+mED1WJ+Rn235tPw43mJtc/MiKOHlJ+59OPfvpld6E/vsLheTLT5hbEijUYQS0xjqbrJMVVwbcQpqPQa54kMNcFovfVN8qV/VuMoALoJdL0t+tS10UTxU2nMh0IaonlqfyhoUEO8xWa2+HhYdvo8M9B3XM1BDn+iYbsud6gIQ1GUEts9/DwGe+8iZia3AiVZ0ZPJ/WO6FqhotUu/yjTlqnUqaMTIagl5mKYRxJNylSLc4GxeoeHpNH36Ac7P/nF/i9PvfFyh3t9OhcQjVwkAZ+nEhNpZqHLh5NJVvHKvi8o6qRBZY6skBC7J94oGn2bFIIKMDmNYR+uz3wlWFf5U0cnQlBLLD96PmkEd5abG3mPxpafPtkcADFefCqVPcvtN7qlRyGoJbZvePiqP/6BM8HfYDiF8bO+H/UdOvFfz1C072dmmGpNMAS1xPqp/K8P9BjdCigSye7XfOH7ks3luPl7OhBUgKMEmn4glrohnCjfbaVFQ1ABjgjzwvdTuTNcXnMuromgAlALFMf6dC5h4pU1EVSoaeSg9Epv6KFks1bqeQNLC0GF2qUwzN/H0leH681Z7k6EoEKNSom2f0rlOh1u86eUQlChNi3RXE+kc5GSTmZfVggq1BaRoteFYnfFM4YP3y0Iggo1xMfxG5LN53hDrBXK3YkQVKgVsyT16XQuoxh2T+l0IKhQ/UjveaM/cm+iSTHZwED9EFSocj6Wuz+eXmuOu9WKhqBCNWuzSU+lc22ay+iGTBeCCtWJfLIv9QS+25A1+ZAj/S8HoNq4We6BWGptMGbC+2CKg6BCtWm1y8+kclnVYYkhRzohqFA9SO95jS/0QMLsI+yLgKBClXCx3CP1jav8kbKuU2oUBBWqwWxZXd/QSspdoxtSLggqWBv5BK8Lxu6MZ2Sumj/M1fzaoOrVCeJjiaYlniBTReeNJoWggiWRw9DzHO7vNGTjdtnotlQCggrWIzPMvdGG6yOJqjxvNCkEFSxmpqSsb8jmVGc1XSadEoIKliHS9Nd84XvqG6vvMumUEFSwhjgvPpJsWuoJ1lRHOg5BBbNjKWqVy/e9hhavYKvNlFIIKpic98jw+vSlwbqqGV5fHAQVTIp0nYtU56OpVjNPYF8xCCqYkZtlvxlJXh2ut1lqrsDyQVDBXOjRgbuPN2RblKq6T22aEFQwEZlmbovUr4s2WGvS3QpAUMEUSNfZKamPNrTkVGfVD9wtAoIKxiMd6S2h+F/XJc22zrd5IKhgsJmS8lhDtr3GhgQWCkEFwzgY9huh+E3RhIyOdCoIKhiAHl1gYn0Kp3b1QlCh0khHenc0eW04bmPx8dMLvymoHIaiFmmuh5MtCUlBR1oQBBUqJMjx36pLXRKI4hppERBUKDuSy4vd/ocSzQGb3ei2WBWCCuWVtUn3xjNLPX5LL6ZmOAQVykWimRsDkdtjaUftTchQcggqlMUCxXF/oqnD+usdmgSCCiUW4YW/iSQuD8UEBieNSgZBhZLhafpyT+CuWLquNubarSQEFUqjU1IfjGfmuby4QFoOCCpMl4/j7wzXXxWOSxhpVDb4zULxSK27ltS6dSlS66IjLSsEFYo0m9S69Zk5Ti/u864ABBUKVi+Id4QTlwTrMPNYxSCoUACZYa7xhb9RlwqINqPbUlsQVNDrAqfn7lg6qzqNbkgtQlBhCuQAdIak/F1daqHbXzvLHJoNggqnEuH426PJywJRBbOlGApBhcmpDHOjP/r1aNKDw1ETQFDheBLNrHT7yOFoHFdHTQNBhWPIp+EszfXNuoZOhxu3j5oKggpHddgVEtGl3iCLXtR8EFSg0qL9rkhipT9sx2Bds8IbU9MCHH9zKH5lKObCJAzmhqDWKBfDXuOP3BxNenFS1woQ1JqjMMzl3tC6aLLOJuGkrlUgqDVEoOkLXb5bo8ksFpKwGgS1JjAUtcLhua2uoUNz4a40K0JQqxxLUUs0963RxBynB5dGrQtBrVrkrZ0la7dHkwvdPh4TAlocglqd2u3yPXWpJZ4A7nepDghqVaGPLOCtrgvXL/cFeZrBGaOqgaBWCdJv5uzybdHkMk8AA4yqD97RakAK3dsiyeXeIFY0rFYIqoWNFbo3h+MrfCEUutUNQbWksUL3VlLougMShzex+uE9tp5OSfl6uH6FN4RCt3YgqJZBQjlPcdwSqV/oDnA0jUK3piCoFiDQ9JmK47Zoco7Dg160NiGopkbenuVO77pIYqbmYjF0oYYhqCalMewql++vIokmRcMwekBQTcfLcmu9wWtD8QZJwYEojEFQTSTK8dcFY18NRAM2u9FtAXNBUE1hpqRcG4iu8UdklkMvCidCUI0k0vSZqvOGYGyR22fDAF04OXw4jCHRzEUe//XBWDumXAAdENRKiwviNb7wFcFYQLShygWdENQK4Sl6pqxeH6xb6va7BNHo5oDFIKhlJ9PMGrf/ikB0jhMLukCRENRyIUVtk026zBtcG4j6RTsORGE6ENTSkxlmvuK8PlQ3z+nF+r9QEghqKUU54XJ/+BJfOC2rOFEEJYSgloBCM4sc7iv8kbNdPjvLIqJQcghq8ViKyoj2S3yhld5gUlKxrCiUD4JaDAfDrHT5L/aHz3B4BNwgCuWHoBaAlLizFe1iX+gCb0jjeJS4UDEI6tQEmiYl7hpPkJS4CUnhcQM3VByCeioJXlzu9l3kC7epDgHLt4BxENRJxAVxscN9kTc0y+ERGcyXC8ZDUI8i5ayP40k+v+INnaa5MBwXTAVBpcIcv8zlO8/tn+dwq7xgdHMAJlGjQSUvO2OTFmju1d5Au+YSGYxSAFOrraAqDJO1yUtc3mWeQNquYDEIsIqa+KT6WHae5j7X5Z3v8NTZZdzIApXxyCMPk6+3337H9HdVtUHVaKZRks/W3EtdXlLc2lHcwsmNJWqc1+ttbm6ZNWuWeT4zVRVUO02HeHG+5lrk9JymusI2G4+Ln1C4PXv2/OY3v96/v2fJkqVGt+Woaghqkhdma675mnuW6myQFYQTijNWo/b392/evJkE9b333kNQp0WlmXqbfaasznW4z1RdIbuM1c2gVERRnDlzJgkqzx+76X/btm1vv/2HXbt2kRirqtrY2HTWWWeNf+Ty+fymTZs++OB90hWTn0okEhdcsPLEPe/bt++FF54/dOgQ2f+CBQsLapU1gipQtMZxzaK9U3OdoTqbFM0viJgIF8rh4MGDb775BnnQ0dEx/uSLL24cf3zgwIG33trEMMz8+fOp0ZRu3Lixq+vjsX8dHBzcunXribvdv39/0SmlzBxUgaKyNqlFccxRtDbVkbIrGkYjQDlNPKXU2Tnr9NPnjX+bTDbMnj07GAySfG7ZsuXll39J+s+xoG7e/DZJKelIFy9e0tTUNDAw8MYbvztuz729vT/72b8VnVLKVEF1MmxUtOXscqvimKVopNtUOYGhKNS0UHmkzyR17MqVK8cK4NWrV+/du+f999//8xFfUqP96tiW5EnylaQ0m82SB3a7fdGixRN3RVL6/PPPkbq36JRSBgaVVLMKyyYEW7OkdChaTtFiNtnD8yIKWjDI2MkkUsp+8cUXr7/++o4d21977bVly5aRpG3c+B87d+6c9Ke6u7vJ10wmc7LdjqWUFNJFp5SqcFAjHN9ol9tktUVWM5KaEO0u0VbJBgBMiVRwgUBg+fLlTz315NatH5KgvvLKyySl9fWJ9vZ2h8OhadqGDU+Mbz80NES+Mie/S9nr9ZKgkvCTw9eJJ6gKUsagxnjBK4g5m5ySlU5Ja5AVr2BjaBoDg8AqSO9Kvu7YsYN8XbVqFTs67Q6pfSdu43Q6yTPbt29PpVKT7mT58hXk6PSzzz576aWX1qxZU9yhXGmCKlK0zLIhXmi2SWlZ65CUmKSEBVHheA7zIYDV7N69+1e/+m/yIBqtG3+SHIvmcjnSN7788i8nbpxKpckB7auvvkIeJxKJgYGB3/729YkXYEm8L7xw1XPPPUtq6Z///D/PP/+CIrJaZFBJ/90o2uJ2pV1SGiQ5bVfiot0piDjxA9Z13EBCWZYXLjxyVJlMJrdu3UryORbRbLaVdI/jm82dO7er62MS4ImXcI4bKWG321evXkOySvbz2muvFjGOQldQPQwbEEQSxVZJbpDUGZISlxQby+GULFQlVVVJOOfOPV1RFPLtOeecS752dXWRT3tTU9PChYvee2/L+MaiKF522VrSi5IQ9vX1aZqjtTV74j5JhUyy+vzzz23evJnjuEJPLNHPfv7p5e/8fvx7kj2VYRWWa7NJCVklRWxKUiI2u4PlbZhaGqCCug70zHjnjbHHx3rUEMffFUk0yGrGJnlFG+byAjCPY0ENCuKVoTimkwYwoQJOJg0NDR3u7ze6wQBVgh49ycTouyxSQFAPHDz40UfbjH51AFWCZdlca1YQdI1gx0VOAAtAUAEsoIDSl+M4TVONbjBASZAjxLyxLWAKWYShgKAqstx48lsEAKB8CggqRjsAGKWAoPb29XV37zG6wQAlQDqdvMGVLyl96VAwyOmbBb6AoPb39+/evdvgFwdQLViWDfj9OjfGWV8AC0BQASygsLO+mXRK//YApkXTTD4/YngjdB6gUgUFled5p9Np8GsDqEkofQEsoIAetaenp2v7DqMbDFACNE3njb4+wzBMc1OjzkH5BQR1JJ8fHBw09rUBVA22kHu/UfoCWEABPSpDMzq7aQCY0ugt42UYlK+qSmu2xehXB1A99Fe/hfSoo4x+aQC1CMEDsAAEFcACEFQACzh2jNozNPRfe3ZiTScAk9h9uG/88bGgdg0cvnrblmL2BwBlhv4TwAIQVAALQFABLABBBbCA/wfkk4iXU5lOsgAAAABJRU5ErkJggg==" />
                                                        <br />
                                                        <a href="#" class="btn btn-success" role="button" id="btn_upload_back">
                                                            <i class="icon-upload" style="color: #ffffff;"></i>Upload
                                                        </a>
                                                    </div>
                                                </div>
                                                -->
                                        </fieldset>
                                        <input type="hidden" value="<?php echo $id_value ?>" id="hidden" name="hidden" />
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="col-md-1 hidden-xs">

                    <!--     <div class="well">

                        <h4>Add User</h4>

                        <p>In order to add a new user, please enter the UserID, Firstname, Lastname and Password, then click the Create Company Button..</p>

                    </div> -->

                </div>
                <!-- /span4 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->
    <div class="text-center">
        <a href="../Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">
            <i class="icon-home" style="color: #ffffff;"></i>Return Home
        </a>
    </div>
    <br />

    <!-- Footer -->
    <?php  include("../include/incFooter-sm.php"); ?>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- /footer -->

    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>
    <script src="./js/libs/bootstrap-toggle.min.js"></script>

    <script src="../components/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../Scripts/sweetalert2.min.js"></script>

    <script>

	    $(document).ready(function() {


			//$('.effective_date_picker').datepicker();
            /*
	        $('#user_type').multiselect({
	            buttonWidth: '100%',
	            maxHeight: 100
	        });
            */
	        //$(".chzn-select").chosen();

	    });


        $('#btn_add_update_id_card').on('click', function (e) {

            if ($('#id_number').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing ID Number');
                modal.find('.modal-body').html('<br /><span style="color:red"><h4>Please enter an <u>ID Number</u>.</h4></span>');
                $('#myModal').modal('show');
                return;
            }

            if ($('#id_description').val() == '') {
                var modal = $('#myModal');
                modal.find('.modal-title').text('Missing ID Description');
                modal.find('.modal-body').html('<br /><span style="color:red"><h4>Please enter an <u>ID Description</u>.</h4></span>');
                $('#myModal').modal('show');
                return;
            }

                    <?php
                    if ($mode == "edit") {
                        $url = "../ws/edit-id-card.php";
                        //$message = "ID has been updated successfully.";
                    }
                    else {
                        $url = "../ws/put-id-card.php";
                        //$message = "ID has been created successfully.";
                    }
                    ?>

            $.ajax({
                type: "POST",
                url: "<?php echo $url; ?>",
                data: $('form.id_card_form').serialize(),
                success: function (msg) {
                    //alert(msg);

                   var msg_type = "error";
                   var msg_title = "Not Saved";

                   if (msg.includes("success")) {
                      msg_type = "success";
                      msg_title = "Successful";
                   }

                    swal({
                        title: msg_title,
                        html: msg,
                        type: msg_type,
                    }).then(function () {
                        if (msg_type == "success") {
                            window.location.href = "../info.php#id_cards";
                        }
                    });
                },
                error: function () {
                    alert("unable to create id card");
                }
            });
        })


        $('#btn_cancel').on('click', function (e) {
             e.preventDefault();
		     window.location.href = "../info.php#id_cards";
        })

    </script>

    <?php include("../include/incUpload.php"); ?>
</body>
</html>
