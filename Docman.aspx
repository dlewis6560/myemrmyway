﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Docman.aspx.vb" Inherits="tracker_Docman" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

<!DOCTYPE html>

<html>
<head runat="server">

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Optional theme -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="admin/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="admin/css/base-admin-3.css" rel="stylesheet" />
    <title></title>

</head>
<body>

    <!-- Navbar -->
    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                    &nbsp;
                </button>
                <a class="navbar-brand pull-left" href="#">&nbsp;EMTeLink Tracker</a>
            </div>
            <!--  <div class="pull-right">
                    <ul class="nav navbar-nav">
                        <li><button type="button" class="btn navbar-btn btn-default" name="btnclose" id="btnclose"  value="Close" onclick="javascript:window.close();">Close</button></li>
                    </ul>     
            </div> -->

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- End navbar -->
    <!-- jumbotron-->

    <!-- End jumbotron-->

    <div class="well well-sm">
        <div class="text-center">
            <h2>
                <i class="shortcut-icon icon-file" style="color: #214D8C;"></i>
                Documents
            </h2>
        </div>
        <!-- End container -->
    </div>

    <div class="container-fluid">

        <div class="row">
            

                <div style="width: 75%; border-color: black; border-style: solid; border-width: 2px;margin:0 auto;">
                      <% 
'example = "c:\inetpub\wwwroot\emtelink\new\Uploads\2\Hand.pdf"

'Dim document_path = "c:\inetpub\wwwroot\emtelink\new\Uploads\"
'Dim document_name = Request.QueryString("doc_id")
'Dim sub_id = 1179
'Dim full_doc_path = document_path & sub_id & "\" & "emfotech_Conditions_Terms-of-Use.pdf"

'Response.Write("<GleamTech:DocumentViewer runat=" & Chr(34) & "server" & Chr(34))
'Response.Write(" Height =" & Chr(34) & "600" & Chr(34))
'  Response.Write(" Document=" & Chr(34) & full_doc_path & Chr(34))
                      %>

                    <GleamTech:DocumentViewer runat="server"
                        Height="600"
                         />
                </div>

        </div>

        <div class="row">
            <br />
            <button type="button" class="btn navbar-btn btn-primary center-block btn-md" style="width: 100px;" name="btnclose1" id="btnclose1" value="Close" onclick="javascript:window.close();">Close</button>
        </div>
    </div>

    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $(function () {

            // We can watch for our custom `fileselect` event like this
            $(document).ready(function () {


            });
            /*
             * For demonstration porpuse, all JavaScript code was incorporated in
             * the HTML file. But when developing your application, your JavaScript code
             * should be in a separated file. Check this page for more information:
             * https://developer.yahoo.com/performance/rules.html#external
             */

        });

    </script>
</body>
</html>
