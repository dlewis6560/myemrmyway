<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>EMTeLink Tracker App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="EMTeLink Traker App">
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->
    <link href="Content/styles.css" rel="stylesheet" />
    <link href="Content/bootstrap-dialog.min.css" rel="stylesheet" />

    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");
    session_start();

    //make sure we have a valid sesion
    if ($_SESSION["valid"] != "TRUE")
    {
        header("Location: index.html");
    };
    
    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];

    $consent_signature = "";
    
    ?>

<style>
    body {
        padding-top: 40px;
    }
</style>
    </head>

<body>

    <!-- Navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="my-navbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="" class="navbar-brand">EMTeLink Tracker</a>
            </div>
            <!-- Navbar Header-->
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="Index.html">
                        <h4>Logoff</h4>
                    </a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="Forms.html">
                        <h4>Forms</h4>
                    </a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="Home.php">
                        <h4>Home</h4>
                    </a></li>
                </ul>
            </div>
        </div>
        <!-- End Container-->
    </nav>
    <!-- End navbar -->
    <!-- jumbotron-->

    <div class="well">
        <div class="container text-center">
            <p>
                <h2>
                    <br />
                    Parental Permission to Travel and Medical Consent Form</h2>
            </p>
        </div>
        <!-- End container -->
    </div>
    <!-- End jumbotron-->
    <div>

        <?php 
        //get user info
        $recordset = $database->select("student", [
            "consent_sign_date",
            "consent_signature"
        ], [
            "userid" => "$userid"
        ]);

        foreach($recordset as $data)
        {
            //signatures #############################################################

            $sign_date = "";
            $myDateTime = DateTime::createFromFormat('Y-m-d', $data["consent_sign_date"]);
            if(!$myDateTime == ''){
                $sign_date = $myDateTime->format('m-d-Y');
            }
            $consent_signature = $data["consent_signature"];
            //########################################################################
        }   
        ?>

    </div>

    <div class="container">
        <section>
            <div id="signatures"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                            <h2 class="title">
                                <u>
                                    Consent Signature <?php echo $userid?>
                                </u>
                            </h2>
                        </div>
                        <div class="panel-body">
                            <form id="signatureform" class="signatureform"">

                                <?php

                                    console.log($userid);
                                    if ($userid == 'admin1') { ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="well">
                                            <h2>Please review the following and indicate your acceptance by signing below.</h2>
                                            <br />
                                            <h3 class="text-left">
                                                I am the parent or legal guardian of this student. Therefore by sigining this statement, I give consent and permission for my child to participate in the below stated band trip(s) using transportation provided by the Test County Board of Education.
                                            </h3>
                                            <br />
                                            <h3 class="text-left">
                                                I understand that during the trip(s), my child will be subject to all Test High School Band policies as well as policies, rules, and regulations of Test High School and the Test County Board of Education.
                                            </h3>
                                            <br />
                                            <h3 class="text-left">
                                                I also give my consent for the school personnel to call for assistance and/or take my child to a doctor/emergency room if treatment appears to be in order.
                                            </h3>
                                            <br />
                                            <h3 class="text-left">
                                                In the event that I cannot be notified of the illness/injury to give parental consent, I authorize the school personnel to sign the necessary consent forms so that my child can receive emergency treatment and hold harmless school and medical officials in the administration of their duties.
                                            </h3>
                                            <br />
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2" class="text-center">
                                                            <h1>Trips</h1>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center" style="width: 10%">Date</th>
                                                        <th class="text-left">Destination</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>9/2/2016</td>
                                                        <td class="text-left">Tallahassee,FL - Game vs Lincoln HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>9/16/2016</td>
                                                        <td class="text-left">Blakely, GA - Game vs Early County HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>9/30/2016</td>
                                                        <td class="text-left">Tallahassee, FL - Game vs Rickards HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>10/21/2016</td>
                                                        <td class="text-left">Thomasville, GA - Game vs Thomas County Central HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>10/28/2016</td>
                                                        <td class="text-left">Hamilton, GA - Game vs Harris County HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td class="text-left">Any Festival we may attend in September or October</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td class="text-left">Any Playoff games that may occur beginning 11/11/16</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <?php  } else { ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="well">
                                            <h2>Please review the following and indicate your acceptance by signing below.</h2>
                                            <br />
                                            <h3 class="text-left">
                                                I am the parent or legal guardian of this student. Therefore by sigining this statement, I give consent and permission for my child to participate in the below stated band trip(s) using transportation provided by the Decatur County Board of Education.
                                            </h3>
                                            <br />
                                            <h3 class="text-left">
                                                I understand that during the trip(s), my child will be subject to all Bainbridge High School Band policies as well as policies, rules, and regulations of Bainbridge High School and the Decatur County Board of Education.
                                            </h3>
                                            <br />
                                            <h3 class="text-left">
                                                I also give my consent for the school personnel to call for assistance and/or take my child to a doctor/emergency room if treatment appears to be in order.
                                            </h3>
                                            <br />
                                            <h3 class="text-left">
                                                In the event that I cannot be notified of the illness/injury to give parental consent, I authorize the school personnel to sign the necessary consent forms so that my child can receive emergency treatment and hold harmless school and medical officials in the administration of their duties.
                                            </h3>
                                            <br />
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2" class="text-center">
                                                            <h1>Trips</h1>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center" style="width: 10%">Date</th>
                                                        <th class="text-left">Destination</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>9/2/2016</td>
                                                        <td class="text-left">Tallahassee,FL - Game vs Lincoln HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>9/16/2016</td>
                                                        <td class="text-left">Blakely, GA - Game vs Early County HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>9/30/2016</td>
                                                        <td class="text-left">Tallahassee, FL - Game vs Rickards HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>10/21/2016</td>
                                                        <td class="text-left">Thomasville, GA - Game vs Thomas County Central HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>10/28/2016</td>
                                                        <td class="text-left">Hamilton, GA - Game vs Harris County HS</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td class="text-left">Any Festival we may attend in September or October</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td class="text-left">Any Playoff games that may occur beginning 11/11/16</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <?php  }  ?>

                                
                                <br />

                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="col-xs-2 pull-left" for="sign-date-picker">Date</label>
                                        <div class="col-xs-10">
                                            <input type="text" class="form-control" name="sign-date-picker" placeholder="date signed" id="sign-date-picker" value="<?php echo $sign_date ?>" />
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="col-xs-2 pull-left" for="signature">Signed</label>
                                        <div class="col-xs-10">
                                            <?php  if ($consent_signature == '') { ?>
                                            <img style="border: dotted; border-width: thin; border-color: black; height: 150px; width: 100%" id="required-img" src="img/sign-required.png" />
                                            <?php  } else { ?>
                                            <img style="border:dotted;border-width:thin;border-color:black;height:150px;width:100%" id="signature-img" src="<?php echo $consent_signature ?>" />
                                            <?php  }  ?>
                                            <canvas class="hide" id="signature" style="height: 150px; border-width: 1px; border-style: dotted; width: 100%" />
                                            <input type="hidden" id="consent_signature_post_field" name="consent_signature_post_field" value="<?php echo "" ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <input type="hidden" id="userid" name="userid" value="<?php echo $userid ?>" />
                                </div>
                                <br />
                                <br />
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="col-xs-2 pull-left">&nbsp;</label>
                                        <div class="col-xs-10">
                                            <div class="navbar btn-toolbar btn-group-lg">
                                                <button type="button" id="btn_edit_signature"  class="btn btn-primary">Edit Signature</button>
                                                <button type="button" id="btn_save_signature"  class="btn hide btn-success">Save</button>
                                                <button type="button" id="btn_cancel_signature" class="btn hide btn-danger">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                            <div>
                              <div class="alert alert-success hide small" role="alert" id="signature-success">Signatures Saved Successfully.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    <br />
    <br />
    <!-- Footer -->

    <footer>
        <div class="container text-center">
            <p>&copy; Copyright @ 2015 EMTeLink</p>
        </div>
        <!-- end Container-->
    </footer>footer>

    </div>

    <script src="Scripts/jquery-1.9.1.min.js"></
    <script src="Scripts/modernizr.custom.34982.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/signatureCapture.js"></script>
    <script src="Scripts/jquery.maskedinput.min.js"></script>
    <script src="Scripts/bootstrap-dialog.min.js"></script>



    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            var sigCapture = null;
            sigCapture = new SignatureCapture("signature");

            $("#sign-date-picker").mask("99/99/9999");

            $("#btn_clear_signature").click(function () {

                //alert("Signature Cleared.");
                sigCapture.clear();

            });

            //######################################################################### Signature buttons ############################################
            //####################################################### Signature EDIT Button ##################################
            $('#btn_edit_signature').on('click', function (e) {
                $(this).hide();
                $('#btn_save_signature').removeClass('hide');
                $('#btn_cancel_signature').removeClass('hide');

                //enable controls for the ins form
                $('#signatureform input').attr('readonly', false);
                $('#signatureform select').attr('disabled', false);

                $('#signature').removeClass('hide');

                $('#signature-img').addClass('hide');
                $('#required-img').addClass('hide');

                sigCapture.clear();

            })

            //####################################################### Signature SAVE Button ##################################
            $('#btn_save_signature').on('click', function (e) {

                if ($('#sign-date-picker').val() == '') {
                    //alert('Please enter a signature date and time.');

                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Missing Signature Date',
                        message: 'Please enter a Signature Date',
                        buttons: [{
                            label: 'Close',
                            action: function (dialogItself) {
                                dialogItself.close();
                            }
                        }]
                    });
                    return;
                    //$(this).parents('p').addClass('warning');
                }

                document.getElementById('consent_signature_post_field').value = signature.toDataURL('image/png');


                $('#signature-img').attr('src', '');
                $('#signature-img').attr('src', signature.toDataURL('image/png'));


                //post the values to the server
                $.ajax({
                    type: "POST",
                    url: "ws/process-consent.php",
                    data: $('form.signatureform').serialize(),
                    success: function (msg) {
                        //$('#sql_stmt').val(msg);
                        $('#signature-success').removeClass('hide');
                        $.wait(function () { $('#signature-success').addClass('hide') }, 2);
                        //alert(msg);
                        //disable controls for the sign form
                        $('#signatureform input').attr('readonly', true);
                        $('#signatureform select').attr('disabled', true);
                    },
                    error: function () {
                        alert("Error saving signature");
                    }
                });

                //hide / show buttons
                $('#btn_save_signature').addClass('hide');
                $('#btn_cancel_signature').addClass('hide');

                $('#btn_edit_signature').show();

                //disable controls for the ins form
                $('#signatureform input').attr('readonly', true);
                $('#signatureform select').attr('disabled', true);

                $('#signature').addClass('hide');
                $('#signature-img').removeClass('hide');

            })

            //####################################################### Signature CANCEL Button ##################################
            $('#btn_cancel_signature').on('click', function (e) {

                //hide / show buttons
                $('#btn_save_signature').addClass('hide');
                $('#btn_cancel_signature').addClass('hide');
                $('#btn_edit_signature').show();

                //disable controls for the ins form
                $('#signatureform input').attr('readonly', true);
                $('#signatureform select').attr('disabled', true);

                $('#signature').addClass('hide');
                $('#signature-img').removeClass('hide');

            })
            //####################################################################################################################


            //##### Initialize when PAGE LOADS #################################
            //initially set all input controls to  and select to disabled
            $('input').attr('readonly', true);
            $('select').attr('disabled', true);

            $.wait = function (callback, seconds) {
                return window.setTimeout(callback, seconds * 1000);
            }

            function isDate(txtDate) {
                var currVal = txtDate;
                if (currVal == '')
                    return true;

                //Declare Regex 
                var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
                var dtArray = currVal.match(rxDatePattern); // is format OK?

                if (dtArray == null)
                    return false;

                //Checks for mm/dd/yyyy format.
                dtMonth = dtArray[1];
                dtDay = dtArray[3];
                dtYear = dtArray[5];

                if (dtMonth < 1 || dtMonth > 12)
                    return false;
                else if (dtDay < 1 || dtDay > 31)
                    return false;
                else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)

                    return false;
                else if (dtMonth == 2) {
                    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                    if (dtDay > 29 || (dtDay == 29 && !isleap))

                        return false;
                }
                return true;
            }

        });
    </script>

</body>
</html>
