        <!-- Upload Document Modal -->
        <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="uploadModalLabel">
                            Upload File
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-10 col-sm-push-1 col-xs-12" style="margin: 0 auto; border-bottom-color: gainsboro; border: none; border-width: 1px; padding: 2px;">
                                <div>
                                    <form id="form-upload" onsubmit="return false">

                                        <div class="form-group" style="margin-bottom:10px;">
                                            <label>Select the file to Upload</label>
                                            <div class="input-group">
                                                <input type="text" id="fileToUploadTextbox" class="form-control" readonly />
                                                <label class="input-group-btn">
                                                    <span class="btn btn-primary">
                                                        Browse&hellip;
                                                        <input type="file" style="display: none;" id="fileToUpload" name="fileToUpload" />
                                                    </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="progress hide" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;" id="progress_bar">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                <span id="progresstext">0% complete</span>
                                            </div>
                                        </div>

                                        <div style="margin-top: 10px; margin-left: 5px; margin-right: 5px; background-color: cornsilk">
                                            <span id="responsetext"></span>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="button-upload">Upload</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>