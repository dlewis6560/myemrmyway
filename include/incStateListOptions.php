    <option value=""></option>
    <option value="AK" <?php if ($SelectedState == "AK") {echo "selected"; } ?>>Alaska</option>
    <option value="AL" <?php if ($SelectedState == "AL") {echo "selected"; } ?>>Alabama</option>
    <option value="AR" <?php if ($SelectedState == "AR") {echo "selected"; } ?>>Arkansas</option>
    <option value="AZ" <?php if ($SelectedState == "AZ") {echo "selected"; } ?>>Arizona</option>
    <option value="CA" <?php if ($SelectedState == "CA") {echo "selected"; } ?>>California</option>
    <option value="CO" <?php if ($SelectedState == "CO") {echo "selected"; } ?>>Colorado</option>
    <option value="CT" <?php if ($SelectedState == "CT") {echo "selected"; } ?>>Connecticut</option>
    <option value="DC" <?php if ($SelectedState == "DC") {echo "selected"; } ?>>District of Columbia</option>
    <option value="DE" <?php if ($SelectedState == "DE") {echo "selected"; } ?>>Delaware</option>
    <option value="FL" <?php if ($SelectedState == "FL") {echo "selected"; } ?>>Florida</option>
    <option value="GA" <?php if ($SelectedState == "GA") {echo "selected"; } ?>>Georgia</option>
    <option value="HI" <?php if ($SelectedState == "HI") {echo "selected"; } ?>>Hawaii</option>
    <option value="IA" <?php if ($SelectedState == "IA") {echo "selected"; } ?>>Iowa</option>
    <option value="ID" <?php if ($SelectedState == "ID") {echo "selected"; } ?>>Idaho</option>
    <option value="IL" <?php if ($SelectedState == "IL") {echo "selected"; } ?>>Illinois</option>
    <option value="IN" <?php if ($SelectedState == "IN") {echo "selected"; } ?>>Indiana</option>
    <option value="KS" <?php if ($SelectedState == "KS") {echo "selected"; } ?>>Kansas</option>
    <option value="KY" <?php if ($SelectedState == "KY") {echo "selected"; } ?>>Kentucky</option>
    <option value="LA" <?php if ($SelectedState == "LA") {echo "selected"; } ?>>Louisiana</option>
    <option value="MA" <?php if ($SelectedState == "MA") {echo "selected"; } ?>>Massachusetts</option>
    <option value="MD" <?php if ($SelectedState == "MD") {echo "selected"; } ?>>Maryland</option>
    <option value="ME" <?php if ($SelectedState == "ME") {echo "selected"; } ?>>Maine</option>
    <option value="MI" <?php if ($SelectedState == "MI") {echo "selected"; } ?>>Michigan</option>
    <option value="MN" <?php if ($SelectedState == "MN") {echo "selected"; } ?>>Minnesota</option>
    <option value="MO" <?php if ($SelectedState == "MO") {echo "selected"; } ?>>Missouri</option>
    <option value="MS" <?php if ($SelectedState == "MS") {echo "selected"; } ?>>Mississippi</option>
    <option value="MT" <?php if ($SelectedState == "MT") {echo "selected"; } ?>>Montana</option>
    <option value="NC" <?php if ($SelectedState == "NC") {echo "selected"; } ?>>North Carolina</option>
    <option value="ND" <?php if ($SelectedState == "ND") {echo "selected"; } ?>>North Dakota</option>
    <option value="NE" <?php if ($SelectedState == "NE") {echo "selected"; } ?>>Nebraska</option>
    <option value="NH" <?php if ($SelectedState == "NH") {echo "selected"; } ?>>New Hampshire</option>
    <option value="NJ" <?php if ($SelectedState == "NJ") {echo "selected"; } ?>>New Jersey</option>
    <option value="NM" <?php if ($SelectedState == "NM") {echo "selected"; } ?>>New Mexico</option>
    <option value="NV" <?php if ($SelectedState == "NV") {echo "selected"; } ?>>Nevada</option>
    <option value="NY" <?php if ($SelectedState == "NY") {echo "selected"; } ?>>New York</option>
    <option value="OH" <?php if ($SelectedState == "OH") {echo "selected"; } ?>>Ohio</option>
    <option value="OK" <?php if ($SelectedState == "OK") {echo "selected"; } ?>>Oklahoma</option>
    <option value="OR" <?php if ($SelectedState == "OR") {echo "selected"; } ?>>Oregon</option>
    <option value="PA" <?php if ($SelectedState == "PA") {echo "selected"; } ?>>Pennsylvania</option>
    <option value="PR" <?php if ($SelectedState == "PR") {echo "selected"; } ?>>Puerto Rico</option>
    <option value="RI" <?php if ($SelectedState == "RI") {echo "selected"; } ?>>Rhode Island</option>
    <option value="SC" <?php if ($SelectedState == "SC") {echo "selected"; } ?>>South Carolina</option>
    <option value="SD" <?php if ($SelectedState == "SD") {echo "selected"; } ?>>South Dakota</option>
    <option value="TN" <?php if ($SelectedState == "TN") {echo "selected"; } ?>>Tennessee</option>
    <option value="TX" <?php if ($SelectedState == "TX") {echo "selected"; } ?>>Texas</option>
    <option value="UT" <?php if ($SelectedState == "UT") {echo "selected"; } ?>>Utah</option>
    <option value="VA" <?php if ($SelectedState == "VA") {echo "selected"; } ?>>Virginia</option>
    <option value="VT" <?php if ($SelectedState == "VT") {echo "selected"; } ?>>Vermont</option>
    <option value="WA" <?php if ($SelectedState == "WA") {echo "selected"; } ?>>Washington</option>
    <option value="WI" <?php if ($SelectedState == "WI") {echo "selected"; } ?>>Wisconsin</option>
    <option value="WV" <?php if ($SelectedState == "WV") {echo "selected"; } ?>>West Virginia</option>
    <option value="WY" <?php if ($SelectedState == "WY") {echo "selected"; } ?>>Wyoming</option>