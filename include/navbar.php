    <nav class="navbar navbar-inverse" id="my-navbar" style="background-color:#474749">

	<div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                    &nbsp;
                </button>
                <a class="navbar-brand" href="./home.php" style="padding:0;"><span><img src="https://www.emtelink.com/myemrmyway/img/MyEMRMyWay_Logo.png" width="90" height="43" alt="logo" style="padding-top:2px; !important"></span></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu">
                            <!--<li><a href="./account.html">Account Settings</a></li>
                             <li><a href="javascript:;">Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:;">Help</a></li> -->
                        </ul>

                    </li>

                    <li>
					
                        <?php if (($page_name != "home.php") && ($page_name != "success.php")) {  ?>
                        <a href="https://www.emtelink.com/myemrmyway/home.php" style="font-size: 16px">
                            <i class="icon-home"></i>&nbsp;Home <?php //echo $strSQL ?>
                        </a>
                    </li>
					<?php }  ?>
					
                    <?php if (($subType != "F") and ($page_name == "user-manager.php")) {  ?>
                    <li>
                        <a href="index.php" style="font-size: 16px">
                            <i class="icon-dashboard"></i>&nbsp;Dashboard
                        </a>
                    </li>
                    <?php }  ?>

                    <li class="dropdown">

                        <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px">
                            <i class="icon-user"></i>
                            <?php echo  $_SESSION["loggedin_fname"] . " " .  $_SESSION["loggedin_lname"] ?>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <!--  <li><a href="javascript:;">My Profile</a></li>
                            <li><a href="javascript:;">My Groups</a></li> 
                            <li class="divider"></li> -->
                            <li>
                                <a href="#" class="logoff" style="font-size: 14px" onclick="return logout();">Logout</a>
                            </li>
                        </ul>

                    </li>
                </ul>

                <!-- <form class="navbar-form navbar-right" role="search">
                   <div class="form-group">
                     <input type="text" class="form-control input-sm search-query" placeholder="Search">
                   </div>
                 </form> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>