<nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                </button>
                <a class="navbar-brand" href="https://www.emtelink.com/myemrmyway/Index.html">EMTeLink Tracker</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu">
                            <!--<li><a href="./account.html">Account Settings</a></li>
                             <li><a href="javascript:;">Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:;">Help</a></li> -->
                        </ul>
                    </li>

                    <li class="dropdown">

                        <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px">
                            <i class="icon-user"></i>
                            <?php echo $loggedin_firstname . " " . $loggedin_lastname ?>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <!--  <li><a href="javascript:;">My Profile</a></li>
                            <li><a href="javascript:;">My Groups</a></li> 
                            <li class="divider"></li> -->
                            <li><a href="https://www.emtelink.com/myemrmyway/Index.html" style="font-size: 14px">Logout</a></li>
                        </ul>

                    </li>
                </ul>

                <!-- <form class="navbar-form navbar-right" role="search">
                   <div class="form-group">
                     <input type="text" class="form-control input-sm search-query" placeholder="Search">
                   </div>
                 </form> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>