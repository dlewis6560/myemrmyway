<div class="row">
    <div class="col-xs-12 center-block">
        <div class="panel-group accordion">
            <div class="panel panel-default" style="border-color:black">

                <div class="panel-heading" style="text-align:center">

                    <h4 class="panel-title">

                        <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne33">
                            <span style="font-size: large;color:#525252;">
                                <i class="icon-info-sign" style="color: #525252"></i>
                                &nbsp;
                                <u>Instructions</u>
                            </span>
                            <span style="font-size: smaller;color:#525252">&nbsp;&nbsp;(Tap to show/hide)</span>
                        </a>
                    </h4>

                </div>

                <div id="collapseOne33" class="panel-collapse collapse">

                    <div class="panel-body">

                        <p style="font-size: medium">
                            <?php echo $instruction_content; ?>
                        </p>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
