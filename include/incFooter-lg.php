<div class="extra text-center hidden-xs" style="background-color:#383838;color:#c4c4c4">
    <div class="container">
        <div class="row">

            <div class="col-md-3">

                <h4>About</h4>

                <ul>
                    <li><a href="https://www.twitter.com/emtelink" target="_blank">Twitter</a></li>
                    <li><a href="https://www.facebook.com/emtelink" target="_blank">Facebook</a></li>
                </ul>

            </div>
            <!-- /span3 -->

            <div class="col-md-3">

                <h4>Support</h4>

                <ul>
                    <li><a href="fag.html">Frequently Asked Questions</a></li>
                    <li><a href="https://emtelink.com/portal/pdf/DeliveryandRefundPolicy.pdf" target="_blank">Delivery &amp; Refund Policy</a></li>
                    <li><a href="javascript:;"></a></li>
                    <li><a href="javascript:;"></a></li>
                </ul>

            </div>
            <!-- /span3 -->

            <div class="col-md-3">

                <h4>Legal</h4>

                <ul>
                    <li><a href="https://www.emtelink.com/portal/pdf/emfotech%20Legal%20Notices%20Policy.pdf" target="_blank">Legal Policy</a></li>
                    <li><a href="https://www.emtelink.com/portal/pdf/emfotech%20Conditions%20Terms-of-Use.pdf" target="_blank">Terms of Use</a></li>
                    <li><a href="https://emtelink.com/portal/pdf/Emfotech%20Privacy%20Policy.pdf" target="_blank">Privacy Policy</a></li>
                    <li><a href="https://emtelink.com/portal/pdf/HIPAA%20Compliance.pdf" target="_blank">HIPAA Compliance</a></li>
                </ul>

            </div>
            <!-- /span3 -->

            <div class="col-md-3">

                <h4>Links</h4>

                <ul>
                    <li><a href="../home.php">Home</a></li>
                    <li><a href="javascript:;"></a></li>
                    <li><a href="javascript:;"></a></li>
                    <li><a href="../Index.hmtl">Logout</a></li>
                </ul>

            </div>
            <!-- /span3 -->

        </div>
        <!-- /row -->

    </div>
    <!-- /container -->

</div>
<!-- /extra -->


<footer>
    <div class="extra text-center" style="background-color:#383838;color:#c4c4c4">
        &copy; Copyright @ 2018 EMTeLink&nbsp;&nbsp;&nbsp;&nbsp;Saving Lives With Technology
		<span class="hidden-xs"><br />Patents #8,308,062 #8,740,089 8,608,078 8,602,311 8,960,555</span>
    </div>
    <!-- end Container-->
</footer>
