<?php

    //make sure we have a valid sesion
    if ($_SESSION["valid"] != "TRUE")
    {
        header("Location: index.html");
    };

    function encryptRJ256($key,$iv,$string_to_encrypt)
    {
        $rtn = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $string_to_encrypt, MCRYPT_MODE_CBC, $iv);
        $rtn = base64_encode($rtn);
        $rtn = str_replace("+", "~", $rtn);
        return($rtn);
    }

    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];
    $uid = $_SESSION["uid"];
    $admin_user = $_SESSION["admin_user"];
    $subType = $_SESSION["subType"];
    $subMaxUsers = $_SESSION["subMaxUsers"];
    $subId = $_SESSION["subId"];

    $loggedin_firstname = $_SESSION["loggedin_fname"];
    $loggedin_lastname = $_SESSION["loggedin_lname"];

    $ky = 'emtelink7+#bbtrm!c8814z5kniletme'; // 32 * 8 = 256 bit key
    $iv = 'kniletmeeeyy66#cs!9hjv88emtelink'; // 32 * 8 = 256 bit iv

    $encrypted_userID = encryptRJ256($ky, $iv, $userid);
    $encrypted_subID = encryptRJ256($ky, $iv, $subId);

    $selected_firstname = $firstname;
    $selected_lastname = $lastname;
	
    $page_name = basename($_SERVER['PHP_SELF'])	

?>