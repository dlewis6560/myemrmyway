<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>MyEMRMyWay :: Report</title>
    <meta name="description" content="MyEMRMyWay App" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");
	

    if (isset($_GET['id'])) {
        $id_value = $_GET['id'];
        //echo $id_value . "<br />";
    } else{
        echo "report user id not found, processing cannot continue.";
        exit;
    }
    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;
	
    $decrypted = Urlcrypt::decrypt($id_value);

    list($report_user_id, $starttime) = explode("|", $decrypted);

    $endtime = time();
    $timediff = $endtime - $starttime;

    function calculateAge($date){

        //return date_diff(date_create($date), date_create('today'))->y;
        $birthDate = explode("-", $date);
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
          ? ((date("Y") - $birthDate[2]) - 1)
          : (date("Y") - $birthDate[2]));
        return $age;


        //list($month,$day,$year) = explode("-",$date);
        //echo $year;
        //$year_diff  = date("Y") - $year;
        //$month_diff = date("m") - $month;
        //$day_diff   = date("d") - $day;
        //if ($day_diff < 0 || $month_diff < 0) $year_diff--;
        //return $year_diff;
    }

    //list($userid, $epoch_value) = split('|', $decrypted);
    //echo "userid: $userid; ev: $epoch_value;<br />";
    //echo $decrypted;

    //        list($report_user_id, $epoch_value) = explode("|", $decrypted);
    //echo $report_user_id;
    //echo $epoch_value;

    //session_start();

    //make sure we have a valid sesion
    //if ($_SESSION["valid"] != "TRUE")
    //{
    //    header("Location: index.html");
    //}; JBrinson




    //$report_user_id = 'dmassey';
    $userid = $report_user_id;
	
	$connection_info =  $_SERVER['HTTP_USER_AGENT'] . " " . $_SERVER['HTTP_HOST'] . " " . $_SERVER['REQUEST_TIME'] . " " . $_SERVER['REMOTE_ADDR'] . " " . $_SERVER['REMOTE_HOST'] . " " . $_SERVER['REMOTE_PORT']  . " " . $_SERVER['REMOTE_USER'] ;
	$database->insert("access_log", [
        "userid" => "$userid",
        "connection_info" => "$connection_info"]);	
	

    ?>


    <title>Emergency Information Report</title>

    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
            mso-font-charset: 0;
            mso-generic-font-family: roman;
            mso-font-pitch: variable;
            mso-font-signature: -536870145 1107305727 0 0 415 0;
        }

        @font-face {
            font-family: "Calibri Light";
            panose-1: 2 15 3 2 2 2 4 3 2 4;
            mso-font-charset: 0;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -1610611985 1073750139 0 0 415 0;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 0;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536870145 1073786111 1 0 415 0;
        }

        @font-face {
            font-family: "Segoe UI";
            panose-1: 2 11 5 2 4 2 4 2 2 3;
            mso-font-charset: 0;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -520084737 -1073683329 41 0 479 0;
        }
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 12.0pt;
            margin-left: 0in;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        h1 {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Heading 1 Char";
            mso-style-next: Normal;
            margin: 0in;
            margin-bottom: .0001pt;
            line-height: 115%;
            mso-pagination: widow-orphan lines-together;
            page-break-after: avoid;
            mso-outline-level: 1;
            font-size: 16.0pt;
            font-family: "Calibri Light",sans-serif;
            mso-ascii-font-family: "Calibri Light";
            mso-ascii-theme-font: major-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: major-fareast;
            mso-hansi-font-family: "Calibri Light";
            mso-hansi-theme-font: major-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: major-bidi;
            color: #2E74B5;
            mso-themecolor: accent1;
            mso-themeshade: 191;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
            font-weight: normal;
        }

        p.MsoHeader, li.MsoHeader, div.MsoHeader {
            mso-style-priority: 99;
            mso-style-link: "Header Char";
            margin: 0in;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            tab-stops: center 3.25in right 6.5in;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoFooter, li.MsoFooter, div.MsoFooter {
            mso-style-priority: 99;
            mso-style-link: "Footer Char";
            margin: 0in;
            margin-bottom: .0001pt;
            text-align: center;
            mso-pagination: widow-orphan;
            tab-stops: center 3.25in right 6.5in;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoListNumber, li.MsoListNumber, div.MsoListNumber {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            margin-top: 12.0pt;
            margin-right: 0in;
            margin-bottom: 12.0pt;
            margin-left: .25in;
            mso-add-space: auto;
            text-indent: -.25in;
            line-height: 115%;
            mso-pagination: widow-orphan;
            mso-list: l0 level1 lfo1;
            tab-stops: list .25in;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoListNumberCxSpFirst, li.MsoListNumberCxSpFirst, div.MsoListNumberCxSpFirst {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-type: export-only;
            margin-top: 12.0pt;
            margin-right: 0in;
            margin-bottom: 0in;
            margin-left: .25in;
            margin-bottom: .0001pt;
            mso-add-space: auto;
            text-indent: -.25in;
            line-height: 115%;
            mso-pagination: widow-orphan;
            mso-list: l0 level1 lfo1;
            tab-stops: list .25in;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoListNumberCxSpMiddle, li.MsoListNumberCxSpMiddle, div.MsoListNumberCxSpMiddle {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-type: export-only;
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 0in;
            margin-left: .25in;
            margin-bottom: .0001pt;
            mso-add-space: auto;
            text-indent: -.25in;
            line-height: 115%;
            mso-pagination: widow-orphan;
            mso-list: l0 level1 lfo1;
            tab-stops: list .25in;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoListNumberCxSpLast, li.MsoListNumberCxSpLast, div.MsoListNumberCxSpLast {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-type: export-only;
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 12.0pt;
            margin-left: .25in;
            mso-add-space: auto;
            text-indent: -.25in;
            line-height: 115%;
            mso-pagination: widow-orphan;
            mso-list: l0 level1 lfo1;
            tab-stops: list .25in;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoClosing, li.MsoClosing, div.MsoClosing {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Closing Char";
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 30.0pt;
            margin-left: 0in;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoSignature, li.MsoSignature, div.MsoSignature {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Signature Char";
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 24.0pt;
            margin-left: 0in;
            mso-add-space: auto;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoSignatureCxSpFirst, li.MsoSignatureCxSpFirst, div.MsoSignatureCxSpFirst {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Signature Char";
            mso-style-type: export-only;
            margin: 0in;
            margin-bottom: .0001pt;
            mso-add-space: auto;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoSignatureCxSpMiddle, li.MsoSignatureCxSpMiddle, div.MsoSignatureCxSpMiddle {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Signature Char";
            mso-style-type: export-only;
            margin: 0in;
            margin-bottom: .0001pt;
            mso-add-space: auto;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoSignatureCxSpLast, li.MsoSignatureCxSpLast, div.MsoSignatureCxSpLast {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Signature Char";
            mso-style-type: export-only;
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 24.0pt;
            margin-left: 0in;
            mso-add-space: auto;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoDate, li.MsoDate, div.MsoDate {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Date Char";
            mso-style-next: Normal;
            margin-top: .25in;
            margin-right: 0in;
            margin-bottom: .25in;
            margin-left: 0in;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-link: "Balloon Text Char";
            margin: 0in;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 9.0pt;
            font-family: "Segoe UI",sans-serif;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        span.MsoPlaceholderText {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-unhide: no;
            color: gray;
        }

        p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing {
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-parent: "";
            margin: 0in;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph {
            mso-style-priority: 34;
            mso-style-qformat: yes;
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 12.0pt;
            margin-left: .5in;
            mso-add-space: auto;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst {
            mso-style-priority: 34;
            mso-style-qformat: yes;
            mso-style-type: export-only;
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 0in;
            margin-left: .5in;
            margin-bottom: .0001pt;
            mso-add-space: auto;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle {
            mso-style-priority: 34;
            mso-style-qformat: yes;
            mso-style-type: export-only;
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 0in;
            margin-left: .5in;
            margin-bottom: .0001pt;
            mso-add-space: auto;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast {
            mso-style-priority: 34;
            mso-style-qformat: yes;
            mso-style-type: export-only;
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 12.0pt;
            margin-left: .5in;
            mso-add-space: auto;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        span.FooterChar {
            mso-style-name: "Footer Char";
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: Footer;
        }

        span.Heading1Char {
            mso-style-name: "Heading 1 Char";
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: "Heading 1";
            mso-ansi-font-size: 16.0pt;
            mso-bidi-font-size: 16.0pt;
            font-family: "Calibri Light",sans-serif;
            mso-ascii-font-family: "Calibri Light";
            mso-ascii-theme-font: major-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: major-fareast;
            mso-hansi-font-family: "Calibri Light";
            mso-hansi-theme-font: major-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: major-bidi;
            color: #2E74B5;
            mso-themecolor: accent1;
            mso-themeshade: 191;
        }

        p.Address, li.Address, div.Address {
            mso-style-name: Address;
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            margin: 0in;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 10.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            font-variant: small-caps;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        span.SignatureChar {
            mso-style-name: "Signature Char";
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: Signature;
        }

        span.ClosingChar {
            mso-style-name: "Closing Char";
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: Closing;
        }

        span.DateChar {
            mso-style-name: "Date Char";
            mso-style-priority: 1;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: Date;
        }

        span.HeaderChar {
            mso-style-name: "Header Char";
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: Header;
        }

        span.BalloonTextChar {
            mso-style-name: "Balloon Text Char";
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: "Balloon Text";
            mso-ansi-font-size: 9.0pt;
            mso-bidi-font-size: 9.0pt;
            font-family: "Segoe UI",sans-serif;
            mso-ascii-font-family: "Segoe UI";
            mso-hansi-font-family: "Segoe UI";
            mso-bidi-font-family: "Segoe UI";
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-font-kerning: 1.0pt;
            mso-ligatures: standard;
            mso-fareast-language: JA;
        }

        .MsoPapDefault {
            mso-style-type: export-only;
            margin-bottom: 12.0pt;
            line-height: 115%;
        }
        /* Page Definitions */
        @page {
            mso-footnote-separator: url("Emergency%20Information%20for_files/header.htm") fs;
            mso-footnote-continuation-separator: url("Emergency%20Information%20for_files/header.htm") fcs;
            mso-endnote-separator: url("Emergency%20Information%20for_files/header.htm") es;
            mso-endnote-continuation-separator: url("Emergency%20Information%20for_files/header.htm") ecs;
        }

        @page WordSection1 {
            size: 8.5in 11.0in;
            margin: .5in .5in .5in .5in;
            mso-header-margin: .5in;
            mso-footer-margin: .5in;
            mso-title-page: yes;
            mso-footer: url("Emergency%20Information%20for_files/header.htm") f1;
            mso-first-footer: url("Emergency%20Information%20for_files/header.htm") ff1;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }
        /* List Definitions */
        @list l0 {
            mso-list-id: -120;
            mso-list-type: simple;
            mso-list-template-ids: -803994084;
        }

        @list l0:level1 {
            mso-level-style-link: "List Number";
            mso-level-tab-stop: .25in;
            mso-level-number-position: left;
            margin-left: .25in;
            text-indent: -.25in;
        }

        @list l1 {
            mso-list-id: 561871367;
            mso-list-type: hybrid;
            mso-list-template-ids: 1578943594 -1339135792 -1906130332 -287559912 100163528 147499534 1610631634 2128134228 -924315870 -360176078;
        }

        @list l1:level1 {
            mso-level-number-format: image;
            list-style-image: url("Emergency%20Information%20for_files/image001.jpg");
            mso-level-text: \F0B7;
            mso-level-tab-stop: .5in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        @list l1:level2 {
            mso-level-number-format: bullet;
            mso-level-text: \F0B7;
            mso-level-tab-stop: 1.0in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        @list l1:level3 {
            mso-level-number-format: bullet;
            mso-level-text: \F0B7;
            mso-level-tab-stop: 1.5in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        @list l1:level4 {
            mso-level-number-format: bullet;
            mso-level-text: \F0B7;
            mso-level-tab-stop: 2.0in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        @list l1:level5 {
            mso-level-number-format: bullet;
            mso-level-text: \F0B7;
            mso-level-tab-stop: 2.5in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        @list l1:level6 {
            mso-level-number-format: bullet;
            mso-level-text: \F0B7;
            mso-level-tab-stop: 3.0in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        @list l1:level7 {
            mso-level-number-format: bullet;
            mso-level-text: \F0B7;
            mso-level-tab-stop: 3.5in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        @list l1:level8 {
            mso-level-number-format: bullet;
            mso-level-text: \F0B7;
            mso-level-tab-stop: 4.0in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        @list l1:level9 {
            mso-level-number-format: bullet;
            mso-level-text: \F0B7;
            mso-level-tab-stop: 4.5in;
            mso-level-number-position: left;
            text-indent: -.25in;
            font-family: Symbol;
        }

        ol {
            margin-bottom: 0in;
        }

        ul {
            margin-bottom: 0in;
        }
        -->
    </style>
</head>

<body>

    <?php
    $recordset = $database->select("user", [
        "subId",
        "firstname",
        "lastname",
        "middleinitial",
        "address",
        "city",
        "st",
        "zip",
        "dob",
        "phone",
        "student_email",
        "parent_email",
        "med_conditions",
        "hospitalizations",
        "serious_injuries",
        "special_diets",
        "add_info",
        "parent_name",
        "parent_addr",
        "parent_day_phone",
        "parent_night_phone",
        "relative_name",
        "relative_phone",
        "relationship",
        "family_doctor",
        "doctor_day_phone",
        "doctor_night_phone",
        "ins_company",
        "ins_phone",
        "groupno",
        "contractno",
        "effectivedate",
        "certification_no",
        "plan_with",
        "add_medical_conditions",
        "primary_name",
        "primary_addr",
        "primary_phone",
        "primary_carrierId",
        "primary_email",
        "primary_relationship",
        "secondary_name",
        "secondary_addr",
        "secondary_phone",
        "secondary_carrierId",
        "secondary_email",
        "secondary_relationship",
        "phy_name",
        "phy_addr",
        "phy_phone",
        "phy_email",
        "phy_type",
        "bloodtype"

    ], [
        "userid" => "$report_user_id"
    ]);

    foreach($recordset as $data)
    {
        $subId = $data["subId"];
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $middleinitial = $data["middleinitial"];
        $address = $data["address"];
        $blood_type = $data["bloodtype"];
        $city = $data["city"];
        $state = $data["st"];
        $zip = $data["zip"];
        //$dob = $data["dob"]->format('m-d-Y');
        //$dob = $data["dob"];
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data["dob"]);
        if(!$myDateTime == ''){
            $dob = $myDateTime->format('m-d-Y');
            $age = calculateAge($dob);
        }

        $phone = $data["phone"];
        $student_email = $data["student_email"];
        $parent_email = $data["parent_email"];

        $med_conditions = $data["med_conditions"];
        $hospitalizations = $data["hospitalizations"];
        $serious_injuries = $data["serious_injuries"];
        $special_diets = $data["special_diets"];
        $add_info = $data["add_info"];
        $parent_name = $data["parent_name"];
        $parent_addr = $data["parent_addr"];
        $parent_day_phone = $data["parent_day_phone"];
        $parent_night_phone = $data["parent_night_phone"];
        $relative_name = $data["relative_name"];
        $relative_phone = $data["relative_phone"];
        $relationship = $data["relationship"];
        $family_doctor = $data["family_doctor"];
        $doctor_day_phone = $data["doctor_day_phone"];
        $doctor_night_phone = $data["doctor_night_phone"];
        $ins_company = $data["ins_company"];
        $ins_phone = $data["ins_phone"];
        $groupno = $data["groupno"];
        $contractno = $data["contractno"];
        //$effectivedate = $data["effectivedate"]->format('m-d-Y');
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data["effectivedate"]);
        if(!$myDateTime == ''){
            $effectivedate = $myDateTime->format('m-d-Y');
        }
        //$effectivedate = $data["effectivedate"];
        $certification_no = $data["certification_no"];
        $plan_with = $data["plan_with"];
        $add_medical_conditions = $data["add_medical_conditions"];

        $primary_name = $data["primary_name"];
        $primary_addr = $data["primary_addr"];
        $primary_phone = $data["primary_phone"];
        $primary_email = $data["primary_email"];
        $primary_relationship = $data["primary_relationship"];
        $primary_carrierId = $data["primary_carrierId"];

        $secondary_name = $data["secondary_name"];
        $secondary_addr = $data["secondary_addr"];
        $secondary_phone = $data["secondary_phone"];
        $secondary_email = $data["secondary_email"];
        $secondary_relationship = $data["secondary_relationship"];
        $secondary_carrierId = $data["secondary_carrierId"];

        $phy_name = $data["phy_name"];
        $phy_addr = $data["phy_addr"];
        $phy_phone = $data["phy_phone"];
        $phy_email = $data["phy_email"];
        $phy_type = $data["phy_type"];

        $medcond=json_decode($med_conditions,true);
        //echo $subId;
    }

    $recordset = $database->select("Subscriptions", [
        "subType"
    ], [
        "subId" => "$subId"
    ]);

    foreach($recordset as $data)
    {
        $subType = $data["subType"];
    }

    ?>


    <?php
    //only process if pw request is less than 24 hour old (60 mins * 60 seconds * 24 hours)
    if ($timediff < 86400) {
    ?>
    <div class="WordSection1">
        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
            <span class="Heading1Char">
                <b>
                    <span style='font-size: 22.0pt; line-height: 115%; color: #595959;'>Emergency Information For:</span>
                </b>
            </span>
        </p>

        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
            <span class="Heading1Char">
                <b>
                    <span style='font-size: 24.0pt; line-height: 115%; color: red'>
                        <?php echo $firstname . " " . $middleinitial . " " . $lastname; ?>
                    </span>
                </b>
            </span>
        </p>
        <br />
        <!-- <p class="MsoNormal" style='text-align: center'>
            <span
                style='font-size: 24.0pt; line-height: 115%;'>
                <img width="148" height="162"
                    src="Emergency%20Information%20for_files/student-pic.jpg"></span><span
                        style='font-size: 24.0pt; line-height: 115%'></span>
        </p> -->

        <div align="center">

            <table class="MsoTableGrid" border="1" style='border-collapse: collapse; border: none;'>
                <tr>
                    <td style="text-align:left">
                        <p class="MsoNormal">
                            <span style='font-size: 14.0pt;'>
                                <a href="https://www.emtelink.com/mobile/"></a>
                            </span>
                        </p>
                    </td>
                    <td style="text-align:right">
                        <p class="MsoNormal">
                            <span style='font-size: 14.0pt'>
                                <a href="javascript:window.print();">Print Report</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href=""></a>
                            </span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center">
                        <p class="MsoNormal" style='margin-bottom: .1in; margin-top: .1in; line-height: normal'>
                            <?php include("include/incProfilepic.php"); ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>DOB</span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border: solid windowtext 1.0pt; border-left: none; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $dob; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Age</span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border: solid windowtext 1.0pt; border-left: none; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $age . " years old"; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Address</span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $address."<br />".$city." ".$state." ".$zip; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>

                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Blood Type</span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $blood_type; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>

                <!-- <tr style='height:22.0pt'>
  <td style='width:89.6pt;border:solid windowtext 1.0pt;border-top:
  none;
  padding:0in 5.4pt 0in 5.4pt;height:22.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:14.0pt'>Last Updated</span></p>
  </td>
  <td style='width:387.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  
  padding:0in 5.4pt 0in 5.4pt;height:22.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:14.0pt'>06/22/2015
  11:02 AM</span></b></p>
  </td>
 </tr> -->
            </table>

        </div>

        <br />

        <div align="center">

            <?php

        $bolAtLeastOneCondition = "false";

        //echo count($medcond);
        for($i = 0, $size = count($medcond); $i < $size; ++$i) {
            //$people[$i]['salt'] = mt_rand(000000, 999999);
            if ($medcond[$i]['checked']==1) {
                $bolAtLeastOneCondition = "true<br />";
                //echo "cond - true<br />";
            }
            else {
                //echo "cond - false<br />";
            }
        }

        //echo "bolAtLeastOneCondition = " . $bolAtLeastOneCondition;

            ?>

            <table class="MsoTableGrid" border="1"
                style='border-collapse: collapse; border: none; width:500pt;'>
                <tr>
                    <td colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #9CC2E5; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <b>
                                <span
                                    style='font-size: 18.0pt'>
                                    Medical Conditions <?php if ($bolAtLeastOneCondition == "false") { echo "- none";} ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>

                <?php
        if ($subType != "F") {
            if($medcond[0]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Mumps</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[1]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Measles</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[2]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Chicken Pox</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[3]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Pneumonia</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[4]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Diabetes</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[5]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Cancer</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[6]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Malaria</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[7]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Seizures</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[8]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Heart Disease</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[9]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Kidney Disease</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[10]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Tuberculosis</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[11]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Rheumatic Fever</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[12]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Hepatitis</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[13]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Blood Pressure Problems</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[14]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Tetanus Toxoid Shot (in last 12 months)</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php }

        }   //subType != F
        else {
            if($medcond[0]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Asthma</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[1]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>COPD</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[2]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Seizure Disorder</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[3]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Dementia</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[4]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Alzheimer</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[5]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Hyperglycemia </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[6]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Hypoglycemia</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[7]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Diabetes Type 1 </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[8]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Diabetes Type 2</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[9]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>High Blood Pressure</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[10]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Contact Lenses</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[11]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Rheumatic Fever</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[12]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Pace Maker</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[13]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Heart Stent</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php } ?>

                <?php if($medcond[14]['checked']==1) { ?>
                <tr style='height: 25.6pt'>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>Tetanus Shot (last 12 mnths)</span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php  } ?>

                <?php if( strlen($add_medical_conditions) > 1) { ?>
                <tr>
                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt;'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <span style=''>
                                <img width="15" height="15"
                                    src="Emergency%20Information%20for_files/image005.jpg" />
                            </span>
                        </p>
                    </td>
                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; white-space:pre-line;'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $add_medical_conditions; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php }

        }   ?>

            </table>

        </div>

        <br />

        <div align="center">
            <table class="MsoTableGrid" border="1"
                style='border-collapse: collapse; border: none; width:500pt;'>
                <tr style='height: 25.6pt'>
                    <td colspan="3" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #F4B083; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <b>
                                <span
                                    style='font-size: 18.0pt'>
                                    Medications
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 25.6pt'>
                    <td style='width: 170.75pt; border: solid windowtext 1.0pt; border-top: none; background: #F8CCAE; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Name</span>
                        </p>
                    </td>
                    <td style='width: 112.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; background: #F8CCAE; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Dosage</span>
                        </p>
                    </td>
                    <td style='width: 193.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; background: #F8CCAE; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>How Often</span>
                        </p>
                    </td>
                </tr>
                <?php
        $recordset = $database->select("user_medications", [
            "medicationId",
            "medication_name",
            "dosage",
            "howoften"
        ], [
            "userid" => "$report_user_id",
             "ORDER" => ['medication_name ASC']
        ]);

        foreach($recordset as $data)
        {
            //$medicationId = $data["medicationId"];
            $medication_name = $data["medication_name"];
            $dosage = $data["dosage"];
            $howoften = $data["howoften"];

                ?>
                <tr style='height: 26.5pt'>
                    <td style='width: 170.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $medication_name; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                    <td style='width: 112.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $dosage; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                    <td style='width: 193.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $howoften; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php
        }

                ?>
            </table>
        </div>

        <br />

        <div align="center">
            <table border="1"
                style='border-collapse: collapse; border: none; width:500pt;'>
                <tr style='height: 25.6pt'>
                    <td colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #A8D08D; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <b>
                                <span
                                    style='font-size: 18.0pt'>
                                    Allergies
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 25.6pt'>
                    <td style='width: 170.6pt; border: solid windowtext 1.0pt; border-top: none; background: #C5E0B3; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Description</span>
                        </p>
                    </td>
                    <td style='width: 306.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; background: #C5E0B3; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Notes</span>
                        </p>
                    </td>
                </tr>
                <?php

        $recordset = $database->select("user_allergies", [
            "allergyId",
            "description",
            "notes"
        ], [
            "userid" => "$report_user_id",
             "ORDER" => ['description ASC']
        ]);

        foreach($recordset as $data)
        {
            $allergyId = $data["allergyId"];
            $description = $data["description"];
            $notes = $data["notes"];

                ?>
                <tr style='height: 26.5pt'>
                    <td style='width: 170.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $description; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                    <td style='width: 306.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $notes; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <?php
        }

                ?>
            </table>
        </div>

        <br />

        <div align="center">
            <table class="MsoTableGrid" border="1" style='border-collapse: collapse; border: none;  width:500pt;'>
                <tr style='height: 25.6pt'>
                    <td style='width: 476.75pt; border: solid windowtext 1.0pt;'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <b>
                                <span
                                    style='font-size: 18.0pt'>
                                    Miscellaneous
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 26.5pt'>
                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #E7E6E6; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <u>
                                <span style='font-size: 14.0pt; color: #3B3838;'>Hospitalizations</span>
                            </u>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $hospitalizations; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 26.5pt'>
                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #E7E6E6; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <u>
                                <span style='font-size: 14.0pt; color: #3B3838;'>
                                    Serious Injuries
  or Illness
                                </span>
                            </u>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td valign="top" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $serious_injuries; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 26.5pt'>
                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #E7E6E6; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <u>
                                <span style='font-size: 14.0pt; color: #3B3838;'>Special Diets</span>
                            </u>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td valign="top" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $special_diets; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 26.5pt'>
                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #E7E6E6; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <u>
                                <span style='font-size: 14.0pt; color: #3B3838;'>Additional Information</span>
                            </u>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td valign="top" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $add_info; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
            </table>

        </div>

        <br />

        <br />

        <div align="center">


            <table class="MsoTableGrid" border="1"
                style='border-collapse: collapse; border: none; width:500pt;'>
                <tr style='height: 25.6pt'>
                    <td colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #BDD6EE; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <b>
                                <span
                                    style='font-size: 18.0pt'>
                                    Emergency Contact Information
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 26.5pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Primary Contact</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $primary_name; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Address</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $primary_addr; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Mobile Phone Number</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $primary_phone; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Email Address</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $primary_email; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Relationship</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $primary_relationship; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 5; height: 22.0pt'>
                    <td width="636" colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #DEEAF6; mso-background-themecolor: accent1; mso-background-themetint: 51; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <o:p>&nbsp;</o:p>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 26.5pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Secondary Contact</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $secondary_name; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Address</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $secondary_addr; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Mobile Phone Number</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $secondary_phone; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Email Address</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $secondary_email; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Relationship</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $secondary_relationship; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 9; height: 22.0pt'>
                    <td width="636" colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #DEEAF6; mso-background-themecolor: accent1; mso-background-themetint: 51; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <o:p>&nbsp;</o:p>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 10; height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Primary Physician</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $phy_name; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 11; height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Address</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $phy_addr; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 12; height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Phone</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $phy_phone; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 12; height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Email</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $phy_email; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 12; height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Type</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $phy_type; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
            </table>

        </div>

        <br />

        <br />

        <div align="center">

            <table class="MsoTableGrid" border="1"
                style='border-collapse: collapse; border: none;'>
                <tr style='height: 25.6pt'>
                    <td width="636" colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #FFC1B3; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                            <b>
                                <span
                                    style='font-size: 18.0pt'>
                                    Insurance Information
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 26.5pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Company Name</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $ins_company; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Group Number</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $groupno; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Contract Number</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $contractno; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Effective Date</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $effectivedate; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 5; height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Certification Number</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $certification_no; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 6; height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Phone</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $ins_phone; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 6; height: 22.0pt'>
                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <span style='font-size: 14.0pt'>Plan With</span>
                        </p>
                    </td>
                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                            <b>
                                <span style='font-size: 14.0pt'>
                                    <?php echo $plan_with; ?>
                                </span>
                            </b>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 6; height: 22.0pt'>
                    <td colspan="2" style='text-align:center;width: 337.65pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: .1in; margin-top: .1in; line-height: normal'>
                            <?php include("include/incInspic-front.php"); ?>
                        </p>
                    </td>
                </tr>
                <tr style='mso-yfti-irow: 6; height: 22.0pt'>
                    <td colspan="2" style='text-align:center;width: 337.65pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                        <p class="MsoNormal" style='margin-bottom: .1in; margin-top: .1in; line-height: normal'>
                            <?php include("include/incInspic-back.php"); ?>
                        </p>
                    </td>
                </tr>
            </table>

        </div>

        <br />
        <br />
        <hr style="width: 50%;height:1px;border:0;background:#333;background-image: linear-gradient(to right, #ccc, #333, #ccc);" />

    </div>


    <div>
        <p class="MsoClosing" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
            <span style='font-size: 9.0pt;'>
                <img width="102" height="27"
                     src="Emergency%20Information%20for_files/image007.png"
                     alt="http://emtelink.com/images/logo.png" />
            </span>
            <br />
            <span style='font-size: 8.0pt'>
                Report Generated by EMTeLink (https://www.emtelink.com)
            </span>
        </p>
    </div>

    <?php } else {

        $dt = new DateTime("@$starttime");

    ?>

    <div class="MsoClosing">
        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
            <span class="Heading1Char">
                <b>
                    <span style='font-size: 22.0pt; line-height: 115%; color: #595959;'>Emergency Information For:</span>
                </b>
            </span>
        </p>

        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
            <span class="Heading1Char">
                <b>
                    <span style='font-size: 24.0pt; line-height: 115%; color: red'>
                        <?php echo $firstname . " " . $middleinitial . " " . $lastname; ?>
                    </span>
                </b>
            </span>
        </p>
        <br />


        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
            <span style='font-size: 20.0pt; line-height: 100%; color: red'>This report has expired.</span>
            <span style='font-size: 18.0pt; line-height: 100%; color: black'>
                <br />
                <br />
                Reports are only valid for 24 Hours
                <br />
                and this report was originally generated at:
                <br />
                <?php echo $dt->format('Y-m-d H:i:s');?>
                <br />
                <br />
                Please contact our support department at: 855-363-6488
                <br />
                <br />
                if you need additional assistance or to have the report resent.
                <br />
                <br />

            </span>
        </p>

        <br />
        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
            <span style='font-size: 14.0pt; line-height: 100%; color: black'>
                &copy; Copyright @ 2016 EMTeLink
                <sup>&reg;</sup>
            </span>
        </p>
    </div>
    <?php } ?>
</body>

</html>
