
<?php

include("include/incConfig.php");
include("include/incFunctions.php");


if (isset($_GET['id'])) {
    $id_value = $_GET['id'];
    //echo $id_value . "<br />";
} else{
    echo "report user id not found, processing cannot continue.";
    exit;
}

use Urlcrypt\Urlcrypt;
require_once 'Urlcrypt.php';

Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
$decrypted = Urlcrypt::decrypt($id_value);


$decrypted = Urlcrypt::decrypt($id_value);

list($report_user_id, $starttime) = explode("|", $decrypted);

$endtime = time();
$timediff = $endtime - $starttime;

function calculateAge($date){

    //return date_diff(date_create($date), date_create('today'))->y;
    $birthDate = explode("-", $date);
    //get age from date or birthdate
    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
      ? ((date("Y") - $birthDate[2]) - 1)
      : (date("Y") - $birthDate[2]));
    return $age;


    //list($month,$day,$year) = explode("-",$date);
    //echo $year;
    //$year_diff  = date("Y") - $year;
    //$month_diff = date("m") - $month;
    //$day_diff   = date("d") - $day;
    //if ($day_diff < 0 || $month_diff < 0) $year_diff--;
    //return $year_diff;
}

//list($userid, $epoch_value) = split('|', $decrypted);
//echo "userid: $userid; ev: $epoch_value;<br />";
//echo $decrypted;

//        list($report_user_id, $epoch_value) = explode("|", $decrypted);
//echo $report_user_id;
//echo $epoch_value;

//session_start();

//make sure we have a valid sesion
//if ($_SESSION["valid"] != "TRUE")
//{
//    header("Location: index.html");
//}; JBrinson




//$report_user_id = 'dmassey';
$userid = $report_user_id;

$recordset = $database->select("user", [
    "subId",
    "firstname",
    "lastname",
    "middleinitial",
    "address",
    "city",
    "st",
    "zip",
    "dob",
    "phone",
    "student_email",
    "parent_email",
    "med_conditions",
    "hospitalizations",
    "serious_injuries",
    "special_diets",
    "add_info",
    "parent_name",
    "parent_addr",
    "parent_day_phone",
    "parent_night_phone",
    "relative_name",
    "relative_phone",
    "relationship",
    "family_doctor",
    "doctor_day_phone",
    "doctor_night_phone",
    "ins_company",
    "ins_phone",
    "groupno",
    "contractno",
    "effectivedate",
    "certification_no",
    "plan_with",
    "add_medical_conditions",
    "primary_name",
    "primary_addr",
    "primary_phone",
    "primary_carrierId",
    "primary_email",
    "primary_relationship",
    "secondary_name",
    "secondary_addr",
    "secondary_phone",
    "secondary_carrierId",
    "secondary_email",
    "secondary_relationship",
    "phy_name",
    "phy_addr",
    "phy_phone",
    "phy_email",
    "phy_type",
    "bloodtype"

], [
    "userid" => "$report_user_id"
]);

foreach($recordset as $data)
{
    $subId = $data["subId"];
    $firstname = $data["firstname"];
    $lastname = $data["lastname"];
    $middleinitial = $data["middleinitial"];
    $address = $data["address"];
    $blood_type = $data["bloodtype"];
    $city = $data["city"];
    $state = $data["st"];
    $zip = $data["zip"];
    //$dob = $data["dob"]->format('m-d-Y');
    //$dob = $data["dob"];
    $myDateTime = DateTime::createFromFormat('Y-m-d', $data["dob"]);
    if(!$myDateTime == ''){
        $dob = $myDateTime->format('m-d-Y');
        $age = calculateAge($dob);
    }

    $phone = $data["phone"];
    $student_email = $data["student_email"];
    $parent_email = $data["parent_email"];

    $med_conditions = $data["med_conditions"];
    $hospitalizations = $data["hospitalizations"];
    $serious_injuries = $data["serious_injuries"];
    $special_diets = $data["special_diets"];
    $add_info = $data["add_info"];
    $parent_name = $data["parent_name"];
    $parent_addr = $data["parent_addr"];
    $parent_day_phone = $data["parent_day_phone"];
    $parent_night_phone = $data["parent_night_phone"];
    $relative_name = $data["relative_name"];
    $relative_phone = $data["relative_phone"];
    $relationship = $data["relationship"];
    $family_doctor = $data["family_doctor"];
    $doctor_day_phone = $data["doctor_day_phone"];
    $doctor_night_phone = $data["doctor_night_phone"];
    $ins_company = $data["ins_company"];
    $ins_phone = $data["ins_phone"];
    $groupno = $data["groupno"];
    $contractno = $data["contractno"];
    //$effectivedate = $data["effectivedate"]->format('m-d-Y');
    $myDateTime = DateTime::createFromFormat('Y-m-d', $data["effectivedate"]);
    if(!$myDateTime == ''){
        $effectivedate = $myDateTime->format('m-d-Y');
    }
    //$effectivedate = $data["effectivedate"];
    $certification_no = $data["certification_no"];
    $plan_with = $data["plan_with"];
    $add_medical_conditions = $data["add_medical_conditions"];

    $primary_name = $data["primary_name"];
    $primary_addr = $data["primary_addr"];
    $primary_phone = $data["primary_phone"];
    $primary_email = $data["primary_email"];
    $primary_relationship = $data["primary_relationship"];
    $primary_carrierId = $data["primary_carrierId"];

    $secondary_name = $data["secondary_name"];
    $secondary_addr = $data["secondary_addr"];
    $secondary_phone = $data["secondary_phone"];
    $secondary_email = $data["secondary_email"];
    $secondary_relationship = $data["secondary_relationship"];
    $secondary_carrierId = $data["secondary_carrierId"];

    $phy_name = $data["phy_name"];
    $phy_addr = $data["phy_addr"];
    $phy_phone = $data["phy_phone"];
    $phy_email = $data["phy_email"];
    $phy_type = $data["phy_type"];

    $medcond=json_decode($med_conditions,true);
    //echo $subId;
}

$recordset = $database->select("Subscriptions", [
    "subType"
], [
    "subId" => "$subId"
]);

foreach($recordset as $data)
{
    $subType = $data["subType"];
}

?>

<table class="table table-bordered" style="width:98%">
    <tr>
        <td colspan="3" style="text-align:center;">
            <span style="font-family:Calibri,sans-serif;font-size: 22.0pt;color: #595959;">
                <strong>Emergency Information</strong>
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align:center;">
            <span style="font-family:Calibri,sans-serif;font-size: 24.0pt;color: red">
                <strong>
                    <?php echo $firstname . " " . $middleinitial . " " . $lastname; ?>
                </strong>
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align:center;">
            <?php include("include/incProfilepic.php"); ?>
        </td>
    </tr>
</table>
<table class="table table-bordered table_style" style="width:98%">
    <tr>
        <th class="th_25">DOB</th>
        <td colspan="2" style="vertical-align:middle">
            <?php echo $dob; ?>
        </td>
    </tr>
    <tr>
        <th class="th_25">Age</th>
        <td colspan="2" style="vertical-align:middle">
            <?php echo $age . " years old"; ?>
        </td>
    </tr>
    <tr>
        <th class="th_25">Address</th>
        <td colspan="2" style="vertical-align:middle">
            <?php echo $address."<br />".$city." ".$state." ".$zip; ?>
        </td>
    </tr>
    <tr>
        <th class="th_25">Blood Type</th>
        <td colspan="2" style="vertical-align:middle">
            <?php echo $blood_type; ?>
        </td>
    </tr>
</table>

<?php
//determine if there is at least one medical condition
$bolAtLeastOneCondition = "false";

//echo count($medcond);
for($i = 0, $size = count($medcond); $i < $size; ++$i) {
    //$people[$i]['salt'] = mt_rand(000000, 999999);
    if ($medcond[$i]['checked']==1) {
        $bolAtLeastOneCondition = "true<br />";
        //echo "cond - true<br />";
    }
    else {
        //echo "cond - false<br />";
    }
}

//echo "bolAtLeastOneCondition = " . $bolAtLeastOneCondition;

?>

<table class="table table-bordered table_style">
    <thead>
        <tr>
            <th colspan="3" style="background:#9CC2E5;">
                <span class="table_header_title">
                    Medical Conditions<?php if ($bolAtLeastOneCondition == "false") { echo "- none";} ?>
                </span>
            </th>
        </tr>
    </thead>

    <tbody>

        <?php
        if ($subType != "F") {

            if($medcond[0]['checked']==1) { ?>
        <tr>
            <td colspan="3">Mumps</td>
        </tr><?php }
            if($medcond[1]['checked']==1) { ?>
        <tr>
            <td colspan="3">Measles</td>
        </tr><?php }
            if($medcond[2]['checked']==1) { ?>
        <tr>
            <td colspan="3">Chicken Pox</td>
        </tr><?php }
            if($medcond[3]['checked']==1) { ?>
        <tr>
            <td colspan="3">Pneumonia</td>
        </tr><?php }
            if($medcond[4]['checked']==1) { ?>
        <tr>
            <td colspan="3">Diabetes</td>
        </tr><?php }
            if($medcond[5]['checked']==1) { ?>
        <tr>
            <td colspan="3">Cancer</td>
        </tr><?php }
            if($medcond[6]['checked']==1) { ?>
        <tr>
            <td colspan="3">Malaria</td>
        </tr><?php }
            if($medcond[7]['checked']==1) { ?>
        <tr>
            <td colspan="3">Seizures</td>
        </tr><?php }
            if($medcond[8]['checked']==1) { ?>
        <tr>
            <td colspan="3">Heart Disease</td>
        </tr><?php }
            if($medcond[9]['checked']==1) { ?>
        <tr>
            <td colspan="3">Kidney Disease</td>
        </tr><?php }
            if($medcond[10]['checked']==1) { ?>
        <tr>
            <td colspan="3">Tuberculosis</td>
        </tr><?php }
            if($medcond[11]['checked']==1) { ?>
        <tr>
            <td colspan="3">Rheumatic Fever</td>
        </tr><?php }
            if($medcond[12]['checked']==1) { ?>
        <tr>
            <td colspan="3">Hepatitis</td>
        </tr><?php }
            if($medcond[13]['checked']==1) { ?>
        <tr>
            <td colspan="3">Blood Pressure Problems</td>
        </tr><?php }
            if($medcond[14]['checked']==1) { ?>
        <tr>
            <td colspan="3">Tetanus Toxoid Shot (in last 12 months)</td>
        </tr><?php }
            if( strlen($add_medical_conditions) > 1) { ?>
        <tr>
            <td colspan='3'>
                <?php  echo $add_medical_conditions;?>
            </td>
        </tr><?php }



        }   //subType != F
        else {

            if($medcond[0]['checked']==1) { ?>
        <tr>
            <td colspan="3">Asthma</td>
        </tr><?php }
            if($medcond[1]['checked']==1) { ?>
        <tr>
            <td colspan="3">COPD</td>
        </tr><?php }
            if($medcond[2]['checked']==1) { ?>
        <tr>
            <td colspan="3">Seizure Disorder</td>
        </tr><?php }
            if($medcond[3]['checked']==1) { ?>
        <tr>
            <td colspan="3">Dementia</td>
        </tr><?php }
            if($medcond[4]['checked']==1) { ?>
        <tr>
            <td colspan="3">Alzheimer</td>
        </tr><?php }
            if($medcond[5]['checked']==1) { ?>
        <tr>
            <td colspan="3">Hyperglycemia</td>
        </tr><?php }
            if($medcond[6]['checked']==1) { ?>
        <tr>
            <td colspan="3">Hypoglycemia</td>
        </tr><?php }
            if($medcond[7]['checked']==1) { ?>
        <tr>
            <td colspan="3">Diabetes Type 1</td>
        </tr><?php }
            if($medcond[8]['checked']==1) { ?>
        <tr>
            <td colspan="3">Diabetes Type 2</td>
        </tr><?php }
            if($medcond[9]['checked']==1) { ?>
        <tr>
            <td colspan="3">High Blood Pressure</td>
        </tr><?php }
            if($medcond[10]['checked']==1) { ?>
        <tr>
            <td colspan="3">Contact Lenses</td>
        </tr><?php }
            if($medcond[11]['checked']==1) { ?>
        <tr>
            <td colspan="3">Rheumatic Fever</td>
        </tr><?php }
            if($medcond[12]['checked']==1) { ?>
        <tr>
            <td colspan="3">Pacemaker</td>
        </tr><?php }
            if($medcond[13]['checked']==1) { ?>
        <tr>
            <td colspan="3">Heart Stent</td>
        </tr><?php }
            if($medcond[14]['checked']==1) { ?>
        <tr>
            <td colspan="3">Tetanus Shot (last 12 mnths)</td>
        </tr><?php }
            if( strlen($add_medical_conditions) > 1) { ?>
        <tr>
            <td colspan='3'>
                <?php  echo $add_medical_conditions;?>
            </td>
        </tr><?php }

        }?>


    </tbody>
</table>
<table class="table table-bordered table_style">
    <thead>
        <tr>
            <th colspan="3" style="background:#F4B083;">
                <span class="table_header_title">Medications</span>
            </th>
        </tr>
        <tr style="background:#F8CCAE;">
            <th>Name</th>
            <th>Dosage</th>
            <th>Frequency</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $recordset = $database->select("user_medications", [
            "medicationId",
            "medication_name",
            "dosage",
            "howoften"
        ], [
            "userid" => "$report_user_id",
             "ORDER" => ['medication_name ASC']
        ]);

        foreach($recordset as $data)
        {
            //$medicationId = $data["medicationId"];
            $medication_name = $data["medication_name"];
            $dosage = $data["dosage"];
            $howoften = $data["howoften"];

        ?>
        <tr>
            <td>
                <?php echo $medication_name; ?>
            </td>
            <td>
                <?php echo $dosage; ?>
            </td>
            <td>
                <?php echo $howoften; ?>
            </td>
        </tr>
    </tbody>
    <?php  }?>
</table>


<table class="table table-bordered table_style">
    <thead>
        <tr>
            <th colspan="3" style="background:#A8D08D;">
                <span class="table_header_title">Allergies</span>
            </th>
        </tr>
        <tr style="background:#C5E0B3">
            <th style="text-align:center">Description</th>
            <th colspan="2" style="text-align:center">Notes</th>
        </tr>
    </thead>
    <tbody>
        <?php

        $recordset = $database->select("user_allergies", [
            "allergyId",
            "description",
            "notes"
        ], [
            "userid" => "$report_user_id",
             "ORDER" => ['description ASC']
        ]);

        foreach($recordset as $data)
        {
            $allergyId = $data["allergyId"];
            $description = $data["description"];
            $notes = $data["notes"];

        ?>
        <tr>
            <td>
                <?php echo $description; ?>
            </td>
            <td colspan="2">
                <?php echo $notes ; ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<br />
<table class="table table-bordered table_style">
    <thead>
        <tr>
            <th colspan="3" style="background:#AFB3FF;">
                <span class="table_header_title">Miscellaneous</span>
            </th>
        </tr>
        <tr>
            <th colspan="3" style="background:#CDD1FF;">
                <span class="">Hospitalizations</span>
            </th>
        </tr>
        <tr>
            <td colspan="3" style="background:#FFFFFF;">
                <span class="">
                    <?php echo $hospitalizations; ?>
                </span>
            </td>
        </tr>
        <tr>
            <th colspan="3" style="background:#CDD1FF;">
                <span class="">Serious Injuries or Illness</span>
            </th>
        </tr>
        <tr>
            <td colspan="3" style="background:#FFFFFF;">
                <span class="">
                    <?php echo $serious_injuries; ?>
                </span>
            </td>
        </tr>
        <tr>
            <th colspan="3" style="background:#CDD1FF;">
                <span class="">Special Diets</span>
            </th>
        </tr>
        <tr>
            <td colspan="3" style="background:#FFFFFF;">
                <span class="">
                    <?php echo $special_diets; ?>
                </span>
            </td>
        </tr>
        <tr>
            <th colspan="3" style="background:#CDD1FF;">
                <span class="">Additional Information</span>
            </th>
        </tr>
        <tr>
            <td colspan="3" style="background:#FFFFFF;">
                <span class="">
                    <?php echo $add_info; ?>
                </span>
            </td>
        </tr>
    </thead>
</table>
<br />
<table class="table table-bordered table_style">
    <thead>
        <tr>
            <th colspan="3" style="background:#BDD6EE;">
                <span class="table_header_title">Emergency Contacts</span>
            </th>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Primary Contact</th>
            <td colspan="2">
                <?php echo $primary_name; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Address</th>
            <td colspan="2">
                <?php echo $primary_addr; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Mobile Phone</th>
            <td colspan="2">
                <?php echo $primary_phone; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Email</th>
            <td colspan="2">
                <?php echo $primary_email; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">How Related</th>
            <td colspan="2">
                <?php echo $primary_relationship; ?>
            </td>
        </tr>
        <tr>
            <td style="background:#DEEAF6" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Secondary Contact</th>
            <td colspan="2">
                <?php echo $secondary_name; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Address</th>
            <td colspan="2">
                <?php echo $secondary_addr; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Mobile Phone</th>
            <td colspan="2">
                <?php echo $secondary_phone; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Email</th>
            <td colspan="2">
                <?php echo $secondary_email; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">How Related</th>
            <td colspan="2">
                <?php echo $secondary_relationship; ?>
            </td>
        </tr>
        <tr>
            <td style="background:#DEEAF6" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Primary Physician</th>
            <td colspan="2">
                <?php echo $phy_name; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Address</th>
            <td colspan="2">
                <?php echo $phy_addr; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Phone</th>
            <td colspan="2">
                <?php echo $phy_phone; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Email</th>
            <td colspan="2">
                <?php echo $phy_email; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#DEEAF6">Type</th>
            <td colspan="2">
                <?php echo $phy_type; ?>
            </td>
        </tr>
    </thead>
</table>
<br />
<table class="table table-bordered table_style">
    <thead>
        <tr>
            <th colspan="3" style="background:#FFC1B3;">
                <span class="table_header_title">Insurance Information</span>
            </th>
        </tr>
        <tr>
            <th style="background:#FFDED7">Company Name</th>
            <td colspan="2">
                <?php echo $ins_company; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#FFDED7">Group Number</th>
            <td colspan="2">
                <?php echo $groupno; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#FFDED7">Contact Nunber</th>
            <td colspan="2">
                <?php echo $contractno; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#FFDED7">Effective Date</th>
            <td colspan="2">
                <?php echo $effectivedate; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#FFDED7">Certification Number</th>
            <td colspan="2">
                <?php echo $certification_no; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#FFDED7">Phone</th>
            <td colspan="2">
                <?php echo $ins_phone; ?>
            </td>
        </tr>
        <tr>
            <th style="background:#FFDED7">Plan With</th>
            <td colspan="2">
                <?php echo $plan_with; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php include("include/incInspic-front.php"); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php include("include/incInspic-back.php"); ?>
            </td>
        </tr>
    </thead>
</table>
<br />


