<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>MyEMRMyWay :: My Password</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="MyEMRMyWay App">
	
	
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->

    <link href="Content/styles.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="admin/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <link href="admin/css/base-admin-3.css" rel="stylesheet" />
    <link href="admin/css/base-admin-3-responsive.css" rel="stylesheet" />

    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");
    session_start();

    //make sure we have a valid sesion
	include("include/session.php");
	
    $consent_signature = "";
    $link_dir = "https://emtelink.com/school_dev/uploads/bhs/";

    ?>

    <script type="text/javascript">

        function afterSuccess() {
            //$('#submit-btn').show(); //hide submit button
            //$('#loading-img').hide(); //hide submit button
            location.reload();
        }

        //function to check file size before uploading.
        function beforeSubmit() {

            //alert("before submit");
            //check whether browser fully supports all File API
            if (window.File && window.FileReader && window.FileList && window.Blob) {

                if (!$('#photoimg').val()) //check empty input filed
                {
                    $("#preview").html("Please select a file to upload.");
                    return false
                }

                var fsize = $('#photoimg')[0].files[0].size; //get file size
                var ftype = $('#photoimg')[0].files[0].type; // get file type


                //allow only valid image file types
                switch (ftype) {
                    case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/jpg':
                        break;
                    default:
                        $("#preview").html("<b>" + ftype + "</b> is an Unsupported file type.<br>Please select an image to upload (jpg/gif/png).");
                        return false
                }

                //Allowed file size is less than 1 MB (1048576)
                if (fsize > 1048576) {
                    $("#preview").html("<b>" + bytesToSize(fsize) + "</b> The Image file is too big! <br />Please reduce the size of your photo using an image editor.");
                    return false
                }
                //$('#submit-btn').hide(); //hide submit button
                //$('#loading-img').show(); //hide submit button
                //$("#preview").html("");
            }
            else {
                //Output error to older browsers that do not support HTML5 File API
                $("#preview").html("Please upgrade your browser, because your current browser does not support the features we need.");
                return false;
            }
        }

        //function to format bites bit.ly/19yoIPO
        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Bytes';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }
    </script>

    <style>

ul, li {
    margin:0;
    padding:0px;
    list-style-type:none;
    text-align:left;
}

#pswd_info {
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
}

#pswd_info h5 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}


.invalid {
    background:url(../images/redx.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    background:url(../images/green-ck.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}


    </style>


</head>

<body>

    <!-- Navbar -->
	<?php include("include/navbar.php"); ?>
    <!-- End navbar -->
	
	
    <!-- jumbotron-->
    <div class="well well-sm">
        <div class="text-center">
            <h2>
                <i class="icon-key"></i>
                Password for <?php echo $firstname . " " . $lastname ?>
            </h2>
        </div>
        <!-- End container -->
    </div>
    <!-- End jumbotron-->

    <div class="container">
        <section>
            <div class="row">

                <?php
                $instruction_content = "Change your password or profile picture below.<br /><br />";
                $instruction_content = $instruction_content . "Select the \"Choose File\" or \"Browse\" button to select a new profile picture.<br /><br />";
                $instruction_content = $instruction_content . "To change your password, enter your old password, your new password, and confirm by entering the new password again.<br />Then click the \"Update Password\" button.";
                include("include/incInstructions.php");
                ?>
                <div id="demographic">
                    <span class="clearfix"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-primary text-center" style="border-color:black;">
                            <div class="panel-heading">
                                <h3 class="title" style="font-weight:900">
                                    <u>Change Password &amp; Profile Picture</u>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="center">
                                    <div id='preview'>
                                        <?php
                                        include("include/incProfilepic.php");
                                        ?>
                                    </div>
                                    <br />
                                    <form id="imageform" method="post" enctype="multipart/form-data" action='ws/processupload.php'>
                                        <input type="file" name="photoimg" id="photoimg" style="margin: auto; width: 90px; height: 25px;" />
                                        <input type="hidden" id="fileuserid" name="fileuserid" value="<?php echo $userid ?>" />
                                    </form>
                                    <br />
                                </div>
                                <form id="profileform" class="profileform">
                                    <br />
                                    <div class="row">
                                        <label class="col-xs-12 col-sm-2 pull-left" for="currentpassword">Current Password</label>
                                        <div class="col-xs-12 col-sm-10">
                                            <input type="password" class="form-control" id="currentpassword" name="currentpassword" placeholder="enter current password" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <label class="col-xs-12 col-sm-2 pull-left" for="newpassword">New Password</label>
                                        <div class="col-xs-12 col-sm-10">
                                            <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="enter new password" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-xs-2 col-sm-3"></div>
                                        <div id="pswd_info" class="col-xs-8 col-sm-6 center-block">
                                            <h5 style="text-align:left">New Password must meet the following requirements:</h5>
                                            <ul>
                                                <li id="letter" class="invalid">
                                                    At least
                                                    <strong>one letter</strong>
                                                </li>
                                                <li id="capital" class="invalid">
                                                    At least
                                                    <strong>one capital letter</strong>
                                                </li>
                                                <li id="number" class="invalid">
                                                    At least
                                                    <strong>one number</strong>
                                                </li>
                                                <li id="length" class="invalid">
                                                    Be at least
                                                    <strong>8 characters</strong>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-2 col-sm-3"></div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <label class="col-xs-12 col-sm-2 pull-left" for="confirmpassword">Confirm New Password</label>
                                        <div class="col-xs-12 col-sm-10">
                                            <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="confirm new password" />
                                        </div>
                                    </div>
                                    <div>
                                        <input type="hidden" id="userid" name="userid" value="<?php echo $userid ?>" />
                                    </div>
                                    <br />
                                    <button disabled type="button" id="btn_update_profile" class="btn input-md" style="background-color:#3881C0;color:white;">Update Password</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <br />

    <div class="text-center">
        <a href="Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">Return To Home Screen</a>
    </div>
    <br />

    <!-- Footer -->
    <footer>
        <div class="extra">
            <div class="container text-center">
                <p>&copy; Copyright @ 2016 EMTeLink</p>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Container-->
    </footer>

    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap-dialog.min.js"></script>
    <script src="Scripts/jquery.form.js"></script>


    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $('input[id=newpassword]').keyup(function () {
                // set password variable
                var pswd = $(this).val();

                //validate the length
                if (pswd.length < 8) {
                    $('#length').removeClass('valid').addClass('invalid');
                } else {
                    $('#length').removeClass('invalid').addClass('valid');
                }

                //validate letter
                if (pswd.match(/[A-z]/)) {
                    $('#letter').removeClass('invalid').addClass('valid');
                } else {
                    $('#letter').removeClass('valid').addClass('invalid');
                }

                //validate capital letter
                if (pswd.match(/[A-Z]/)) {
                    $('#capital').removeClass('invalid').addClass('valid');
                } else {
                    $('#capital').removeClass('valid').addClass('invalid');
                }

                //validate number
                if (pswd.match(/\d/)) {
                    $('#number').removeClass('invalid').addClass('valid');
                } else {
                    $('#number').removeClass('valid').addClass('invalid');
                }


                if ($("#number").hasClass('valid') && $("#letter").hasClass('valid') && $("#capital").hasClass('valid') && $("#length").hasClass('valid')) {

                    //alert('number valid');
                    $("#btn_update_profile").prop('disabled', false);
                }
                else {

                    $("#btn_update_profile").prop('disabled', true);
                }
            })

            //####################################################### Signature EDIT Button ##################################
            $('#btn_update_profile').on('click', function (e) {
                //$(this).hide();

                //alert($('#currentpassword').val());
                //alert($('#newpassword').val());
                //alert($('#confirmpassword').val());

                //verify current pw entered
                if ($('#currentpassword').val() == '') {
                    var modal = $('#myModal');
                    modal.find('.modal-title').text('Missing Current Password');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter your <u>current</u> password.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                if ($('#newpassword').val() == '') {
                    var modal = $('#myModal');

                    modal.find('.modal-title').text('Missing New Password');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter your <u>new</u> password.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                if ($('#confirmpassword').val() == '') {
                    var modal = $('#myModal');
                    modal.find('.modal-title').text('Missing Confirmation');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please <u>confirm</u> your new password.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                if ($('#confirmpassword').val() != $('#newpassword').val()) {
                    var modal = $('#myModal');

                    modal.find('.modal-title').text('New Password and Confirmation don\'t match');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please make sure your <u>new</u> and <u>confirm</u> password match.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                if ($('#currentpassword').val() == $('#newpassword').val()) {
                    var modal = $('#myModal');

                    modal.find('.modal-title').text('New Password Error');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Your <u>new password</u> <u>cannot be the same</u> as your <u>current</u> password.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                //alert("here");
                //post the values to the server
                $.ajax({
                    type: "POST",
                    url: "ws/process_password.php",
                    data: $('form.profileform').serialize(),
                    success: function (msg) {
                        //$('#sql_stmt').val(msg);
                        //$('#signature-success').removeClass('hide');
                        //$.wait(function () { $('#signature-success').addClass('hide') }, 2);
                        //alert(msg);
                        //disable controls for the sign form
                        if (msg == 'invalid') {

                            var modal = $('#myModal');
                            modal.find('.modal-title').text('Invalid Current Password');
                            modal.find('.modal-body').html('<br /><span style="color:red"><h5>The password you entered is incorrect, please try again.</h5></span>');
                            $('#myModal').modal('show');
                            return;

                        }

                        if (msg == 'success') {

                            $('#newpassword').val('');
                            $('#currentpassword').val('');
                            $('#confirmpassword').val('');

                            var modal = $('#myModal');
                            modal.find('.modal-title').text('Password Updated');
                            modal.find('.modal-body').html('<br /><span style="color:green"><h5>Password updated successfully.</h5></span>');
                            $('#myModal').modal('show');
                            return;
                        }
                    },
                    error: function () {
                        alert("Error saving profile");
                    }
                });


            })


            $('#photoimg').on('change', function () {
                $("#preview").html('');
                $("#preview").html('<img src="img/loader.gif" alt="Uploading...."/>');
                $("#imageform").ajaxForm({
                    target: '#preview',   // target element(s) to be updated with server response
                    beforeSubmit: beforeSubmit,  // pre-submit callback
                    success: afterSuccess,  // post-submit callback
                    resetForm: true        // reset the form after successful submit
                }).submit();

            });


        });
    </script>







</body>
</html>
