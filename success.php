
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>MyEMRMyWay :: Success</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="MyEMRMyWay App">
    <!-- Latest compiled and minified CSS -->
    <!-- Optional theme -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/styles.css" rel="stylesheet" />

    <style>
        .extra {
            border-top: 1px solid #000;
            padding: 10px 0;
            font-size: 11px;
            color: #BBB;
            background: #1A1A1A;
            -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.3);
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.3);
        }
    </style>
</head>

<body>

    <!-- Navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="my-navbar" style="background-color:#474749">
        <div class="container">
            <div class="navbar-header" style="background-color:#474749">
                <a class="navbar-brand" href="./home.php" style="padding:0;"><span><img src="https://www.emtelink.com/myemrmyway/img/MyEMRMyWay_Logo.png" width="90" height="43" alt="logo" style="padding-top:2px; !important"></span></a>
            </div><!-- Navbar Header-->
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <!-- <li><a href="#Register">Register</a></li> -->
                </ul>
            </div>
        </div><!-- End Container-->
    </nav><!-- End navbar -->

    <!-- jumbotron-->
    <br />
    <br />
    <div class="well">
        <div class="container text-center">
            <h2>
                Congratulations 
                <br />
                your payment has been successfully processed.
            </h2>
        </div>
        <!-- End container -->
    </div>
    <!-- End jumbotron-->
    <div class="container">
        <section>
            <div id="login">
                <span class="clearfix"></span>
            </div>
            <div class="row">
                <div class="col-xs-1 col-sm-2 col-md-3"></div>
                <div class="col-xs-10 col-sm-8 col-md-6">
                    <div class="account-wall text-center">
                        <img src="https://www.emtelink.com/myemrmyway/img/MyEMRMyWay_Logo_color.png" alt="MyEMRMyWay Logo" />
                        <br />
                        <br />
                        <h3>
                            <u>
                                <a href='https://www.emtelink.com/myemrmyway'>Click Here to Go To The Logon Page</a>
                            </u>
                        </h3>
                    </div>
                </div>
                <div class="col-xs-1 col-sm-2 col-md-3"></div>
            </div>
            <br />
        </section>
    </div>

    <div class="navbar navbar-fixed-bottom">
        <footer>
            <div class="extra text-center">
                &copy; Copyright @ 2018 EMTeLink&nbsp;&nbsp;Saving Lives With Technology
                <br />
            </div>
            <!-- end Container-->
        </footer>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>
