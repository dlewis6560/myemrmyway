<?php

include("../include/incConfig.php");

if (isset($_POST['userid_contact'])) {
  
  $userid = strip_tags($_POST['userid_contact']);

  $primary_name = strip_tags($_POST['primary_name']);
  $primary_addr = strip_tags($_POST['primary_addr']);
  $primary_phone = strip_tags($_POST['primary_phone']);
  $primary_carrier = strip_tags($_POST['primary_carrier']);

  //echo "carrier =" . $primary_carrier;
  $recordset = $database->select("carriers", [
            "Id"
        ], [
            "carrier" => "$primary_carrier"
        ]);

  foreach($recordset as $data)
  {
      $primary_carrierId = $data["Id"];
  }   
  //echo $primary_carrierId;


  //$primary_carrierId = strip_tags($_POST['primary_carrierId']);
  $primary_email = strip_tags($_POST['primary_email']);
  $primary_relationship = strip_tags($_POST['primary_relationship']);

  $secondary_name = strip_tags($_POST['secondary_name']);
  $secondary_addr = strip_tags($_POST['secondary_addr']);
  $secondary_phone = strip_tags($_POST['secondary_phone']);
  $secondary_carrier = strip_tags($_POST['secondary_carrier']);

    //echo "carrier =" . $secondary_carrier;
  $recordset = $database->select("carriers", [
            "Id"
        ], [
            "carrier" => "$secondary_carrier"
        ]);

  foreach($recordset as $data)
  {
      $secondary_carrierId = $data["Id"];
  }   
  //echo $secondary_carrierId;
  $secondary_email = strip_tags($_POST['secondary_email']);
  $secondary_relationship = strip_tags($_POST['secondary_relationship']);

  $phy_name = strip_tags($_POST['phy_name']);
  $phy_addr = strip_tags($_POST['phy_addr']);
  $phy_phone = strip_tags($_POST['phy_phone']);
  $phy_email = strip_tags($_POST['phy_email']);
  $phy_type = strip_tags($_POST['phy_type']);

  $parent_name = strip_tags($_POST['parent_name']);
  $parent_addr = strip_tags($_POST['parent_addr']);
  $parent_day_phone = strip_tags($_POST['parent_day_phone']);
  $parent_night_phone = strip_tags($_POST['parent_night_phone']);
  $relative_name = strip_tags($_POST['relative_name']);
  $relationship = strip_tags($_POST['relationship']);
  $relative_phone = strip_tags($_POST['relative_phone']);
  $family_doctor = strip_tags($_POST['family_doctor']);
  $doctor_day_phone = strip_tags($_POST['doctor_day_phone']);
  $doctor_night_phone = strip_tags($_POST['doctor_night_phone']);

  $database->update("user", [
      "primary_name" => $primary_name,
      "primary_addr" => $primary_addr,
      "primary_phone" => $primary_phone,
      "primary_email" => $primary_email,
      "primary_carrierId" => $primary_carrierId,
      "primary_relationship" => $primary_relationship,
      "secondary_name" => $secondary_name,
      "secondary_addr" => $secondary_addr,
      "secondary_phone" => $secondary_phone,
      "secondary_email" => $secondary_email,
      "secondary_carrierId" => $secondary_carrierId,
      "secondary_relationship" => $secondary_relationship,
      "phy_name" => $phy_name,
      "phy_addr" => $phy_addr,
      "phy_phone" => $phy_phone,
      "phy_email" => $phy_email,
      "phy_type" => $phy_type
	 ], [
		"userid" => $userid
	 ]);

  //echo "primary carrier" . $primary_carrier;

  //echo "Contact Information Saved Successfully";

}?>

