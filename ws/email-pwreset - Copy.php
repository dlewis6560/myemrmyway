<?php
require '../PhpMailer/PHPMailerAutoload.php';

use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';

include("../Include/incConfig.php");


if (isset($_POST['email'])) {

    //get the posted values from the consent page ajax
    $emailaddress = $_POST['email'];

    //make sure email exists
    $count = $database->count("user", [
	"student_email" => $emailaddress
    ]);

    if ($count < 1)
    {
        echo "noexist";
        exit;
    }

    $verification_code = strtoupper(generateRandomString(5));

	$recordset = $database->select("user", [
		"userid", "firstname"
	], [
        "student_email" => $emailaddress
	]);

	foreach($recordset as $data)
	{
		$userid = $data["userid"];
        $firstname = $data["firstname"];
	}

    //send an email
    Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
    $encrypted = Urlcrypt::encrypt($userid . "|" . time());

    $mail = new PHPMailer;

    //$mail->SMTPDebug = 2;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.mailgun.org';                     // Specify main and backup SMTP servers // secure.emailsrvr.com
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'postmaster@emtelink.com';          // SMTP username
    $mail->Password = 'f6175a9ce7d5a17d09ba7a2e81c54109'; // SMTP password
    mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted

    //$mail->isSMTP();                                      // Set mailer to use SMTP
    //$mail->Host = 'secure.emailsrvr.com';                     // Specify main and backup SMTP servers // secure.emailsrvr.com
    //$mail->SMTPAuth = true;                               // Enable SMTP authentication
    //$mail->Username = 'info@emtelink.com';          // SMTP username
    //$mail->Password = 'fish12'; // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted

    $mail->From = 'support@EMTeLink.com';
    $mail->FromName = 'support@EMTeLink.com';
    $mail->addAddress($emailaddress);     // Add a recipient
    $mail->AddBCC("dean@emtelink.com","Dean Massey");
    $mail->AddBCC("dlewis6560@aol.com","Donnie Lewis");

    $mail->addReplyTo('support@EMTeLink.com', 'support@EMTeLink.com');
    //$mail->addCC('cc@example.com');
    ////$mail->addBCC('bcc@example.com');

    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true);                                    // Set email format to HTML

    $mail->Subject = 'EMTELINK Password Reset Request';
    $mail->Body    = '<!DOCTYPE html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
    <title></title>
    <style type=text/css>
        .text-style {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
            font-style: normal;
        }

        .text-xsmall {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            font-style: normal;
        }
    </style>
</head>
<body>
    <div class=text-style>
        Hi '.$firstname.',
        <br />
        <br />
        Click the link below to reset your password.
        <br />
        <br />
        <a href=https://www.emtelink.com/tracker/pwd-reset.php?id=' .  $encrypted.' =>Click To Reset Your Password </a>
        <br /><br />
        If you have any questions, please contact our support department at support@emtelink.com.
        <br /><br />
        - The EMTELINK Team
    </div>
    <br /> <br />
    <div class=text-xsmall>
        This email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager by responding to
        support@EMTeLink.com. Please do not disseminate, distribute or copy this e-mail. Please notify the EMTeLink immediately by e-mail if you have received this e-mail
        by mistake and delete this e-mail from your system.
    </div>
</body>
</html>';

    $mail->AltBody = 'Hi '.$firstname.',

Copy and Paste to your Browser the link below to reset your password.

https://www.emtelink.com/tracker/pwd-reset.php?id =' . $encrypted.'

If you have any questions, please contact our support department at support@emtelink.com.


- The EMTELINK Team

This email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager by responding to support@EMTeLink.com. Please do not disseminate, distribute or copy this e-mail. Please notify the EMTeLink immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system.';

if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo ("success - " . $verification_code);
    }

    exit;
}

//exit;

function ajaxResponse($status, $message, $data = NULL, $mg = NULL) {
    $response = array (
      'status' => $status,
      'message' => $message,
      'data' => $data,
      'mailgun' => $mg
      );
    $output = json_encode($response);
    exit($output);
}

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


?>