<?php

include("../include/incConfig.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    session_start();
    //make sure we have a valid sesion
	include("../include/session.php");


if (isset($_POST['ins_company'])) {

    $id_value =  htmlspecialchars($_POST["hidden"]);
    $decrypted = Urlcrypt::decrypt($id_value);
    $mode = "";
    //echo "id_value = " . $id_value;
    //echo "decrypted = " . $decrypted;

    list($companyid, $mode, $starttime) = explode("|", $decrypted);

    //echo "companyid=" . $companyid;
    //exit;

	//$character = json_decode($data);
	//echo $character->name;

	/*
	if ($_POST['hmac'] !== create_hmac($secret, $_SERVER['REQUEST_URI'], $hidden)) {
		// invalid HMAC
		echo 'invalid';
	} else {

		echo var_dump($hidden);
	} */

	//echo $uid;

    //get the posted values from the consent page ajax
    //$uid = strip_tags($_POST['uid']);

    $ins_company_name = filter_var($_POST['ins_company'], FILTER_SANITIZE_STRING);
	$groupno = filter_var($_POST['groupno'], FILTER_SANITIZE_STRING);
	$contractno = filter_var($_POST['contractno'], FILTER_SANITIZE_STRING);
	$effective_date = filter_var($_POST['effective_date_picker'], FILTER_SANITIZE_STRING);
	$certification_no = filter_var($_POST['certification_no'], FILTER_SANITIZE_STRING);
	$phone_number = filter_var($_POST['phone_number'], FILTER_SANITIZE_STRING);
	$plan_with= filter_var($_POST['plan_with'], FILTER_SANITIZE_STRING);

    $ins_company_name_length = strlen($ins_company_name);
	$groupno_length = strlen($groupno);
	$contractno_length = strlen($contractno);
	$effective_date_length = strlen($effective_date);
	$certification_no_length = strlen($certification_no);
	$phone_number_length = strlen($phone_number);
	$plan_with_length = strlen($plan_with);

    $lastModified = gmdate("Y-m-d H:i:s");

	if ($ins_company_name_length < 2){
	  $output= "Invalid Company Name. It must be greater than 2 characters.";
	  echo $output;
	  exit;
	}

	if ($ins_company_name_length > 128){
	  $output= "Invalid Company Name. It cannot be greater than 128 characters.";
	  echo $output;
	  exit;
	}

    $database->update("user_ins_companies", [
        "CompanyName" => "$ins_company_name",
        "GroupNumber" => "$groupno",
        "ContractNumber" => "$contractno",
        "EffectiveDate" => "$effective_date",
        "CertificationNumber" => "$certification_no",
        "PhoneNumber" => "$phone_number",
        "PlanWith" => "$plan_with",
        "EditDate" => "$lastModified"
    ], [
        "CompanyId" => $companyid
    ]);

    echo ("Company has been updated successfully.|");
    exit;
}
else{
    echo ("new Company not added");
}

?>
