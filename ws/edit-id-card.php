<?php

include("../include/incConfig.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    session_start();
    //make sure we have a valid sesion
	include("../include/session.php");

if (isset($_POST['id_number'])) {

    $id_value =  htmlspecialchars($_POST["hidden"]);
    $decrypted = Urlcrypt::decrypt($id_value);
    $mode = "";
    //echo "id_value = " . $id_value;
    //echo "decrypted = " . $decrypted;

    list($cardid, $mode, $starttime) = explode("|", $decrypted);

    $id_number = filter_var($_POST['id_number'], FILTER_SANITIZE_STRING);
	$id_description = filter_var($_POST['id_description'], FILTER_SANITIZE_STRING);

    $id_number_length = strlen($id_number);
	$id_description_length = strlen($id_description);

    $lastModified = gmdate("Y-m-d H:i:s");

	if ($id_number_length < 1){
	  $output= "Invalid ID Number. It must be greater than 1 character.";
	  echo $output;
	  exit;
	}

	if ($id_number_length > 50){
	  $output= "Invalid ID Number. It cannot be greater than 50 characters.";
	  echo $output;
	  exit;
	}

    $database->update("user_ids", [
        "idnumber" => "$id_number",
        "description" => "$id_description",
        "EditDate" => "$lastModified"
    ], [
        "uniqueId" => $cardid
    ]);

    echo ("ID has been created successfully.");
    exit;
}
else{
    echo ("new Card not added");
}

?>
