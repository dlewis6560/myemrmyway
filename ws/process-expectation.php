<?php

include("../include/incConfig.php");

if (isset($_POST['userid'])) {

    //get the posted values from the consent page ajax
    $userid = strip_tags($_POST['userid']);

    $sign_date =  strip_tags($_POST['sign-date-picker']);
    $parent_name = $_POST['parent_name'];
    $parent_signature = $_POST['parent_signature_post_field'];
    $student_name = $_POST['student_name'];
    $student_signature = $_POST['student_signature_post_field'];


    if (strlen($sign_date) < 1) { $sign_date = NULL;}

    if (strlen($parent_signature_post_field) < 1) { $parent_signature_post_field = NULL;}

    if (strlen($student_signature_post_field) < 1) { $student_signature_post_field = NULL;}


    //update the consent fields in the student table
    $database->update("user", [
      "expectation_sign_date" => $sign_date,
      "expectation_parent_name" => $parent_name,
      "expectation_parent_signature" => $parent_signature,
      "expectation_student_name" => $student_name,
      "expectation_student_signature" => $student_signature
	  ], [
		  "userid" => $userid
	]);

    echo $parent_signature_post_field;
}
?>
