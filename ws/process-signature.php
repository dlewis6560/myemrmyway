<?php

include("../include/incConfig.php");

if (isset($_POST['userid_signature'])) {
  
    $userid = strip_tags($_POST['userid_signature']);

    $sign_date =  strip_tags($_POST['sign-date-picker']);
    $parent_signature = $_POST['parent_signature_post_field'];

    $witness_signature = $_POST['witness_signature_post_field'];

    //echo $witness_signature;
    //exit;

  $database->update("user", [
    "sign_date" => $sign_date,
    "parent_signature" => $parent_signature,
    "witness_signature" => $witness_signature

	], [
		"userid" => $userid
	]);  

  //echo  $database->error();

}?>
