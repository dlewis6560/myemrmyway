<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>MyEMRMyWay :: Activate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="MyEMRMyWay App">
    <!-- Latest compiled and minified CSS -->
    <!-- Optional theme -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/styles.css" rel="stylesheet" />
    <link rel="shortcut icon" href="https://emtelink.com/tracker/emtelink.ico" />

    <style>
        .extra {
            border-top: 1px solid #000;
            padding: 10px 0;
            font-size: 11px;
            color: #BBB;
            background: #1A1A1A;
            -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.3);
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.3);
        }
    </style>
</head>

<?php

include("../Include/incConfig.php");

if (isset($_GET['id'])) {
    $id_value = $_GET['id'];
    //echo $id_value . "<br />";
} else{
    echo "id not found, processing cannot continue.";
    exit;
}


use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';

Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
$decrypted = Urlcrypt::decrypt($id_value);


list($userid, $epoch_value) = explode("|", $decrypted);

//echo $userid;
//echo $epoch_value;
//echo $decrypted;
//echo $decrypted;
//exit;

$num_rows_updated = $database->update('Subscriptions', 
       array (
              'bolActive' => 1,
       ), 
       array (
              'subUserId' => $userid
       )
);

//echo "num rows updated = " . $num_rows_updated . "<br />";

if ($num_rows_updated < 1)
{
    echo "<br />There was an issue activating your account.<br /><br />Please email support@emtelink and provide the following reference code: UTA:". $id_value;
    exit;
}

?>

<body>

    <!-- Navbar -->
<nav class="navbar navbar-inverse" id="my-navbar" style="background-color:#474749">

	<div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                    &nbsp;
                </button>
                <a class="navbar-brand" href="./home.php" style="padding:0;"><span><img src="https://www.emtelink.com/myemrmyway/img/MyEMRMyWay_Logo.png" width="90" height="43" alt="logo" style="padding-top:2px; !important"></span></a>
            </div>
        </div>
        <!-- /.container -->
    </nav>
    <!-- End navbar -->
    <!-- jumbotron-->
    <br />
    <br />
    <div class="well">
        <div class="container text-center">
            <h2>
                Congratulations <span style="color:#ED1C24"><?php echo $userid ?></span>,<br />your account has been successfully activated.</h2>
        </div>
        <!-- End container -->
    </div>
    <!-- End jumbotron-->
    <div class="container">
        <section>
            <div id="login">
                <span class="clearfix"></span>
            </div>
            <div class="row">
                <div class="col-xs-1 col-sm-2 col-md-3"></div>
                <div class="col-xs-10 col-sm-8 col-md-6">
                    <div class="account-wall text-center">
                        <h3>
                            <u>
                                <a href='https://www.emtelink.com/myemrmyway'>Click Here to Go To The Logon Page</a>
                            </u>
                        </h3>
                        <br />
                        <br />
						<br />
						<br />
					<h3>Powered by</h3>
                        <img class="profile-img" src="../img/logo.png" alt="EMTeLink Logo" />
                    </div>
                    <!--<a href="#" class="text-center new-account" data-toggle="modal" data-target="#signupModal">Create an account </a>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

                </div>
                <div class="col-xs-1 col-sm-2 col-md-3"></div>
            </div>
            <br />
        </section>
    </div>

    <div class="navbar navbar-fixed-bottom">
        <footer>
            <div class="extra text-center">
                &copy; Copyright @ 2018 EMTeLink&nbsp;&nbsp;Saving Lives With Technology
                <br />
            </div>
            <!-- end Container-->
        </footer>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>
