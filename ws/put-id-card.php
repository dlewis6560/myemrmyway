<?php

include("../include/incConfig.php");

    session_start();
    //make sure we have a valid sesion
	include("../include/session.php");

if (isset($_POST['id_number'])) {
	
    $id_number = filter_var($_POST['id_number'], FILTER_SANITIZE_STRING);
	$id_description = filter_var($_POST['id_description'], FILTER_SANITIZE_STRING);
	
    $id_number_length = strlen($id_number);
	$id_description_length = strlen($id_description);

    $lastModified = gmdate("Y-m-d H:i:s");

	if ($id_number_length < 1){
	  $output= "Invalid ID Number. It must be greater than 1 character.";
	  echo $output;
	  exit;
	}

	if ($id_number_length > 50){
	  $output= "Invalid ID Number. It cannot be greater than 50 characters.";
	  echo $output;
	  exit;
	}
	
	if ($id_description_length < 1){
	  $output= "Invalid ID Description. It must be greater than 1 character.";
	  echo $output;
	  exit;
	}

	if ($id_description_length > 100){
	  $output= "Invalid ID Description. It cannot be greater than 100 characters.";
	  echo $output;
	  exit;
	}	

	$count = $database->count("user_ids", [
	    "description" => "$id_description"
    ]);

    if ($count>0){
        echo "Sorry, the <u>ID Description</u>: <b>". $id_description . "</b> already exists.<br><br>Enter a different description and try again.";
        exit;
    }

	$count = $database->count("user_ids", [
	    "idnumber" => "$id_number"
    ]);

    if ($count>0){
		echo "Sorry, the ID Number <span style='color:red'><strong><u>". $id_number . "</u></strong></span><br/>already exists in the system.<br /><br />Enter a different ID Number and try again.";
        exit;
    }
	
	$database->insert("user_ids", [
        "idnumber" => "$id_number",
        "description" => "$id_description",
        "editdate" => "$lastModified",
        "uid" => $uid		
    ]);
	
    echo ("ID has been created successfully.");
    exit;
}
else{
    echo ("new Card not added, ID Number not found");
}

?>
