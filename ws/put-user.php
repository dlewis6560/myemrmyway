<?php

include("../Include/incConfig.php");

if (isset($_POST['newuserid'])) {

    //get the posted values from the consent page ajax
    $userid = strip_tags($_POST['newuserid']);

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $password1 = $_POST['password1'];
    $password2 = $_POST['password2'];
    $subId = $_POST['subId'];
    $user_type = $_POST['user_type'];
	$user_groupid = $_POST['user_group'];
	$manage_all_groups = false;

    //echo $manage_all_groups;
    //exit;

	if ($_POST['manage_all_groups'] == "on")
	{
	  $manage_all_groups = true;
	}
	else {
	  $manage_all_groups = false;
	}

    if ($user_type == "Admin") {
        $user_type_bit = 1;
    }
    else {

        $user_type_bit = 0;
		$manage_all_groups = false;
    }


    $count = $database->count("user", [
	    "userid" => "$userid"
    ]);

    if ($count>0){
        echo "Sorry, the userid [". $userid . "] already exists.\n\nEnter a different userid and try again.";
        exit;
    }

    $count = $database->count("user", [
	    "student_email" => "$email"
    ]);

    if ($count>0){
        echo "Sorry, the email [". $email . "] already exists.\n\nEnter a different email and try again.";
        exit;
    }

	//get the uid
	$uid = $database->insert("user", [
	    "rowsap" => password_hash("$password1", PASSWORD_DEFAULT),
		"firstname" => "$firstname",
        "lastname" => "$lastname",
        "student_email" => "$email",
        "userid" => "$userid",
        "subID" => "$subId",
        "admin_user" => $user_type_bit,
	]);

	$database->insert("user_groups", [
	    "uid" => $uid,
		"gid" => $user_groupid,
	]);


	$path = "C:\\inetpub\\wwwroot\\emtelink\\new\\uploads\\" . $subId;
	mkdir($path, null, true);

    //########################### start send the user a welcome email ###################################
    //error_log("start curl request", 0);
    //$curl = curl_init();
    //// Set some options - we are passing in a useragent too here
    //curl_setopt_array($curl, array(
    //    CURLOPT_RETURNTRANSFER => 1,
    //    CURLOPT_URL => 'https://www.emtelink.com/myemrmyway/ws/email-welcome.php',
    //    CURLOPT_USERAGENT => 'MyEMRMyWay Welcome Email Request',
    //    CURLOPT_POST => 1,
    //    CURLOPT_POSTFIELDS => array(
    //        'email' => $email
    //    )
    //));
    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    ////error_log("curl request:".$curl, 0);

    ////error_log("send curl request", 0);

    //// Send the request & save response to $resp
    //$resp = curl_exec($curl);

    ////error_log("curl response:" . $resp, 0);

    //// Close request to clear up some resources
    //curl_close($curl);
    //########################### end send the user a welcome email ###################################
    //echo($result);
    //echo ("New user created successfully and welcome email sent to " . $email . ".");

    echo ("New user created successfully.");

    exit;
}
else{
    echo ("new user not added.");
}

?>
