<?php

include("../include/incConfig.php");

if (isset($_POST['userid'])) {

    //get the posted values from the consent page ajax
    $userid = strip_tags($_POST['userid']);
    $sign_date =  strip_tags($_POST['sign-date-picker']);
    $consent_signature = $_POST['consent_signature_post_field'];

    //update the consent fields in the student table
    $database->update("user", [
      "consent_sign_date" => $sign_date,
      "consent_signature" => $consent_signature,
	  ], [
		  "userid" => $userid
	]);  


}?>
