<?php

include("../include/incConfig.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    session_start();
    //make sure we have a valid sesion
	include("../include/session.php");

if (isset($_POST['full_name'])) {

    $id_value =  htmlspecialchars($_POST["hidden"]);
    $decrypted = Urlcrypt::decrypt($id_value);
    $mode = "";
    //echo "id_value = " . $id_value;
    //echo "decrypted = " . $decrypted;

    list($contactid, $mode, $starttime) = explode("|", $decrypted);
	
    $full_name = filter_var($_POST['full_name'], FILTER_SANITIZE_STRING);
	$contact_address = filter_var($_POST['contact_address'], FILTER_SANITIZE_STRING);
	$contact_city = filter_var($_POST['contact_city'], FILTER_SANITIZE_STRING);
	$contact_state = filter_var($_POST['contact_state'], FILTER_SANITIZE_STRING);
	$contact_zip = filter_var($_POST['contact_zip'], FILTER_SANITIZE_STRING);
	$contact_phone = filter_var($_POST['contact_phone'], FILTER_SANITIZE_STRING);
	$contact_email= filter_var($_POST['contact_email'], FILTER_SANITIZE_STRING);
	$contact_relationship= filter_var($_POST['contact_relationship'], FILTER_SANITIZE_STRING);
	
    $full_name_length = strlen($full_name);
	$contact_address_length = strlen($contact_address);
	$contact_city_length = strlen($contact_city);
	$contact_state_length = strlen($contact_state);
	$contact_zip_length = strlen($contact_zip);
	$contact_phone_length = strlen($contact_phone);
	$contact_email_length = strlen($contact_email);	
	$contact_relationship_length = strlen($contact_relationship);	

    $lastModified = gmdate("Y-m-d H:i:s");

	if ($full_name_length < 1){
	  $output= "Invalid Contact. It must be greater than 1 character.";
	  echo $output;
	  exit;
	}

	if ($full_name_length > 75){
	  $output= "Invalid Contact. It cannot be greater than 75 characters.";
	  echo $output;
	  exit;
	}

    $database->update("user_contacts", [
        "FullName" => "$full_name",
        "Address1" => "$contact_address",
        "City" => "$contact_city",
        "State" => "$contact_state",
        "Zip" => "$contact_zip",
        "MobileNumber" => "$contact_phone",
        "Email" => "$contact_email",
		"Relationship" => "$contact_relationship",
        "EditDate" => "$lastModified"
    ], [
        "ContactId" => $contactid
    ]);
	

    echo ("Contact has been updated successfully.");
    exit;
}
else{
    echo ("new Contact not added");
}

?>
