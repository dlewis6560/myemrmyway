<?php

try {

session_start(); //to ensure you are using same session
session_destroy(); //destroy the session
echo 'success';
exit;

}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
?>

