<?php

try {

    include("../include/incConfig.php");
    include("../include/incFunctions.php");

    $userid = $_POST["userid"];
    $newpassword = $_POST["newpassword"];
    $confirmpassword = $_POST["confirmpassword"];

	$database->update("user", [
		"rowsap" => password_hash($newpassword, PASSWORD_DEFAULT)
	], [
		"userid" => $userid
	]);

    echo ("success");
    exit;

}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
?>

