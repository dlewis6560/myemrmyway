<?php

include("../Include/incConfig.php");

if (isset($_POST['groupname'])) {

    //get the posted values from the consent page ajax
    $groupname = $_POST['groupname'];
    $subId = $_POST['subId'];

    $count = $database->count("groups", [
	    "gname" => "$groupname"
    ], [
             "subID" => "$subId"
     ]);

    if ($count>0){
        echo "Sorry, the group name [". $groupname . "] already exists.\n\nEnter a different group name and try again.";
        exit;
    }

	$uid = $database->insert("groups", [
		"gname" => "$groupname",
        "gmaxusers" => "10",
        "subID" => "$subId"
	]);

    echo ("new group created successfully.");
    exit;
}
else{
    echo ("new group not added.");
}

?>
