<?php

include("../include/incConfig.php");

use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';
Urlcrypt::$key = $mykey;

session_start();
//make sure we have a valid sesion
include("../include/session.php");

if (isset($_POST['ins_company'])) {

	//echo file_get_contents("php://input");

	/*
	if ($_POST['hmac'] !== create_hmac($secret, $_SERVER['REQUEST_URI'], $hidden)) {
		// invalid HMAC
		echo 'invalid';
	} else {

		echo var_dump($hidden);
	} */

	//echo $uid;

    //get the posted values from the consent page ajax
    //$uid = strip_tags($_POST['uid']);

    $ins_company_name = filter_var($_POST['ins_company'], FILTER_SANITIZE_STRING);
	$groupno = filter_var($_POST['groupno'], FILTER_SANITIZE_STRING);
	$contractno = filter_var($_POST['contractno'], FILTER_SANITIZE_STRING);
	$effective_date = filter_var($_POST['effective_date_picker'], FILTER_SANITIZE_STRING);
	$certification_no = filter_var($_POST['certification_no'], FILTER_SANITIZE_STRING);
	$phone_number = filter_var($_POST['phone_number'], FILTER_SANITIZE_STRING);
	$plan_with= filter_var($_POST['plan_with'], FILTER_SANITIZE_STRING);

    $ins_company_name_length = strlen($ins_company_name);
	$groupno_length = strlen($groupno);
	$contractno_length = strlen($contractno);
	$effective_date_length = strlen($effective_date);
	$certification_no_length = strlen($certification_no);
	$phone_number_length = strlen($phone_number);
	$plan_with_length = strlen($plan_with);

	try {
		$dateTimeObject = new DateTime($effective_date);
	} catch (Exception $exc) {
		  $output= "The value you entered for effective date is not a valid date.";
		  echo $output;
		  exit;
	}

	if ($ins_company_name_length < 2){
	  $output= "Invalid Company Name. It must be greater than 2 characters.";
	  echo $output;
	  exit;
	}

	if ($ins_company_name_length > 128){
	  $output= "Invalid Company Name. It cannot be greater than 128 characters.";
	  echo $output;
	  exit;
	}

    $count = $database->count("user_ins_companies", [
		"AND" => [
		"CompanyName" => "$ins_company_name",
		"uid" => $uid
	]]);


    if ($count>0){
        echo "Sorry, the Company Name <span style='color:red'><strong><u>". $ins_company_name . "</u></strong></span> already exists.<br><br>Enter a different Company Name and try again.";
        exit;
    }

	$InsertedCompanyId = $database->insert("user_ins_companies", [
        "CompanyName" => "$ins_company_name",
        "GroupNumber" => "$groupno",
        "ContractNumber" => "$contractno",
        "EffectiveDate" => "$effective_date",
		"CertificationNumber" => "$certification_no",
        "PhoneNumber" => "$phone_number",
        "PlanWith" => "$plan_with",
        "uid" => $uid]
    );

    $encrypted = Urlcrypt::encrypt($InsertedCompanyId . "|" . "edit". "|" . time());

    echo ("Company has been created successfully.|" . $encrypted);
    exit;
}
else{
    echo ("new Company not added, Company Name not found");
}



?>
