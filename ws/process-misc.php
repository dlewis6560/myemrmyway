<?php

include("../include/incConfig.php");

if (isset($_POST['misc_userid'])) {

    //get the posted values from the consent page ajax
    $userid = strip_tags($_POST['misc_userid']);
    $textarea_notes = strip_tags($_POST['textarea-notes']);
    $textarea_special_diets = strip_tags($_POST['textarea-special-diets']);
    $textarea_serious_injuries = strip_tags($_POST['textarea-serious-injuries']);
    $textare_hospitalizations = strip_tags($_POST['textarea-hospitalizations']);


    //update the consent fields in the student table
    $database->update("user", [
      "hospitalizations" => $textare_hospitalizations,
      "serious_injuries" => $textarea_serious_injuries,
      "special_diets" => $textarea_special_diets,
      "add_info" => $textarea_notes
	  ], [
		  "userid" => $userid
	]);  


}?>



