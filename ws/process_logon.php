<?php

try {

include("../include/incConfig.php");
session_start();

//if we have a valid session, then no need to login in again

$userid = $_POST["userid"];
$password = $_POST["password"];

//echo $userid . " -- " . $password;

//$subId = $_SESSION["subId"];

//file_put_contents('debug_log.txt',PHP_EOL . PHP_EOL . "userid:" . $userid . " | password:" . $password ,FILE_APPEND | LOCK_EX);

$recordset = $database->select("user", "rowsap",["userid" => "$userid"]);
$db_hashed_password=$recordset[0];

//echo password_verify($password, $db_hashed_password);

if (password_verify($password, $db_hashed_password)==1) {
    $bolValidUser ="TRUE";

    $recordset = $database->select("user", "subID",["userid" => "$userid"]);
    $user_subscription_id=$recordset[0];

    //########### check to see if payment has been received ##################
    $count = $database->count("pp_payments", [
		"AND" => [
		"payment_status" => "Completed",
		"userid" => "$userid"
	]]);

    $free_account = $database->count("Subscriptions", [
		"AND" => [
		"promo_code" => "myemrmyway_free",
		"subId" => "$user_subscription_id"
	]]);

    //if no payment, then don't let the user log in
    if ($count < 1){
        //see if this is a free account
        if ($free_account < 1){
        echo "notpaid";
        exit;
        }
    }
    //#########################################################################

    $_SESSION["bolValidUser"] =  "TRUE";

        //get user profile information
    $recordset = $database->select("user", [
        "uid",
        "subID",
        "userid",
        "firstname",
        "lastname",
        "admin_user"
    ], [
        "userid" => "$userid"
    ]);

    foreach($recordset as $record)
    {
        //echo "firstname:" . $record["firstname"] . " - lastname:" . $record["lastname"] . "<br/>";

        $_SESSION["uid"] = $record["uid"];
        $_SESSION["userid"] = $record["userid"];
        $_SESSION["loggedin_userid"] = $record["userid"]; //this stays constant throughout a session so that admin user can edit his/her own account (edit buttons are disabled for other users)
        $_SESSION["loggedin_fname"] = $record["firstname"]; //this stays constant throughout a session so that admin user can edit his/her own account (edit buttons are disabled for other users)
        $_SESSION["loggedin_lname"] = $record["lastname"]; //this stays constant throughout a session so that admin user can edit his/her own account (edit buttons are disabled for other users)
        $_SESSION["firstname"] =  $record["firstname"];
        $_SESSION["lastname"] =  $record["lastname"];
        $_SESSION["admin_user"] =  $record["admin_user"];
        $_SESSION["valid"] = "TRUE";

        $_SESSION["subId"] = $record["subID"];
    }


    $recordset = $database->select("Subscriptions", [
        "subType",
        "subMaxUsers"
    ], [
        "subId" => $_SESSION["subId"]
    ]);

    foreach($recordset as $record)
    {
        //echo "firstname:" . $record["firstname"] . " - lastname:" . $record["lastname"] . "<br/>";
        $_SESSION["subType"] = $record["subType"];
        $_SESSION["subMaxUsers"] =  $record["subMaxUsers"];
    }
    echo "success";
    exit;

}
else {
    //echo "code:".password_verify($password, $db_hashed_password);
    //exit;
}
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
?>

