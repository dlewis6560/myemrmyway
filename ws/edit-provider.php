<?php

include("../include/incConfig.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    session_start();
    //make sure we have a valid sesion
	include("../include/session.php");


    if (isset($_POST['prov_name'])) {

    $id_value =  htmlspecialchars($_POST["hidden"]);
    $decrypted = Urlcrypt::decrypt($id_value);
    $mode = "";
    //echo "id_value = " . $id_value;
    //echo "decrypted = " . $decrypted;

    list($providerid, $mode, $starttime) = explode("|", $decrypted);

    //echo "companyid=" . $companyid;
    //exit;

	//$character = json_decode($data);
	//echo $character->name;

	/*
	if ($_POST['hmac'] !== create_hmac($secret, $_SERVER['REQUEST_URI'], $hidden)) {
		// invalid HMAC
		echo 'invalid';
	} else {

		echo var_dump($hidden);
	} */

	//echo $uid;

    //get the posted values from the consent page ajax
    //$uid = strip_tags($_POST['uid']);

    $prov_name = filter_var($_POST['prov_name'], FILTER_SANITIZE_STRING);
    $prov_address = filter_var($_POST['prov_address'], FILTER_SANITIZE_STRING);
    $prov_city = filter_var($_POST['prov_city'], FILTER_SANITIZE_STRING);
    $prov_state = filter_var($_POST['prov_state'], FILTER_SANITIZE_STRING);
    $prov_zip = filter_var($_POST['prov_zip'], FILTER_SANITIZE_STRING);
    $prov_phone = filter_var($_POST['prov_phone'], FILTER_SANITIZE_STRING);
    $prov_fax= filter_var($_POST['prov_fax'], FILTER_SANITIZE_STRING);
    $prov_email= filter_var($_POST['prov_email'], FILTER_SANITIZE_STRING);
    $prov_speciality= filter_var($_POST['prov_speciality'], FILTER_SANITIZE_STRING);
    $prov_primary= filter_var($_POST['prov_primary'], FILTER_SANITIZE_STRING);

    $prov_name_length = strlen($prov_name);
    $prov_address_length = strlen($prov_address);
    $prov_city_length = strlen($prov_city);
    $prov_state_length = strlen($prov_state);
    $prov_zip_length = strlen($prov_zip);
    $prov_phone_length = strlen($prov_phone);
    $prov_fax_length = strlen($prov_fax);
    $prov_email_length = strlen($prov_email);
    $prov_speciality_length = strlen($prov_speciality);
    $prov_primary_length = strlen($prov_primary);

    $lastModified = gmdate("Y-m-d H:i:s");

	if ($prov_name_length < 2){
        $output= "Invalid Provider Name. It must be greater than 2 characters.";
        echo $output;
        exit;
	}

	if ($prov_name_length > 50){
        $output= "Invalid Provider Name. It cannot be greater than 50 characters.";
        echo $output;
        exit;
	}

    $database->update("user_providers", [
    "Name" => "$prov_name",
    "Address" => "$prov_address",
    "City" => "$prov_city",
    "State" => "$prov_state",
    "Zip" => "$prov_zip",
    "PhoneNumber" => "$prov_phone",
    "FaxNumber" => "$prov_fax",
    "Email" => "$prov_email",
    "Specialty" => "$prov_speciality",
    "Primary" => "$prov_primary",
    "EditDate" => "$lastModified",
    ], [
        "ProviderId" => $providerid
    ]);

    echo ("Provider has been updated successfully.");
    exit;
}
else{
    echo ("Provider not updated");
}

?>
