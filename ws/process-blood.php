<?php

include("../include/incConfig.php");

if (isset($_POST['blood_userid'])) {

    //get the posted values from the consent page ajax
    $userid = strip_tags($_POST['blood_userid']);
    $blood_type = strip_tags($_POST['blood_type']);

    //update the consent fields in the user table
    $database->update("user", [
      "bloodtype" => $blood_type
	  ], [
		  "userid" => $userid
	]);  

}?>



