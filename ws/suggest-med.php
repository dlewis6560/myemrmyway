<?php

include("../include/incConfig.php");

    /* retrieve the search term that autocomplete sends */
    $term = trim(strip_tags($_GET['term'])); 
    $a_json = array();
    $a_json_row = array();

    //tablename
    //fields
    //order by
	$recordset = $database->select("MedicationItems",["MedicationName","MedicationId"], 
        ["MedicationName[~]" => $term."%"],
        ["ORDER" => "MedicationItems.MedicationName"]);

	foreach($recordset as $data)
	{
        $a_json_row["id"] = $data["MedicationId"];
        $a_json_row["value"] = $data["MedicationName"];
        $a_json_row["label"] = $data["MedicationName"];
        array_push($a_json, $a_json_row);
	}   

    // jQuery wants JSON data
    echo json_encode($a_json);

?>



