<?php

include("../include/incConfig.php");

//echo $_POST['allergy_userid'];
//exit;

if (isset($_POST['allergy_userid'])) {


    //get the posted values from the consent page ajax
    $userid = strip_tags($_POST['allergy_userid']);
    $description = strip_tags($_POST['allergy_desc']);
    $notes = strip_tags($_POST['allergy_notes']);

    $mode = strip_tags($_POST['allergy_mode']);
    $allergy_item = strip_tags($_POST['allergy_itemno']);
    
    //echo $mode;
    //exit;

    if ($mode=="edit") {
        //update the student medication table
        $database->update("user_allergies", [
            "description" => $description,
            "notes" => $notes
        ], [
            "allergyId" => $allergy_item
        ]);
    }
    else {
        $database->insert("user_allergies", [
          "description" => $description,
          "notes" => $notes,
          "userid" => $userid
          ]);  
    }

}?>



