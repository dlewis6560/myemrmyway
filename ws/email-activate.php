<?php
require '../PhpMailer/PHPMailerAutoload.php';

use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';

include("../Include/incConfig.php");

if (isset($_POST['firstname'])) {

    //get the posted values from the consent page ajax
    $firstname = ucfirst($_POST['firstname']);
    $lastname = ucfirst($_POST['lastname']);
    $emailaddress = $_POST['email'];
    $userid = strtoupper($_POST['userid']);
    $pwd = $_POST['pwd'];
    $max_users = $_POST['max_users'];
    $promo_code = strtoupper($_POST['promo_code']);

    if ($max_users > 10) {
        $max_users = 10;
    }

    //make sure userid is not already registered
    $count = $database->count("user", [
	"userid" => $userid
    ]);

    if ($count > 0)
    {
        echo "uexists";
        exit;
    }

    //make sure email is not already registered
    $count = $database->count("user", [
	"student_email" => $emailaddress
    ]);

    if ($count > 0)
    {
        echo "eexists";
        exit;
    }

    $verification_code = strtoupper(generateRandomString(5));

    //need to create a subscription
	$subId = $database->insert('Subscriptions',
        array(
		'subUserId' => $userid,
        'subName' => $lastname . ' Family',
        'subStartDate' => date('Y-m-d'),
        'subEndDate' => date('Y-m-d',strtotime(date('Y-m-d', mktime()) . ' + 365 day')),  //one year from today
        'subType' => 'F',
        'subMaxUsers' => $max_users,
        'promo_code' => $promo_code
        )
	);

    if ($subId < 1)
    {
        echo "suberr";
        exit;
    }

//create user link to subscription as admin
	$database->insert('user',
        array(
	    'rowsap' => password_hash($pwd, PASSWORD_DEFAULT),
		'firstname' => $firstname,
        'lastname' => $lastname,
        'student_email' => $emailaddress,
        'userid' => $userid,
        'subID' => $subId,
        'admin_user' => 1,
        'force_pw_reset' => 1,
        'promo_code' => $promo_code
	    )
    );


    //send an email
    Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
    $encrypted = Urlcrypt::encrypt($userid . "|" . time());

    $mail = new PHPMailer;

    //$mail->SMTPDebug = 2;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.mailgun.org';                     // Specify main and backup SMTP servers // secure.emailsrvr.com
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'postmaster@emtelink.com';          // SMTP username
    $mail->Password = 'f6175a9ce7d5a17d09ba7a2e81c54109'; // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted


    $mail->From = 'support@EMTeLink.com';
    $mail->FromName = 'support@EMTeLink.com';
    $mail->addAddress($emailaddress);     // Add a recipient
    $mail->AddBCC("dean@emfotech.com","Dean Massey");
    $mail->AddBCC("dlewis6560@aol.com","Donnie Lewis");

    $mail->addReplyTo('support@EMTeLink.com', 'support@EMTeLink.com');
    //$mail->addCC('cc@example.com');
    ////$mail->addBCC('bcc@example.com');

    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true);                                    // Set email format to HTML

    $mail->Subject = 'MyEMRMyWay Family Account Activation for ' . $firstname . " " . $lastname;
    $mail->Body    = '<!DOCTYPE html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
    <title></title>
    <style type=text/css>
        .text-style {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
            font-style: normal;
        }

        .text-xsmall {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            font-style: normal;
        }
    </style>
</head>
<body>
    <div class=text-style>
        Hi '.$firstname.',
        <br />
        <br /> Your MyEMRMyWay Family Account has been successfully created.  Your UserID is ' . $userid . '.
        <br />
        <br />
        Click the link below to complete the activation process and start using your new account.
        <br />
        <br />
        <a href=https://www.emtelink.com/myemrmyway/ws/activate-account.php?id=' .  $encrypted.' =>Click To Activate the MyEMRMyWay Family Account For ' .  $firstname  . ' ' . $lastname . '</a>
        <br /><br />
        If you have any questions or suggestions, please contact our support department at support@emtelink.com.
        <br /><br />
        Thanks for choosing MyEMRMyWay to organize your electronic health record and be prepared for emergencies.
        <br /><br />
        You will be able to access your families electronic health record from any device, anywhere, anytime ... 24 hours a day, 7 days a week, 365 days a year.
        <br />
        <br /><br />
        - The MyEMRMyWay Team
    </div>
    <br /> <br />
    <div class=text-xsmall>
        This email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager by responding to
        support@EMTeLink.com. Please do not disseminate, distribute or copy this e-mail. Please notify the EMTeLink immediately by e-mail if you have received this e-mail
        by mistake and delete this e-mail from your system.
    </div>
</body>
</html>';

    $mail->AltBody = 'Hi '.$firstname.',

Your MyEMRMyWay Family Account has been successfully created.


Copy and Paste to your Browser the link below to complete the activation process and start using your new account.

https://www.emtelink.com/myemrmyway/ws/activate-account.php?id=' . $encrypted.'

If you have any questions or suggestions, please contact our support department at support@emtelink.com.
Thanks for choosing MyEMRMyWay to organize your electronic health record and be prepared for emergencies.
You will be able to access your families electronic health record from any device, anywhere, anytime ... 24 hours a day, 7 days a week, 365 days a year.


- The MyEMRMyWay Team

This email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager by responding to support@EMTeLink.com. Please do not disseminate, distribute or copy this e-mail. Please notify the EMTeLink immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system.';

    /*
    $mail->smtpConnect([
    'tls' => [
    'verify_peer' => false,
    'verify_peer_name' => false,
    'allow_self_signed' => true
    ]
    ]);

    if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
    echo 'Message has been sent';
    }
     */

    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {

        if (strtolower($promo_code) == 'myemrmyway_free') {
            echo ("success - " . "free - " . (strtolower($promo_code) == 'myemrmyway_free'));
        }
        else {
            echo ("success - " . $verification_code . " - " . (strtolower($promo_code) == 'myemrmyway_free'));
        }
    }

    exit;
}

//exit;

function ajaxResponse($status, $message, $data = NULL, $mg = NULL) {
    $response = array (
      'status' => $status,
      'message' => $message,
      'data' => $data,
      'mailgun' => $mg
      );
    $output = json_encode($response);
    exit($output);
}

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


?>