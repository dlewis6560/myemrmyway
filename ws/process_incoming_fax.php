<?php

//phpinfo();
//exit;

require '../PhpMailer/PHPMailerAutoload.php';

include_once('./php-qrcode-detector-decoder/lib/QrReader.php');

include("../include/incConfig.php");

use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';
Urlcrypt::$key = $mykey;

////the PDF file
////$pdf = "records_request_2.pdf[0]";

////$image = new Imagick();
////$image->newImage(1, 1, new ImagickPixel('#ffffff'));
////$image->setImageFormat('png');
////$pngData = $image->getImagesBlob();
////echo strpos($pngData, "\x89PNG\r\n\x1a\n") === 0 ? 'Ok' : 'Failed';


$myurl = $tmp_fax_filename . '['.'0'.']';
//$myurl = 'records_request_2.pdf['.'0'.']';
$image = new Imagick($myurl);
////$image->setResolution( 300, 300 );
////$image->setImageFormat( "png" );
////$image->writeImage('../tmp/newfilename.png');

////retrieve the first page of the PDF as image
//$image = new imagick($pdf.'[0]');
//$img   = new imagick($pdf_file.'[0]');

////pass it to the QRreader library
$qrcode = new QrReader($myurl);

$text = $qrcode->text(); //return decoded text from QR Code

//no QR Code found
if ($text == "") {
    exit;
}

//decrypt it
$decrypted = Urlcrypt::decrypt($text);
list($newDocId, $subId, $uid, $starttime) = explode("|", $decrypted);

//makeDir ("C:\\inetpub\\wwwroot\\emtelink\\new\\Uploads\\" . $subId . "\\");

$target_dir = "C:\\inetpub\\wwwroot\\emtelink\\new\\Uploads\\" . $subId . "\\";
$target_file = $newDocId . ".pdf";

$doc_description = "";

rename($tmp_fax_filename, $target_dir . $target_file);


$where = "where uniqueId=" . $newDocId;

//get the document description
$strSQL = "SELECT doc_description ";
$strSQL = $strSQL . "FROM [user_docs] " . $where . " ";

$recordset = $database->query($strSQL);
foreach($recordset as $data)
{
    $doc_description = $data["doc_description"];
}

$doc_description = str_replace("Requested","Received",$doc_description);

$database->update("user_docs", [
    "file_name" => "$target_file",
    "mime_type" => "application/pdf",
    "Status" => "Received",
    "doc_description" => "$doc_description",
    "doc_name" => "Records Received"
], [
    "uniqueId" => $newDocId
]);


////print $newDocId . "|" . $subId . "|" . $uid  . "|" . $starttime;

////clear the resources used by Imagick
$image->clear();

$requestEmail = "";
$firstname = "";
$lastname = "";
$user_email = "";

///send email to user notifying of records received
$recordset = $database->select("user", [
    "firstname",
    "lastname",
    "student_email"
], [
    "uid" => "$uid"
]);


foreach($recordset as $data)
{
    //demographic data ########################################################
    $firstname = $data["firstname"];
    $lastname = $data["lastname"];
    $user_email = $data["student_email"];

    //$records_recv_date = $myDateTime->format('m-d-Y');

}


$mail = new PHPMailer;

//$mail->SMTPDebug = 2;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.mailgun.org';                     // Specify main and backup SMTP servers // secure.emailsrvr.com
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'postmaster@emtelink.com';          // SMTP username
$mail->Password = 'f6175a9ce7d5a17d09ba7a2e81c54109'; // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted

//$mail->isSMTP();                                      // Set mailer to use SMTP
//$mail->Host = 'secure.emailsrvr.com';                     // Specify main and backup SMTP servers // secure.emailsrvr.com
//$mail->SMTPAuth = true;                               // Enable SMTP authentication
//$mail->Username = 'info@emtelink.com';          // SMTP username
//$mail->Password = 'fish12'; // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted

$mail->From = 'support@EMTeLink.com';
$mail->FromName = 'support@EMTeLink.com';
$mail->addAddress($requestMethodValue);     // Add a recipient
$mail->AddBCC("dean@emtelink.com","Dean Massey");
$mail->AddBCC("dlewis6560@aol.com","Donnie Lewis");

$mail->addReplyTo($requestEmail, $requestEmail);
//$mail->addCC('cc@example.com');
////$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                    // Set email format to HTML

$mail->Subject = 'MyEMRMyWay Medical Records : ' . $doc_description;

$mail->Body    = '<!DOCTYPE html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
    <title></title>
    <style type=text/css>
        .text-style {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
            font-style: normal;
        }

        .text-xsmall {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            font-style: normal;
        }
    </style>
</head>
<body>
    <div class=text-style>
        Greetings ' . $firstname . ' ' . $lastname . ', you have received your requested medical records.' . '
        <br />
        <br />
        ' . $doc_description . '
        <br />
        <br />
        Login to your secure portal <a href="https://www.emtelink.com/MyEMRMyWay">MyEMRMyWay Portal</a> to view the records.
        <br />
        <br />
        If you have any questions, please contact our support department at support@emtelink.com.
        <br /><br />
        - The MyEMRMyWay Team
    </div>
    <br /> <br />
    <div class=text-xsmall>
        This email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager by responding to
        support@EMTeLink.com. Please do not disseminate, distribute or copy this e-mail. Please notify the EMTeLink immediately by e-mail if you have received this e-mail
        by mistake and delete this e-mail from your system.
    </div>
</body>
</html>';

//#############################################  S  E  N  D     M A I L ####################################################################
  if(!$mail->send()) {
      //echo 'Message could not be sent.';
      //echo 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
      //echo ("success - " . $verification_code);
  }


exit;



?>