<?php 

try {

include("../include/incConfig.php");
include("../include/incFunctions.php");
session_start();

 //make sure we have a valid sesion
if ($_SESSION["valid"] != "TRUE")
  {
    header("Location: index.html");
  };
    

$firstname = $_SESSION["firstname"];
$lastname = $_SESSION["lastname"];
$userid = $_SESSION["userid"];
 

//if we have a valid session, then no need to login in again
$currentpassword = $_POST["currentpassword"];
$newpassword = $_POST["newpassword"];
$confirmpassword = $_POST["confirmpassword"];

//alert($currentpassword);
//exit;

//file_put_contents('debug_log.txt',PHP_EOL . PHP_EOL . "userid:" . $userid . " | password:" . $password ,FILE_APPEND | LOCK_EX);

$recordset = $database->select("user", "rowsap",["userid" => "$userid"]);
$db_hashed_password=$recordset[0];

if (password_verify($currentpassword, $db_hashed_password)==1) {

    $bolValidCurrent ="TRUE";
    
	$database->update("user", [
		"rowsap" => password_hash($newpassword, PASSWORD_DEFAULT)
	], [
		"userid" => $userid
	]);

    echo ("success");
    exit; 
}
else {
    echo ("invalid");
    exit;
}
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
?>

