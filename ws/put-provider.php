<?php

include("../include/incConfig.php");

    session_start();
    //make sure we have a valid sesion
	include("../include/session.php");

    if (isset($_POST['prov_name'])) {

	//echo file_get_contents("php://input");

	/*
	if ($_POST['hmac'] !== create_hmac($secret, $_SERVER['REQUEST_URI'], $hidden)) {
		// invalid HMAC
		echo 'invalid';
	} else {

		echo var_dump($hidden);
	} */

	//echo $uid;

    //get the posted values from the consent page ajax
    //$uid = strip_tags($_POST['uid']);

    $prov_name = filter_var($_POST['prov_name'], FILTER_SANITIZE_STRING);
    $prov_address = filter_var($_POST['prov_address'], FILTER_SANITIZE_STRING);
    $prov_city = filter_var($_POST['prov_city'], FILTER_SANITIZE_STRING);
    $prov_state = filter_var($_POST['prov_state'], FILTER_SANITIZE_STRING);
    $prov_zip = filter_var($_POST['prov_zip'], FILTER_SANITIZE_STRING);
    $prov_phone = filter_var($_POST['prov_phone'], FILTER_SANITIZE_STRING);
    $prov_fax= filter_var($_POST['prov_fax'], FILTER_SANITIZE_STRING);
    $prov_email= filter_var($_POST['prov_email'], FILTER_SANITIZE_STRING);
    $prov_speciality= filter_var($_POST['prov_speciality'], FILTER_SANITIZE_STRING);
    $prov_primary= filter_var($_POST['prov_primary'], FILTER_SANITIZE_STRING);

    $prov_name_length = strlen($prov_name);
    $prov_address_length = strlen($prov_address);
    $prov_city_length = strlen($prov_city);
    $prov_state_length = strlen($prov_state);
    $prov_zip_length = strlen($prov_zip);
    $prov_phone_length = strlen($prov_phone);
    $prov_fax_length = strlen($prov_fax);
    $prov_email_length = strlen($prov_email);
    $prov_speciality_length = strlen($prov_speciality);
    $prov_primary_length = strlen($prov_primary);

	if ($prov_name_length < 2){
	  $output= "Invalid Provider Name. It must be greater than 2 characters.";
	  echo $output;
	  exit;
	}

	if ($prov_name_length > 128){
	  $output= "Invalid Provider Name. It cannot be greater than 50 characters.";
	  echo $output;
	  exit;
	}

    //see if this provider already exists for this user
    $count = $database->count("user_providers", [
		"AND" => [
		"Name" => "$prov_name",
		"uid" => $uid
	]]);


    if ($count>0){
        echo "Sorry, the Provider Name <span style='color:red'><strong><u>". $prov_name . "</u></strong></span><br />already exists.<br /><br />Enter a different Provider Name and try again.";
        exit;
    }

    $lastModified = gmdate("Y-m-d H:i:s");

	$database->insert("user_providers", [
        "Name" => "$prov_name",
        "Address" => "$prov_address",
        "City" => "$prov_city",
        "State" => "$prov_state",
		"Zip" => "$prov_zip",
        "PhoneNumber" => "$prov_phone",
        "FaxNumber" => "$prov_fax",
        "Email" => "$prov_email",
        "Specialty" => "$prov_speciality",
        "Primary" => "$prov_primary",
        "EditDate" => "$lastModified",
        "uid" => $uid]
    );

    echo ("Company has been created successfully.");
    exit;
}
else{
    echo ("new Provider not added, Provider Name not found");
}

?>
