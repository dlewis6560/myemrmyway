<?php


$received_array = print_r( $_REQUEST, true );

$tmp_file_name = tempnam("../tmp","NoT");

$fp = file_put_contents( $tmp_file_name, $received_array );

$handle = fopen($tmp_file_name, "r");
if ($handle) {
    while (($line = fgets($handle)) !== false) {
        $line = trim($line);
        // process the line read.
        if ( ($line !== "Array") && ($line !== "(") && ($line !== ")") )
        {
            list($field, $value) = explode(' => ', $line);
            //echo $field . " - " . $value;
            if ($field == "[recvid]") {
                $recvid = $value;
            }
        }
    }
    fclose($handle);
} else {
    // error opening the file.
}

// now that we have the received fax id, go get the fax
include("getfax.php");

// now the received fax is in the tmp directory, scan for qr code on the 1st page
// then move it to the subscribers folder
include("process_incoming_fax.php");



//$fp = file_put_contents( '../tmp/' . 'faxage_notify' . '.log', $received_array, FILE_APPEND );



?>