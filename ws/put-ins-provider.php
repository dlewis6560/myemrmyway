<?php

include("../Include/incConfig.php");

if (isset($_POST['id_number'])) {

    //get the posted values from the consent page ajax
    $uid = strip_tags($_POST['uid']);

    $id_number = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST["id_number"]);
    $id_description = $_POST['id_description'];

    $count = $database->count("user_ids", [
	    "idnumber" => "$id_number"
    ]);

    if ($count>0){
        echo "Sorry, the id number <b>". $id_number . "</b> already exists.<br><br>Enter a different id number and try again.";
        exit;
    }
 
	$database->insert("user_ids", [
        "idnumber" => "$id_number",
        "description" => "$id_description",
        "uid" => "$uid"]
    );
	
    echo ("success");
    exit;
}
else{
    echo ("new id not added, id number not found");
}

?>
