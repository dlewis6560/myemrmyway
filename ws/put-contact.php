<?php

include("../include/incConfig.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;

    session_start();
    //make sure we have a valid sesion
	include("../include/session.php");

if (isset($_POST['full_name'])) {
	
    $full_name = filter_var($_POST['full_name'], FILTER_SANITIZE_STRING);
	$contact_address = filter_var($_POST['contact_address'], FILTER_SANITIZE_STRING);
	$contact_city = filter_var($_POST['contact_city'], FILTER_SANITIZE_STRING);
	$contact_state = filter_var($_POST['contact_state'], FILTER_SANITIZE_STRING);
	$contact_zip = filter_var($_POST['contact_zip'], FILTER_SANITIZE_STRING);
	$contact_phone = filter_var($_POST['contact_phone'], FILTER_SANITIZE_STRING);
	$contact_email= filter_var($_POST['contact_email'], FILTER_SANITIZE_STRING);
	$contact_relationship= filter_var($_POST['contact_relationship'], FILTER_SANITIZE_STRING);
	
    $full_name_length = strlen($full_name);
	$contact_address_length = strlen($contact_address);
	$contact_city_length = strlen($contact_city);
	$contact_state_length = strlen($contact_state);
	$contact_zip_length = strlen($contact_zip);
	$contact_phone_length = strlen($contact_phone);
	$contact_email_length = strlen($contact_email);	
	$contact_relationship_length = strlen($contact_relationship);
	
	$lastModified = gmdate("Y-m-d H:i:s");

	if ($full_name_length < 1){
	  $output= "Invalid Contact. It must be greater than 1 character.";
	  echo $output;
	  exit;
	}

	if ($full_name_length > 75){
	  $output= "Invalid Contact. It cannot be greater than 75 characters.";
	  echo $output;
	  exit;
	}
	
    $count = $database->count("user_contacts", [
		"AND" => [
		"FullName" => "$full_name",
		"uid" => $uid
	]]);
	

    if ($count>0){
		echo "Sorry, the Contact Name <span style='color:red'><strong><u>". $full_name . "</u></strong></span><br />already exists.<br><br>Enter a different Contact Name and try again.";
        exit;
    }
 
	$database->insert("user_contacts", [
        "FullName" => "$full_name",
        "Address1" => "$contact_address",
        "City" => "$contact_city",
        "State" => "$contact_state",
        "Zip" => "$contact_zip",
        "MobileNumber" => "$contact_phone",
        "Email" => "$contact_email",
		"Relationship" => "$contact_relationship",
        "EditDate" => "$lastModified",
        "uid" => $uid]
    );
	
    echo ("Contact has been added successfully.");
    exit;
}
else{
    echo ("new Contact not added, Contact Name not found");
}

?>
