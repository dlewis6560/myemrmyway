<?php

include("../include/incConfig.php");

//echo $_POST['add_userid'];
//exit;

if (isset($_POST['add_userid'])) {


    //get the posted values from the consent page ajax
    $userid = strip_tags($_POST['add_userid']);
    $med_name = strip_tags($_POST['med_name']);
    $dosage = strip_tags($_POST['dosage']);
    $how_often = strip_tags($_POST['how_often']);
    $mode = strip_tags($_POST['mode']);
    $med_item = strip_tags($_POST['itemno']);
    
    //echo $mode;
    //exit;

    if ($mode=="edit") {
        //update the student medication table
        $database->update("user_medications", [
            "medication_name" => $med_name,
            "dosage" => $dosage,
            "howoften" => $how_often
        ], [
            "medicationId" => $med_item
        ]);
    }
    else {
        $database->insert("user_medications", [
          "medication_name" => $med_name,
          "dosage" => $dosage,
          "howoften" => $how_often,
          "userid" => $userid
          ]);  
    }

}?>



