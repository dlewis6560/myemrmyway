<?php

include("../include/incConfig.php");

if (isset($_POST['userid_ins'])) {
  
  $userid = strip_tags($_POST['userid_ins']);
  $ins_company = strip_tags($_POST['ins_company']);
  $ins_phone = strip_tags($_POST['ins_phone']);
  $groupno = strip_tags($_POST['groupno']);
  $contactno = strip_tags($_POST['contactno']);
  $effective_date = strip_tags($_POST['effective-date-picker']);
  $certification_no = strip_tags($_POST['certification_no']);
  $plan_with = strip_tags($_POST['plan_with']);

      
  $database->update("user", [
      "ins_company" => $ins_company,
      "groupno" => $groupno,
      "contractno" => $contactno,
      "effectivedate" => $effective_date,
      "certification_no" => $certification_no,
      "plan_with" => $plan_with,
      "ins_phone" => $ins_phone

	], [
		"userid" => $userid
	]);

  //echo "Insurance Information Saved Successfully";

}?>

