<?php

include("../include/incConfig.php");

    /* retrieve the search term that autocomplete sends */
    $term = trim(strip_tags($_GET['term'])); 
    $a_json = array();
    $a_json_row = array();

    //tablename
    //fields
    //order by
	$recordset = $database->select("AllergyItems",["Description","AllergyId"], 
        ["Description[~]" => $term."%"],
        ["ORDER" => "AllergyItems.Description"]);

	foreach($recordset as $data)
	{
        $a_json_row["id"] = $data["AllergyId"];
        $a_json_row["value"] = $data["Description"];
        $a_json_row["label"] = $data["Description"];
        array_push($a_json, $a_json_row);
	}   

    // jQuery wants JSON data
    echo json_encode($a_json);

?>



