<?php
require '../PhpMailer/PHPMailerAutoload.php';

//use Urlcrypt\Urlcrypt;
//require_once '../Urlcrypt.php';

include("../Include/incConfig.php");

//patient info
$requestName = $_POST['name'];
$requestDOB = $_POST['dob'];
$requestPhone = $_POST['phone'];
$requestEmail = $_POST['email'];
$requestAddress = $_POST['address'];

//info about facility or person records are being requested from
$requestPersonOrFacilityName = $_POST['requestPersonOrFacilityName'];
$requestPersonOrFacilityAddress = $_POST['requestPersonOrFacilityAddress'];
$requestMethod = $_POST['requestMethod'];
$requestMethodValue = $_POST['requestMethodValue'];

//info about records requested
$request_from_date = $_POST['request_from_date'];
$request_to_date = $_POST['request_to_date'];
$requestNotes = $_POST['requestNotes'];

$signature = $_POST['signatureimageDataURL'];

include("../request_records.php");

//echo $pdf_content;
//exit;

//echo $requestName . " " . $requestDOB . " " . $requestPhone . " " . $requestEmail . " " . $requestAddress . " " . $requestPersonOrFacilityName . " "  . $requestPersonOrFacilityAddress . " " . $requestMethod . " " . $requestMethodValue . " " . $request_from_date . " " . $request_to_date . " " . $requestNotes . " ";

//$output_string = "";
//foreach ($_POST as $key => $value)
//    $output_string = $output_string . $key.'='.$value.'<br />';
//echo $output_string;

//echo "done";

//exit;

//echo ("here 1...<br />");

//error_log("received http:" . file_get_contents( 'php://input' ), 0);


//echo file_get_contents( 'php://input' );

//if (isset($_POST['email'])) {
    //echo ("here 2...");

    //get the posted values from the consent page ajax
    $emailaddress = $_POST['email'];

    //$verification_code = strtoupper(generateRandomString(5));

    //$recordset = $database->select("user", [
    //    "userid", "firstname"
    //], [
    //    "student_email" => $emailaddress
    //]);

    //foreach($recordset as $data)
    //{
    //    $userid = $data["userid"];
    //    $firstname = $data["firstname"];
    //}

    //send an email
    //Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
    //$encrypted = Urlcrypt::encrypt($userid . "|" . time());

    $mail = new PHPMailer;

    //$mail->SMTPDebug = 2;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.mailgun.org';                     // Specify main and backup SMTP servers // secure.emailsrvr.com
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'postmaster@emtelink.com';          // SMTP username
    $mail->Password = 'f6175a9ce7d5a17d09ba7a2e81c54109'; // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted

    //$mail->isSMTP();                                      // Set mailer to use SMTP
    //$mail->Host = 'secure.emailsrvr.com';                     // Specify main and backup SMTP servers // secure.emailsrvr.com
    //$mail->SMTPAuth = true;                               // Enable SMTP authentication
    //$mail->Username = 'info@emtelink.com';          // SMTP username
    //$mail->Password = 'fish12'; // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted

    $mail->From = 'support@EMTeLink.com';
    $mail->FromName = 'support@EMTeLink.com';
    $mail->addAddress($requestMethodValue);     // Add a recipient
    $mail->AddBCC("dean@emtelink.com","Dean Massey");
    $mail->AddBCC("dlewis6560@aol.com","Donnie Lewis");

    $mail->addReplyTo($requestEmail, $requestEmail);
    //$mail->addCC('cc@example.com');
    ////$mail->addBCC('bcc@example.com');

    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true);                                    // Set email format to HTML

    $mail->Subject = 'Medical Record Request for ' . $requestName;
    //$mail->AddAttachment('c:\windows\temp\records_request_2.pdf');
    $mail->addStringAttachment($pdf_content, 'Medical Record Request For ' . $requestName . '.pdf');
    $mail->Body    = '<!DOCTYPE html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
    <title></title>
    <style type=text/css>
        .text-style {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
            font-style: normal;
        }

        .text-xsmall {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            font-style: normal;
        }
    </style>
</head>
<body>
    <div class=text-style>
        Dear Provider,
        <br />
        <br />
        Please find attached a request for medical records from ' . $requestName . '.
        <br />
        <br />
        The attached request contains a description of the records requested and where to send them.
        <br />
        <br />
        If you have any questions, please contact our support department at support@emtelink.com.
        <br /><br />
        - The MyEMRMyWay Team
    </div>
    <br /> <br />
    <div class=text-xsmall>
        This email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager by responding to
        support@EMTeLink.com. Please do not disseminate, distribute or copy this e-mail. Please notify the EMTeLink immediately by e-mail if you have received this e-mail
        by mistake and delete this e-mail from your system.
    </div>
</body>
</html>';

//#############################################  S  E  N  D     M A I L ####################################################################
    if ($requestMethod == "Email") {

        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo ("success - " . $verification_code);
        }

    };
//#############################################  S  E  N  D     F A X ####################################################################
    if ($requestMethod == "Fax") {
        $url = "http://api.faxage.com/httpsfax.php";
        $username = "emtelink";
        $company = "47514";
        $password = "Fish@1234";

        $tagname = "MyEMRMyWay";
        $tagnumber = "(850) 807-5001";

        $fields_string = "";

        //$dst = "850-807-5001";  //destination fax number
        $dst = $requestMethodValue;

        $b64data = base64_encode($pdf_content);

        $fields = array (
            'username' => urlencode($username),
            'company' => urlencode($company),
            'password' => urlencode($password),
            'callerid' => urlencode("MyEMRMyWay"),
            'faxno' => urlencode($dst),
            'recipname' => urlencode("Donnie Lewis"),
            'operation' => urlencode("sendfax"),
            'tagname' => urlencode($tagname),
            'tagnumber' => urlencode($tagnumber),
            'faxfilenames[0]' => urlencode('Medical Record Request For ' . $requestName . '.pdf'),
            'faxfiledata[0]' => $b64data
        );

        $j = 0;
        foreach($fields as $key => $value) {
            if($j > 0) {
                $fields_string .= "&";
            }
            $fields_string .= $key . '=' . $value;
            $j++;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        $result = curl_exec($ch);

        curl_close($ch);

        echo ("success - " . $result);
    }


//########################################################################################################################################

    exit;

//}
//echo ("here 4...");
//exit;

function ajaxResponse($status, $message, $data = NULL, $mg = NULL) {
    $response = array (
      'status' => $status,
      'message' => $message,
      'data' => $data,
      'mailgun' => $mg
      );
    $output = json_encode($response);
    echo($output);
}

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


?>