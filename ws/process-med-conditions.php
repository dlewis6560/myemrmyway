<?php

include("../include/incConfig.php");

if (isset($_POST['med_conditions'])) {

    //get the posted values from the medical page ajax
    $userid = strip_tags($_POST['userid']);
    $med_conditions = $_POST['med_conditions'];

    //echo $med_conditions;

    //update the med_conditions field in the student table
    $database->update("user", [
      "med_conditions" => $med_conditions
	  ], [
		  "userid" => $userid
	]);  

    exit;
    //echo "medical conditions saved successfully";
}?>
