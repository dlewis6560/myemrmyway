<?php

include("../include/incConfig.php");

if (isset($_POST['subId'])) {

  $subId = strip_tags($_POST['subId']);
  $subName = strip_tags($_POST['subName']);
  $address1 = strip_tags($_POST['address1']);
  $address2 = strip_tags($_POST['address2']);
  $city = strip_tags($_POST['city']);
  $state = strip_tags($_POST['state']);
  $zip = strip_tags($_POST['zip']);

      
  $database->update("Subscriptions", [
      "subName" => $subName,
      "subAddress1" => $address1,
      "subAddress2" => $address2,
      "subCity" => $city,
      "subState" => $state,
      "subZip" => $zip,

	], [
		"subId" => $subId
	]);

  //var_dump($database->error());
  //var_dump( $database->log() );

  echo "Account updated successfully.";
  exit;



}?>

