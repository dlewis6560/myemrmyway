<?php
//include('db.php');
//session_start();
//$session_id='1'; //$session id
$path = "uploads/";

include("../include/incConfig.php");
include("../include/incFunctions.php");
session_start();

//make sure we have a valid sesion
if ($_SESSION["valid"] != "TRUE")
{
    header("Location: ../index.html");
};

$firstname = $_SESSION["firstname"];
$lastname = $_SESSION["lastname"];
$userid = $_SESSION["userid"];

$destination_folder = "C:/inetpub/wwwroot/emtelink/new/school_dev/uploads/BHS/"; //upload directory ends with / (slash)
$link_dir = "http://emtelink.com/school_dev/uploads/bhs/";


$valid_formats = array("jpg", "png", "gif", "bmp");

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
    $name = $_FILES['photoimg']['name'];
    $size = $_FILES['photoimg']['size'];
    $userid = $_POST['userid'];
    
    if(strlen($name))
    {
        list($txt, $ext) = explode(".", $name);
        if(in_array($ext,$valid_formats))
        {
            if($size<(1024*1024))
            {
                $actual_image_name = $userid . "_profile." . $ext;
                //$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                $tmp = $_FILES['photoimg']['tmp_name'];

                //echo "actual_image_name = " . $destination_folder . $actual_image_name;

                //echo "tmp = " . $tmp;

                if(move_uploaded_file($tmp, $destination_folder.$actual_image_name))
                	{
                //	//mysqli_query($db,"UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");
                //		
                		echo "<img src=\"" . $link_dir . $actual_image_name . "\"  class=\"preview\">";
                	}
                else
                	echo "failed";

                //$imgData = file_get_contents($filename);

                //$fp = fopen($tmp, 'r'); 
                //$imgData = fread($fp, filesize($tmp)); 
                //$imgData = addslashes($imgData); 
                //$imgData = $database->quote($imgData); 
                

                //fclose($fp);

                //echo "<img src=\"upload/$img\">";

                //$size = getimagesize($filename);

                //update the consent fields in the student table
                //$database->update("student", [
                  //"profile_pic" => $imgData,
                  //], [
                  //    "userid" => $userid
                //]);  

            }
            else
                echo "Image file size max 1 MB";					
        }
        else
            echo "Invalid file format..";	
    }
    
    else
        echo "Please select image..!";
    
    exit;
}

?>