<!DOCTYPE html>
<html lang="en">

<head>

    <?php

    include("include/incConfig.php");

    session_start();

	include("include/session.php");
	
    ?>

    <meta charset="utf-8" />
    <title>MyEMRMyWay :: Home</title>
    <meta name="description" content="MyEMRMyWay App">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="admin/css/font-awesome.min.css" rel="stylesheet" />

    <link href="admin/css/base-admin-3.css" rel="stylesheet" />

    <link rel="shortcut icon" href="https://emtelink.com/myemrmyway/emtelink.ico" />

    <style>

        .progress {background: rgba(209, 209, 209, 1); border: 1px solid rgba(0, 0, 0, 1); border-radius: 4px; height: 20px;}
.progress-bar-custom {background: rgba(9, 168, 35, 1);}

        .selectwidthauto {
            width: auto !important;
            display: inline-block;
        }

		
@media (max-width: 768px) { /* use the max to specify at each container level */
	.widget .widget-content {
		padding: 10px 6px;
		background: #FFF;
		border: 1px solid #D5D5D5;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		border-radius: 5px;
	}
	
	.col-md-12 {
		padding-right:4px !important;
		padding-left:4px !important;		
	}

	.col-xs-12 {
		padding-right:4px !important;
		padding-left:4px !important;		
	}
	
	.well-sm {
		padding-right:4px !important;
		padding-left:4px !important;	
	}

	h2, .h2 {
		font-size: 26px  !important;
	}	
	
	.row {
    padding-top: 5px;
    }
}
    </style>
</head>

<body>

    <?php



    //echo "<br /><br /><br /><br /><br /><br />";
    //echo $_SESSION["userid"] . "</br>";
    //echo $_SESSION["firstname"] . "</br>" ;
    //echo $_SESSION["lastname"] . "</br>" ;
    //echo $_SESSION["valid"] . "</br>";
    //echo $_SESSION["admin_user"] . "</br>";

    $recordset = $database->select("Subscriptions", [
           "bolForms", "bolDocuments"
            ], [
                 "subID" => "$subId"
            ]);

            foreach($recordset as $data)
            {
              $ShowForms = $data["bolForms"];
              $ShowDocs = $data["bolDocuments"];
            }
			
    include("include/navbar.php");			

   ?>

   
    <!-- Navbar -->

    <!-- End navbar -->
    <!-- jumbotron-->

    <!-- End jumbotron-->

    <div class="well well-sm hidden-xs">
        <div class="text-center">
            <h2>
                <i class="icon-home" style="color: #214D8C;"></i>
                Home
            </h2>
        </div>
        <!-- End container -->
    </div>

    <div class="container">

        <div class="row">
          <div class="col-sm-2 col-xs-1"></div>
          <div class="col-sm-8 col-xs-10">
              <div class="widget stacked" style="border:solid;border-width:thin;border-color:gray">
                  <div class="widget-header text-center">
                      <?php if ($_SESSION["admin_user"] == 1) {  //don't show user list for non-admin users ?>
                      <i class="icon-user" style="color: #214D8C;"></i>
                      &nbsp;
                      <span style="font-size: medium;">
                          Select User:
                      &nbsp;
                          <select id="current_user" name="current_user" class="selectwidthauto" style="font-size: medium; font-weight: 700; background-color: #ffffff; color: red;" onchange="getval(this);">
                              <?php
                                  $recordset = $database->select("user", [
                                      "userid",
                                      "firstname",
                                      "lastname"
                                  ], [
                                      "subID" => "$subId"
                                  ]);

                                  foreach($recordset as $data)
                                  {
                                      $db_userid = $data["userid"];
                                      $selected_firstname = $data["firstname"];
                                      $selected_lastname = $data["lastname"];

                                      if ($db_userid == $userid) {
                                          echo "<option selected value=\"" . $db_userid . "\" label=\"" . $selected_firstname . " " . $selected_lastname . "\"></option>";
                                      }
                                      else {
                                          echo "<option value=\"" . $db_userid . "\" label=\"" . $selected_firstname . " " . $selected_lastname . "\"></option>";
                                      }
                                  }
                              ?>
                          </select>&nbsp;
                          <!-- <label for="current_user"><?php echo $selected_firstname . " " . $selected_lastname ?></label>-->
                      </span>
                      <?php } ?>

                      <?php if ($_SESSION["admin_user"] != 1) {

                               if ($subType != "F") {
                                   echo "<span style=\"font-size:medium;font-weight: 700;\">Parent Portal For " . $firstname . " " . $lastname . "</h3>";
                               }
                               else {
                                   echo "<span style=\"font-size:medium;font-weight: 700;\">" . $lastname . " Family Portal</h3>";
                               }
                              }
                      ?>
                  </div>

                  <div class="text-center" style="font-size: medium;color:red;font-weight:600">
                      <span class="alert-success hide" id="user-switched-success" style="height: 36px;">
                          <strong>User Switched</strong>
                      </span>
                  </div>
                      <!-- /widget-header -->

                      <div class="widget-content">

                          <?php if ($subType != "F") {

                                  $recordset = $database->select("user", [
                                      "address",
                                      "firstname",
                                      "lastname",
                                      "middleinitial",
                                      "dob",
                                      "gender",
                                      "city",
                                      "st",
                                      "zip",
                                      "student_email",
                                      "parent_email",
                                      "ins_company",
                                      "ins_phone",
                                      "groupno",
                                      "contractno",
                                      "effectivedate",
                                      "certification_no",
                                      "plan_with",
                                      "parent_name",
                                      "parent_addr",
                                      "parent_day_phone",
                                      "parent_night_phone",
                                      "relative_name",
                                      "relationship",
                                      "relative_phone",
                                      "family_doctor",
                                      "doctor_day_phone",
                                      "doctor_night_phone",
                                      "sign_date",
                                      "parent_signature",
                                      "witness_signature",
                                      "primary_name",
                                      "primary_addr",
                                      "primary_phone",
                                      "primary_carrierId",
                                      "primary_email",
                                      "primary_relationship",
                                      "bloodtype",
                                      "expectation_sign_date",
                                      "expectation_parent_name",
                                      "expectation_parent_signature",
                                      "expectation_student_name",
                                      "expectation_student_signature",
                                      "consent_sign_date",
                                      "consent_signature"
                                  ], [
                                      "userid" => "$userid"
                                  ]);

                                  $profile_field_count = 0;
                                  $profile_complete_percentage = 0;

                                  $medical_field_count = 0;
                                  $medical_complete_percentage = 0;

                                  $form_field_count = 0;
                                  $form_complete_percentage = 0;


                                  foreach($recordset as $data)
                                  {
                                      //demographic data ########################################################
                                      if (strlen($data["firstname"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }

                                      if (strlen($data["lastname"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }

                                      if (strlen($data["dob"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }

                                      if (strlen($data["address"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }

                                      if (strlen($data["city"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }

                                      if (strlen($data["st"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }

                                      if (strlen($data["zip"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }

                                      if (strlen($data["student_email"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }


                                      /*get the percentage counts

                                      --Profile Button--
                                      Demogrpahic:                               First name, last Name, DOB, Address, City, State, Zip, Email (8 fields)
                                      Primary Contact:                           Name, Address, Email, Relationship (4 fields)
                                      Consent For Emergency Treatment Signature: Parent Signature, Witness Signature (2 fields)

                                      --Medical Button--
                                      BloodType                                  BloodType


                                      all fields > 0 length = 100% , if 12 = 100, then 3 = 25% (number of elements > than 0 length / 12

                                       */

                                      //insurance data ########################################################
                                      /* $ins_company = $data["ins_company"];
                                      $groupno = $data["groupno"];
                                      $contractno = $data["contractno"];
                                      $certification_no = $data["certification_no"];
                                      $plan_with = $data["plan_with"];

                                      $effectivedate = "";
                                      $myDateTime = DateTime::createFromFormat('Y-m-d', $data["effectivedate"]);
                                      if(!$myDateTime == ''){
                                      $effectivedate = $myDateTime->format('m-d-Y');
                                      } */



                                      //contact data ########################################################
                                      $primary_name = $data["primary_name"];
                                      $primary_addr = $data["primary_addr"];
                                      $primary_phone = $data["primary_phone"];
                                      $primary_email = $data["primary_email"];
                                      $primary_relationship = $data["primary_relationship"];
                                      $primary_carrierId = $data["primary_carrierId"];

                                      if (strlen($data["primary_name"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }
                                      if (strlen($data["primary_addr"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }
                                      if (strlen($data["primary_email"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }
                                      if (strlen($data["primary_relationship"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }



                                      //signatures #############################################################

                                      $sign_date = "";
                                      $myDateTime = DateTime::createFromFormat('Y-m-d', $data["sign_date"]);
                                      if(!$myDateTime == ''){
                                          $sign_date = $myDateTime->format('m-d-Y');
                                      }
                                      $parent_signature = $data["parent_signature"];
                                      $witness_signature = $data["witness_signature"];

                                      if (strlen($data["parent_signature"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }
                                      if (strlen($data["witness_signature"]) > 0) {
                                          $profile_field_count = $profile_field_count + 1;
                                      }


                                      //medical fields
                                      if (strlen($data["bloodtype"]) > 0) {
                                          $medical_field_count = $medical_field_count + 1;
                                      }

                                      //form fields
                                      if (strlen($data["expectation_sign_date"]) > 0) {
                                          $form_field_count = $form_field_count + 1;
                                          //echo "1";
                                      }

                                      if (strlen($data["expectation_parent_name"]) > 0) {
                                          $form_field_count = $form_field_count + 1;
                                          //echo "2";
                                      }

                                      if (strlen($data["expectation_parent_signature"]) > 0) {
                                          $form_field_count = $form_field_count + 1;
                                          //echo "3";
                                      }

                                      if (strlen($data["expectation_student_name"]) > 0) {
                                          $form_field_count = $form_field_count + 1;
                                          //echo "4";
                                      }

                                      if (strlen($data["expectation_student_signature"]) > 0) {
                                          $form_field_count = $form_field_count + 1;
                                          //echo "5";
                                      }

                                      if (strlen($data["consent_sign_date"]) > 0) {
                                          $form_field_count = $form_field_count + 1;
                                          //echo "6";
                                      }

                                      if (strlen($data["consent_signature"]) > 0) {
                                          $form_field_count = $form_field_count + 1;
                                          //echo "7";
                                      }

                                      //echo "<tr><td>" . $userid . "</td><td>" . $firstname . " " . $lastname . "</td></tr>";
                                      //########################################################################
                                  }

                                  //echo "profile_field_count: " . $profile_field_count;
                                  $profile_complete_percentage = round(($profile_field_count / 14) * 100);
                                  $medical_complete_percentage = round(($medical_field_count / 1) * 100);
                                  $form_complete_percentage = round(($form_field_count / 7) * 100);

                                  //if there are forms, then include them in the overall percentage 0=false, 1=true
                                  if ($ShowForms !=0) {
                                      $overall_percentage = round(($profile_complete_percentage + $medical_complete_percentage + $form_complete_percentage ) / 3);
                                  }
                                  else {
                                      $overall_percentage = round(($profile_complete_percentage + $medical_complete_percentage ) / 2);
                                  }


                          ?>
                          <div>
                              <span style="font-size:medium;font-weight:600;font-family:'Segoe UI';">Percent Complete (Profile, Medical, Forms)</span>
                              <div class="progress">
                                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $overall_percentage;?>"
                                      aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $overall_percentage;?>%">
                                      <?php echo $overall_percentage;?>%
                                  </div>
                              </div>


                              <div class="shortcuts">

                                  <!--	<a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-list-alt"></i>
                                        <span class="shortcut-label">Apps</span>
                                    </a>

                                    <a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-bookmark"></i>
                                        <span class="shortcut-label">Bookmarks</span>
                                    </a>

                                    <a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-signal"></i>
                                        <span class="shortcut-label">Reports</span>
                                    </a>

                                    <a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-comment"></i>
                                        <span class="shortcut-label">Comments</span>
                                    </a>
                                    <a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-picture"></i>
                                        <span class="shortcut-label">Photos</span>
                                    </a>

                                     -->
                                  <a href="info.php" class="shortcut">
                                      <i class="shortcut-icon  icon-info-sign"></i>

                                      <span class="shortcut-label">
                                          <b>
                                              Profile
                                              <?php
                                  if ($profile_complete_percentage > 99) {
                                      echo "<i class=\"shortcut-icon icon-check\" style=\"color:green;font-size:large\"></i>";
                                  } else {
                                      echo "<span style=\"color:red;font-size:small\">(" . $profile_complete_percentage . "%)</span>";
                                  }
                                              ?>
                                          </b>
                                      </span>
                                  </a>
                                  <a href="Medical.php" class="shortcut">
                                      <i class="shortcut-icon icon-h-sign"></i>
                                      <span class="shortcut-label">
                                          Medical
                                          <?php
                                  if ($medical_complete_percentage > 99) {
                                      echo "<i class=\"shortcut-icon icon-check\" style=\"color:green;font-size:large\"></i>";
                                  } else {
                                      echo "<span style=\"color:red;font-size:small\">(" . $medical_complete_percentage . "%)</span>";
                                  }
                                          ?>
                                      </span>
                                  </a>

                                  <?php

                                  if ($ShowForms!=0) {
                                  ?>
                                  <a href="admin/form-manager.php" class="shortcut">
                                      <i class="shortcut-icon  icon-file-text"></i>
                                      <span class="shortcut-label">
                                          Forms
                                          <?php
                                      if ($form_complete_percentage > 99) {
                                          echo "<i class=\"shortcut-icon icon-check\" style=\"color:green;font-size:large\"></i>";
                                      } else {
                                          echo "<span style=\"color:red;font-size:small\">(" . $form_complete_percentage . "%)</span>";
                                      }
                                          ?>
                                      </span>
                                  </a>
                                  <?php
                                  }
                                  ?>

                                  <a href="EmergencyInfo.php" target="_blank" class="shortcut">
                                      <i class="shortcut-icon icon-medkit"></i>
                                      <span class="shortcut-label">Report</span>
                                  </a>

                                  <?php
                                  //if (($userid === 'dlewis6560') || ($userid === 'dmassey') || ($userid === 'pmalley') || ($userid === 'etowey') || ($userid === 'gkeeney')
                                  //    || ($userid === 'dsimison') || ($userid === 'cnichols') || ($userid === 'jeason') || ($userid === 'emchorey')) {
                                  ?>

                                  <?php

                                    //echo "showdocs" . " =" . $ShowDocs;

                                  if ($ShowDocs!=0) {
                                  ?>
								  
                                  <a href="admin/mydocuments.php?<?php echo "p1=" . $encrypted_userID . "&p2=" . $encrypted_subID; ?>" target="_self" class="shortcut">
                                      <i class="shortcut-icon icon-file"></i>
                                      <span class="shortcut-label">Documents</span>
                                  </a>
                                  <?php };  ?>

                                  <?php
                                  if ($admin_user != 0) {   // if not an admin user, don't show this button
                                  ?>
                                  <a href="admin/index.php" class="shortcut">
                                      <i class="shortcut-icon icon-star"></i>
                                      <span class="shortcut-label">Dashboard</span>
                                  </a>
                                  <?php };  ?>

                                  <a href="pwd.php" class="shortcut">
                                      <i class="shortcut-icon icon-key"></i>
                                      <span class="shortcut-label">Password</span>
                                  </a>

                                  <!--
                            <a href="admin/id-numbers.php" class="shortcut">
                                <i class="shortcut-icon icon-credit-card"></i>
                                <span class="shortcut-label">ID Numbers</span>
                            </a>
                            -->


                                  <!--
                            <a href="javascript:;" class="shortcut">
                                <i class="shortcut-icon icon-question"></i>
                                <span class="shortcut-label">Help</span>
                            </a>
                            -->

                                  <a href="#" class="shortcut" onclick="logout();">
                                      <i class="shortcut-icon icon-signout"></i>
                                      <span class="shortcut-label">Logout</span>
                                  </a>

                              </div>

                              <?php };  ?>

                              <?php if ($subType == "F") {  ?>

                              <div class="shortcuts">


                                  <a href="info.php" class="shortcut">

                                      <i style="color:#125886;" class="shortcut-icon icon-info-sign"></i>
                                      <span class="shortcut-label">
                                          <b>Profile</b>
                                      </span>
                                  </a>

                                  <a href="Medical.php" class="shortcut">
                                      <i style="color:#45B178;" class="shortcut-icon icon-h-sign"></i>
                                      <span class="shortcut-label">Medical</span>
                                  </a>
								  
                                  <a href="providers.php" class="shortcut">
                                      <i style="color:#8C3785;" class="shortcut-icon icon-user-md"></i>
                                      <span class="shortcut-label">Providers</span>
                                  </a>								  
                                  <!--
                                  <a href="admin/id-numbers.php" class="shortcut">
                                      <i style="color:#158D9F;" class="shortcut-icon icon-credit-card"></i>
                                      <span class="shortcut-label">ID Numbers</span>
                                  </a>
								  -->
                                  
                                  <a href="admin/user-manager.php" class="shortcut">
                                      <i style="color:#B4141C;text-shadow: 2px 2px #82868C;" class="shortcut-icon icon-user"></i>
                                      <span class="shortcut-label">Family</span>
                                  </a>

                                  <?php if ($_SESSION["admin_user"] == 1) { ?>
                                  <a href="admin/my-account.php" class="shortcut">
                                      <i style="color:#87CD8C;" class="shortcut-icon icon-sign-blank "></i>
                                      <span class="shortcut-label">Account</span>
                                  </a>
                                  <?php  } ?>

                                  <?php
                                      //if (($userid === 'dlewis6560') || ($userid === 'dmassey') || (strtoupper($userid) === 'EMCHOREY')) {   // if not dlewis6560, don't show this button
                                  ?>

                                  <a href="admin/mydocuments.php?<?php echo "p1=" . $encrypted_userID . "&p2=" . $encrypted_subID; ?>" target="_self" class="shortcut">
                                      <i style="color:#641D44;" class="shortcut-icon icon-file"></i>
                                      <span class="shortcut-label">Documents</span>
                                  </a>
                                  <?php //};  ?>

                                  <a href="pwd.php" class="shortcut">
                                      <i style="color:#4977D8;" class="shortcut-icon icon-key"></i>
                                      <span class="shortcut-label">Password</span>
                                  </a>
                                  <a href="EmergencyInfo.php" target="_blank" class="shortcut">
                                      <i style="color:#C50077;" class="shortcut-icon icon-medkit"></i>
                                      <span class="shortcut-label">Report</span>
                                  </a>

                                  <!--
                            <a href="javascript:;" class="shortcut">
                                <i class="shortcut-icon icon-question"></i>
                                <span class="shortcut-label">Help</span>
                            </a>
                             -->
                                  <a href="#" class="shortcut" onclick="return logout();">
                                      <i class="shortcut-icon icon-signout"></i>
                                      <span class="shortcut-label">Logout</span>
                                  </a>

                              </div>

                              <?php };  ?>

                              <!-- /shortcuts -->

                          </div>
                          <!-- /widget-content -->

                      </div>
                      <!-- /widget -->
                  </div>
            </div>
            <div class="col-sm-2 col-xs-1"></div>
        </div>

            <?php if ($subType == "F") {  ?>
            <div class="row hidden-xs">
                <div class="col-sm-2 col-xs-1"></div>
                <div class="col-sm-8 col-xs-10">
                    <div class="well">
                        <h2>Welcome to your Family Account</h2>
                        <p>
                            In order to populate your Electronic Medical Record, visit each of the sections and enter the information for each family member.
                            <br />
                            <br />
                            Be sure to provide your Government Issued ID, Driver&quot;s License, Healthcare Provider or other identifying card information so that registered providers can access your information in case there is an emergency.
                        </p>
                        <p>After you have entered your information, select "Report" to view the report that can be provided to the first responder. Remember that the more information you provide, the more informed a first responder will be.</p>
                        <center>
                            <img src="img/family-multi.jpg" class="img-responsive img-rounded hidden-xs" alt="family" />
                        </center>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-1"></div>
            </div>
            <?php };  ?>

            <?php if ($subType != "F") {  ?>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 center-block">
                    <div class="well">
                        <?php if ($admin_user != 0) { ?>
                           
                        <div class="text-center"><h3><u>Welcome to your Admin Portal</u></h3></div>
                        <p style="font-size:medium">
                            In order to populate your Electronic Medical Record, visit each section and enter the information.
                            <br /><br />
                            You can use the dropdown list to change the Current User and view the selected Student&apos;s information.
                            <br /><br />
                            Tap the &quot;Admin Button&quot; to view statistics, manage users and groups, and run reports.
                        </p>
                        <p><br /></p>
                        <?php } else {?>
                        <div class="text-center"><h3><u>Welcome to your Parent Portal</u></h3></div>
                        <p style="font-size:medium">
                            In order to populate your child&apos;s Emergency Medical Information, visit the &quot;Profile&quot;, &quot;Medical&quot;, and &quot;Forms&quot; sections and enter the information.
                            <br />
                        <p style="font-size:medium">
                        After you have entered your information, select "Report" to view the report that can be provided to the first responder. Remember that the more information you provide, the more informed a first responder will be.</p>
                        <p style="font-size:medium">
                            <a href="https://emtelink.com/families.php" target="_blank"><img src="img/e-logo.png" style="width:24px;height:24px" /> Click here to sign-up for an individual or family account today.<br />A portion of the proceeds are donated back to the school or group (band, football team, etc.)</a>
                        </p>
                        <?php } ?>

                        <center>
                        <?php
                          if ($admin_user != 0) {   
                            echo "<img src=\"img/admin_portal.jpg\" class=\"img-responsive img-rounded hidden-xs\" alt=\"school\" />";
                          } else {
                            echo "<img src=\"img/family-multi.jpg\" class=\"img-responsive img-rounded hidden-xs\" alt=\"school\" />";
                          }
                        ?>
                        </center>
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>
            <?php };  ?>

        </div>

        <br />

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="signupModal" class="modal fade" role="dialog">
            <div>
                <div class="modal-header">
                    <a class="close" data-dismiss="modal">×</a>
                    <h3>Sign Up</h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <fieldset class="hide">
                            <a class="btn">GitHub</a>
                            <a class="btn">Google</a>
                        </fieldset>
                        <fieldset>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-prepend">
                                        <span class="add-on">
                                            <i class="icon-user"></i>
                                        </span>
                                        <input autofocus="" class="input-medium" placeholder="Name" required="" type="text" />
                                    </div>
                                    <p class="help-block">name used only for display</p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-prepend">
                                        <span class="add-on">
                                            <i class="icon-envelope"></i>
                                        </span>
                                        <input class="input-medium" placeholder="Email address" required="" type="text" />
                                    </div>
                                    <p class="help-block">you will receive confirmation email</p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-prepend">
                                        <span class="add-on">
                                            <i class="icon-key"></i>
                                        </span>
                                        <input class="input-medium" placeholder="Password" required="" type="password" />
                                    </div>
                                    <p class="help-block">must be at least 8 alphanumeric characters</p>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    <div class="modal-footer">
                        <a class="">Sign In</a>
                        <button class="btn btn-warning" id="signupButtonElement">Sign Up</button>
                        <div class=""></div>
                        <p class="muted">
                            By continuing, you agree to MyEMRMyWay Terms and Conditions
                            <a href="/privacy.html">Privacy Policy</a>
                            and
                            <a href="/terms.html">Terms of Service</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <br />
        <br />
        <!-- Footer -->

        <?php  include("include/incFooter-lg.php"); ?>
        <!-- /extra -->

        <script src="Scripts/jquery-1.9.1.min.js"></script>
        <script src="Scripts/bootstrap.min.js"></script>
        <script src="Scripts/eldarion-ajax.min.js"></script>

        <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            // onClick on 'Browse' load the internal page
            $(function () {
                $('#user_info').click(function () {
                    window.load('info.php');
                });
            });


        });

        function getval(sel) {
            //alert(sel.value);

            $.ajax({
                type: "POST",
                url: "ws/process-session-change.php",
                data: { "userid": sel.value },
                success: function (msg) {
                    //alert(msg);
                    //alert("User switched to " + sel.value);
                    $('#user-switched-success').removeClass('hide');
                    $.wait(function () { $('#user-switched-success').addClass('hide'); location.reload(); }, 1);

                },
                error: function () {
                    //alert(msg);
                },
                complete: function () {
                    //alert(msg);
                }

            });
        }

        $.wait = function (callback, seconds) {
            return window.setTimeout(callback, seconds * 1000);
        }

        $('.popper').popover({
            placement: 'bottom',
            container: 'body',
            html: true,
            content: function () {
                return $(this).next('.popper-content').html();
            }
        });


        function logout() {
            //alert('logout');
            $.ajax({
                type: "POST",
                url: "ws/process_logoff.php",
                success: function (msg) {
                    //alert(msg);

                    window.location.replace("https://www.emtelink.com/myemrmyway/index.html");


                },
                error: function (data) {
                    alert(data);
                }
            });
        }

        </script>

</body>
</html>
