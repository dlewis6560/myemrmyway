<?php

include("include/incFunctions.php");

use Urlcrypt\Urlcrypt;
require_once '../Urlcrypt.php';
Urlcrypt::$key = $mykey;

session_start();

//make sure we have a valid sesion
include("include/session.php");

// reference the Dompdf namespace
require_once 'libs/dompdf/autoload.inc.php';
include('libs/phpqrcode/qrlib.php');

include("Include/base64_logo.php");

use Dompdf\Dompdf;

//var set in email-request-records
//echo $requestName;
//exit;

//insert record into doc table
//
$newDocId = $database->insert("user_docs", [
   "uid" => $uid,
   "doc_name" => "Records Requested",
   "doc_description" => "Requested from " . $requestPersonOrFacilityName,
   "status" => "Request Sent"
   //"createdate" => "uploaded by user"
]);


$codeContents = Urlcrypt::encrypt($newDocId . "|" . $subId  . "|" . $uid . "|" . time());
//echo $codeContents;

$fileName = 'C:\Windows\Temp\qr_'.md5($codeContents).'.png';

QRcode::png  ($codeContents,
$outfile = $fileName,
$level = QR_ECLEVEL_Q,
$size = 3,
$margin = 1,
$saveandprint = false
);

$imageData = base64_encode(file_get_contents($fileName));
$src = 'data:png;base64,'.$imageData;
$src=str_replace(" ","",$src);

$logo_src = 'data:png;base64,'.$logo_image;
$logo_src=str_replace(" ","",$logo_src);



// instantiate and use the dompdf class
$dompdf = new Dompdf();

// (Optional) Setup the paper size and orientation
//$dompdf->setPaper('Letter', 'portrait');

$sRequest = '<div><h2>HIPAA Patient Request for Health Information</h2></div>
             <div align="left" style="width:96%;margin-left:2%;font-size: 14px;"><i>I am requesting that a copy of my Requested Health Information (as defined below) be sent to the Destination (as defined below). I understand that the Health Insurance Portability and Accountability Act (HIPAA) gives me the right to access the medical and health information (protected health information or PHI) about me and the right to have that PHI transmitted directly to a person designated by me if I indicate as such.</i></div>
<br />
             <div align="left" class="div-style">Patient Information</div>
             <table border="1" cellpadding="10" style="width:100%;">
<tr>
  <td>Name: <strong>' . $requestName . '</strong></td>
  <td colspan="3">DOB: <strong>' . $requestDOB . '</strong></td>
</tr>
<tr>
  <td>Phone: <strong>' . $requestPhone . '</strong></td>
  <td colspan="3">Email: <strong>' . $requestEmail . '</strong></td>
</tr>
<tr>
  <td colspan="4">Address: <strong>' . $requestAddress . '</strong></td>
</tr>
</table>

<div align="left" class="div-style">Covered Entity Information</div>
<table border="1" cellpadding="10" style="width:100%;">
<tr>
  <td>Person of Facility Name: <strong>' . $requestPersonOrFacilityName . '</strong></td>
</tr>
<tr>

  <td>Address: <strong>' . $requestPersonOrFacilityAddress . '</strong></td>
</tr>
</table>

<div align="left" class="div-style"><u>I am Requesting the following Records</u></div>
<table border="0" cellpadding="10" style="width:100%;">
<tr>
  <td>Dates of Service: <strong>From ' . $request_from_date . ' thru ' . $request_to_date . '</strong></td>
</tr>
<tr>
  <td>Notes: <strong>' . $requestNotes . '</strong></td>
</tr>
</table>

<div align="left" class="div-style"><u>Record Delivery</u></div><br>
<div align="left" style="padding-left:12px;">Please either email or fax the records</div>
<table border="0" cellpadding="10" style="width:100%;">
<tr>
  <td>Email: <strong>' . $requestMethodValue . '</strong></td>
</tr>
<tr>
  <td>Fax: <strong>(850) 807-5001  *<u>Be Sure To Use Provided Cover Sheet</u>*</strong></td>
</tr>
</table>

<div align="left" class="div-style"><u>Signature</u></div>

<table border="0" cellpadding="10" style="width:100%;padding-top:10px;">
<tr>
  <td><img height="75px" width="203px" src="'.$signature.'"/>&nbsp;&nbsp;Signed this date of <strong>3/3/2018 1:32AM</strong></td>
</tr>
</table>
<div align="left" style="width:96%;margin-left:2%;font-size: 14px;"><i>If there are any questions about this access request, please contact me by the phone number or by email address listed above. This is a HIPAA-compliant Patient Access Request. Per Health & Human Services guidance, all covered entities must act on this request within 30 days.</i></div>
<div id="wrapper">
     <div id="bottomDweller"><img width="261" height="48" src="' . $logo_src . '"/></div>
</div>
';

$sINfo = '<div class="page_break"><h2>Background Information Regarding This Form</h2></div>
             <div align="left" style="width:96%;margin-left:2%;font-size: 16px;">
This is a <strong>Patient Access Request</strong>. Under HIPAA, patients have a federal right to receive a copy of Protected Health Information in their designated record set.<br /><br />
A <strong>Patient Access Request</strong> is not the same as a Patient Authorization. The only elements that HIPAA requires in a <strong>Patient Access Request</strong> are that:<br /><br />
<span class="div-style">"...the request must be in writing, signed by the individual, and clearly identify the designated person or entity and where to send the PHI.
45 CFR 164.524(c)(3)(ii)"</span>
<br /><br />
Per Guidance from HHS, a provider is not permitted to require use of an Authorization if a patient submits a <strong>Patient Access Request</strong>. Per HHS,<br /><br />
<span class="div-style">"...a HIPAA authorization is not required for individuals to request access to their PHI, including to direct a copy to a third party.
(https://www.hhs.gov/hipaa/for-professionals/faq/2033/when-do-the-hipaa-privacy-rule-limitations-on-fees/index.html")</span>
<br /><br />
Unlike a HIPAA Authorization, a provider must act on a <strong>Patient Access Request</strong> within 30 days, and any fees charged to patients must be within the strict limits set by HIPAA.<br /><br />
<span class="div-style">For more information on the <strong>Patient Access Request</strong> form, please visit www.patientbank.us/form or the HHS Guidance at https://www.hhs.gov/hipaa/for- professionals/privacy/guidance/access/.</span>
<div id="wrapper1">
     <div id="bottomDweller"><img width="261" height="48" src="' . $logo_src . '"/></div>
</div>
</div>
';


$str_html = '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <style>
      @page {
        size: letter portrait;
        margin:12.7;padding:12.7;
      }
      body {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        text-align: center;
        border: thin solid black;
      }

      .page_break { page-break-before: always; }

      .header-style {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        text-align: center;
        border: thin solid black;
      }

    hr {
        display: block;
        margin-top: 0.5em;
        margin-bottom: 0.5em;
        margin-left: auto;
        margin-right: auto;
        border-style: dashed;
        border-width: 1px;
	    border-top: 1px dashed #8c8b8b;
	    border-bottom: 1px dashed #fff;
    }

    .div-style {
       padding-left:12px;
       background-color:#E7E7E7;
       width:98%;
       margin-left:1px;
       font-weight:500;
       color:#808080;
       height:18px;
       valign:middle;
    }

#wrapper {height: 50px; position:relative;}
#wrapper1 {height: 50px; position:relative;}
#wrapper2 {height: 50px; position:relative;}
#bottomDweller {
     position: absolute;
     bottom:0;
     left: 254px;
     padding-bottom:4px}

    </style>
    <title>Medical Records Request for ' . $requestName . '</title>
</head>
<body>'.$sRequest.$sINfo.'
    <table border="0" style="width:100%" class="page_break">
           <tr><td colspan="2" align="center"><span style="font-size:30px"><br />Medical Records Response for<br /><strong><u>' . $requestName . '</u></strong></span><br /><br /><br /></td></tr>
           <tr><td colspan="2" align="center"><span style="font-size:30px">Please Use This Cover Sheet<br/>when faxing Medical Records for<br /><strong><u>' . $requestName . '</u><br /><br />
Fax Number for Records<br/>(850) 807-5001
</strong></span><br /><br /><br /><br /></td></tr>
           <tr>
              <td colspan="2" align="center"><img src="'.$src.'"/><br /><br /><br /><br /></td>
           </tr>
           <tr>
              <td colspan="2" align="center">' . $requestName . '<br />' . $requestAddress . '<br />' . $requestPhone . '<br />' . $requestEmail . '</td>
           </tr>
        </table>
<div id="wrapper2">
     <div id="bottomDweller"><img width="261" height="48" src="' . $logo_src . '"/></div>
</div>
</body>
</html>';



$html_prefix = "<html><head> <style>div {display:flex; div * {margin-top:auto;margin-bottom:auto;}</style></head><body>";
$html_post = "</body></html>";

$dompdf->loadHtml($str_html);

$dompdf->render();
$pdf_content =  $dompdf->output();

// Render the HTML as PDF
//$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream('records_request',array('Attachment'=>0));
?>
