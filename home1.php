<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <title>Dashboard :: EMTeLink Tracker</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="admin/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="admin/css/font-awesome.min.css" rel="stylesheet">

    <link href="admin/css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">

    <link href="admin/css/base-admin-3.css" rel="stylesheet">
    <link href="admin/css/base-admin-3-responsive.css" rel="stylesheet">

    <link href="admin/css/pages/dashboard.css" rel="stylesheet">

    <link href="admin/css/custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php

    include("include/incConfig.php");
    session_start();

    $total_user_count = $database->count("user", [
	  "admin_user" => 0
        ]);

    $total_not_signed_expectation = $database->count("user", [
	"AND" => [
	  "expectation_student_signature" => null,
      "admin_user" => 0]
        ]);

    $total_wo_consent_signature = $database->count("user", [
     "AND" => [
       "consent_signature" => null,
       "admin_user" => 0]
         ]);

    $total_wo_witness_signature = $database->count("user", [
	"AND" => [
	  "witness_signature" => null,
      "admin_user" => 0]
    ]);

    $total_wo_parent_signature = $database->count("user", [
	"AND" => [
	  "parent_signature" => null,
      "admin_user" => 0]
        ]);

    $total_wo_ins_co = $database->count("user", [
	"AND" => [
	  "ins_company" => null,
      "admin_user" => 0]
        ]);

    $total_one_or_more_not_signed = round(($total_not_signed_expectation + $total_wo_consent_signature + $total_wo_witness_signature + $total_wo_parent_signature ) / 4,0);

    $total_percent_complete = 100 - round(($total_one_or_more_not_signed / $total_user_count) * 100,0);

    /*    select firstname + ' ' + lastname from student
    where expectation_student_signature is Null
    order by firstname


    select firstname + ' ' + lastname from student
    where consent_signature is Null
    order by firstname

    select firstname + ' ' + lastname from student
    where witness_signature is Null
    order by firstname


    select firstname + ' ' + lastname from student
    where parent_signature is Null
    order by firstname

    select firstname + ' ' + lastname from student
    where ins_company is Null
    order by firstname */



    ?>

</head>

<body>

    <?php 

    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];
    $admin_user = $_SESSION["admin_user"];

    $_SESSION["current_page"] = 1;

    ?>

    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                </button>
                <a class="navbar-brand" href="./index.html">EMTeLink Tracker</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu">
                            <!--<li><a href="./account.html">Account Settings</a></li>
                             <li><a href="javascript:;">Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:;">Help</a></li> -->
                        </ul>
                    </li>

                    <li><a href="../home.php" style="font-size: 16px"><i class="icon-home"></i>&nbsp;Home</a></li>

                    <li class="dropdown">

                        <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 16px">
                            <i class="icon-user"></i>
                            <?php echo $firstname . " " . $lastname ?>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <!--  <li><a href="javascript:;">My Profile</a></li>
                            <li><a href="javascript:;">My Groups</a></li> 
                            <li class="divider"></li> -->
                            <li><a href="../Index.html" style="font-size: 14px">Logout</a></li>
                        </ul>

                    </li>
                </ul>

                <!-- <form class="navbar-form navbar-right" role="search">
                   <div class="form-group">
                     <input type="text" class="form-control input-sm search-query" placeholder="Search">
                   </div>
                 </form> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="subnavbar">

        <div class="subnavbar-inner">

            <div class="container">

                <!-- <a href="javascript:;" class="subnav-toggle" data-toggle="collapse" data-target=".subnav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>

                </a> -->


            </div>
            <!-- /container -->

        </div>
        <!-- /subnavbar-inner -->

    </div>
    <!-- /subnavbar -->


    <div class="main">

        <div class="container">

                <div class="col-md-4"></div>
                <div class="col-md-4">

                    <div class="widget stacked">

                        <div class="widget-header">
                            <i class="icon-bookmark"></i>
                            <h3>Shortcuts</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="shortcuts">

                                <!--	<a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-list-alt"></i>
                                        <span class="shortcut-label">Apps</span>
                                    </a>

                                    <a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-bookmark"></i>
                                        <span class="shortcut-label">Bookmarks</span>
                                    </a>

                                    <a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-signal"></i>
                                        <span class="shortcut-label">Reports</span>
                                    </a>

                                    <a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-comment"></i>
                                        <span class="shortcut-label">Comments</span>
                                    </a>
                                    <a href="javascript:;" class="shortcut">
                                        <i class="shortcut-icon icon-picture"></i>
                                        <span class="shortcut-label">Photos</span>
                                    </a>

                                     -->
                                <a href="../home.php" class="shortcut">
                                    <i class="shortcut-icon icon-home"></i>
                                    <span class="shortcut-label">Home</span>
                                </a>

                                <a href="user-manager.php?current_page=1" class="shortcut">
                                    <i class="shortcut-icon icon-user"></i>
                                    <span class="shortcut-label">Users</span>
                                </a>

                                <a href="javascript:;" class="shortcut">
                                    <i class="shortcut-icon icon-group"></i>
                                    <span class="shortcut-label">Groups</span>
                                </a>

                                <a href="javascript:;" class="shortcut">
                                    <i class="shortcut-icon icon-signal"></i>
                                    <span class="shortcut-label">Reports</span>
                                </a>


                                <a href="javascript:;" class="shortcut">
                                    <i class="shortcut-icon icon-cog"></i>
                                    <span class="shortcut-label">Settings</span>
                                </a>

                                <a href="javascript:;" class="shortcut">
                                    <i class="shortcut-icon icon-upload"></i>
                                    <span class="shortcut-label">Workflow</span>
                                </a>
								
                                <a href="javascript:;" class="shortcut">
                                    <i class="shortcut-icon icon-question"></i>
                                    <span class="shortcut-label">Help</span>
                                </a>


                                <a href="../Index.html" class="shortcut">
                                    <i class="shortcut-icon icon-signout"></i>
                                    <span class="shortcut-label">Logoff</span>
                                </a>
                                <br />
                                <br />
                            </div>
                            <!-- /shortcuts -->
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->


                </div>
                <div class="col-md-4"></div>
				
            </div>
            <!-- /row -->
            <br />

        </div>
        <!-- /container -->

    </div>
    <!-- /main -->


    <footer>
        <div class="container text-center">
            <p>&copy; Copyright @ 2015 EMTeLink</p>
        </div>
        <!-- end Container-->
    </footer>





    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/libs/jquery-1.9.1.min.js"></script>
    <script src="./js/libs/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="./js/libs/bootstrap.min.js"></script>

    <script src="./js/plugins/flot/jquery.flot.js"></script>
    <script src="./js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="./js/plugins/flot/jquery.flot.resize.js"></script>

    <script>

        $(function () {

            var data = [];
            var series = 2;
            data[0] = { label: "Complete", data: Math.floor(<?php echo $total_percent_complete/100; ?> * 100) + 1}
            data[1] = { label: "Incomplete", data: Math.floor(<?php echo 1 - ($total_percent_complete/100); ?> * 100) + 1 }

            $.plot($("#donut-chart"), data,
            {
                colors: ["#F90", "#222", "#777", "#AAA"],
                series: {
                    pie: {
                        innerRadius: 0.5,
                        show: true
                    }
                }
            });

        });
    </script>
</body>
</html>
