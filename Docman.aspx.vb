﻿
Imports System.IO
Imports System.Security.Cryptography


Imports GleamTech.FileUltimate
Imports GleamTech.DocumentUltimate.Web

Partial Class tracker_Docman
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        'Set this property only if you have a valid license key, otherwise do Not
        'set it so DocumentUltimate runs in trial mode.
        FileUltimateConfiguration.Current.LicenseKey = "QUY68J8PDL-KQVB3D3JED-RWQ8T9EA1J-U445DA1C5G-8BAPC27QXK-6RU7BHFY4J-UNYJF464XN-7HF"

        'The default CachePath value Is "~/App_Data/DocumentCache"
        'Both virtual And physical paths are allowed.
        DocumentUltimateWebConfiguration.Current.CachePath = "C:\inetpub\wwwroot\emtelink\new\newsite\myEMRmyway\App_Data\DocumentCache"
	
		

        If Not IsPostBack Then
            'PopulateUserSelector()
        End If

        'Shared 256 bit Key and IV here
        Dim sKy As String = "emtelink7+#bbtrm!c8814z5kniletme"  '32 chr shared ascii string (32 * 8 = 256 bit)
        Dim sIV As String = "kniletmeeeyy66#cs!9hjv88emtelink"  '32 chr shared ascii string (32 * 8 = 256 bit)

        Dim strEncryptedUserID As String = Request.QueryString("p1")
        Dim strEncryptedSubID As String = Request.QueryString("p2")

        Dim strUserID As String
        Dim strSubID As String

        Try
            strUserID = DecryptRJ256(sKy, sIV, strEncryptedUserID)
            strSubID = DecryptRJ256(sKy, sIV, strEncryptedSubID)


            'Response.Write("<BR />user path:" + user_path)
            'user_path =  & strSubID & "\Docs\" & strUserID & "\"
            Dim basePath As String = "C:\Uploads\"
            Dim QuotaMB As Integer = 30 'the max amount of drive space (in megabytes) per userid
            'SetDynamicFolderAndPermissions(strSubID, strUserID, basePath, QuotaMB)
            'Response.Write("<BR />" + fileManager.RootFolders(0).Name)
            'Response.Write("<BR />" + fileManager.RootFolders(0).Location.ToString())

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Public Function DecryptRJ256(ByVal prm_key As String, ByVal prm_iv As String, ByVal prm_text_to_decrypt As String) As String

        Dim sEncryptedString As String = Replace(prm_text_to_decrypt, "~", "+")

        Dim myRijndael As New RijndaelManaged
        myRijndael.Padding = PaddingMode.Zeros
        myRijndael.Mode = CipherMode.CBC
        myRijndael.KeySize = 256
        myRijndael.BlockSize = 256

        Dim key() As Byte
        Dim IV() As Byte

        key = System.Text.Encoding.ASCII.GetBytes(prm_key)
        IV = System.Text.Encoding.ASCII.GetBytes(prm_iv)

        Dim decryptor As ICryptoTransform = myRijndael.CreateDecryptor(key, IV)

        Dim sEncrypted As Byte() = Convert.FromBase64String(sEncryptedString)

        Dim fromEncrypt() As Byte = New Byte(sEncrypted.Length) {}

        Dim msDecrypt As New MemoryStream(sEncrypted)
        Dim csDecrypt As New CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)

        csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length)

        Return (System.Text.Encoding.ASCII.GetString(fromEncrypt))

    End Function


    Public Function EncryptRJ256(ByVal prm_key As String, ByVal prm_iv As String, ByVal prm_text_to_encrypt As String)

        Dim sToEncrypt As String = prm_text_to_encrypt

        Dim myRijndael As New RijndaelManaged
        myRijndael.Padding = PaddingMode.Zeros
        myRijndael.Mode = CipherMode.CBC
        myRijndael.KeySize = 256
        myRijndael.BlockSize = 256

        Dim encrypted() As Byte
        Dim toEncrypt() As Byte
        Dim key() As Byte
        Dim IV() As Byte

        key = System.Text.Encoding.ASCII.GetBytes(prm_key)
        IV = System.Text.Encoding.ASCII.GetBytes(prm_iv)

        Dim encryptor As ICryptoTransform = myRijndael.CreateEncryptor(key, IV)

        Dim msEncrypt As New MemoryStream()
        Dim csEncrypt As New CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)

        toEncrypt = System.Text.Encoding.ASCII.GetBytes(sToEncrypt)

        csEncrypt.Write(toEncrypt, 0, toEncrypt.Length)
        csEncrypt.FlushFinalBlock()

        encrypted = msEncrypt.ToArray()

        Return (Convert.ToBase64String(encrypted))

    End Function

End Class
