<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>MyEMRMyWay :: Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="MyEMRMyWay App">
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->
    <!--    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" /> -->
    <link href="Content/styles.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="admin/css/font-awesome.min.css" rel="stylesheet">
	

    <link href="admin/css/base-admin-3.css" rel="stylesheet">
    <link href="admin/css/base-admin-3-responsive.css" rel="stylesheet">
	<link href="Content/sweetalert2.min.css" rel="stylesheet" />	


    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");

    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';
    Urlcrypt::$key = $mykey;
	
    session_start();

    //make sure we have a valid sesion
	include("include/session.php");

    if ($subType == "F") {
        $dont_hide_Edit_Buttons = true;
    } else {
        $dont_hide_Edit_Buttons = (strcmp($loggedin_userid, $userid) == 0) || ($admin_user == 0);
    }

    //get the subscription name to use in the consent statement
    $recordset = $database->select("Subscriptions", [
        "subName"
    ], [
        "subId" => "$subId"
    ]);

    foreach($recordset as $data)
    {
        $subName = $data["subName"];
    }   
    
    $consent_statement = "I/We give " . $subName . " My/Our permission to allow the emergency treatment of the above named student.";

    ?>

    <style>
      .glyphicon {
          font-size: 14px;
      }

    </style>

</head>


<body>

    <!-- Navbar -->
    <?php include("include/navbar.php"); ?>
    <!-- End navbar -->

    <!-- jumbotron-->
    <div class="well well-sm">
        <div class="text-center">
            <h2>Profile for <?php echo $firstname . " " . $lastname ?></h2>
        </div>
                        <div class="text-center">
                                <div id='preview'>
                                    <?php
                                    include("include/incProfilepic.php");
                                    ?>
                                </div>
                                <br />
                                <?php 
                                //echo "admin_user :" . $admin_user . "</br>";
                                //echo "subType :" . $subType . "</br>";
                                //echo "loggedin_userid :" . $loggedin_userid . "</br>";
                                //echo "userid :" . $userid . "</br>";
                                //echo "strcmp:" . strcmp($loggedin_userid, $userid);
                                //echo "donthideeditbuttons:" . $dont_hide_Edit_Buttons;

                                if ($dont_hide_Edit_Buttons == true) { ?>
                                <form id="imageform" method="post" enctype="multipart/form-data" action='ws/processupload.php'>
                                        <input type="file" name="photoimg" id="photoimg" style="margin: auto; width: 90px; height: 25px;" />
                                        <input type="hidden" id="fileuserid" name="fileuserid" value="<?php echo $userid ?>" />
                                </form>
                                <h6>Select button to change picture.</h6>
                                <br />
                            <?php }?>
                            </div>
        <!-- End container -->
    </div>

    <!-- End jumbotron-->

    <div>

        <?php 
        //get user info
        //get user profile information
        $recordset = $database->select("user", [
            "address",
            "middleinitial",
            "dob",
            "gender",
            "city",
            "st",
            "zip",
            "student_email",
            "parent_email",
            "ins_company",
            "ins_phone",
            "groupno",
            "contractno",
            "effectivedate",
            "certification_no",
            "plan_with",
            "parent_name",
            "parent_addr",
            "parent_day_phone",
            "parent_night_phone",
            "relative_name",
            "relationship",
            "relative_phone",
            "family_doctor",
            "doctor_day_phone",
            "doctor_night_phone",
            "sign_date",
            "parent_signature",
            "witness_signature",
            "primary_name",
            "primary_addr",
            "primary_phone",
            "primary_carrierId",
            "primary_email",
            "primary_relationship",
            "secondary_name",
            "secondary_addr",
            "secondary_phone",
            "secondary_carrierId",
            "secondary_email",
            "secondary_relationship",
            "phy_name",
            "phy_addr",
            "phy_phone",
            "phy_email",
            "phy_type"
        ], [
            "userid" => "$userid"
        ]);


        foreach($recordset as $data)
        {
            //demographic data ########################################################
            $middleinitial = $data["middleinitial"];
            $address = $data["address"];
            $city = $data["city"];
            $state = $data["st"];
            $zip = $data["zip"];

            $gender = $data["gender"];
            $student_email = $data["student_email"];
            $parent_email = $data["parent_email"];

            $dob = "";
            $myDateTime = DateTime::createFromFormat('Y-m-d', $data["dob"]);
            if(!$myDateTime == ''){
                $dob = $myDateTime->format('m-d-Y');
            }

            //insurance data ########################################################
            $ins_company = $data["ins_company"];
            $groupno = $data["groupno"];
            $contractno = $data["contractno"];
            $certification_no = $data["certification_no"];
            $plan_with = $data["plan_with"];

            $effectivedate = "";
            $myDateTime = DateTime::createFromFormat('Y-m-d', $data["effectivedate"]);
            if(!$myDateTime == ''){
                $effectivedate = $myDateTime->format('m-d-Y');
            }

            //contact data ########################################################
            $primary_name = $data["primary_name"];
            $primary_addr = $data["primary_addr"];
            $primary_phone = $data["primary_phone"];
            $primary_email = $data["primary_email"];
            $primary_relationship = $data["primary_relationship"];
            $primary_carrierId = $data["primary_carrierId"];

            $secondary_name = $data["secondary_name"];
            $secondary_addr = $data["secondary_addr"];
            $secondary_phone = $data["secondary_phone"];
            $secondary_email = $data["secondary_email"];
            $secondary_relationship = $data["secondary_relationship"];
            $secondary_carrierId = $data["secondary_carrierId"];

            $phy_name = $data["phy_name"];
            $phy_addr = $data["phy_addr"];
            $phy_phone = $data["phy_phone"];
            $phy_email = $data["phy_email"];
            $phy_type = $data["phy_type"];

            $parent_addr = $data["parent_addr"];
            $parent_day_phone = $data["parent_day_phone"];
            $parent_night_phone = $data["parent_night_phone"];
            
            $relative_name = $data["relative_name"];
            $relationship = $data["relationship"];
            $relative_phone = $data["relative_phone"];
            
            $family_doctor = $data["family_doctor"];
            $doctor_day_phone = $data["doctor_day_phone"];
            $doctor_night_phone = $data["doctor_night_phone"];

            $ins_phone = $data["ins_phone"];

            //signatures #############################################################

            $sign_date = "";
            $myDateTime = DateTime::createFromFormat('Y-m-d', $data["sign_date"]);
            if(!$myDateTime == ''){
                $sign_date = $myDateTime->format('m-d-Y');
            }
            $parent_signature = $data["parent_signature"];
            $witness_signature = $data["witness_signature"];
            
            //echo "<tr><td>" . $userid . "</td><td>" . $firstname . " " . $lastname . "</td></tr>";
            //########################################################################
        }   
        
        //set the $primary_carrier text value (from the primary_carrierId stored with the user record )
        $recordset = $database->select("carriers", ["Carrier"], ["Id" => $primary_carrierId]);
        foreach($recordset as $data)  
        {
            $primary_carrier = $data["Carrier"]; 
        }   

        //set the $secondary_carrier text value (from the secondary_carrierId stored with the user record )
        $recordset = $database->select("carriers", ["Carrier"], ["Id" => $secondary_carrierId]);
        foreach($recordset as $data)  
        {
            $secondary_carrier = $data["Carrier"]; 
        }   

        ?>


    </div>
    <div class="container">
        <section>
            <?php 
            $instruction_content = "Enter your Demographic Information, ID Cards, Insurance, and Emergency Contact Information below.<br /><br />";
            $instruction_content = $instruction_content . "Tap an Edit button below to enable changes, and then Tap the Save button when done making changes.<br /><br />";
            $instruction_content = $instruction_content . "Tap the Header of each section to expand or collapse it.";
            include("include/incInstructions.php");
            ?>

            <div id="demographic"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel-group accordion1">
                    <div class="panel panel-primary open" style="border-color:black;">
                        <div class="panel-heading text-center">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion1" href="#collapseDemographic">
                                        <span style="font-size: large"><i class="icon-user" style="color: #ffffff"></i>&nbsp;&nbsp;<u><strong>Demographic Information</strong></u><br /><small>Tap the &quot;Edit Demographic Information&quot; button below to change data and then Save when done.</small></span>
                                    </a>
                                </div>
                            </div>
                         <div id="collapseDemographic" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form id="demographicform" class="demographicform">
                                <div class="row">
                                    <label class="col-xs-12 col-sm-1 pull-left" for="first_name">Name</label>
                                    <div class="col-xs-12 col-sm-4">
                                        <input  type="text" class="form-control" id="first_name" name="first_name" placeholder="first name" value="<?php echo $firstname ?>" />
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <input  type="text" class="form-control" id="middle_initial" name="middle_initial" placeholder="middle Initial" value="<?php echo $middleinitial ?>" />
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <input  type="text" class="form-control" id="last_name" name="last_name" placeholder="last name" value="<?php echo $lastname ?>" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <label class="col-xs-12 col-sm-1 pull-left" for="dob-picker">DOB</label>
                                    <div class="col-xs-12  col-sm-5">
                                        <input  id="dob-picker" name="dob-picker" type="text" class="date-picker form-control" placeholder="date of birth"value="<?php echo $dob ?>" />
                                    </div>
                                    <label class="col-xs-12 col-sm-1 pull-left"" for="age">Gender</label>
                                    <div class="col-xs-12  col-sm-5">
                                        <select class="form-control" id="gender" name="gender" style="background-color:#EEEEEE; color:black" >
                                            <option>&nbsp;</option>
                                            <option <?php if ($gender == 'Female') echo "selected"; ?>>Female</option>
                                            <option <?php if ($gender == 'Male') echo "selected"; ?>>Male </option>
                                        </select>
                                    </div>
                                </div>

                                <br />
                                <div class="row">
                                    <label class="col-xs-12 col-sm-1 pull-left" for="address">Address</label>
                                    <div class="col-xs-12 col-sm-11">
                                        <input  type="text" class="form-control" id="address" name="address" placeholder="street address" value="<?php echo $address ?>" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <label class="col-xs-12 col-sm-1 pull-left" for="city">City</label>
                                    <div class="col-xs-12  col-sm-3">
                                        <input  id="city" name="city" type="text" class="form-control" placeholder="city"value="<?php echo $city ?>" />
                                    </div>
                                    <label class="col-xs-12 col-sm-1 pull-left"" for="state">State</label>
                                    <div class="col-xs-12  col-sm-3">
                                        <?php $SelectedState = $state; ?>
                                        <select class="form-control" id="state" name="state">
                                            <?php include("include/incStateListOptions.php"); ?>
                                        </select>
                                    </div>
                                    <label class="col-xs-12 col-sm-1 pull-left"" for="zip">Zip</label>
                                    <div class="col-xs-12  col-sm-3">
                                      <input  id="zip" name="zip" type="text" class="form-control" placeholder="zip" value="<?php echo $zip  ?>" />
                                    </div>
                                </div>
                                
                                <br />
                                <div class="row">
                                    <label class="col-xs-12 col-sm-1 pull-left" for="student_email">Email</label>
                                    <div class="col-xs-12 col-sm-11">
                                        <input  type="text" class="form-control" id="student_email"  name="student_email" placeholder="email address" value="<?php echo $student_email ?>" />
                                    </div>
                                </div>
                                <!--
                                <br />
                                <div class="row">
                                    <label class="col-xs-3 pull-left" for="parent_email">Parent&apos;s Email Address</label>
                                    <div class="col-xs-9">
                                        <input  type="text" class="form-control" id="parent_email" name="parent_email" placeholder="parent&apos;s email address" value="<?php echo $parent_email ?>" />
                                    </div>
                                </div>
                                -->
                                <div class="row">
                                <br />
                                <br />
                                <div class="col-xs-12" style="text-align:center">
                                    <?php if ($dont_hide_Edit_Buttons == true) { ?>
                                    <button type="button" id="btn_edit_demographic" style="background-color:#3881C0;color:white;" class="btn">Edit Demographic Information</button>
                                    <?php }?>
                                    <button type="button" id="btn_cancel_demographic" class="btn hide btn-danger">Cancel</button>
                                    <button type="button" id="btn_save_demographic" class="btn hide btn-success">Save</button>
                                    <input type="hidden" id="userid" name="userid" value="<?php echo $userid ?>" />
                                </div>

                                    </div>
                            </form>
                            <div>
                                <div class="alert alert-success hide small" role="alert" id="demo-success">Demographic Information Saved Successfully.</div>
                            </div>
                        </div>
                             </div>
                    </div>
                        </div>
                </div>
            </div>
        </section>
		
        <!-- ID Numbers -->
        <section>
            <div id="id_cards"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel-group accordion_id_numbers">
                        <div class="panel panel-warning" style="border-color: black;">
                            <div class="panel-heading text-center">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion_id_numbers" href="#collapseIDNumbers">
                                        <span style="font-size: large"><i class="icon-credit-card" style="color: #000000"></i>&nbsp;&nbsp;<u><strong>ID Cards</strong></u></span>
                                    </a>
                                    <h5>Tap "Add ID" to add or tap an icon to edit or delete.</h5>
                                </div>
                            </div>
                            <div id="collapseIDNumbers" class="panel-collapse in">
                                <div class="panel-body" id="id_number_panel_body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-striped center" style="width: 85%;" id="id_number_table">
                                                <thead>
                                                    <tr class="text-left input-lg">
                                                        <th style="text-align: left">ID Number</th>
                                                        <th style="text-align: left" class="hidden-xs">Description</th>
                                                        <th style="text-align: left" class="hidden-xs">Date Updated</th>	
                                                        <th style="text-align: left">&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                    $strSQL = "SELECT uniqueId,idnumber,description,editdate ";
                                                    $strSQL = $strSQL . "FROM [user_ids] where uid=" . $uid . " ";
                                                    $strSQL = $strSQL . "ORDER BY description ";

                                                    //echo $strSQL;
                                                    $recordset = $database->query($strSQL);

                                                    foreach($recordset as $data)
                                                    {
                                                        $uniqueId = $data["uniqueId"];
                                                        $idnumber = $data["idnumber"];
                                                        $desc = $data["description"];
                                                        $EditDate = new DateTime($data["editdate"]);

                                                        echo ("<tr class=\"text-left input-lg\">");
                                                        echo ("  <td>" . $idnumber . "</td>");
                                                        echo ("  <td>" . $desc . "</td>");

                                                        echo ("  <td class=\"hidden-xs\">" . $EditDate->format('m/d/y h:i') . "</td>");
                                                        echo ("  <td style=\"text-align:left;padding:10px\">");
														
														$encrypted = Urlcrypt::encrypt($uniqueId . "|" . "edit". "|" . time());
                                                        
                                                        echo ("    <div class=\"id_record\"><span class=\"name hide\">" . $encrypted . "</span>");
                                                        if ($dont_hide_Edit_Buttons == true) {
                                                            echo ("      <button style=\"color:green\" type=\"button\" class=\"editIdButton btn btn-sm btn-success glyphicon glyphicon-new-window glyphicon-edit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit ID\"></button>");
                                                            echo ("      <button style=\"color:red\"  type=\"button\" class=\"deleteIdButton btn btn-sm btn-danger glyphicon glyphicon-trash\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete ID\"></button>");
                                                            echo ("      <button style=\"color:black\" type=\"button\" class=\"qrIdButton btn btn-sm btn-default glyphicon glyphicon-qrcode\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Print QR Code Sheet\"></button>");
                                                        }
                                                        echo ("    </div>");
                                                        echo ("  </td>");
                                                        echo ("</tr>");
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <div class="alert alert-danger hide" role="alert" id="id-deleted">ID Card Deleted.</div>
                                    <br>
                                    <button type="button" id="btn_add_id_number" class="btn btn-default btn-md">Add ID Card</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		

        <!-- Insurance Companies -->
        <section>
            <div id="insurance_companies"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel-group accordion_insurance_companies">
                        <div class="panel panel-success" style="border-color: black;">
                            <div class="panel-heading text-center">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion_insurance_companies" href="#collapseInsuranceCompanies">
                                        <span style="font-size: large"><i class="icon-book" style="color: #3C763D"></i>&nbsp;&nbsp;<u><strong>Insurance Companies</strong></u></span>
                                    </a>
                                    <h5>Tap "Add Company" to add or tap an icon to edit or delete.</h5>
                                </div>
                            </div>
                            <div id="collapseInsuranceCompanies" class="panel-collapse in">
                                <div class="panel-body" id="insurance_companies_panel_body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-striped center" style="width: 85%;" id="insurance_company_table">
                                                <thead>
                                                    <tr class="text-left input-lg">
                                                        <th style="text-align: left">Name</th>
                                                        <th style="text-align: left">Phone</th>
														<th style="text-align: left" class="hidden-xs">Date Updated</th>														
                                                        <th style="text-align: left" class="hidden-xs">Ins Card</th>
                                                        <th style="text-align: left">&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    $recordset = $database->select("user_ins_companies", [
													    "CompanyId",
                                                        "CompanyName",
                                                        "PhoneNumber",
                                                        "EditDate"
                                                    ], [
                                                        "uid" => $uid,
                                                         "ORDER" => ['CompanyName']
                                                    ]);

                                                    foreach($recordset as $data)
                                                    {
														$CompanyId = $data["CompanyId"];
                                                        $CompanyName = $data["CompanyName"];
                                                        $PhoneNumber = $data["PhoneNumber"];

                                                        //date_default_timezone_set('Africa/Nairobi');
                                                        // Then call the date functions
                                                        //$date = date('Y-m-d H:i:s');
														$EditDate = new DateTime($data["EditDate"]);
														
                                                        echo ("<tr class=\"text-left input-lg\">");
                                                        echo ("  <td>" . $CompanyName . "</td>");
                                                        echo ("  <td>" . $PhoneNumber . "</td>");
                                                        echo ("  <td class=\"hidden-xs\">" . $EditDate->format('m/d/y h:i') . "</td>");
                                                        echo ("  <td style=\"text-align:left;padding:10px\">");
														
														$encrypted = Urlcrypt::encrypt($CompanyId . "|" . "edit". "|" . time());
                                                        
                                                        echo ("    <div class=\"ins_record\"><span class=\"name hide\">" . $encrypted . "</span>");
                                                        if ($dont_hide_Edit_Buttons == true) {
                                                            echo ("      <button style=\"color:green\" type=\"button\" class=\"editInsButton btn btn-sm btn-success glyphicon glyphicon-new-window glyphicon-edit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit Company\"></button>");
                                                            echo ("      <button style=\"color:red\"  type=\"button\" class=\"deleteInsButton btn btn-sm btn-danger glyphicon glyphicon-trash\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Company\"></button>");
                                                            echo ("      <button style=\"color:#C17714\" type=\"button\" class=\"cardInsButton btn btn-sm btn-primary glyphicon glyphicon-credit-card\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Display Card\"></button>");
                                                        }
                                                        echo ("    </div>");
                                                        echo ("  </td>");
                                                        echo ("</tr>");
                                                    }   

                                                    ?>
													
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <div class="alert alert-danger hide" role="alert" id="contact-deleted">Company Deleted.</div>
                                    <br>
                                    <button type="button" id="btn_add_insurance_company" class="btn btn-success btn-md">Add Company</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Emergency Contacts -->
        <section>
            <div id="emergency_contacts"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel-group accordion_emergency_contacts">
                        <div class="panel panel-danger" style="border-color: black;">
                            <div class="panel-heading text-center">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion_emergency_contacts" href="#collapseEmergencyContacts">
                                        <span style="font-size: large"><i class="icon-phone" style="color: #A94442"></i>&nbsp;&nbsp;<u><strong>Emergency Contacts</strong></u></span>

                                        <h5>Tap "Add Contact" to add or tap an icon to edit or delete.</h5>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseEmergencyContacts" class="panel-collapse in">
                                <div class="panel-body" id="emergency_contact_panel_body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-striped center" style="width: 85%;" id="emergency_contact_table">
                                                <thead>
                                                    <tr class="text-left input-lg">
                                                        <th style="text-align: left">Name</th>
                                                        <th style="text-align: left">Phone</th>
                                                        <th style="text-align: left"  class="hidden-xs">Email</th>
                                                        <th style="text-align: left" class="hidden-xs">Date Updated</th>
                                                        <th style="text-align: left">&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    $recordset = $database->select("user_contacts", [
													    "ContactId",
                                                        "FullName",
                                                        "Email",
                                                        "MobileNumber",
                                                        "EditDate"
                                                    ], [
                                                        "uid" => $uid,
                                                         "ORDER" => ['FullName']
                                                    ]);

                                                    foreach($recordset as $data)
                                                    {
														$ContactId = $data["ContactId"];
                                                        $FullName = $data["FullName"];
                                                        $MobileNumber = $data["MobileNumber"];
                                                        $Email = $data["Email"];

                                                        //date_default_timezone_set('Africa/Nairobi');
                                                        // Then call the date functions
                                                        //$date = date('Y-m-d H:i:s');
														$EditDate = new DateTime($data["EditDate"]);
														
                                                        echo ("<tr class=\"text-left input-lg\">");
                                                        echo ("  <td>" . $FullName . "</td>");
                                                        echo ("  <td>" . $MobileNumber . "</td>");
                                                        echo ("  <td class=\"hidden-xs\">" . $Email . "</td>");
                                                        echo ("  <td class=\"hidden-xs\">" . $EditDate->format('m/d/y h:i') . "</td>");
                                                        echo ("  <td style=\"text-align:left;padding:10px\">");
														
														$encrypted = Urlcrypt::encrypt($ContactId . "|" . "edit". "|" . time());
                                                        
                                                        echo ("    <div class=\"contact_record\"><span class=\"name hide\">" . $encrypted . "</span>");
                                                        if ($dont_hide_Edit_Buttons == true) {
                                                            echo ("      <button style=\"color:green\" type=\"button\" class=\"editContactButton btn btn-sm btn-success glyphicon glyphicon-new-window glyphicon-edit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit Contact\"></button>");
                                                            echo ("      <button style=\"color:red\"  type=\"button\" class=\"deleteContactButton btn btn-sm btn-danger glyphicon glyphicon-trash\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Contact\"></button>");
                                                        }
                                                        echo ("    </div>");
                                                        echo ("  </td>");
                                                        echo ("</tr>");
                                                    }   
                                                    ?>
													
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <div class="alert alert-danger hide" role="alert" id="contact-deleted">Contact Deleted.</div>
                                    <br>
                                    <button type="button" id="btn_add_contact" class="btn btn-danger btn-md">Add Contact</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <!-- no signature block for family acct -->
    <div class="container <?php if ($subType == "F") { echo "hide"; } ?>" >
        <section>
            <div id="signatures"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary text-center" style="border-color:black;">
                        <div class="panel-heading">
                            <h3 class="title"><u>Consent For Emergency Treatment - Signatures</u></h3>
                        </div>
                        <div class="panel-body">
                            <form id="signatureform" class="signatureform">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="well-sm">
                                            <h3><?php echo $consent_statement ?></h3>
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="col-xs-2 pull-left hidden-xs" for="sign-date-picker">Date</label>
                                        <div class="col-xs-10">
                                            <input type="text" class="form-control" id="sign-date-picker" name="sign-date-picker" placeholder="mm/dd/yyyy" value="<?php echo $sign_date ?>" />
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="col-xs-2 pull-left hidden-xs" for="signature">Signed</label>
                                        <div class="col-xs-10">
                                            <img style="border:dotted;border-width:thin;border-color:black;height:150px;width:100%" id="signature-img" src="<?php echo $parent_signature ?>" />
                                            <canvas class="hide" id="signature" style="height: 150px; border-width: 1px; border-style: dotted; width: 100%" />
                                            <input type="hidden" id="parent_signature_post_field" name="parent_signature_post_field" value="<?php echo "" ?>" />
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="col-xs-2 pull-left hidden-xs" for="signature1">Witness</label>
                                        <div class="col-xs-10">
                                            <img style="border:dotted;border-width:thin;border-color:black;height:150px;width:100%" id="signature1-img" src="<?php echo $witness_signature ?>" />
                                            <canvas class="hide" id="signature1" style="height: 150px; border-width: 1px; border-style: dotted; width: 100%" />
                                            <input type="hidden" id="witness_signature_post_field" name="witness_signature_post_field" value="<?php echo "" ?>" />

                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <input type="hidden" id="userid_signature" name="userid_signature" value="<?php echo $userid ?>" />
                                </div>
                                <br />
                                <div class="btn-toolbar btn-group-md">
                                    <?php if ($dont_hide_Edit_Buttons == true) { ?>
                                    <button type="button" id="btn_edit_signature" class="btn btn-info">Edit Signatures</button>
                                    <?php }?>
                                    <button type="button" id="btn_cancel_signature" class="btn hide btn-danger">Cancel</button>
                                    <button type="button" id="btn_save_signature" class="btn hide btn-success">Save</button>
                                </div>
                            </form>
                            <div>
                                <div class="alert alert-success hide small" role="alert" id="signature-success">Signatures Saved Successfully.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <br />
    <div class="text-center">
        <a href="Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button"><i class="icon-home" style="color: #ffffff;"></i> Return Home</a>
    </div>
	
	
    <div id="addContact" class="modal fade" role="dialog">
        <form id="add_contact_form" class="add_contact_form">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="ch_id_modal_title"><i style="color:#FFF;" class="shortcut-icon icon-user-md"></i>&nbsp;&nbsp;Add Contact</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="form-group  col-xs-12">
                                <!-- Name -->
                                <label for="contactName" class="control-label">Name</label>
                                <input type="text" class="form-control" id="contactName" name="contactName" style="margin-bottom:3px" placeholder="contacts full name" autofocus="autofocus">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group  col-xs-12">
                                <!-- Address -->
                                <label for="contactAddress" class="control-label">Address</label>
                                <input type="text" class="form-control" id="contactAddress" name="contactAddress" placeholder="Street address, P.O. box, company name, c/o">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group  col-xs-12">
                                <!-- City-->
                                <label for="contactCity" class="control-label">City</label>
                                <input type="text" class="form-control" id="contactCity" name="contactCity">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6">
                                <!-- State Button -->
                                <label for="contactState" class="control-label">State</label>
                                <select class="form-control" id="contactState" name="contactState" placeholder="Select state">
                                    <option value="">Select a state</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-12 col-sm-6">
                                <!-- Zip Code-->
                                <label for="contactZip" class="control-label">Zip Code</label>
                                <input type="tel" class="form-control" id="contactZip" name="contactZip">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6">
                                <!-- Phone Number -->
                                <label for="contactPhone" class="control-label">Phone #</label>
                                <input type="tel" class="form-control" id="contactPhone" name="contactPhone" placeholder="">
                            </div>

                            <div class="form-group col-xs-12 col-sm-6">
                                <!-- Email -->
                                <label for="contactEmail" class="control-label">Email</label>
                                <input type="text" class="form-control" id="contactEmail" name="contactEmail">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success" id="btn_save">Save</button>
                        <div>
                            <div class="alert alert-success hide text-center" role="alert" id="id-success">Contact added successfully.</div>
                            <div class="alert alert-danger hide text-center" role="alert" id="id-number-required">A name is required.</div>
                            <div class="alert alert-danger hide text-center" role="alert" id="id-description-required">A description is required.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <input type="hidden" id="id_uid" name="id_uid" value="<?php echo $uid ?>" />
            </div>
        </form>
    </div>	
	
   
    <br />
    <br />
    <!-- Footer -->
    <?php  include("include/incFooter-sm.php"); ?>


    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/jquery.form.js"></script>
    <script src="Scripts/modernizr.custom.34982.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap-datepicker.min.js"></script>
    <script src="Scripts/signatureCapture.js"></script>
    <script src="Scripts/jquery.maskedinput.min.js"></script>
    <script src="Scripts/bootstrap-dialog.min.js"></script>
    <script src="Scripts/sweetalert2.min.js"></script>	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>

<script type="text/javascript">

    function afterSuccess() {
        //$('#submit-btn').show(); //hide submit button
        //$('#loading-img').hide(); //hide submit button
        location.reload();
    }

    //function to check file size before uploading.
    function beforeSubmit() {

        //alert("before submit");
        //check whether browser fully supports all File API
        if (window.File && window.FileReader && window.FileList && window.Blob) {

            if (!$('#photoimg').val()) //check empty input filed
            {
                $("#preview").html("Please select a file to upload.");
                return false
            }

            var fsize = $('#photoimg')[0].files[0].size; //get file size
            var ftype = $('#photoimg')[0].files[0].type; // get file type


            //allow only valid image file types 
            switch (ftype) {
                case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/jpg':
                    break;
                default:
                    $("#preview").html("<b>" + ftype + "</b> is an Unsupported file type.<br>Please select an image to upload (jpg/png).");
                    return false
            }

            //Allowed file size is less than 5 MB
            if (fsize > 5048576) {
                $("#preview").html("<b>" + bytesToSize(fsize) + "</b> The Image file is too big! <br />Please reduce the size of your photo using an image editor.");
                return false
            }
            //$('#submit-btn').hide(); //hide submit button
            //$('#loading-img').show(); //hide submit button
            //$("#preview").html("");
        }
        else {
            //Output error to older browsers that do not support HTML5 File API
            $("#preview").html("Please upgrade your browser, because your current browser does not support the features we need.");
            return false;
        }
    }

    //function to format bites bit.ly/19yoIPO
    function bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Bytes';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }
    </script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {


            $("#parent_day_phone").mask("(999) 999-9999");
            $("#parent_night_phone").mask("(999) 999-9999");
            $("#doctor_day_phone").mask("(999) 999-9999");
            $("#doctor_night_phone").mask("(999) 999-9999");
            $("#relative_phone").mask("(999) 999-9999");

            $("#dob-picker").mask("99/99/9999");
            $("#effective-date-picker").mask("99/99/9999");
            $("#sign-date-picker").mask("99/99/9999");

            //$('#dob-picker').datepicker({
            //    format: "mm/dd/yyyy",
            //    autoclose: true
            //});

            /* $('#effective-date-picker').datepicker({
                format: "mm/dd/yyyy",
                autoclose: true
            }); */


            /* $('#sign-date-picker').datepicker({
                format: "mm/dd/yyyy",
                autoclose: true

            });*/

            var sigCapture = null;
            sigCapture = new SignatureCapture("signature");

            var sigCapture1 = null;
            sigCapture1 = new SignatureCapture("signature1");
			
            //####################################################### Add ID Number button  ##################################
            $('#btn_add_id_number').on('click', function (e) {
                e.preventDefault();


                window.location = "admin/id_cards_manage.php";

            })			

            //####################################################### Contact Add button  ##################################
            $('#btn_add_contact').on('click', function (e) {
               
                //alert($(this).attr('id'));
                //var user_id_to_change = $(this).attr('id');
                //$("#id_uid").val(user_id_to_change);
                //alert($("#id_uid").val());

                //alert($(this).attr('id'));
                //$("#id_number").val("");
                //$("#id_description").val("");

                //enable controls for the contact form
                //$('#addContact input').attr('readonly', false);
                //$('#addContact select').attr('disabled', false);
                //$('#addContact select').css({ 'background': '#FFFFFF' });

                //$('#addContact').modal('show');
                e.preventDefault();
                window.location = "admin/contacts_manage.php";

            })

            //################################################## Contact edit button ###############################################
            $('.editContactButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {

                    //find the contact record
                    record = $(this).parents('.contact_record');
                    var contact_item = record.find('.name').html();

                    //alert(contact_item);
                    $window_url = "admin/contacts_manage.php?id=" + contact_item;
                    //alert($window_url);
                    $(location).attr('href', $window_url);

                });


            //################################################## contact delete button ###############################################
            $('.deleteContactButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {
                    swal({
                      title: 'Delete Emergency Contact?',
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonColor: '#41A24D',
                      cancelButtonColor: '#D54F49',
                      confirmButtonText: 'Yes',
                      cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            record = $(this).parents('.contact_record');
                            var contact_item = record.find('.name').html();
                            //alert(ins_item);
							
						    var values = {
					 		  'itemno': contact_item,
							  //'id': document.getElementById('elem2').value
							};
							
							$.ajax({
								type: "POST",
								url: "ws/delete-contact.php",
								data: values,
								success: function (msg) {
									swal({
										title: "Deleted",
										text: "Emergency Contact has been deleted." + msg,
										type: "success"
									}).then(function() {
										location.reload();
									});
									
									//$('#demo-success').removeClass('hide');
									//$.wait(function () { $('#demo-success').addClass('hide') }, 2);

									/* BootstrapDialog.show({
										message: msg,
										buttons: [{
											label: 'Close',
											action: function(dialogItself){
												dialogItself.close();
											}
										}]
									});
									*/
									//alert(msg);
									//$('#sug').val(msg);
									//$("#thanks").html(msg)
									//$("#form-content").modal('hide');
									//window.location.href = "home.html";					
								},
								error: function () {
									alert("Error deleting company information");
								},
								complete: function () {
								}
							});							
							
                            //$('#med-deleted').removeClass('hide');
                      }
                    })
                });			


            //****************** profile picture button ***********************************
            $('#photoimg').on('change', function () {

                //alert("here");

                $("#preview").html('');
                $("#preview").html('<img src="img/loader.gif" alt="Uploading...."/>');
                $("#imageform").ajaxForm({
                    target: '#preview',   // target element(s) to be updated with server response 
                    beforeSubmit: beforeSubmit,  // pre-submit callback 
                    success: afterSuccess,  // post-submit callback 
                    resetForm: true        // reset the form after successful submit 
                }).submit();

            });

            //######################################################################### demographic buttons ############################################
            //####################################################### Demographic EDIT Button ##################################
            $('#btn_edit_demographic').on('click', function (e) {
                //alert("test");
                $(this).hide();
                $('#btn_save_demographic').removeClass('hide');
                $('#btn_cancel_demographic').removeClass('hide');

                //save their current values in case of cancel
                $('#demographicform input,select').each(
                    function () {
                        current_values[$(this).attr('name')] = $(this).val();
                    }
                );

                //enable controls for the demographic form
                $('#demographicform input').attr('readonly', false);
                $('#demographicform select').attr('disabled', false);
                $('#demographicform select').css({ 'background': '#FFF5EC' });

                //$('#myModal').modal('show');

            })
            //####################################################### Demographic SAVE Button ##################################
            $('#btn_save_demographic').on('click', function (e) {

                //alert('saving');

                //setTimeout('', 1000);
                //post the values to the server
                var txtVal = $('#dob-picker').val();
                if (!isDate(txtVal)) {
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Invalid Date',
                        message: 'Please enter a valid date for date of birth',
                        buttons: [{
                            label: 'Close',
                            action: function (dialogItself) {
                                dialogItself.close();
                            }
                        }]
                    });
                    //alert('The date of birth is not valid.');
                    return;
                }


                $.ajax({
                    type: "POST",
                    url: "ws/process-demographic.php",
                    data: $('form.demographicform').serialize(),
                    success: function (msg) {
                        $('#demo-success').removeClass('hide');
                        $.wait(function () { $('#demo-success').addClass('hide') }, 2);

                        /* BootstrapDialog.show({
                            message: msg,
                            buttons: [{
                                label: 'Close',
                                action: function(dialogItself){
                                    dialogItself.close();
                                }
                            }]
                        });
                        */
                        //alert(msg);
                        //$('#sug').val(msg);
                        //$("#thanks").html(msg)
                        //$("#form-content").modal('hide');
                        //window.location.href = "home.html";					
                    },
                    error: function () {
                        alert("Error saving demographic information");
                    },
                    complete: function () {
                        $.unblockUI();
                    }

                });

                //hide / show buttons
                $('#btn_save_demographic').addClass('hide');
                $('#btn_cancel_demographic').addClass('hide');
                $('#btn_edit_demographic').show();

                //disable controls for the demographic form
                $('#demographicform input').attr('readonly', true);
                $('#demographicform select').attr('disabled', true);
                $('#demographicform select').css({ 'background': '#EEEEEE' });
            })
            //####################################################### Demographic CANCEL Button ##################################
            $('#btn_cancel_demographic').on('click', function (e) {
                //revert the values
                $('#demographicform input,select').each(
                   function () {
                       $(this).val(current_values[$(this).attr('name')]);
                       //alert(current_values[$(this).attr('name')]);
                   }
                 );

                //hide / show buttons
                $('#btn_save_demographic').addClass('hide');
                $('#btn_cancel_demographic').addClass('hide');
                $('#btn_edit_demographic').show();

                //disable controls for the demographic form
                $('#demographicform input').attr('readonly', true);
                $('#demographicform select').attr('disabled', true);
                $('#demographicform select').css({ 'background': '#EEEEEE' });
            })


            //######################################################################### Insurance buttons ############################################
            //####################################################### Insurance EDIT Button ##################################
            $('#btn_edit_ins').on('click', function (e) {
                $(this).hide();
                $('#btn_save_ins').removeClass('hide');
                $('#btn_cancel_ins').removeClass('hide');

                //save their current values in case of cancel
                $('#insuranceform input,select').each(
                    function () {
                        current_values[$(this).attr('name')] = $(this).val();
                    }
                );

                //enable controls for the ins form
                $('#insuranceform input').attr('readonly', false);
                $('#insuranceform select').attr('disabled', false);
                $('#insuranceform select').css({ 'background': '#FFF5EC' });

            })
            //####################################################### Insurance SAVE Button ##################################
            $('#btn_save_ins').on('click', function (e) {

                var txtVal = $('#effective-date-picker').val();
                if (!isDate(txtVal)) {
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Invalid Date',
                        message: 'Please enter a valid effective date',
                        buttons: [{
                            label: 'Close',
                            action: function (dialogItself) {
                                dialogItself.close();
                            }
                        }]
                    });
                    //alert('The effective date is not valid.');
                    return;
                }

                //post the values to the server
                $.ajax({
                    type: "POST",
                    url: "ws/process-insurance.php",
                    data: $('form.insuranceform').serialize(),
                    success: function (msg) {
                        $('#ins-success').removeClass('hide');
                        $.wait(function () { $('#ins-success').addClass('hide') }, 2);
                        //alert(msg);
                    },
                    error: function () {
                        alert("Error saving insurance information");
                    }
                });

                //hide / show buttons
                $('#btn_save_ins').addClass('hide');
                $('#btn_cancel_ins').addClass('hide');
                $('#btn_edit_ins').show();

                //disable controls for the ins form
                $('#insuranceform input').attr('readonly', true);
                $('#insuranceform select').attr('disabled', true);
            })
            //####################################################### Insurance Cancel Button ##################################
            $('#btn_cancel_ins').on('click', function (e) {
                //revert the values
                $('#insuranceform input,select').each(
                   function () {
                       $(this).val(current_values[$(this).attr('name')]);
                   }
                 );

                //hide / show buttons
                $('#btn_save_ins').addClass('hide');
                $('#btn_cancel_ins').addClass('hide');
                $('#btn_edit_ins').show();

                //disable controls for the ins form
                $('#insuranceform input').attr('readonly', true);
                $('#insuranceform select').attr('disabled', true);
                $('#insuranceform select').css({ 'background': '#EEEEEE' });
            })



            //######################################################################### Contact buttons ############################################
            //####################################################### Contact EDIT Button ##################################
            $('#btn_edit_contact').on('click', function (e) {
                $(this).hide();
                $('#btn_save_contact').removeClass('hide');
                $('#btn_cancel_contact').removeClass('hide');

                //save their current values in case of cancel
                $('#contactform input,select').each(
                    function () {
                        current_values[$(this).attr('name')] = $(this).val();
                    }
                );

                //disable controls for the contact form
                //$('#contactform input').attr('readonly', false);
                //$('#contactform select').attr('disabled', false);

            })
            //####################################################### Contact SAVE Button ##################################
            $('#btn_save_contact').on('click', function (e) {

                //post the values to the server
                $.ajax({
                    type: "POST",
                    url: "ws/process-contact.php",
                    data: $('form.contactform').serialize(),
                    success: function (msg) {
                        //alert(msg);
                        $('#contact-success').removeClass('hide');
                        //$('#contact-success').html(msg);
                        $.wait(function () { $('#contact-success').addClass('hide') }, 2);
                    },
                    error: function () {
                        alert("Error saving contact information");
                    }
                });

                //hide / show buttons
                $('#btn_save_contact').addClass('hide');
                $('#btn_cancel_contact').addClass('hide');
                $('#btn_edit_contact').show();

                //disable controls for the ins form
                //$('#contactform input').attr('readonly', true);
                //$('#contactform select').attr('disabled', true);
                //$('#contactform select').css({ 'background': '#EEEEEE' });
            })
            //####################################################### Contact Cancel Button ##################################
            $('#btn_cancel_contact').on('click', function (e) {
                //revert the values
                $('#contactform input,select').each(
                   function () {
                       $(this).val(current_values[$(this).attr('name')]);
                   }
                 );

                //hide / show buttons
                $('#btn_save_contact').addClass('hide');
                $('#btn_cancel_contact').addClass('hide');
                $('#btn_edit_contact').show();

                //disable controls for the ins form
                //$('#contactform input').attr('readonly', true);
                //$('#contactform select').attr('disabled', true);
                //$('#contactform select').css({ 'background': '#EEEEEE' });
            })


            //######################################################################### Signature buttons ############################################
            //####################################################### Signature EDIT Button ##################################
            $('#btn_edit_signature').on('click', function (e) {
                $(this).hide();
                $('#btn_save_signature').removeClass('hide');
                $('#btn_cancel_signature').removeClass('hide');

                //enable controls for the ins form
                $('#signatureform input').attr('readonly', false);
                $('#signatureform select').attr('disabled', false);

                $('#signature').removeClass('hide');
                $('#signature1').removeClass('hide');

                $('#signature-img').addClass('hide');
                $('#signature1-img').addClass('hide');

                sigCapture.clear();
                sigCapture1.clear();

            })

            //####################################################### Signature SAVE Button ##################################
            $('#btn_save_signature').on('click', function (e) {

                if ($('#sign-date-picker').val() == '') {
                    //alert('Please enter a signature date and time.');

                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: 'Missing Signature Date',
                        message: 'Please enter a Signature Date',
                        buttons: [{
                            label: 'Close',
                            action: function (dialogItself) {
                                dialogItself.close();
                            }
                        }]
                    });
                    return;
                    //$(this).parents('p').addClass('warning');
                }

                document.getElementById('parent_signature_post_field').value = signature.toDataURL('image/png');
                document.getElementById('witness_signature_post_field').value = signature1.toDataURL('image/png');


                $('#signature-img').attr('src', '');
                $('#signature-img').attr('src', signature.toDataURL('image/png'));

                $('#signature1-img').attr('src', '');
                $('#signature1-img').attr('src', signature1.toDataURL('image/png'));

                //post the values to the server
                $.ajax({
                    type: "POST",
                    url: "ws/process-signature.php",
                    data: $('form.signatureform').serialize(),
                    success: function (msg) {
                        //$('#sql_stmt').val(msg);
                        $('#signature-success').removeClass('hide');
                        $.wait(function () { $('#signature-success').addClass('hide') }, 2);
                        //alert(msg);
                        //disable controls for the sign form
                        $('#signatureform input').attr('readonly', true);
                        $('#signatureform select').attr('disabled', true);
                    },
                    error: function () {
                        alert("Error saving signature");
                    }
                });

                //hide / show buttons
                $('#btn_save_signature').addClass('hide');
                $('#btn_cancel_signature').addClass('hide');
                $('#btn_edit_signature').show();

                //disable controls for the ins form
                $('#signatureform input').attr('readonly', true);
                $('#signatureform select').attr('disabled', true);

                $('#signature').addClass('hide');
                $('#signature1').addClass('hide');

                $('#signature-img').removeClass('hide');
                $('#signature1-img').removeClass('hide');

            })
			
            //####################################################### Add Insurance Company #################################
            $('#btn_add_insurance_company').on('click', function (e) {

                e.preventDefault();
                //alert("test");
                window.location = "admin/ins_company_manage.php";


            })			

            //####################################################### Signature CANCEL Button #################################
            $('#btn_cancel_signature').on('click', function (e) {

                //hide / show buttons
                $('#btn_save_signature').addClass('hide');
                $('#btn_cancel_signature').addClass('hide');
                $('#btn_edit_signature').show();

                //disable controls for the ins form
                $('#signatureform input').attr('readonly', true);
                $('#signatureform select').attr('disabled', true);

                $('#signature').addClass('hide');
                $('#signature1').addClass('hide');

                $('#signature-img').removeClass('hide');
                $('#signature1-img').removeClass('hide');

            })
            //#################################################################################################################

            $('#frontphotoimg').on('change', function () {
                $("#preview-front").html('');
                $("#preview-front").html('<img src="img/loader.gif" alt="Uploading...."/>');
                //alert("submitting");
                $("#frontimageform").ajaxForm({
                    target: '#preview-front',   // target element(s) to be updated with server response 
                    beforeSubmit: beforeSubmitfront,  // pre-submit callback 
                    success: afterSuccessfront,  // post-submit callback 
                    resetForm: true        // reset the form after successful submit 
                }).submit();

            });
            //#################################################################################################################
            $('#backphotoimg').on('change', function () {
                $("#preview-back").html('');
                $("#preview-back").html('<img src="img/loader.gif" alt="Uploading...."/>');
                //alert("submitting");
                $("#backimageform").ajaxForm({
                    target: '#preview-back',   // target element(s) to be updated with server response 
                    beforeSubmit: beforeSubmitback,  // pre-submit callback 
                    success: afterSuccessback,  // post-submit callback 
                    resetForm: true        // reset the form after successful submit 
                }).submit();

            });
            //#################################################################################################################

            //##### Initialize when PAGE LOADS #################################
            //initially set all input controls to  and select to disabled
            $('input').attr('readonly', true);
            $('select').attr('disabled', true);

            $('#frontphotoimg').attr('readonly', false);
            $('#backphotoimg').attr('readonly', false);
            $('#photoimg').attr('readonly', false);

            var current_values = []; //used to store all the current values of controls in a form when in edit mode so can revert on cancel

            $.wait = function (callback, seconds) {
                return window.setTimeout(callback, seconds * 1000);
            }

            function isDate(txtDate) {
                var currVal = txtDate;
                if (currVal == '')
                    return true;

                //Declare Regex 
                var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
                var dtArray = currVal.match(rxDatePattern); // is format OK?

                if (dtArray == null)
                    return false;

                //Checks for mm/dd/yyyy format.
                dtMonth = dtArray[1];
                dtDay = dtArray[3];
                dtYear = dtArray[5];

                if (dtMonth < 1 || dtMonth > 12)
                    return false;
                else if (dtDay < 1 || dtDay > 31)
                    return false;
                else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)

                    return false;
                else if (dtMonth == 2) {
                    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                    if (dtDay > 29 || (dtDay == 29 && !isleap))

                        return false;
                }
                return true;
            }

            function afterSuccessfront() {
                //$('#loading-img').hide(); //hide submit button
                location.reload();
            }

            //function to check file size before uploading.
            function beforeSubmitfront() {

                //check whether browser fully supports all File API
                if (window.File && window.FileReader && window.FileList && window.Blob) {

                    if (!$('#frontphotoimg').val()) //check empty input filed
                    {
                        $("#preview-front").html("Please select a file to upload.");
                        return false
                    }

                    //alert("here");
                    var fsize = $('#frontphotoimg')[0].files[0].size; //get file size
                    var ftype = $('#frontphotoimg')[0].files[0].type; // get file type

                    //allow only valid image file types 
                    switch (ftype) {
                        case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/jpg':
                            break;
                        default:
                            $("#preview-front").html("<b>" + ftype + "</b> is an Unsupported file type.<br>Please select an image to upload (jpg/gif/png).");
                            return false
                    }

                    //Allowed file size is less than 1 MB (1048576)
                    if (fsize > 5048576) {
                        $("#preview-front").html("<b>" + bytesToSize(fsize) + "</b> The Image file is too big! <br />Please reduce the size of your photo using an image editor.");
                        return false
                    }
                    //$('#submit-btn').hide(); //hide submit button
                    //$('#loading-img').show(); //hide submit button
                    //$("#preview-front").html("");
                }
                else {
                    //Output error to older browsers that do not support HTML5 File API
                    $("#preview-front").html("Please upgrade your browser, because your current browser does not support the features we need.");
                    return false;
                }
            }

            function afterSuccessback() {
                //$('#loading-img').hide(); //hide submit button
                location.reload();
            }

            //function to check file size before uploading.
            function beforeSubmitback() {

                //check whether browser fully supports all File API
                if (window.File && window.FileReader && window.FileList && window.Blob) {

                    if (!$('#backphotoimg').val()) //check empty input filed
                    {
                        $("#preview-back").html("Please select a file to upload.");
                        return false
                    }

                    //alert("here");
                    var fsize = $('#backphotoimg')[0].files[0].size; //get file size
                    var ftype = $('#backphotoimg')[0].files[0].type; // get file type

                    //allow only valid image file types 
                    switch (ftype) {
                        case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/jpg':
                            break;
                        default:
                            $("#preview-back").html("<b>" + ftype + "</b> is an Unsupported file type.<br>Please select an image to upload (jpg/gif/png).");
                            return false
                    }

                    //Allowed file size is less than 1 MB (1048576)
                    if (fsize > 5048576) {
                        $("#preview-back").html("<b>" + bytesToSize(fsize) + "</b> The Image file is too big! <br />Please reduce the size of your photo using an image editor.");
                        return false
                    }
                    //$('#submit-btn').hide(); //hide submit button
                    //$('#loading-img').show(); //hide submit button
                    //$("#preview-back").html("");
                }
                else {
                    //Output error to older browsers that do not support HTML5 File API
                    $("#preview-back").html("Please upgrade your browser, because your current browser does not support the features we need.");
                    return false;
                }
            }

            //function to format bites bit.ly/19yoIPO
            function bytesToSize(bytes) {
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return '0 Bytes';
                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            }
			
            //################################################## insurance edit button ###############################################
            $('.editInsButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {

                    //set which record we're editing so we can update it later
                    record = $(this).parents('.ins_record');
                    //populate the editing form within the dialog
                    ////$('#myInput').val(record.find('.name').html());
                    var ins_item = record.find('.name').html();
					
                    //alert(ins_item);
                    $window_url = "admin/ins_company_manage.php?id=" + ins_item;
                    //alert($window_url);
					$(location).attr('href',$window_url);
                    //show the dialog
                    //$("#dialogContent").dialog("open")

                    /*   $.getJSON('ws/get-medication.php?itemno=' + med_item, function (data) {
                        //alert(data); //uncomment this for debug
                        //alert (data.name+" "+data.dosage+" "+data.howoften); //further debug
                        //$('#showdata').html("<p>item1=" + data.item1 + " item2=" + data.item2 + " item3=" + data.item3 + "</p>");
                        //look up the record values, using the record number
                        $('#addMedication').modal('show');
                        $('#medication_modal_title').text("Edit Medication");
                        $('#mode').val("edit");
                        $('#itemno').val(med_item);
                        $("#med_name").val(data.name);
                        $("#dosage").val(data.dosage);
                        $("#how_often").val(data.howoften);
                    }); */

                });


            //################################################## insurance delete button ###############################################
            $('.deleteInsButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {
                    swal({
                      title: 'Delete Insurance Company?',
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonColor: '#41A24D',
                      cancelButtonColor: '#D54F49',
                      confirmButtonText: 'Yes',
                      cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            record = $(this).parents('.ins_record');
                            var ins_item = record.find('.name').html();
                            //alert(ins_item);
							
						    var values = {
					 		  'itemno': ins_item,
							  //'id': document.getElementById('elem2').value
							};
							
							$.ajax({
								type: "POST",
								url: "ws/delete-ins-company.php",
								data: values,
								success: function (msg) {
									swal({
										title: "Deleted",
										text: "Insurance Company has been deleted." + msg,
										type: "success"
									}).then(function() {
										location.reload();
									});
									
									//$('#demo-success').removeClass('hide');
									//$.wait(function () { $('#demo-success').addClass('hide') }, 2);

									/* BootstrapDialog.show({
										message: msg,
										buttons: [{
											label: 'Close',
											action: function(dialogItself){
												dialogItself.close();
											}
										}]
									});
									*/
									//alert(msg);
									//$('#sug').val(msg);
									//$("#thanks").html(msg)
									//$("#form-content").modal('hide');
									//window.location.href = "home.html";					
								},
								error: function () {
									alert("Error deleting company information");
								},
								complete: function () {
								}
							});							
							
                            //$('#med-deleted').removeClass('hide');
                      }
                    })
                });

            //################################################## insurance show card button ###############################################
            $('.cardInsButton')
                .click(function () {

                    //set which record we're editing so we can update it later
                    record = $(this).parents('.ins_record');
                    var ins_item = record.find('.name').html();

                    swal(
                      'Ins Card Display',
                      'Coming soon...',
                      'info'
                    )


                });

            //################################################## id edit button ###############################################
            $('.editIdButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {

                    //set which record we're editing
                    record = $(this).parents('.id_record');
                    var id_item = record.find('.name').html();
					
                    //alert(ins_item);
					$window_url = "admin/id_cards_manage.php?id=" + id_item;
                    //alert($window_url);
					$(location).attr('href',$window_url);


                });

            //################################################## id delete button ###############################################
            $('.deleteIdButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {
                    swal({
                      title: 'Delete ID Card?',
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonColor: '#41A24D',
                      cancelButtonColor: '#D54F49',
                      confirmButtonText: 'Yes',
                      cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            record = $(this).parents('.id_record');
                            var id_item = record.find('.name').html();
                            //alert(ins_item);
							
						    var values = {
					 		  'itemno': id_item,
							  //'id': document.getElementById('elem2').value
							};
							
							$.ajax({
								type: "POST",
								url: "ws/delete-id-card.php",
								data: values,
                                success: function (msg) {
									swal({
										title: "Deleted",
										text: "ID Card has been deleted." + msg,
										type: "success"
									}).then(function() {
										location.reload();
									});
								},
								error: function () {
									alert("Error id card information");
								},
								complete: function () {
								}
							});							
                      }
                    })
                });	


            //################################################## qr button ###############################################
            $('.qrIdButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {

                    record = $(this).parents('.id_record');
                    var id_item = record.find('.name').html();
                    //alert(ins_item);

                    window.open("https://emtelink.com/tracker/printqr.php?idnum=" + id_item);

                });	


        });



    </script>

</body>
</html>
