<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>EMTeLink Mobile</title>
    <meta name="viewport" content="width=device-width, initial-scale=0.5">
    <meta name="description" content="EMTeLink Traker App">

    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");


    if (isset($_GET['id'])) {
        $id_value = $_GET['id'];
        //echo $id_value . "<br />";
    } else{
        echo "report user id not found, processing cannot continue.";
        exit;
    }

    use Urlcrypt\Urlcrypt;
    require_once 'Urlcrypt.php';

    Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
    $decrypted = Urlcrypt::decrypt($id_value);


    $decrypted = Urlcrypt::decrypt($id_value);

    list($report_user_id, $starttime) = explode("|", $decrypted);

    $endtime = time();
    $timediff = $endtime - $starttime;

    function calculateAge($date){

        //return date_diff(date_create($date), date_create('today'))->y;
        $birthDate = explode("-", $date);
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
          ? ((date("Y") - $birthDate[2]) - 1)
          : (date("Y") - $birthDate[2]));
        return $age;


        //list($month,$day,$year) = explode("-",$date);
        //echo $year;
        //$year_diff  = date("Y") - $year;
        //$month_diff = date("m") - $month;
        //$day_diff   = date("d") - $day;
        //if ($day_diff < 0 || $month_diff < 0) $year_diff--;
        //return $year_diff;
    }

    //list($userid, $epoch_value) = split('|', $decrypted);
    //echo "userid: $userid; ev: $epoch_value;<br />";
    //echo $decrypted;

    //        list($report_user_id, $epoch_value) = explode("|", $decrypted);
    //echo $report_user_id;
    //echo $epoch_value;

    //session_start();

    //make sure we have a valid sesion
    //if ($_SESSION["valid"] != "TRUE")
    //{
    //    header("Location: index.html");
    //}; JBrinson




    //$report_user_id = 'dmassey';
    $userid = $report_user_id;

    ?>


    <title>Emergency Information Report</title>

    <style>
        .table_style {
            width: 98%;
            font-family: Calibri,sans-serif;
            font-size: 16.0pt;
            color: #000000;
            padding: 10px;
            border: solid;
            border-color: black;
            border-width: thin;
        }

        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid #595959;
        }
    </style>
</head>

<body>

    <?php
    $recordset = $database->select("user", [
        "subId",
        "firstname",
        "lastname",
        "middleinitial",
        "address",
        "city",
        "st",
        "zip",
        "dob",
        "phone",
        "student_email",
        "parent_email",
        "med_conditions",
        "hospitalizations",
        "serious_injuries",
        "special_diets",
        "add_info",
        "parent_name",
        "parent_addr",
        "parent_day_phone",
        "parent_night_phone",
        "relative_name",
        "relative_phone",
        "relationship",
        "family_doctor",
        "doctor_day_phone",
        "doctor_night_phone",
        "ins_company",
        "ins_phone",
        "groupno",
        "contractno",
        "effectivedate",
        "certification_no",
        "plan_with",
        "add_medical_conditions",
        "primary_name",
        "primary_addr",
        "primary_phone",
        "primary_carrierId",
        "primary_email",
        "primary_relationship",
        "secondary_name",
        "secondary_addr",
        "secondary_phone",
        "secondary_carrierId",
        "secondary_email",
        "secondary_relationship",
        "phy_name",
        "phy_addr",
        "phy_phone",
        "phy_email",
        "phy_type",
        "bloodtype"

    ], [
        "userid" => "$report_user_id"
    ]);

    foreach($recordset as $data)
    {
        $subId = $data["subId"];
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $middleinitial = $data["middleinitial"];
        $address = $data["address"];
        $blood_type = $data["bloodtype"];
        $city = $data["city"];
        $state = $data["st"];
        $zip = $data["zip"];
        //$dob = $data["dob"]->format('m-d-Y');
        //$dob = $data["dob"];
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data["dob"]);
        if(!$myDateTime == ''){
            $dob = $myDateTime->format('m-d-Y');
            $age = calculateAge($dob);
        }

        $phone = $data["phone"];
        $student_email = $data["student_email"];
        $parent_email = $data["parent_email"];

        $med_conditions = $data["med_conditions"];
        $hospitalizations = $data["hospitalizations"];
        $serious_injuries = $data["serious_injuries"];
        $special_diets = $data["special_diets"];
        $add_info = $data["add_info"];
        $parent_name = $data["parent_name"];
        $parent_addr = $data["parent_addr"];
        $parent_day_phone = $data["parent_day_phone"];
        $parent_night_phone = $data["parent_night_phone"];
        $relative_name = $data["relative_name"];
        $relative_phone = $data["relative_phone"];
        $relationship = $data["relationship"];
        $family_doctor = $data["family_doctor"];
        $doctor_day_phone = $data["doctor_day_phone"];
        $doctor_night_phone = $data["doctor_night_phone"];
        $ins_company = $data["ins_company"];
        $ins_phone = $data["ins_phone"];
        $groupno = $data["groupno"];
        $contractno = $data["contractno"];
        //$effectivedate = $data["effectivedate"]->format('m-d-Y');
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data["effectivedate"]);
        if(!$myDateTime == ''){
            $effectivedate = $myDateTime->format('m-d-Y');
        }
        //$effectivedate = $data["effectivedate"];
        $certification_no = $data["certification_no"];
        $plan_with = $data["plan_with"];
        $add_medical_conditions = $data["add_medical_conditions"];

        $primary_name = $data["primary_name"];
        $primary_addr = $data["primary_addr"];
        $primary_phone = $data["primary_phone"];
        $primary_email = $data["primary_email"];
        $primary_relationship = $data["primary_relationship"];
        $primary_carrierId = $data["primary_carrierId"];

        $secondary_name = $data["secondary_name"];
        $secondary_addr = $data["secondary_addr"];
        $secondary_phone = $data["secondary_phone"];
        $secondary_email = $data["secondary_email"];
        $secondary_relationship = $data["secondary_relationship"];
        $secondary_carrierId = $data["secondary_carrierId"];

        $phy_name = $data["phy_name"];
        $phy_addr = $data["phy_addr"];
        $phy_phone = $data["phy_phone"];
        $phy_email = $data["phy_email"];
        $phy_type = $data["phy_type"];

        $medcond=json_decode($med_conditions,true);
        //echo $subId;
    }

    $recordset = $database->select("Subscriptions", [
        "subType"
    ], [
        "subId" => "$subId"
    ]);

    foreach($recordset as $data)
    {
        $subType = $data["subType"];
    }

    ?>

    <div class="container">
        <center>
            <div col-xs-12>
                <row>
                    <div>
                        <button class="btn btn-lg btn-primary btn-block" style="width:200px;" type="button" onclick="javascript: window.location.assign('Index.html');">Back</button>
                    </div>
                    <br />
                    <table class="table table-bordered">
                        <tr>
                            <td colspan="3" style="text-align:center;">
                                <span style="font-family:Calibri,sans-serif;font-size: 22.0pt;color: #595959;">
                                    <strong>Emergency Information</strong>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align:center;">
                                <span style="font-family:Calibri,sans-serif;font-size: 24.0pt;color: red">
                                    <strong><?php echo $firstname . " " . $middleinitial . " " . $lastname; ?></strong>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align:center;">
                                <?php include("include/incProfilepic.php"); ?>
                            </td>
                        </tr>
                    </table>
                    <table class="table table_style" style="text-align:center;font-size:large">
                        <tr><th style="width:40%;background:#E7E5E5">DOB</th><td colspan="2"><?php echo $dob; ?></td></tr>
                        <tr><th style="width:40%;background:#E7E5E5">Age</th><td colspan="2"><?php echo $age . " years old"; ?></td></tr>
                        <tr><th style="width:40%;background:#E7E5E5">Address</th><td colspan="2"><?php echo $address."<br />".$city." ".$state." ".$zip; ?></td></tr>
                        <tr><th style="width:40%;background:#E7E5E5">Blood Type</th><td colspan="2"><?php echo $blood_type; ?></td></tr>
                    </table>

                    <?php
                    $bolAtLeastOneCondition = "false";
                    //echo count($medcond);
                    for($i = 0, $size = count($medcond); $i < $size; ++$i) {
                        if ($medcond[$i]['checked']==1) {
                            $bolAtLeastOneCondition = "true";
                            //echo "cond - true<br />";
                        }
                        else {
                            //echo "cond - false<br />";
                        }
                    }
                   //echo "bolAtLeastOneCondition = " . $bolAtLeastOneCondition;
                    ?>


                            <table class="MsoTableGrid" border="1"
                                style='border-collapse: collapse; border: none; width:500pt;'>
                                <tr>
                                    <td colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #9CC2E5; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <b>
                                                <span
                                                    style='font-size: 18.0pt'>
                                                    Medical Conditions <?php if ($bolAtLeastOneCondition == "false") { echo "- none";} ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>

                                <?php
        if ($subType != "F") {
            if($medcond[0]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Mumps</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[1]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Measles</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[2]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Chicken Pox</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[3]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Pneumonia</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[4]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Diabetes</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[5]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Cancer</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[6]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Malaria</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[7]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Seizures</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[8]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Heart Disease</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[9]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Kidney Disease</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[10]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Tuberculosis</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[11]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Rheumatic Fever</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[12]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Hepatitis</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[13]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Blood Pressure Problems</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[14]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Tetanus Toxoid Shot (in last 12 months)</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php }

        }   //subType != F
        else {
            if($medcond[0]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Asthma</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[1]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>COPD</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[2]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Seizure Disorder</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[3]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Dementia</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[4]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Alzheimer</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[5]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Hyperglycemia </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[6]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Hypoglycemia</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[7]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Diabetes Type 1 </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[8]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Diabetes Type 2</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[9]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>High Blood Pressure</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[10]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Contact Lenses</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[11]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Rheumatic Fever</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[12]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Pace Maker</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[13]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Heart Stent</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php if($medcond[14]['checked']==1) { ?>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>Tetanus Shot (last 12 mnths)</span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php  } ?>

                                <?php if( strlen($add_medical_conditions) > 1) { ?>
                                <tr>
                                    <td style='width: 89.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt;'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <span style=''>
                                                <img width="15" height="15"
                                                    src="Emergency%20Information%20for_files/image005.jpg" />
                                            </span>
                                        </p>
                                    </td>
                                    <td style='width: 387.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; white-space:pre-line;'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $add_medical_conditions; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php }

        }   ?>

                            </table>

                        </div>

                        <br />

                        <div align="center">
                            <table class="MsoTableGrid" border="1"
                                style='border-collapse: collapse; border: none; width:500pt;'>
                                <tr style='height: 25.6pt'>
                                    <td colspan="3" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #F4B083; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <b>
                                                <span
                                                    style='font-size: 18.0pt'>
                                                    Medications
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 170.75pt; border: solid windowtext 1.0pt; border-top: none; background: #F8CCAE; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Name</span>
                                        </p>
                                    </td>
                                    <td style='width: 112.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; background: #F8CCAE; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Dosage</span>
                                        </p>
                                    </td>
                                    <td style='width: 193.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; background: #F8CCAE; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>How Often</span>
                                        </p>
                                    </td>
                                </tr>
                                <?php
        $recordset = $database->select("user_medications", [
            "medicationId",
            "medication_name",
            "dosage",
            "howoften"
        ], [
            "userid" => "$report_user_id",
             "ORDER" => ['medication_name ASC']
        ]);

        foreach($recordset as $data)
        {
            //$medicationId = $data["medicationId"];
            $medication_name = $data["medication_name"];
            $dosage = $data["dosage"];
            $howoften = $data["howoften"];

                                ?>
                                <tr style='height: 26.5pt'>
                                    <td style='width: 170.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $medication_name; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                    <td style='width: 112.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $dosage; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                    <td style='width: 193.5pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $howoften; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php
        }

                                ?>
                            </table>
                        </div>

                        <br />

                        <div align="center">
                            <table border="1"
                                style='border-collapse: collapse; border: none; width:500pt;'>
                                <tr style='height: 25.6pt'>
                                    <td colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #A8D08D; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <b>
                                                <span
                                                    style='font-size: 18.0pt'>
                                                    Allergies
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 170.6pt; border: solid windowtext 1.0pt; border-top: none; background: #C5E0B3; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Description</span>
                                        </p>
                                    </td>
                                    <td style='width: 306.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; background: #C5E0B3; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Notes</span>
                                        </p>
                                    </td>
                                </tr>
                                <?php

        $recordset = $database->select("user_allergies", [
            "allergyId",
            "description",
            "notes"
        ], [
            "userid" => "$report_user_id",
             "ORDER" => ['description ASC']
        ]);

        foreach($recordset as $data)
        {
            $allergyId = $data["allergyId"];
            $description = $data["description"];
            $notes = $data["notes"];

                                ?>
                                <tr style='height: 26.5pt'>
                                    <td style='width: 170.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $description; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                    <td style='width: 306.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $notes; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <?php
        }

                                ?>
                            </table>
                        </div>

                        <br />

                        <div align="center">
                            <table class="MsoTableGrid" border="1" style='border-collapse: collapse; border: none;  width:500pt;'>
                                <tr style='height: 25.6pt'>
                                    <td style='width: 476.75pt; border: solid windowtext 1.0pt;'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <b>
                                                <span
                                                    style='font-size: 18.0pt'>
                                                    Miscellaneous
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 26.5pt'>
                                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #E7E6E6; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <u>
                                                <span style='font-size: 14.0pt; color: #3B3838;'>Hospitalizations</span>
                                            </u>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $hospitalizations; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 26.5pt'>
                                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #E7E6E6; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <u>
                                                <span style='font-size: 14.0pt; color: #3B3838;'>
                                                    Serious Injuries
  or Illness
                                                </span>
                                            </u>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td valign="top" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $serious_injuries; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 26.5pt'>
                                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #E7E6E6; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <u>
                                                <span style='font-size: 14.0pt; color: #3B3838;'>Special Diets</span>
                                            </u>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td valign="top" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $special_diets; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 26.5pt'>
                                    <td style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #E7E6E6; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <u>
                                                <span style='font-size: 14.0pt; color: #3B3838;'>Additional Information</span>
                                            </u>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td valign="top" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $add_info; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                            </table>

                        </div>

                        <br />

                        <br />

                        <div align="center">


                            <table class="MsoTableGrid" border="1"
                                style='border-collapse: collapse; border: none; width:500pt;'>
                                <tr style='height: 25.6pt'>
                                    <td colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #BDD6EE; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <b>
                                                <span
                                                    style='font-size: 18.0pt'>
                                                    Emergency Contact Information
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 26.5pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Primary Contact</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $primary_name; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Address</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $primary_addr; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Mobile Phone Number</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $primary_phone; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Email Address</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $primary_email; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Relationship</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $primary_relationship; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 5; height: 22.0pt'>
                                    <td width="636" colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #DEEAF6; mso-background-themecolor: accent1; mso-background-themetint: 51; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <o:p>&nbsp;</o:p>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 26.5pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Secondary Contact</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $secondary_name; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Address</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $secondary_addr; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Mobile Phone Number</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $secondary_phone; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Email Address</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $secondary_email; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Relationship</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $secondary_relationship; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 9; height: 22.0pt'>
                                    <td width="636" colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; border-top: none; background: #DEEAF6; mso-background-themecolor: accent1; mso-background-themetint: 51; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <o:p>&nbsp;</o:p>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 10; height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Primary Physician</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $phy_name; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 11; height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Address</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $phy_addr; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 12; height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Phone</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $phy_phone; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 12; height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Email</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $phy_email; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 12; height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Type</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $phy_type; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                            </table>

                        </div>

                        <br />

                        <br />

                        <div align="center">

                            <table class="MsoTableGrid" border="1"
                                style='border-collapse: collapse; border: none;'>
                                <tr style='height: 25.6pt'>
                                    <td width="636" colspan="2" style='width: 476.75pt; border: solid windowtext 1.0pt; background: #FFC1B3; padding: 0in 5.4pt 0in 5.4pt; height: 25.6pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center; line-height: normal'>
                                            <b>
                                                <span
                                                    style='font-size: 18.0pt'>
                                                    Insurance Information
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 26.5pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Company Name</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 26.5pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $ins_company; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Group Number</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $groupno; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Contract Number</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $contractno; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Effective Date</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $effectivedate; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 5; height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Certification Number</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $certification_no; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 6; height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Phone</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $ins_phone; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 6; height: 22.0pt'>
                                    <td width="185" style='width: 139.1pt; border: solid windowtext 1.0pt; border-top: none; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <span style='font-size: 14.0pt'>Plan With</span>
                                        </p>
                                    </td>
                                    <td width="450" style='width: 337.65pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; line-height: normal'>
                                            <b>
                                                <span style='font-size: 14.0pt'>
                                                    <?php echo $plan_with; ?>
                                                </span>
                                            </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 6; height: 22.0pt'>
                                    <td colspan="2" style='text-align:center;width: 337.65pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: .1in; margin-top: .1in; line-height: normal'>
                                            <?php include("include/incInspic-front.php"); ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr style='mso-yfti-irow: 6; height: 22.0pt'>
                                    <td colspan="2" style='text-align:center;width: 337.65pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 22.0pt'>
                                        <p class="MsoNormal" style='margin-bottom: .1in; margin-top: .1in; line-height: normal'>
                                            <?php include("include/incInspic-back.php"); ?>
                                        </p>
                                    </td>
                                </tr>
                            </table>

                        </div>

                        <br />
                        <br />
                        <hr style="width: 50%;height:1px;border:0;background:#333;background-image: linear-gradient(to right, #ccc, #333, #ccc);" />

                    </div>


                    <div>
                        <p class="MsoClosing" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
                            <span style='font-size: 9.0pt;'>
                                <img width="102" height="27"
                                    src="Emergency%20Information%20for_files/image007.png"
                                    alt="http://emtelink.com/images/logo.png" />
                            </span>
                            <br />
                            <span style='font-size: 8.0pt'>
                                Report Generated by EMTeLink (https://www.emtelink.com)
                            </span>
                        </p>
                    </div>

                    <?php } else {

        $dt = new DateTime("@$starttime");

                    ?>

                    <div class="MsoClosing">
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
                            <span class="Heading1Char">
                                <b>
                                    <span style='font-size: 22.0pt; line-height: 115%; color: #595959;'>Emergency Information For:</span>
                                </b>
                            </span>
                        </p>

                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
                            <span class="Heading1Char">
                                <b>
                                    <span style='font-size: 24.0pt; line-height: 115%; color: red'>
                                        <?php echo $firstname . " " . $middleinitial . " " . $lastname; ?>
                                    </span>
                                </b>
                            </span>
                        </p>
                        <br />


                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
                            <span style='font-size: 20.0pt; line-height: 100%; color: red'>This report has expired.</span>
                            <span style='font-size: 18.0pt; line-height: 100%; color: black'>
                                <br />
                                <br />
                                Reports are only valid for 24 Hours
                                <br />
                                and this report was originally generated at:
                                <br />
                                <?php echo $dt->format('Y-m-d H:i:s');?>
                                <br />
                                <br />
                                Please contact our support department at: 855-363-6488
                                <br />
                                <br />
                                if you need additional assistance or to have the report resent.
                                <br />
                                <br />

                            </span>
                        </p>

                        <br />
                        <p class="MsoNormal" style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: center'>
                            <span style='font-size: 14.0pt; line-height: 100%; color: black'>
                                &copy; Copyright @ 2016 EMTeLink
                                <sup>&reg;</sup>
                            </span>
                        </p>
                    </div>

</body>

</html>
