﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="~/tracker/Fileman.aspx.vb" %>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.FileUltimate" Assembly="GleamTech.FileUltimate" %>

<!DOCTYPE html>

<html>
<head runat="server">

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="admin/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <title></title>
</head>
<body>

    <!-- Navbar -->
    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-reorder"></i>
                    &nbsp;
                </button>
                <a class="navbar-brand" href="#">&nbsp;EMTeLink Tracker</a>
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- End navbar -->
    <!-- jumbotron-->

    <!-- End jumbotron-->

    <div class="well well-sm">
        <div class="text-center">
            <h2>
                <i class="shortcut-icon icon-file"  style="color: #214D8C;"></i>
                Documents
            </h2>
        </div>
        <!-- End container -->
    </div>

    <div class="container">

      <div class="row">
          <div class="col-xs-12" style="margin: 0 auto;border-bottom-color:gainsboro;border:solid;border-width:1px;padding:2px;height:605px;">

          <GleamTech:FileManager ID="fileManager" runat="server" 
                           Resizable="True"
						   ShowRibbon="False"
                           CollapseRibbon ="True"
						   ShowFoldersPane="False"
						   ViewLayout="MediumIcons"
                           Width="100%"
                           Height="600px"
						   >
        
            <GleamTech:FileManagerRootFolder Name="My Documents" Location="c:\inetpub\wwwroot\emtelink\new\Uploads\2\Documents\dlewis6560">
                <GleamTech:FileManagerAccessControl Path="\" AllowedPermissions="Full" Quota="15MB"/>
            </GleamTech:FileManagerRootFolder>   

        </GleamTech:FileManager>
    
	      </div>
      </div>
    </div>

  
</body>
</html>