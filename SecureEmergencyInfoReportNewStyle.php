<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=95%, initial-scale=1.0, user-scalable=yes" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-dialog.min.css" rel="stylesheet" />



    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");


    if (isset($_GET['id'])) {
        $id_value = $_GET['id'];
        //echo $id_value . "<br />";
    } else{
        echo "report user id not found, processing cannot continue.";
        exit;
    }

    use Urlcrypt\Urlcrypt;
    require_once 'Urlcrypt.php';

    Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
    $decrypted = Urlcrypt::decrypt($id_value);


    $decrypted = Urlcrypt::decrypt($id_value);

    list($report_user_id, $starttime) = explode("|", $decrypted);

    $endtime = time();
    $timediff = $endtime - $starttime;

    function calculateAge($date){

        //return date_diff(date_create($date), date_create('today'))->y;
        $birthDate = explode("-", $date);
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
          ? ((date("Y") - $birthDate[2]) - 1)
          : (date("Y") - $birthDate[2]));
        return $age;


        //list($month,$day,$year) = explode("-",$date);
        //echo $year;
        //$year_diff  = date("Y") - $year;
        //$month_diff = date("m") - $month;
        //$day_diff   = date("d") - $day;
        //if ($day_diff < 0 || $month_diff < 0) $year_diff--;
        //return $year_diff;
    }

    //list($userid, $epoch_value) = split('|', $decrypted);
    //echo "userid: $userid; ev: $epoch_value;<br />";
    //echo $decrypted;

    //        list($report_user_id, $epoch_value) = explode("|", $decrypted);
    //echo $report_user_id;
    //echo $epoch_value;

    //session_start();

    //make sure we have a valid sesion
    //if ($_SESSION["valid"] != "TRUE")
    //{
    //    header("Location: index.html");
    //}; JBrinson




    //$report_user_id = 'dmassey';
    $userid = $report_user_id;

    ?>


    <title>Emergency Information Report</title>

    <style>
        @media (min-width: 200px) {
            .table_style {
                width: 100%;
                font-family: Calibri,sans-serif;
                font-size: 11.0pt;
                font-weight: 500;
                color: #000000;
                padding: 4px;
                text-align: center;
                border-color: black;
            }

            .table_header_title {
                font-family: Calibri,sans-serif;
                font-size: 15.0pt;
                font-weight: 700;
                color: #000000;
            }

            .th_25 {
                width: 25%;
                font-family: Calibri,sans-serif;
                font-size: 11.0pt;
                background-color: #E7E5E5;
                vertical-align: middle;
                color: #000000;
                padding: 4px;
                text-align: center;
            }
        }

        @media (min-width: 350px) {
            .table_style {
                width: 100%;
                font-family: Calibri,sans-serif;
                font-size: 14.0pt;
                font-weight: 500;
                color: #000000;
                padding: 4px;
                text-align: center;
                border-color: black;
            }

            .table_header_title {
                font-family: Calibri,sans-serif;
                font-size: 15.0pt;
                font-weight: 700;
                color: #000000;
            }

            .th_25 {
                width: 25%;
                font-family: Calibri,sans-serif;
                font-size: 12.0pt;
                background-color: #E7E5E5;
                vertical-align: middle;
                color: #000000;
                padding: 4px;
                text-align: center;
            }
        }



        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid #595959;
            text-align: center;
        }

        .row-no-padding > [class*="col-"] {
            padding-left: 8px !important;
            padding-right: 8px !important;
        }


        @media (min-width: 600px) {

            .table_style {
                width: 100%;
                font-family: Calibri,sans-serif;
                font-size: 18.0pt;
                color: #000000;
                padding: 4px;
                text-align: center;
                border-color: black;
            }

            .table_header_title {
                font-family: Calibri,sans-serif;
                font-size: 22.0pt;
                font-weight: 600;
                color: #000000;
            }

            .th_25 {
                width: 25%;
                font-family: Calibri,sans-serif;
                font-size: 18.0pt;
                background-color: #E7E5E5;
                vertical-align: middle;
                color: #000000;
                padding: 4px;
                text-align: center;
            }
        }
    </style>
</head>

<body>

    <?php
    $recordset = $database->select("user", [
        "subId",
        "firstname",
        "lastname",
        "middleinitial",
        "address",
        "city",
        "st",
        "zip",
        "dob",
        "phone",
        "student_email",
        "parent_email",
        "med_conditions",
        "hospitalizations",
        "serious_injuries",
        "special_diets",
        "add_info",
        "parent_name",
        "parent_addr",
        "parent_day_phone",
        "parent_night_phone",
        "relative_name",
        "relative_phone",
        "relationship",
        "family_doctor",
        "doctor_day_phone",
        "doctor_night_phone",
        "ins_company",
        "ins_phone",
        "groupno",
        "contractno",
        "effectivedate",
        "certification_no",
        "plan_with",
        "add_medical_conditions",
        "primary_name",
        "primary_addr",
        "primary_phone",
        "primary_carrierId",
        "primary_email",
        "primary_relationship",
        "secondary_name",
        "secondary_addr",
        "secondary_phone",
        "secondary_carrierId",
        "secondary_email",
        "secondary_relationship",
        "phy_name",
        "phy_addr",
        "phy_phone",
        "phy_email",
        "phy_type",
        "bloodtype"

    ], [
        "userid" => "$report_user_id"
    ]);

    foreach($recordset as $data)
    {
        $subId = $data["subId"];
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $middleinitial = $data["middleinitial"];
        $address = $data["address"];
        $blood_type = $data["bloodtype"];
        $city = $data["city"];
        $state = $data["st"];
        $zip = $data["zip"];
        //$dob = $data["dob"]->format('m-d-Y');
        //$dob = $data["dob"];
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data["dob"]);
        if(!$myDateTime == ''){
            $dob = $myDateTime->format('m-d-Y');
            $age = calculateAge($dob);
        }

        $phone = $data["phone"];
        $student_email = $data["student_email"];
        $parent_email = $data["parent_email"];

        $med_conditions = $data["med_conditions"];
        $hospitalizations = $data["hospitalizations"];
        $serious_injuries = $data["serious_injuries"];
        $special_diets = $data["special_diets"];
        $add_info = $data["add_info"];
        $parent_name = $data["parent_name"];
        $parent_addr = $data["parent_addr"];
        $parent_day_phone = $data["parent_day_phone"];
        $parent_night_phone = $data["parent_night_phone"];
        $relative_name = $data["relative_name"];
        $relative_phone = $data["relative_phone"];
        $relationship = $data["relationship"];
        $family_doctor = $data["family_doctor"];
        $doctor_day_phone = $data["doctor_day_phone"];
        $doctor_night_phone = $data["doctor_night_phone"];
        $ins_company = $data["ins_company"];
        $ins_phone = $data["ins_phone"];
        $groupno = $data["groupno"];
        $contractno = $data["contractno"];
        //$effectivedate = $data["effectivedate"]->format('m-d-Y');
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data["effectivedate"]);
        if(!$myDateTime == ''){
            $effectivedate = $myDateTime->format('m-d-Y');
        }
        //$effectivedate = $data["effectivedate"];
        $certification_no = $data["certification_no"];
        $plan_with = $data["plan_with"];
        $add_medical_conditions = $data["add_medical_conditions"];

        $primary_name = $data["primary_name"];
        $primary_addr = $data["primary_addr"];
        $primary_phone = $data["primary_phone"];
        $primary_email = $data["primary_email"];
        $primary_relationship = $data["primary_relationship"];
        $primary_carrierId = $data["primary_carrierId"];

        $secondary_name = $data["secondary_name"];
        $secondary_addr = $data["secondary_addr"];
        $secondary_phone = $data["secondary_phone"];
        $secondary_email = $data["secondary_email"];
        $secondary_relationship = $data["secondary_relationship"];
        $secondary_carrierId = $data["secondary_carrierId"];

        $phy_name = $data["phy_name"];
        $phy_addr = $data["phy_addr"];
        $phy_phone = $data["phy_phone"];
        $phy_email = $data["phy_email"];
        $phy_type = $data["phy_type"];

        $medcond=json_decode($med_conditions,true);
        //echo $subId;
    }

    $recordset = $database->select("Subscriptions", [
        "subType"
    ], [
        "subId" => "$subId"
    ]);

    foreach($recordset as $data)
    {
        $subType = $data["subType"];
    }

    ?>


    <?php
    //only process if pw request is less than 24 hour old (60 mins * 60 seconds * 24 hours)
    if ($timediff < 86400) {
    ?>

    <div class="wrapper container-fluid">
        <div class="row row-no-padding">
            <br />
            <div class="col-xs-12">
                <div style="text-align:center">
                    <button class="btn btn-lg btn-success" style="display: inline-block;" type="button" onclick="javascript: localStorage.home = 'true';window.location.assign('index.html');">Logoff</button>&nbsp;&nbsp;
                    <button class="btn btn-lg btn-danger email_rpt" style="display: inline-block;" type="button">Email Report</button>
                </div>
                <br />
                <table class="table table-bordered" style="width:100%">
                    <tr>
                        <td colspan="3" style="text-align:center;">
                            <span style="font-family:Calibri,sans-serif;font-size: 22.0pt;color: #595959;">
                                <strong>Emergency Information</strong>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:center;">
                            <span style="font-family:Calibri,sans-serif;font-size: 24.0pt;color: red">
                                <strong>
                                    <?php echo $firstname . " " . $middleinitial . " " . $lastname; ?>
                                </strong>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:center;">
                            <?php include("include/incProfilepic.php"); ?>
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered table_style">
                    <tr>
                        <th class="th_25">DOB</th>
                        <td colspan="2" style="vertical-align:middle"><?php echo $dob; ?></td>
                    </tr>
                    <tr>
                        <th class="th_25">Age</th>
                        <td colspan="2" style="vertical-align:middle"><?php echo $age . " years old"; ?></td>
                    </tr>
                    <tr>
                        <th class="th_25">Address</th>
                        <td colspan="2" style="vertical-align:middle">
                            <?php echo $address."<br />".$city." ".$state." ".$zip; ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="th_25">Blood Type</th>
                        <td colspan="2" style="vertical-align:middle"><?php echo $blood_type; ?></td>
                    </tr>
                </table>

                <?php
                    //determine if there is at least one medical condition
                    $bolAtLeastOneCondition = "false";

                    //echo count($medcond);
                    for($i = 0, $size = count($medcond); $i < $size; ++$i) {
                        //$people[$i]['salt'] = mt_rand(000000, 999999);
                        if ($medcond[$i]['checked']==1) {
                            $bolAtLeastOneCondition = "true<br />";
                            //echo "cond - true<br />";
                        }
                        else {
                            //echo "cond - false<br />";
                        }
                    }

                    //echo "bolAtLeastOneCondition = " . $bolAtLeastOneCondition;

                ?>

                <table class="table table-bordered table_style">
                    <thead>
                        <tr>
                            <th colspan="3" style="background:#9CC2E5;">
                                <span class="table_header_title">Medical Conditions<?php if ($bolAtLeastOneCondition == "false") { echo "- none";} ?></span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>

                        <?php
                      if ($subType != "F") {

                          if($medcond[0]['checked']==1) { ?>  <tr><td colspan="3">Mumps</td></tr> <?php }
                          if($medcond[1]['checked']==1) { ?>  <tr><td colspan="3">Measles</td></tr> <?php }
                          if($medcond[2]['checked']==1) { ?>  <tr><td colspan="3">Chicken Pox</td></tr> <?php }
                          if($medcond[3]['checked']==1) { ?>  <tr><td colspan="3">Pneumonia</td></tr> <?php }
                          if($medcond[4]['checked']==1) { ?>  <tr><td colspan="3">Diabetes</td></tr> <?php }
                          if($medcond[5]['checked']==1) { ?>  <tr><td colspan="3">Cancer</td></tr> <?php }
                          if($medcond[6]['checked']==1) { ?>  <tr><td colspan="3">Malaria</td></tr> <?php }
                          if($medcond[7]['checked']==1) { ?>  <tr><td colspan="3">Seizures</td></tr> <?php }
                          if($medcond[8]['checked']==1) { ?>  <tr><td colspan="3">Heart Disease</td></tr> <?php }
                          if($medcond[9]['checked']==1) { ?>  <tr><td colspan="3">Kidney Disease</td></tr> <?php }
                          if($medcond[10]['checked']==1) { ?>  <tr><td colspan="3">Tuberculosis</td></tr> <?php }
                          if($medcond[11]['checked']==1) { ?>  <tr><td colspan="3">Rheumatic Fever</td></tr> <?php }
                          if($medcond[12]['checked']==1) { ?>  <tr><td colspan="3">Hepatitis</td></tr> <?php }
                          if($medcond[13]['checked']==1) { ?>  <tr><td colspan="3">Blood Pressure Problems</td></tr> <?php }
                          if($medcond[14]['checked']==1) { ?>  <tr><td colspan="3">Tetanus Toxoid Shot (in last 12 months)</td></tr> <?php }
                          if( strlen($add_medical_conditions) > 1) { ?>  <tr><td colspan='3'><?php  echo $add_medical_conditions;?></td></tr> <?php }
                          


                      }   //subType != F
                      else {

                          if($medcond[0]['checked']==1) { ?>  <tr><td colspan="3">Asthma</td></tr>  <?php } 
                          if($medcond[1]['checked']==1) { ?>  <tr><td colspan="3">COPD</td></tr> <?php }
                          if($medcond[2]['checked']==1) { ?>  <tr><td colspan="3">Seizure Disorder</td></tr> <?php }
                          if($medcond[3]['checked']==1) { ?>  <tr><td colspan="3">Dementia</td></tr> <?php }
                          if($medcond[4]['checked']==1) { ?>  <tr><td colspan="3">Alzheimer</td></tr> <?php }
                          if($medcond[5]['checked']==1) { ?>  <tr><td colspan="3">Hyperglycemia</td></tr> <?php }
                          if($medcond[6]['checked']==1) { ?>  <tr><td colspan="3">Hypoglycemia</td></tr> <?php }
                          if($medcond[7]['checked']==1) { ?>  <tr><td colspan="3">Diabetes Type 1</td></tr> <?php }
                          if($medcond[8]['checked']==1) { ?>  <tr><td colspan="3">Diabetes Type 2</td></tr> <?php }
                          if($medcond[9]['checked']==1) { ?>  <tr><td colspan="3">High Blood Pressure</td></tr> <?php }
                          if($medcond[10]['checked']==1) { ?>  <tr><td colspan="3">Contact Lenses</td></tr> <?php }
                          if($medcond[11]['checked']==1) { ?>  <tr><td colspan="3">Rheumatic Fever</td></tr> <?php }
                          if($medcond[12]['checked']==1) { ?>  <tr><td colspan="3">Pacemaker</td></tr> <?php }
                          if($medcond[13]['checked']==1) { ?>  <tr><td colspan="3">Heart Stent</td></tr> <?php }
                          if($medcond[14]['checked']==1) { ?>  <tr><td colspan="3">Tetanus Shot (last 12 mnths)</td></tr> <?php }
                          if( strlen($add_medical_conditions) > 1) { ?>  <tr><td colspan='3'><?php  echo $add_medical_conditions;?></td></tr> <?php }
                      
                      }?>


                    </tbody>
                </table>
                <table class="table table-bordered table_style">
                    <thead>
                        <tr>
                            <th colspan="3" style="background:#F4B083;">
                                <span class="table_header_title">Medications</span>
                            </th>
                        </tr>
                        <tr style="background:#F8CCAE;">
                            <th>Name</th>
                            <th>Dosage</th>
                            <th>Frequency</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
        $recordset = $database->select("user_medications", [
            "medicationId",
            "medication_name",
            "dosage",
            "howoften"
        ], [
            "userid" => "$report_user_id",
             "ORDER" => ['medication_name ASC']
        ]);

        foreach($recordset as $data)
        {
            //$medicationId = $data["medicationId"];
            $medication_name = $data["medication_name"];
            $dosage = $data["dosage"];
            $howoften = $data["howoften"];

                        ?>
                        <tr>
                            <td><?php echo $medication_name; ?></td>
                            <td><?php echo $dosage; ?></td>
                            <td><?php echo $howoften; ?></td>
                        </tr>
                    </tbody>
                    <?php  }?>
                </table>


                <table class="table table-bordered table_style">
                    <thead>
                        <tr>
                            <th colspan="3" style="background:#A8D08D;">
                                <span class="table_header_title">Allergies</span>
                            </th>
                        </tr>
                        <tr style="background:#C5E0B3">
                            <th style="text-align:center">Description</th>
                            <th colspan="2" style="text-align:center">Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

        $recordset = $database->select("user_allergies", [
            "allergyId",
            "description",
            "notes"
        ], [
            "userid" => "$report_user_id",
             "ORDER" => ['description ASC']
        ]);

        foreach($recordset as $data)
        {
            $allergyId = $data["allergyId"];
            $description = $data["description"];
            $notes = $data["notes"];

                        ?>
                        <tr>
                            <td><?php echo $description; ?></td>
                            <td colspan="2"><?php echo $notes ; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <br />
                <table class="table table-bordered table_style">
                    <thead>
                        <tr>
                            <th colspan="3" style="background:#AFB3FF;">
                                <span class="table_header_title">Miscellaneous</span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="3" style="background:#CDD1FF;">
                                <span class="">Hospitalizations</span>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3" style="background:#FFFFFF;">
                                <span class=""><?php echo $hospitalizations; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3" style="background:#CDD1FF;">
                                <span class="">Serious Injuries or Illness</span>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3" style="background:#FFFFFF;">
                                <span class=""><?php echo $serious_injuries; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3" style="background:#CDD1FF;">
                                <span class="">Special Diets</span>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3" style="background:#FFFFFF;">
                                <span class=""><?php echo $special_diets; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3" style="background:#CDD1FF;">
                                <span class="">Additional Information</span>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3" style="background:#FFFFFF;">
                                <span class=""><?php echo $add_info; ?></span>
                            </td>
                        </tr>
                    </thead>
                </table>
                <br />
                <table class="table table-bordered table_style">
                    <thead>
                        <tr>
                            <th colspan="3" style="background:#BDD6EE;">
                                <span class="table_header_title">Emergency Contacts</span>
                            </th>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Primary Contact</th>
                            <td colspan="2"><?php echo $primary_name; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Address</th>
                            <td colspan="2"><?php echo $primary_addr; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Mobile Phone</th>
                            <td colspan="2"><?php echo $primary_phone; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Email</th>
                            <td colspan="2"><?php echo $primary_email; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">How Related</th>
                            <td colspan="2"><?php echo $primary_relationship; ?></td>
                        </tr>
                        <tr>
                            <td style="background:#DEEAF6" colspan="3">&nbsp;</td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Secondary Contact</th>
                            <td colspan="2"><?php echo $secondary_name; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Address</th>
                            <td colspan="2"><?php echo $secondary_addr; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Mobile Phone</th>
                            <td colspan="2"><?php echo $secondary_phone; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Email</th>
                            <td colspan="2"><?php echo $secondary_email; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">How Related</th>
                            <td colspan="2"><?php echo $secondary_relationship; ?></td>
                        </tr>
                        <tr>
                            <td style="background:#DEEAF6" colspan="3">&nbsp;</td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Primary Physician</th>
                            <td colspan="2"><?php echo $phy_name; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Address</th>
                            <td colspan="2"><?php echo $phy_addr; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Phone</th>
                            <td colspan="2"><?php echo $phy_phone; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Email</th>
                            <td colspan="2"><?php echo $phy_email; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#DEEAF6">Type</th>
                            <td colspan="2"><?php echo $phy_type; ?></td>
                        </tr>
                    </thead>
                </table>
                <br />
                <table class="table table-bordered table_style">
                    <thead>
                        <tr>
                            <th colspan="3" style="background:#FFC1B3;">
                                <span class="table_header_title">Insurance Information</span>
                            </th>
                        </tr>
                        <tr>
                            <th style="background:#FFDED7">Company Name</th>
                            <td colspan="2"><?php echo $ins_company; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#FFDED7">Group Number</th>
                            <td colspan="2"><?php echo $groupno; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#FFDED7">Contact Nunber</th>
                            <td colspan="2"><?php echo $contractno; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#FFDED7">Effective Date</th>
                            <td colspan="2"><?php echo $effectivedate; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#FFDED7">Certification Number</th>
                            <td colspan="2"><?php echo $certification_no; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#FFDED7">Phone</th>
                            <td colspan="2"><?php echo $ins_phone; ?></td>
                        </tr>
                        <tr>
                            <th style="background:#FFDED7">Plan With</th>
                            <td colspan="2"><?php echo $plan_with; ?></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <?php include("include/incInspic-front.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <?php include("include/incInspic-back.php"); ?>
                            </td>
                        </tr>
                    </thead>
                </table>
                <br />
                <div style="text-align:center">
                    <button class="btn btn-lg btn-success" style="display: inline-block;" type="button" onclick="javascript: localStorage.home = 'true';window.location.assign('index.html');">Logoff</button>&nbsp;&nbsp;
                    <button class="btn btn-lg btn-danger email_rpt" style="display: inline-block;" type="button">Email Report</button>
                </div>
                <br />
                <br />
            </div>
        </div>

        <br />
        <div class="text-center">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAAAbCAYAAACOY/p9AAAAAXNSR0IArs4c6QAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUATWljcm9zb2Z0IE9mZmljZX/tNXEAAAvESURBVGhD7Vl/kFVVHT/33Pt+7LLAvlXKJSAWSQwQIkrFGYcam6AcQ5ummLAEBmIXWNssKjV5rmNTw6QtsiyL0JKIPypr0tJx0hg0S6OkBRQEEVZJGFr2J7v73r5377l9PnfPedx9gP4DDG+G78xnzz3nnZ/f3+esU1tbKy7ShccB58Lb0sUdkQMFLZirk0lnthD+h4jSel7MFttrZ7iFJPKCFcyyxk3zi0eNv71dCO9MDLfxg+8Le5q7d8V2MeOvFwVzHjjg+3JWke1Ms30lLAqAa+JP2HyiliXavaySmczx87Cls7pEQVpMdV3dEC9eOpXmQNBkKBwJ0F+l2R7IyRK+8nc6Hem3zyrXzsNkBSkYES/5FARS1p7N/sET6h9S+ceUEDHLFldCPDcVS3tCP5hnB+Ky9q6prek7D7w8q0sUpGBcV6QQWubWL1+wLZ8bi+saV/fF44+XOM71CjYDe3rzrHLsPE1WkIJZt3zRDsOfqvqN4xzb/k7Etsb2u+rhtTWVW9F2T9qWL8C1OVKJ184TL8/qMjnBJJPJEsz8MT17fgrKBKcTl9Gj6BfF9zTt1pvRlj7djtCvAu0fB46gz37Uh+N7JHDGLCrwPggR6H/oDjA85bgfAW9NmhvB14k1yxe9Ydarrt94le84TycikQphSZFW6Th+2+q4kbe8qH/c9UXCk6K0qnEj98u5B8gRtpMWvYnWw3vuq61V2NtQtI4CeO6jWL/rDGcif0qBXvRpYR+MvRzFaOAdtB0Oj8NvRZoHnJf9WZIvDIet6J9LStC3WPelTFKBYNA4BsUWYCpAxp88xMBKrN+Nfr9E+QTwZYAOfDPaKrHAIGaj7Q789kMAjBXHUf8ayhuAuwCEgwAMA2ZurslNc8MvtCWTN2fLK9YOceI3pH0VCMbxRSwr3XvxGQhmZTIp2y4be38ZhNKhlEhIBHphBQxNi1TMUUV2zLHjrm89qtexjLYV+1asL+Ju+50QX9S/LUR5P8BzzAFe0u25AmcYgsqTwKe5R+BmtH0d5UMAz3kQ9a+CF7tCY2/C90agE/gM8FngMR4HWKTnI/8prPXATN13sbEYTnC9nrANJSVphEMBkP6p+9wSWpgM/ynQYtqwCAXwQKjPCHxPBmgtuHaILBABeBgSGX8MoFDYvmN1+cSh7TI91bGsSETISAw6cEK5HbFeRaUIqHXEuEnSsb6UAreHwVra3UyLcNN1wY/R6MyolCMoCMeSxeYArMeRQqdd11NZf/PekwpFhtFjcH+HQnsPf45FhUKhZhvL+B6+P6o70XKqgcWhQdPxTWt8DyBPrwHoOaiY+9gP/KI1PwJcBXQCyyDcZ4xgOAGJQvkCfmjGgLDV2GjLoO1e3S+DktrFRccCLXoRav49uk8HygRAxu/Ri/8YJcdyg38GYsBaPYbrBS6z1e24xokUj2Da6wIxMFP53tYHVyw6oOeGGNXkS2Q00o/fu113m5d2q9fVVL5R/bPGcpWI3xWVlpXSabMZQ6GkPC/T53mLNyxfuFnvmXu4QvfZi/JIbo3BH/QmFArpVfCiFOW4vL5z0A7vmHNpHBP0Rxtd5hRd5zl2oX4dyt8AdKPvAPPQjwYgHB0zKC1SC8A4Miy0IDUZWVBA1BgSzZUH+ApATdmm229FSXNkwD0K0Lr+C7yFBXtREtQSboQMIb2M307o74GioWlSXEqHTKe2e7hEQsdeDvdR0k90e97BfuWt6jja0vRkbW0WGdkYrzS+eagTubKXY0JEobjKz6bdzPxEl/tSdV3jqDU1ldwb44Zh8D7s5UxPN+bsTL3/pcfQGzDG0j19C2D9G8AvcMZylDnBoE4j+KTe0m6UeE0KLOUS4N/At7E2FSMgduYEn9B1SjSX8eCbfKF7wbzJJpR0SSRujIeiYIJD4fcyFD8C6DHWAfTbJG6CripMtBgSY0LYJweNtrSmw40JaryNEhqedZR6PTxBNJ15ujeVeWLDnZW0TFHV0DQvUly0EneYK/KFUkT35amU8jLzVKZne2/psNWyP8M4SOKZyBxSc94+gyrORuU0TD6Ib2o3XRb5QwVcCYwHqJQL0X81SvL0MoCehYyfADCWkLqBVXpdPhVRKIMslYKhxtPvmQEsw/GFqkfTo7kzSSBxIU5OYuZF+q7u8zjKVwFujsTMLZfl6UNO1L/xgPS/OVqZfCx6vNyabrIJbtAVVqtw3UEZD7T9fQ5a1tB0i5JySSwSmcUnmJ4891WC+NOr3M6Ml50bPdbz9/jIxCue73dhvJnvar04LWV7eC+hb1oCGU/ao93S50L1IzgXEwMKhlbB0mS4FBxfHuYCVHIe7V2ASk16PV8obOS5DZMYlOcB9HEm9gRxEwNbsTCzCCMwxgxj8pfiN25mOUCzvg+gsEv1wrn0VtepncZC6ToYc3LUWpIag5g/ztWvXlRVT/idXSIdWIah6vpHpqiI3xiNRGbEwPwU+veFhMKNUyh9rtviutl50dbDO73ysc9d6kSmtmX7fx6aylgClYTWfTqipjN5ITH+cnoTL/6j2/+E8m6AbroKMK8NB3DGFMYwKyPxHExiKDha3RL8Rnf+bHhhCoAZCYlZww506Dzt1k5uhJkLtZxWwNhAwdYDdGUPYPw+LMRYQ2JMCbKPELE/TZyULzQhi+zxcGVlxmIseAsIJzWuo8PEuWCg63ijS+3YjH5soycvntANDgG63Oxrntsz74Q7tL105Li/lDnOdSn0xRNOsK4O4Cbw04L6ddJjEjnqBRVwEsDEhtQMkPnGUwTCxLnf11bzA1QZW43i7tTjTBho0fc6JkLXAozvjRg7G+25VwoK5kY9kOb6DDqQAWZjND1u5Pt6AnalUFoBujh+c9O8YDHm8J5DYrZBYht9cpi4GTP/oLjBTsrxJ5dKuB+InfEF1xNS2dF4ORMSKkVAbjqzu6vI7h5iO2gf8JTsyiCvYDmdWfdR2d9ZJZySkaVx8beE40zmxakPgUapjGEALdv4fe6LsZOTmf3RQzQCeIMLiFM0A0wEeK9hPax4zPSYMjOxIe9Ir4CnzF4ZY0hBgIcQ2tG+BJ/PARQ074QUDnkbuCwelu6F39xcPpGxjEHG5b2JwXR71DhqCwVD7fiJ1hqaqDHz3adkXEKY1JwJwSnvWNK3JiF7YpocOCaYHJjkV8jh0c+j+nuzuQ01le8tXde0PmrbK3BnyT37d3vuu8jiatdVLdiEG/9MiHZLkZSj2j1PMAlQntjvtOZem+lGafW0dt5jeKfIJ97eTUbGszNrNWeg+2sxA/Dbbvz2FOoMCaRmgJdRzmvuO7lkB/2ZdvN68ZBeowH1+cxgKYxZAM1scH45MDGlTlPsARYAfPIIM5M+9UVuDpMxuyDRfd12mr7658CqGCj/B5xyZ1CeV99u+b/FG1cubQXfI1KprluTdSVbamu4l4Ds/u5kuz/8QMSW1yrlF1uW2lHckdm06s7KAa1zRToTtaq73WyQpndJ4TieOobX5qAOokvj7Z/KZLynmZ5WQ540A4wjFJ6x/j/im3w4jHMzswzT7ajQCmhtjB1dYDYvrXN0J1plmBpQoRXRqrheEN8dDOQg4sOIiw0ijOVGB7kqHaNO6WsGUks+aCE8UJ7i3ti/qrHpxkR56VO4q1TCWlrYtqamhm73YY1g2qr6xqlLGpqmrl+68EW8qwWXtTORzha53mnXDI0bpEAYR60/Jc1nf7ooFMxMc4Q2xq9BWWWIHxSGUercGJN9fdD+L4jfpLKmJGKRWW3Ceh53llVDM/azq2puC+5HeDezOhKjJ6qo803fKaqWQtHPFzQVjGCQCUzLIugUO/YE15e/6rHVoaUNv95lS9l1vLxijJRyWpltD29zs30i21+QT/1hTSoIwaysqxveVpyYgvtM8BrAHLbYtisc26oYuOfI4GU0A8FJX+y3WtODLq2FaDoFIZieeMnlwrLGBqkgiE554P/6uQeFoD3G3NqSbyO4D7rzXBTMOeJAv7InxW0Zy+Y9t+QvxzTKt/zwW9852tG5n/b/iV1r3bExb+0AAAAASUVORK5CYII=" alt="EMTeLink logo Picture" />
            <br />
            <span style='font-size: 8.0pt'>
                Report Generated by EMTeLink (https://www.emtelink.com)
            </span>
        </div>

    </div>
    

    <?php } else {

        $dt = new DateTime("@$starttime");

    ?>

    
    <?php } ?>

    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap-dialog.min.js"></script>
</body>

</html>
