<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>EMTeLink Tracker App</title>
    <meta name="description" content="EMTeLink Traker App" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->

    <link href="Content/styles.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="admin/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <link href="admin/css/base-admin-3.css" rel="stylesheet" />
    <link href="admin/css/base-admin-3-responsive.css" rel="stylesheet" />

    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");

    if (isset($_GET['id'])) {
        $id_value = $_GET['id'];
        //echo $id_value . "<br />";
    } else{
        echo "id not found, processing cannot continue.";
        exit;
    }


    use Urlcrypt\Urlcrypt;
    require_once '../Urlcrypt.php';

    Urlcrypt::$key = "acad1248103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2facad1248";
    $decrypted = Urlcrypt::decrypt($id_value);

    list($userid, $starttime) = explode("|", $decrypted);

    $endtime = time();
    $timediff = $endtime - $starttime;

	$recordset = $database->select("user", [
		"firstname", "lastname"
	], [
        "userid" => $userid
	]);

	foreach($recordset as $data)
	{
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
	}

    ?>

    <style>

ul, li {
    margin:0;
    padding:0px;
    list-style-type:none;
    text-align:left;
}

#pswd_info {
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
}

#pswd_info h5 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}


.invalid {
    background:url(../images/redx.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    background:url(../images/green-ck.jpg) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}


    </style>


</head>

<body>

    <!-- Navbar -->


    <nav class="navbar navbar-inverse" role="navigation">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="./index.html">&nbsp;EMTeLink Tracker</a>
            </div>
        </div>
        <!-- /.container -->
    </nav>
    <!-- End navbar -->
    <!-- jumbotron-->
    <div class="well well-sm">
        <div class="text-center">
            <h2>
                <i class="icon-key"></i>
                Create Password for <?php echo $firstname . " " . $lastname . " (UserID = " . $userid . ")" ?>
            </h2>
        </div>
        <!-- End container -->
    </div>
    <!-- End jumbotron-->

    <div class="container">
        <section>
            <div class="row">
                <div id="demographic">
                    <span class="clearfix"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-primary text-center" style="border-color:black;">
                            <div class="panel-heading">
                                <h3 class="title" style="font-weight:900">
                                    <u>Create Password</u>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <?php
                                    //only process if pw request is less than 3 days old
                                if ($timediff < 259200) { ?>
                                <form id="profileform" class="profileform">
                                    <br />
                                    <div class="row">
                                        <label class="col-xs-12 col-sm-2 pull-left" for="newpassword">New Password</label>
                                        <div class="col-xs-12 col-sm-10">
                                            <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="enter new password" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-xs-2 col-sm-3"></div>
                                        <div id="pswd_info" class="col-xs-8 col-sm-6 center-block">
                                            <h5 style="text-align:left">New Password must meet the following requirements:</h5>
                                            <ul>
                                                <li id="letter" class="invalid">
                                                    At least
                                                    <strong>one letter</strong>
                                                </li>
                                                <li id="capital" class="invalid">
                                                    At least
                                                    <strong>one capital letter</strong>
                                                </li>
                                                <li id="number" class="invalid">
                                                    At least
                                                    <strong>one number</strong>
                                                </li>
                                                <li id="length" class="invalid">
                                                    Be at least
                                                    <strong>8 characters</strong>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-2 col-sm-3"></div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <label class="col-xs-12 col-sm-2 pull-left" for="confirmpassword">Confirm New Password</label>
                                        <div class="col-xs-12 col-sm-10">
                                            <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="confirm new password" />
                                        </div>
                                    </div>
                                    <div>
                                        <input type="hidden" id="userid" name="userid" value="<?php echo $userid ?>" />
                                    </div>
                                    <br />
                                    <button disabled type="button" id="btn_update_profile" class="btn input-md" style="background-color:#3881C0;color:white;">Update Password</button>
                                     &nbsp;
                                    <?php } else {?>
                                    <h4>
                                        <span style="color:red">This create password request has expired.</span>
                                        <br />
                                        <br />
                                        Please go to the Logon Screen and
                                        <br />
                                        <br />
                                        click the Forgot Password link to
                                        <br />
                                        <br />
                                        send a password reset email.
                                        <br />
                                        <br />
                                    </h4>
                                    <?php } ?>
                                    <button type="button" id="btn_goto_logon" class="btn input-md btn-success" onclick="window.location='index.html'">Go To Logon</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="text-center">
                <p>
                    &copy; Copyright @ 2016 EMTeLink<sup>&reg;</sup>
                </p>
            </div>
        </section>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap-dialog.min.js"></script>
    <script src="Scripts/jquery.form.js"></script>


    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $('input[id=newpassword]').keyup(function () {
                // set password variable
                var pswd = $(this).val();

                //validate the length
                if (pswd.length < 8) {
                    $('#length').removeClass('valid').addClass('invalid');
                } else {
                    $('#length').removeClass('invalid').addClass('valid');
                }

                //validate letter
                if (pswd.match(/[A-z]/)) {
                    $('#letter').removeClass('invalid').addClass('valid');
                } else {
                    $('#letter').removeClass('valid').addClass('invalid');
                }

                //validate capital letter
                if (pswd.match(/[A-Z]/)) {
                    $('#capital').removeClass('invalid').addClass('valid');
                } else {
                    $('#capital').removeClass('valid').addClass('invalid');
                }

                //validate number
                if (pswd.match(/\d/)) {
                    $('#number').removeClass('invalid').addClass('valid');
                } else {
                    $('#number').removeClass('valid').addClass('invalid');
                }


                if ($("#number").hasClass('valid') && $("#letter").hasClass('valid') && $("#capital").hasClass('valid') && $("#length").hasClass('valid')) {

                    //alert('number valid');
                    $("#btn_update_profile").prop('disabled', false);
                }
                else {

                    $("#btn_update_profile").prop('disabled', true);
                }
            })

            //####################################################### Signature EDIT Button ##################################
            $('#btn_update_profile').on('click', function (e) {
                //$(this).hide();

                //alert($('#newpassword').val());
                //alert($('#confirmpassword').val());


                if ($('#newpassword').val() == '') {
                    var modal = $('#myModal');

                    modal.find('.modal-title').text('Missing New Password');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please enter your <u>new</u> password.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                if ($('#confirmpassword').val() == '') {
                    var modal = $('#myModal');
                    modal.find('.modal-title').text('Missing Confirmation');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please <u>confirm</u> your new password.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                if ($('#confirmpassword').val() != $('#newpassword').val()) {
                    var modal = $('#myModal');

                    modal.find('.modal-title').text('New Password and Confirmation don\'t match');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Please make sure your <u>new</u> and <u>confirm</u> password match.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                if ($('#currentpassword').val() == $('#newpassword').val()) {
                    var modal = $('#myModal');

                    modal.find('.modal-title').text('New Password Error');
                    modal.find('.modal-body').html('<br /><span style="color:red"><h5>Your <u>new password</u> <u>cannot be the same</u> as your <u>current</u> password.</h5></span>');
                    $('#myModal').modal('show');
                    return;
                }

                //alert("here");
                //post the values to the server
                $.ajax({
                    type: "POST",
                    url: "ws/process_password_reset.php",
                    data: $('form.profileform').serialize(),
                    success: function (msg) {
                        //$('#sql_stmt').val(msg);
                        //$('#signature-success').removeClass('hide');
                        //$.wait(function () { $('#signature-success').addClass('hide') }, 2);
                        //alert(msg);
                        //disable controls for the sign form
                        if (msg == 'success') {

                            $('#length').removeClass('valid').addClass('invalid');
                            $('#letter').removeClass('valid').addClass('invalid');
                            $('#capital').removeClass('valid').addClass('invalid');
                            $('#number').removeClass('valid').addClass('invalid');
                            $('#newpassword').val('');
                            $('#confirmpassword').val('');

                            var modal = $('#myModal');
                            modal.find('.modal-title').text('Password Updated');
                            modal.find('.modal-body').html('<br /><span style="color:green"><h5>Password updated successfully.</h5></span>');
                            $('#myModal').modal('show');
                            return;
                        }
                    },
                    error: function () {
                        alert("Error saving profile");
                    }
                });


            })

        });
    </script>


</body>
</html>
