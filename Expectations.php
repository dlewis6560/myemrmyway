<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <title>EMTeLink Tracker App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="EMTeLink Traker App">
    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->
    <link href="Content/styles.css" rel="stylesheet" />
    <link href="Content/bootstrap-dialog.min.css" rel="stylesheet" />

    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");
    session_start();

    //make sure we have a valid sesion
    if ($_SESSION["valid"] != "TRUE")
    {
        header("Location: index.html");
    };
    
    $firstname = $_SESSION["firstname"];
    $lastname = $_SESSION["lastname"];
    $userid = $_SESSION["userid"];
    $loggedin_userid = $_SESSION["loggedin_userid"];
    $admin_user = $_SESSION["admin_user"];
    $subType = $_SESSION["subType"];

    $parent_signature = "";
    $student_signature = "";

    if ($subType == "F") {
        $dont_hide_Edit_Buttons = true;
    } else {
        $dont_hide_Edit_Buttons = (strcmp($loggedin_userid, $userid) == 0) || ($admin_user == 0);
    }
    
    ?>

    <style>
        body {
            padding-top: 40px;
        }
    </style>

</head>
<body>

    <!-- Navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="my-navbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="" class="navbar-brand">EMTeLink Tracker</a>
            </div>
            <!-- Navbar Header-->
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="Index.html">
                        <h4>Logoff</h4>
                    </a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="admin/form-manager.php">
                        <h4>Forms</h4>
                    </a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="Home.php">
                        <h4>Home</h4>
                    </a></li>
                </ul>
            </div>

        </div>
        <!-- End Container-->
    </nav>
    <!-- End navbar -->
    <!-- jumbotron-->

    <div>

        <?php 
        //get user info
        $recordset = $database->select("user", [
            "expectation_sign_date",
            "expectation_parent_name",
            "expectation_parent_signature",
            "expectation_student_name",
            "expectation_student_signature"
        ], [
            "userid" => "$userid"
        ]);

        foreach($recordset as $data)
        {
            //signatures #############################################################

            $sign_date = "";
            $myDateTime = DateTime::createFromFormat('Y-m-d', $data["expectation_sign_date"]);
            if(!$myDateTime == ''){
                $sign_date = $myDateTime->format('m-d-Y');
            }
            $parent_name = $data["expectation_parent_name"];
            $parent_signature = $data["expectation_parent_signature"];
            $student_name = $data["expectation_student_name"];
            $student_signature = $data["expectation_student_signature"];

            //########################################################################
        }   
        ?>

    </div>

    <div class="well">
        <div class="container text-center">
            <h2>
                <br />
                EXPECTATIONS FOR MEMBERS IN GOOD STANDING WITH<br />
                <br />
                THE BAINBRIDGE HIGH SCHOOL BAND</h2>
        </div>
        <!-- End container -->
    </div>
    <!-- End jumbotron-->

    <div class="container">
        <section>
            <div id="signatures"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                            <h1><u>Expectations</u></h1>
                        </div>
                        <div class="panel-body">
                            <form id="signatureform" class="signatureform">

                                <?php  if (($userid == 'admin1') || ($userid == 'student1')) { ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="well">
                                            <h2>As members of the <b>Test</b> High School Band, you are expected to:</h2>
                                            <br />

                                            <dl class="dl-horizontal" style="font-size: 20px">
                                                <dt>1.</dt>
                                                <dd class="text-left">Be on time.</dd>

                                                <dt>2.</dt>
                                                <dd class="text-left">Be where you are supposed to be. Be doing what you are supposed to be doing.</dd>

                                                <dt>3.</dt>
                                                <dd class="text-left">Follow classroom rules and procedures.</dd>

                                                <dt>4.</dt>
                                                <dd class="text-left">Be prepared for all classes, rehearsals, and performances.</dd>

                                                <dt>5.</dt>
                                                <dd class="text-left">Make a positive contribution to the band program.</dd>

                                                <dt>6.</dt>
                                                <dd class="text-left">Display conduct becoming of young ladies and gentlemen at all times.</dd>

                                                <dt>7.</dt>
                                                <dd class="text-left">Meet eligibility requirements as defined by school policy.</dd>

                                                <dt>8.</dt>
                                                <dd class="text-left">Attend all rehearsals.</dd>

                                                <dt>9.</dt>
                                                <dd class="text-left">Attend all concerts.</dd>

                                                <dt>10.</dt>
                                                <dd class="text-left">
                                                    Follow all policies of the
                                                    <b>Test</b>
                                                    County Board of Education and
                                                    <b>Test</b>
                                                    High School as set forth in the student handbook.
                                                </dd>
                                            </dl>

                                            <h2>
                                                Any student who does not meet the high standards of the
                                                <b>Test</b>
                                                High School Band is subject to review, disciplinary action, and/or dismissal at the discretion of the Band Director.</h2>
                                        </div>
                                    </div>
                                </div>

                                <?php  } else { ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="well">
                                            <h2>As members of the Bainbridge High School Band, you are expected to:</h2>
                                            <br />

                                            <dl class="dl-horizontal" style="font-size: 20px">
                                                <dt>1.</dt>
                                                <dd class="text-left">Be on time.</dd>

                                                <dt>2.</dt>
                                                <dd class="text-left">Be where you are supposed to be. Be doing what you are supposed to be doing.</dd>

                                                <dt>3.</dt>
                                                <dd class="text-left">Follow classroom rules and procedures.</dd>

                                                <dt>4.</dt>
                                                <dd class="text-left">Be prepared for all classes, rehearsals, and performances.</dd>

                                                <dt>5.</dt>
                                                <dd class="text-left">Make a positive contribution to the band program.</dd>

                                                <dt>6.</dt>
                                                <dd class="text-left">Display conduct becoming of young ladies and gentlemen at all times.</dd>

                                                <dt>7.</dt>
                                                <dd class="text-left">Meet eligibility requirements as defined by school policy.</dd>

                                                <dt>8.</dt>
                                                <dd class="text-left">Attend all rehearsals.</dd>

                                                <dt>9.</dt>
                                                <dd class="text-left">Attend all concerts.</dd>

                                                <dt>10.</dt>
                                                <dd class="text-left">Follow all policies of the Decatur County Board of Education and Bainbridge High School as set forth in the student handbook.</dd>
                                            </dl>

                                            <h2>Any student who does not meet the high standards of the Bainbridge High School Band is subject to review, disciplinary action, and/or dismissal at the discretion of the Band Director.</h2>
                                        </div>
                                    </div>
                                </div>

                                <?php  }  ?>



                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="col-xs-2 pull-left form input-lg" for="sign-date-picker" style="font-size: 22px">Date</label>
                                        <div class="col-xs-10">
                                            <input type="text" style="font-size:20px"  class="form-control" name="sign-date-picker" placeholder="date signed" id="sign-date-picker" value="<?php echo $sign_date ?>" />
                                        </div>
                                    </div>
                                </div>
                                <hr style="border-color: #808080" />

                                <div class="well-sm">
                                    <h2>Parent / Guardian's Name</h2>
                                    <br />
                                    <div class="row">
                                        <label class="col-xs-2 pull-left" style="font-size: 18px" for="parents_name">Parent&apos;s Name</label>
                                        <div class="col-xs-10">
                                            <input type="text" class="form-control input-lg" id="parent_name" name="parent_name" placeholder="enter parent&apos;s name" value="<?php echo $parent_name ?>">
                                        </div>
                                    </div>
                                    <br />


                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="col-xs-2 pull-left" for="signature">Signed</label>
                                            <div class="col-xs-10">
                                                <button type="button" id="btn_clear_signature1" class="btn btn-danger hide">Clear Signature</button>
                                                <img style="border:dotted;border-width:thin;border-color:black;height:150px;width:100%" id="signature-img" src="<?php echo $parent_signature ?>" />
                                                <canvas id="signature" class="hide" style="height: 150px; border-width: 1px; border-style: dotted; width: 100%" />
                                                <input type="hidden" id="parent_signature_post_field" name="parent_signature_post_field" value="<?php echo '' ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                                <hr style="border-color: #808080" />

                                <?php  if (($userid == 'admin1') || ($userid == 'student1')) { ?>

                                <div class="row">
                                    <well>
                                        <p class="col-xs-12" style="font-size:20px">
                                            I understand that while traveling and participating with the
                                            <b>Test</b>
                                            High School Band, it is my responsibility to meet and follow the expectations and policies of the
                                            <b>Test</b>
                                            High School Band,
                                            <b>Test</b>
                                            High School, and the
                                            <b>Test</b>
                                            County Board of Education.
                                        </p>
                                    </well>
                                </div>

                                <?php  } else { ?>

                                <div class="row">
                                    <well>
                                        <p class="col-xs-12" style="font-size:20px">I understand that while traveling and participating with the Bainbridge High School Band, it is my responsibility to meet and follow the expectations and policies of the Bainbridge High School Band, Bainbridge High School, and the Decatur County Board of Education.</p>
                                    </well>
                                </div>

                                <?php  }  ?>


                                <div class="well-sm">
                                    <h2>Student&apos;s Signature</h2>
                                    <br />
                                    <div class="row">
                                        <label class="col-xs-2 pull-left" style="font-size: 18px" for="student_name">Student&apos;s Name</label>
                                        <div class="col-xs-10">
                                            <input type="text" class="form-control input-lg" id="student_name" name="student_name" placeholder="enter student&apos;s name" value="<?php echo $student_name ?>">
                                        </div>
                                    </div>
                                    <br />

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="col-xs-2 pull-left" for="signature1">Signed</label>
                                            <div class="col-xs-10">
                                                <button type="button" id="btn_clear_signature" class="btn btn-danger hide">Clear Signature</button>
                                                <img style="border:dotted;border-width:thin;border-color:black;height:150px;width:100%" id="signature1-img" src="<?php echo $student_signature ?>" />
                                                <canvas id="signature1" class="hide" style="height: 150px; border-width: 1px; border-style: dotted; width: 100%" />
                                                <input type="hidden" id="student_signature_post_field" name="student_signature_post_field" value="<?php echo '' ?>" />

                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <input type="hidden" id="userid" name="userid" value="<?php echo $userid ?>" />
                                    </div>
                                    <br />
                                    <br />
                                    <div class="navbar btn-toolbar btn-group-lg">
                                        <?php if ($dont_hide_Edit_Buttons == true) {?>
                                        <button type="button" id="btn_edit_signature" class="btn btn-primary">Edit Signatures</button>
                                        <?php }?>
                                        <button type="button" id="btn_cancel_signature" class="btn hide btn-danger">Cancel</button>
                                        <button type="button" id="btn_save_signature" class="btn hide btn-success">Save</button>
                                    </div>
                                </div>
                            </form>
                            <div>
                                <div class="alert alert-success hide small" role="alert" id="signature-success">Signatures Saved Successfully.</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <br />
    <br />
    <!-- Footer -->

    <footer>
        <div class="container text-center">
            <p>&copy; Copyright @ 2016 EMTeLink</p>
        </div>
        <!-- end Container-->
    </footer>

    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/modernizr.custom.34982.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap-datepicker.min.js"></script>
    <script src="Scripts/jquery.maskedinput.min.js"></script>
    <script src="Scripts/signatureCapture.js"></script>

    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            var sigCapture = null;
            sigCapture = new SignatureCapture("signature");

            var sigCapture1 = null;
            sigCapture1 = new SignatureCapture("signature1");


            /* $('#sign-date-picker').datepicker({
                format: "mm/dd/yyyy",
                autoclose: true

            }); */

            //######################################################################### Signature buttons ############################################
            //####################################################### Signature EDIT Button ##################################
            $('#btn_edit_signature').on('click', function (e) {
                $(this).hide();

                //show save and cancel buttons
                $('#btn_save_signature').removeClass('hide');
                $('#btn_cancel_signature').removeClass('hide');
                $('#btn_clear_signature').removeClass('hide');
                $('#btn_clear_signature1').removeClass('hide');

                //enable controls for the ins form
                $('#signatureform input').attr('readonly', false);
                $('#signatureform select').attr('disabled', false);

                //show the signature canvas's to capture signature
                $('#signature').removeClass('hide');
                $('#signature1').removeClass('hide');

                //hide the images
                $('#signature-img').addClass('hide');
                $('#signature1-img').addClass('hide');

                //sigCapture.clear();
                //sigCapture1.clear();
                $('#signature1-img').addClass('hide');

                //var c = document.getElementById("signature");
                //var ctx = c.getContext("2d");
                //ctx.putImageData($('#parent_signature'), 0, 0);


            })

            //####################################################### Signature SAVE Button ##################################
            $('#btn_save_signature').on('click', function (e) {


                //alert($('#sign-date-picker').val());
                //exit;
                //get the image data from the canvas and save it to hidden fields to post to the server
                //a canvas can't post to the server
                document.getElementById('parent_signature_post_field').value = signature.toDataURL('image/png');
                document.getElementById('student_signature_post_field').value = signature1.toDataURL('image/png');

                $('#signature-img').attr('src', '');
                $('#signature-img').attr('src', signature.toDataURL('image/png'));

                $('#signature1-img').attr('src', '');
                $('#signature1-img').attr('src', signature1.toDataURL('image/png'));

                //post the values to the server
                $.ajax({
                    type: "POST",
                    url: "ws/process-expectation.php",
                    data: $('form.signatureform').serialize(),
                    success: function (msg) {
                        //$('#sql_stmt').val(msg);
                        $('#signature-success').removeClass('hide');
                        $.wait(function () { $('#signature-success').addClass('hide') }, 2);
                        //alert(msg);
                        //disable controls for the sign form
                        $('#signatureform input').attr('readonly', true);
                        $('#signatureform select').attr('disabled', true);
                    },
                    error: function () {
                        alert("Error saving signature");
                    }
                });

                //hide / show buttons
                $('#btn_save_signature').addClass('hide');
                $('#btn_cancel_signature').addClass('hide');

                $('#btn_clear_signature').addClass('hide');
                $('#btn_clear_signature1').addClass('hide');

                $('#btn_edit_signature').show();

                //disable controls for the ins form
                $('#signatureform input').attr('readonly', true);
                $('#signatureform select').attr('disabled', true);

                $('#signature').addClass('hide');
                $('#signature1').addClass('hide');

                $('#signature-img').removeClass('hide');
                $('#signature1-img').removeClass('hide');

            })

            //####################################################### Signature CANCEL Button #################################
            $('#btn_cancel_signature').on('click', function (e) {

                //hide / show buttons
                $('#btn_save_signature').addClass('hide');
                $('#btn_cancel_signature').addClass('hide');
                $('#btn_clear_signature').addClass('hide');
                $('#btn_clear_signature1').addClass('hide');
                $('#btn_edit_signature').show();

                //disable controls for the ins form
                $('#signatureform input').attr('readonly', true);
                $('#signatureform select').attr('disabled', true);

                //hide the canvas panals
                $('#signature').addClass('hide');
                $('#signature1').addClass('hide');

                //show the image placeholder's
                $('#signature-img').removeClass('hide');
                $('#signature1-img').removeClass('hide');

            })

            //####################################################### Signature Clear Buttons ##################################
            $('#btn_clear_signature').on('click', function (e) {
                sigCapture1.clear();
            })

            $('#btn_clear_signature1').on('click', function (e) {
                sigCapture.clear();
            })
            //#################################################################################################################

            //initially set all input controls to  and select to disabled
            $('input').attr('readonly', true);
            $('select').attr('disabled', true);

            //initialization section
            $("#sign-date-picker").mask("99/99/9999");

            $.wait = function (callback, seconds) {
                return window.setTimeout(callback, seconds * 1000);
            }

            function isDate(txtDate) {
                var currVal = txtDate;
                if (currVal == '')
                    return true;

                //Declare Regex 
                var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
                var dtArray = currVal.match(rxDatePattern); // is format OK?

                if (dtArray == null)
                    return false;

                //Checks for mm/dd/yyyy format.
                dtMonth = dtArray[1];
                dtDay = dtArray[3];
                dtYear = dtArray[5];

                if (dtMonth < 1 || dtMonth > 12)
                    return false;
                else if (dtDay < 1 || dtDay > 31)
                    return false;
                else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)

                    return false;
                else if (dtMonth == 2) {
                    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                    if (dtDay > 29 || (dtDay == 29 && !isleap))

                        return false;
                }
                return true;
            }


        });
    </script>

</body>
</html>
