<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>MyEMRMyWay :: Medical</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="MyEMRMyWay App">
    <!-- Latest compiled and minified CSS -->

    <!-- Latest compiled and minified CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Optional theme -->

    <link href="Content/styles.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="admin/css/font-awesome.min.css" rel="stylesheet">

    <link href="admin/css/base-admin-3.css" rel="stylesheet">
    <link href="admin/css/base-admin-3-responsive.css" rel="stylesheet">
    <link href="Content/bootstrap-dialog.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="Content/sweetalert2.min.css" rel="stylesheet" />

    <style>
        .ui-autocomplete {
            max-height: 200px;
            max-width: 420px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }

        .glyphicon {
            font-size: 14px;
        }

        @media (max-width: 768px) { /* use the max to specify at each container level */
            .widget .widget-content {
                padding: 10px 10px;
                background: #FFF;
                border: 1px solid #D5D5D5;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
            }

            .col-md-12 {
                padding-right: 1px !important;
                padding-left: 1px !important;
            }

            .col-xs-12 {
                padding-right: 4px !important;
                padding-left: 4px !important;
            }

            .well-sm {
                padding-right: 4px !important;
                padding-left: 4px !important;
            }

            h2, .h2 {
                font-size: 26px !important;
            }
        }

</style>

    <?php

    include("include/incConfig.php");
    include("include/incFunctions.php");

    session_start();

    //make sure we have a valid sesion
    include("include/session.php");

	
	$dont_hide_Edit_Buttons = true;

    if ($subType == "F") {
        $dont_hide_Edit_Buttons = true;
    } else {
		
        //$dont_hide_Edit_Buttons = (strcmp($loggedin_userid, $userid) == 0) || ($admin_user == 0);
    }
    
    ?>

    <style>
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

        /*** basic styles ***/
        label {
            font-size: 18px;
            color: #337AB7;
        }


        /*** custom checkboxes ***/
        input[type=checkbox] {
            display: none;
        }
            /* to hide the checkbox itself */
            input[type=checkbox] + label:before {
                font-family: FontAwesome;
                display: inline-block;
                color: #337AB7;
            }

            input[type=checkbox] + label:before {
                content: "\f096";
            }
            /* unchecked icon */
            input[type=checkbox] + label:before {
                letter-spacing: 10px;
            }
            /* space between checkbox and label */

            input[type=checkbox]:checked + label:before {
                content: "\f046";
            }
            /* checked icon */
            input[type=checkbox]:checked + label:before {
                letter-spacing: 5px;
            }
        /* allow space for check mark */
    </style>
</head>


<body>

    <!-- Navbar -->
	<?php include("include/navbar.php"); ?>
    <!-- End navbar -->
	
    <!-- jumbotron-->
    <div class="well well-sm">
        <div class="text-center">
            <h2><img src="img/medical-records.jpg" class="img-circle" alt="medical records" width="64" height="64" style="border:solid;border-width:medium"> Medical Information for <?php echo $firstname . " " . $lastname ?></h2>
        </div>
        <!-- End container -->
    </div>
    <!-- End jumbotron-->

    <div>

        <?php 
        $recordset = $database->select("user", [
            "med_conditions",
            "hospitalizations",
            "serious_injuries",
            "special_diets",
            "add_info",
            "add_medical_conditions",
            "bloodtype"
        ], [
            "userid" => "$userid"
        ]);

        foreach($recordset as $data)
        {
            $med_conditions = $data["med_conditions"];
            $textarea_notes = $data["add_info"];
            $textarea_special_diets = $data["special_diets"];
            $textarea_serious_injuries = $data["serious_injuries"];
            $textarea_hospitalizations = $data["hospitalizations"];
            $textarea_additionalmedicalconditions = $data["add_medical_conditions"];
            $blood_type = $data["bloodtype"];
        }   

        $medcond=json_decode($med_conditions,true);

        ?>

    </div>

    <script>

        //var jo = JSON.parse(JSON.stringify(var_med_conditions));

        var med_record;
        var allergy_record;
    </script>
    <div class="container">

        <section>
            <?php 
            $instruction_content = "Enter your Medical Conditions, Medications, Allergies, and Additional Medical Information below.<br /><br />";
            $instruction_content = $instruction_content . "Tap the Header of each section to expand or collapse it.";
            include("include/incInstructions.php");
            ?>

            <div id="medical_conditions"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                        <div class="panel panel-primary" style="border-color: black;">
                            <div class="panel-heading text-center">
                                <div class="panel-title">
                                    <a  data-toggle="collapse" href="#collapseConditions">
                                        <span style="font-size: large"><i class="icon-stethoscope" style="color:#ffffff"></i>&nbsp;&nbsp;<u><strong>Medical Conditions</strong></u></span>
                                    
                                    <h5>Select Checkboxes and then Select Save button</h5></a>
                                </div>
                            </div>
                              <div class="panel-body text-center collapse in" id="collapseConditions">
                                  <div class="row">
                                      <?php if ($subType != "F") {  ?>
                                      <div class="col-xs-12 col-sm-4" style="text-align:left">
                                          <ul class="nav nav-list">
                                              <li>
                                                  <input id="med_mumps" name="med_mumps" type="checkbox" <?php if($medcond[0]['checked']==1){echo ('checked');} ?> /><label for="med_mumps">Mumps</label>
                                              </li>
                                              <li>
                                                  <input id="med_measles" name="med_measles" type="checkbox" <?php if($medcond[1]['checked']==1){echo ('checked');} ?> /><label for="med_measles">Measles</label>
                                              </li>
                                              <li>
                                                  <input id="med_chickenpox" name="med_chickenpox" type="checkbox" <?php if($medcond[2]['checked']==1){echo ('checked');} ?> /><label for="med_chickenpox">Chicken Pox</label>
                                              </li>
                                              <li>
                                                  <input id="med_pneumonia" name="med_pneumonia" type="checkbox" <?php if($medcond[3]['checked']==1){echo ('checked');} ?> /><label for="med_pneumonia">Pneumonia</label>
                                              </li>
                                              <li>
                                                  <input id="med_diabetes" name="med_diabetes" type="checkbox" <?php if($medcond[4]['checked']==1){echo ('checked');} ?> /><label for="med_diabetes">Diabetes</label>
                                              </li>
                                          </ul>
                                      </div>
                                      <div class="col-xs-12 col-sm-4" style="text-align:left">
                                          <ul class="nav nav-list">
                                              <li>
                                                  <input id="med_cancer" name="med_cancer" type="checkbox" <?php if($medcond[5]['checked']==1){echo ('checked');} ?> /><label for="med_cancer">Cancer</label>
                                              </li>
                                              <li>
                                                  <input id="med_malaria" name="med_malaria" type="checkbox" <?php if($medcond[6]['checked']==1){echo ('checked');} ?> /><label for="med_malaria">Malaria</label>
                                              </li>
                                              <li>
                                                  <input id="med_seizures" name="med_seizures" type="checkbox" <?php if($medcond[7]['checked']==1){echo ('checked');} ?> /><label for="med_seizures">Seizures</label>
                                              </li>
                                              <li>
                                                  <input id="med_heartdisease" name="med_heartdisease" type="checkbox" <?php if($medcond[8]['checked']==1){echo ('checked');} ?> /><label for="med_heartdisease">Heart Disease</label>
                                              </li>
                                              <li>
                                                  <input id="med_kidneydisease" name="med_kidneydisease" type="checkbox" <?php if($medcond[9]['checked']==1){echo ('checked');} ?> /><label for="med_kidneydisease">Kidney Disease</label>
                                              </li>
                                          </ul>
                                      </div>

                                      <div class="col-xs-12 col-sm-4" style="text-align:left">
                                          <ul class="nav nav-list">
                                              <li>
                                                  <input id="med_tuberculosis" name="med_tuberculosis" type="checkbox" <?php if($medcond[10]['checked']==1){echo ('checked');} ?> /><label for="med_tuberculosis">Tuberculosis</label>
                                              </li>
                                              <li>
                                                  <input id="med_rheumaticfever" name="med_rheumaticfever" type="checkbox" <?php if($medcond[11]['checked']==1){echo ('checked');} ?> /><label for="med_rheumaticfever">Rheumatic Fever</label>
                                              </li>
                                              <li>
                                                  <input id="med_hepatitis" name="med_hepatitis" type="checkbox" <?php if($medcond[12]['checked']==1){echo ('checked');} ?> /><label for="med_hepatitis">Hepatitis</label>
                                              </li>
                                              <li>
                                                  <input id="med_bloodpressureproblems" name="med_bloodpressureproblems" type="checkbox" <?php if($medcond[13]['checked']==1){echo ('checked');} ?> /><label for="med_bloodpressureproblems">Blood Pressure Problems</label>
                                              </li>
                                              <li>
                                                  <input id="med_tetanus" name="med_tetanus" type="checkbox" <?php if($medcond[14]['checked']==1){echo ('checked');} ?> /><label for="med_tetanus">Tetanus Toxoid Shot (in last 12 months)</label>
                                              </li>
                                          </ul>
                                      </div>

                                      <?php } else {  ?>

                                      <div class="col-xs-12 col-sm-4" style="text-align:left">
                                          <ul class="nav nav-list">
                                              <li>
                                                  <input id="med_mumps" name="med_mumps" type="checkbox" <?php if($medcond[0]['checked']==1){echo ('checked');} ?> /><label for="med_mumps">Asthma</label>
                                              </li>
                                              <li>
                                                  <input id="med_measles" name="med_measles" type="checkbox" <?php if($medcond[1]['checked']==1){echo ('checked');} ?> /><label for="med_measles">COPD</label>
                                              </li>
                                              <li>
                                                  <input id="med_chickenpox" name="med_chickenpox" type="checkbox" <?php if($medcond[2]['checked']==1){echo ('checked');} ?> /><label for="med_chickenpox">Seizure Disorder</label>
                                              </li>
                                              <li>
                                                  <input id="med_pneumonia" name="med_pneumonia" type="checkbox" <?php if($medcond[3]['checked']==1){echo ('checked');} ?> /><label for="med_pneumonia">Dementia</label>
                                              </li>
                                              <li>
                                                  <input id="med_diabetes" name="med_diabetes" type="checkbox" <?php if($medcond[4]['checked']==1){echo ('checked');} ?> /><label for="med_diabetes">Alzheimer</label>
                                              </li>
                                          </ul>
                                      </div>
                                      <div class="col-xs-12 col-sm-4" style="text-align:left">
                                          <ul class="nav nav-list">
                                              <li>
                                                  <input id="med_cancer" name="med_cancer" type="checkbox" <?php if($medcond[5]['checked']==1){echo ('checked');} ?> /><label for="med_cancer">Hyperglycemia</label>
                                              </li>
                                              <li>
                                                  <input id="med_malaria" name="med_malaria" type="checkbox" <?php if($medcond[6]['checked']==1){echo ('checked');} ?> /><label for="med_malaria">Hypoglycemia</label>
                                              </li>
                                              <li>
                                                  <input id="med_seizures" name="med_seizures" type="checkbox" <?php if($medcond[7]['checked']==1){echo ('checked');} ?> /><label for="med_seizures">Diabetes Type 1</label>
                                              </li>
                                              <li>
                                                  <input id="med_heartdisease" name="med_heartdisease" type="checkbox" <?php if($medcond[8]['checked']==1){echo ('checked');} ?> /><label for="med_heartdisease">Diabetes Type 2</label>
                                              </li>
                                              <li>
                                                  <input id="med_kidneydisease" name="med_kidneydisease" type="checkbox" <?php if($medcond[9]['checked']==1){echo ('checked');} ?> /><label for="med_kidneydisease">High Blood Pressure</label>
                                              </li>
                                          </ul>
                                      </div>
                                      <div class="col-xs-12 col-sm-4" style="text-align:left">
                                          <ul class="nav nav-list">
                                              <li>
                                                  <input id="med_tuberculosis" name="med_tuberculosis" type="checkbox" <?php if($medcond[10]['checked']==1){echo ('checked');} ?> /><label for="med_tuberculosis">Contact Lenses</label>
                                              </li>
                                              <li>
                                                  <input id="med_rheumaticfever" name="med_rheumaticfever" type="checkbox" <?php if($medcond[11]['checked']==1){echo ('checked');} ?> /><label for="med_rheumaticfever">Rheumatic Fever</label>
                                              </li>
                                              <li>
                                                  <input id="med_hepatitis" name="med_hepatitis" type="checkbox" <?php if($medcond[12]['checked']==1){echo ('checked');} ?> /><label for="med_hepatitis">Pacemaker</label>
                                              </li>
                                              <li>
                                                  <input id="med_bloodpressureproblems" name="med_bloodpressureproblems" type="checkbox" <?php if($medcond[13]['checked']==1){echo ('checked');} ?> /><label for="med_bloodpressureproblems">Heart Stent</label>
                                              </li>
                                              <li>
                                                  <input id="med_tetanus" name="med_tetanus" type="checkbox" <?php if($medcond[14]['checked']==1){echo ('checked');} ?> /><label for="med_tetanus">Tetanus Shot (last 12 mnths)</label>
                                              </li>
                                          </ul>
                                      </div>
                                      <?php }  ?>

                                      <br />

                                      <form id="medical_conditions_form" class="medical_conditions_form">
                                          <input type="hidden" id="med_conditions" name="med_conditions" value="" />
                                          <input type="hidden" id="userid" name="userid" value="<?php echo $userid ?>" />
                                          <input type="hidden" id="med_conditions_server" name="med_conditions_server" value="" />
                                          <!-- <input type="text" id="med_return" name="med_return" value="" /> -->
                                      </form>
                                      <?php if ($dont_hide_Edit_Buttons == true) {?>
                                      <button type="submit" id="btn_manage_medical" style="font-weight:800;background-color:#3881C0;color:white;" class="btn btn-md">Click To Save Checked Items Above</button>
                                      <?php }?>
                                      <br /><hr />
                                  </div>

                                  <div class="row">
                                      <div class="col-xs-1"></div>
                                      <div class="col-xs-10">

                                          <label for="textarea-medicalconditions" style="font-size: 16px"><u><strong>List Additional Medical Conditions Below</strong></u></label>
                                          <textarea id="textarea-medicalconditions" style="font-size: 16px" name="textarea-medicalconditions" readonly rows="4" class="form-control" placeholder="Enter Additional Medical Conditions"><?php echo $textarea_additionalmedicalconditions ?></textarea>
                                          <br />
                                          <?php if ($dont_hide_Edit_Buttons == true) {?>
                                          <button type="submit" id="btn_edit_medical_conds" style="font-weight:800;background-color:#3881C0;color:white;" class="btn btn-md">Edit Medical Conditions</button>
                                          <?php }?>
                                          <button type="button" id="btn-cancel-add-med" class="btn btn-danger btn-md hide">Cancel</button>
                                          <button type="button" id="btn-save-add-med" class="btn btn-success btn-md hide">Save</button>
                                      </div>
                                      <div class="col-xs-1"></div>
                                  </div>
                        </div>
                        <div class="panel-footer text-center">
                            <br />
                            <div class="row">
                                <div>
                                    <div class="alert alert-success hide" role="alert" id="medcond-success"><h4><strong>Medical Conditions Saved Successfully.</strong></h4></div>
                                </div>
                            </div>
                        </div>
                 </div>
           </div>
         </div>
        </section>

        <section>
            <div id="bloodtype"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <form id="medical_blood_form" class="medical_blood_form">
                        <section>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group accordion5">
                                        <div class="panel panel-danger" style="border-color: black;">
                                            <div class="panel-heading text-center">
                                                <div class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion5" href="#collapseBlood">
                                                        <span style="font-size: large">
                                                            <i class="icon-random" style="color: #A94442"></i>
                                                            &nbsp;&nbsp;
                                                            <u>
                                                                <strong>Blood Type</strong>
                                                            </u>
                                                        </span>
                                                        <h5>Select "Edit Blood Type" to change and then Save when done.</h5>
                                                    </a>
                                                </div>
                                            </div>
                                            <div id="collapseBlood" class="panel-collapse collapse in text-center">
                                                <div class="panel-body text-center">
                                                    <label for="blood_type" style="font-size: 16px;color:Red;">Select Your Blood Type</label>
                                                    <span style="font-size: 14px;" class="label label-success hide" id="alert-success-bloodtype"><br />Save was successful.</span>
                                                    <select disabled class="form-control" id="blood_type" name="blood_type" style="background-color:#FFFFFF; color:black;">
                                                        <option></option>
                                                        <option <?php if ($blood_type == 'Unknown') {echo 'selected';}?>>Unknown</option>
                                                        <option <?php if ($blood_type == 'A+')  {echo 'selected';}?>>A+</option>
                                                        <option <?php if ($blood_type == 'O+')  {echo 'selected';}?>>O+</option>
                                                        <option <?php if ($blood_type == 'B+')  {echo 'selected';}?>>B+</option>
                                                        <option <?php if ($blood_type == 'AB+') {echo 'selected';}?>>AB+</option>
                                                        <option <?php if ($blood_type == 'A-')  {echo 'selected';}?>>A-</option>
                                                        <option <?php if ($blood_type == 'O-')  {echo 'selected';}?>>O-</option>
                                                        <option <?php if ($blood_type == 'B-')  {echo 'selected';}?>>B-</option>
                                                        <option <?php if ($blood_type == 'AB-') {echo 'selected';}?>>AB-</option>
                                                    </select>

                                                    <div>
                                                        <input type="hidden" id="blood_userid" name="blood_userid" value="<?php echo $userid ?>" />
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center">
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="btn-toolbar btn-group-md">
                                                                <?php if ($dont_hide_Edit_Buttons == true) {?>
                                                                <button type="button" id="btn-edit-blood" class="btn btn-danger btn-md">Edit Blood Type</button>
                                                                <?php }?>
                                                                <button type="button" id="btn-cancel-blood" class="btn btn-danger btn-md hide">Cancel</button>
                                                                <button type="button" id="btn-save-blood" class="btn btn-success btn-md hide">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </section>

        <section>
            <div id="medications"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel-group accordion2">
                        <div class="panel panel-success open" style="border-color: black;">
                            <div class="panel-heading text-center">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion2" href="#collapseMedications">
                                        <span style="font-size: large"><i class="icon-medkit" style="color: #3C763D"></i>&nbsp;&nbsp;<u><strong>Medications</strong></u></span>
                                    
                                    <h5>Tap "Add Medication" to add or tap an icon to edit or delete.</h5></a>
                                </div>
                            </div>
                            <div id="collapseMedications" class="panel-collapse collapse in">
                                <div class="panel-body" id="medication_panel_body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-striped center" style="width: 85%;" id="medication_table">
                                                <thead>
                                                    <tr class="text-left input-lg">
                                                        <th style="text-align: left">Name</th>
                                                        <th style="text-align: left">Dosage</th>
                                                        <th style="text-align: left">How Often</th>
                                                        <th style="text-align: left">&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php 
                                                    $recordset = $database->select("user_medications", [
                                                        "medicationId",
                                                        "medication_name",
                                                        "dosage",
                                                        "howoften"
                                                    ], [
                                                        "userid" => "$userid",
                                                         "ORDER" => ['medication_name ASC']
                                                    ]);

                                                    foreach($recordset as $data)
                                                    {
                                                        $medicationId = $data["medicationId"];
                                                        $medication_name = $data["medication_name"];
                                                        $dosage = $data["dosage"];
                                                        $howoften = $data["howoften"];

                                                        echo ("<tr class=\"text-left input-lg\">");
                                                        echo ("  <td>" . $medication_name . "</td>");
                                                        echo ("  <td>" . $dosage . "</td>");
                                                        echo ("  <td>" . $howoften . "</td>");
                                                        echo ("  <td style=\"text-align:left;padding:10px\">");
                                                        echo ("    <div class=\"record\"><span class=\"name hide\">" . $medicationId . "</span>");
                                                        if ($dont_hide_Edit_Buttons == true) {
                                                            echo ("      <button style=\"color:green\" type=\"button\" class=\"editButton btn btn-xs btn-success glyphicon glyphicon-new-window glyphicon-edit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit Medication\"></button>");
                                                            echo ("      <button style=\"color:red\"  type=\"button\" class=\"deleteButton btn btn-xs btn-danger glyphicon glyphicon-trash\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Medication\"></button>");
                                                        }
                                                        echo ("    </div>");
                                                        echo ("  </td>");
                                                        echo ("</tr>");
                                                    }   
                                                    echo ("</td></tr></table>")

                                                    ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <div class="alert alert-danger hide" role="alert" id="med-deleted">Medication Deleted.</div>
                                    <br />
                                    <?php if ($dont_hide_Edit_Buttons == true) {?>
                                    <button type="button" id="btn_add_medication" class="btn btn-success btn-md">Add Medication</button>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div id="allergies_div"><span class="clearfix"></span></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel-group accordion4">
                        <div class="panel panel-success open" style="border-color: black;">
                            <div class="panel-heading text-center">
                                <div class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion4" href="#collapseAllergies">
                                        <span style="font-size: large"><i class="icon-leaf" style="color: #3C763D"></i>&nbsp;&nbsp;<u><strong>Allergies</strong></u></span>
                                    
                                    <h5>Tap "Add Allergy" to add or tap an icon to edit or delete.</h5></a>
                                </div>
                            </div>
                            <div id="collapseAllergies" class="panel-collapse collapse in">

                                <div class="panel-body" id="allergy_panel_body">
                                    <table class="table table-striped center" style="width: 85%;" id="allergy_table">
                                        <thead>
                                            <tr class="text-left input-lg">
                                                <th class="text-left">Description</th>
                                                <th class="text-left">Notes</th>
                                                <th class="text-center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php 
                                            $recordset = $database->select("user_allergies", [
                                                "allergyId",
                                                "description",
                                                "notes"
                                            ], [
                                                "userid" => "$userid",
                                                 "ORDER" => ['description ASC']
                                            ]);

                                            foreach($recordset as $data)
                                            {
                                                $allergyId = $data["allergyId"];
                                                $description = $data["description"];
                                                $notes = $data["notes"];

                                                echo ("<tr class=\"text-left input-lg\">");
                                                echo ("  <td>" . $description . "</td>");
                                                echo ("  <td>" . $notes . "</td>");
                                                echo ("  <td>");
                                                echo ("    <div class=\"record_allergy\"><span class=\"name hide\">" . $allergyId . "</span>");
                                                if ($dont_hide_Edit_Buttons == true) {
                                                    echo ("      <button type=\"button\" style=\"color:green\" class=\"editAllergyButton btn btn-xs btn-success glyphicon glyphicon-edit\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit Allergy\"></button>");
                                                    echo ("      <button type=\"button\" style=\"color:red\" class=\"deleteAllergyButton btn btn-xs btn-danger glyphicon glyphicon-trash\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Allergy\"></button>");
                                                }
                                                echo ("    </div>");
                                                echo ("  </td>");
                                                echo ("</tr>");
                                            }   
                                            echo ("</td></tr></table>")

                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="panel-footer text-center">
                                    <div class="alert alert-danger hide text-center" role="alert" id="allergy-deleted"><h5><strong>Allergy Deleted.</strong></h5></div>
                                    <br />
                                    <?php if ($dont_hide_Edit_Buttons == true) {?>
                                    <button type="button" id="btn_add_allergy" class="btn btn-success btn-md">Add Allergy</button>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="container">
        <form id="medical_misc_form" class="medical_misc_form">
            <section>
                <div id="notes"><span class="clearfix"></span></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel-group accordion5">
                            <div class="panel panel-danger open" style="border-color: black;">
                                <div class="panel-heading text-center">
                                    <div class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion5" href="#collapseMisc">
                                            <span style="font-size: large"><i class="icon-random" style="color: #A94442"></i>&nbsp;&nbsp;<u><strong>Additional Information</strong></u></span>
                                        
                                        <h5>Select "Edit Information" to add or change and then Save when done.</h5></a>
                                    </div>
                                </div>
                                <div id="collapseMisc" class="panel-collapse collapse in">

                                    <div class="panel-body">
                                        <table class="table table-striped center" style="width: 98%;">
                                            <tbody>
                                                <tr class="text-left input-lg">
                                                    <td>
                                                        <label for="textarea-hospitalizations" style="font-size: 16px">Hospitalizations (when, where, why, Dr.&apos;s Name)</label>
                                                        <textarea id="textarea-hospitalizations" style="font-size: 16px" name="textarea-hospitalizations" readonly rows="3" class="form-control" placeholder="Hospitalizations (when, where, why, Dr&apos;s Name)"><?php echo $textarea_hospitalizations ?></textarea>
                                                    </td>
                                                </tr>

                                                <tr class="text-left input-lg">
                                                    <td>
                                                        <label for="textarea-serious-injuries" style="font-size: 16px">Serious Injuries or Illness</label>
                                                        <textarea id="textarea-serious-injuries" style="font-size: 16px" name="textarea-serious-injuries" readonly rows="3" class="form-control" placeholder="Serious Injuries or Illness"><?php echo $textarea_serious_injuries ?></textarea>
                                                    </td>
                                                </tr>
                                                <tr class="text-left input-lg">
                                                    <td>
                                                        <label for="textarea-special-diets" style="font-size: 16px">Special Diets</label>
                                                        <textarea id="textarea-special-diets" style="font-size: 16px" name="textarea-special-diets" readonly rows="3" class="form-control" placeholder="Special Diets"><?php echo $textarea_special_diets ?></textarea>
                                                    </td>
                                                </tr>
                                                <tr class="text-left input-lg">
                                                    <td>
                                                        <label for="textarea-notes" style="font-size: 16px">Additional Information</label>
                                                        <textarea id="textarea-notes" style="font-size: 16px" name="textarea-notes" rows="3" readonly class="form-control" placeholder="please provide us with any additional information of which you feel we should be aware"><?php echo $textarea_notes ?></textarea>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div>
                                            <input type="hidden" id="misc_userid" name="misc_userid" value="<?php echo $userid ?>" />
                                        </div>
                                    </div>
                                    <div class="panel-footer text-center">
                                        <br />
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="btn-toolbar btn-group-md">
                                                    <?php if ($dont_hide_Edit_Buttons == true) {?>
                                                    <button type="button" id="btn-edit-misc" class="btn btn-danger btn-md">Edit Information</button>
                                                    <?php }?>
                                                    <button type="button" id="btn-cancel-misc" class="btn btn-danger btn-md hide">Cancel</button>
                                                    <button type="button" id="btn-save-misc" class="btn btn-success btn-md hide">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div>
                                                <div class="alert alert-success hide" role="alert" id="alert-success">
                                                    <h4><strong>Miscellaneous Information Saved Successfully.</strong></h4>
                                                </div>
                                            </div>
                                        </div>

                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <br />
    <div class="text-center">
        <a href="Home.php" class="btn btn-group-md" style="color:white;background-color:#214D8C" role="button">Return To Home Screen</a>
    </div>
    <div id="addMedication" class="modal fade" role="dialog">
        <form id="add_medication_form" class="add_medication_form">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="medication_modal_title">Add Medication</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="med_name">Medication Name</label>
                            <div class="col-xs-9">
                                <input type="text" placeholder="enter medication name" name="med_name" id="med_name" class="ui-autocomplete-input form-control input-md" autocomplete="off"  value="<?php echo "" ?>" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="dosage">Dosage</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control input-md" id="dosage" name="dosage" placeholder="enter dosage" value="<?php echo "" ?>">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="how_often">Frequency</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control input-md" id="how_often" name="how_often" placeholder="how often" value="<?php echo "" ?>">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-success" id="btn_save_medication">Save</button>
                        <br />
                        <br />
                        <div>
                            <div class="alert alert-success hide" role="alert" id="medication-success"><h4><strong>Medication Information Saved Successfully.</strong></h4></div>
                        </div>
                    </div>
                </div>
                <div>
                    <input type="hidden" id="mode" name="mode" value="add" />
                    <input type="hidden" id="add_userid" name="add_userid" value="<?php echo $userid ?>" />
                    <input type="hidden" id="itemno" name="itemno" value="" />
                </div>
            </div>
        </form>
    </div>


    <div id="addAllergy" class="modal fade" role="dialog">
        <form id="add_allergy_form" class="add_allergy_form">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="allergy_modal_title">Add Allergy</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="allergy_desc">Description</label>
                            <div class="col-xs-9">
                                <input type="text" placeholder="enter allergy description" id="allergy_desc" name="allergy_desc" class="ui-autocomplete-input form-control input-md" autocomplete="off"  value="<?php echo "" ?>" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <label class="col-xs-3 pull-left" style="font-size: 16px; color: green; font-weight: 500" for="allergy_notes">Notes</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control input-md" id="allergy_notes" name="allergy_notes" placeholder="enter notes" value="<?php echo "" ?>">
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-success" id="btn_save_allergy">Save</button>
                        <br />
                        <br />
                        <div>
                            <div class="alert alert-success hide text-center" role="alert" id="add-allergy-success"><h5><strong>Allergy Information Saved Successfully.</strong></h5></div>
                        </div>
                    </div>
                </div>
                <div>
                    <input type="hidden" id="allergy_mode" name="allergy_mode" value="add" />
                    <input type="hidden" id="allergy_userid" name="allergy_userid" value="<?php echo $userid ?>" />
                    <input type="hidden" id="allergy_itemno" name="allergy_itemno" value="" />
                </div>
            </div>
        </form>
    </div>
    <br />
    <br />
    <!-- Footer -->
    <?php  include("include/incFooter-sm.php"); ?>

    

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/bootstrap-datepicker.min.js"></script>

    <script src="Scripts/jquery.maskedinput.min.js"></script>
    <script src="Scripts/sweetalert2.min.js"></script>


    <script type="text/javascript">
        // When the document is ready

        $.wait = function (callback, seconds) {
            return window.setTimeout(callback, seconds * 1000);
        }


        $(document).ready(function () {

            $('#med_name').autocomplete({
                source: 'ws/suggest-med.php',
                minLength: 1,
                max: 10,
                scroll: true
            });

            $("#med_name").autocomplete("option", "appendTo", ".add_medication_form");

            $('#allergy_desc').autocomplete({
                source: 'ws/suggest-allergy.php',
                minLength: 1,
                max: 10,
                scroll: true
            });

            $("#allergy_desc").autocomplete("option", "appendTo", ".add_allergy_form");

            
            $text1 = "";
            $text2 = "";
            $test3 = "";
            $test4 = "";


            $('#btn_manage_medical').on('click', function (e) {

                //iterate the medical conditons checkboxes and build a JSON array to store

                var med = { conditions: [] };
                $(':checkbox[name^="med_"]').each(function (index) {

                    med.conditions.push({
                        "name": this.name,
                        "checked": this.checked
                    });

                    //alert(this.name + " " + this.checked);
                    //this.value = true;
                });

                //alert(JSON.stringify(med.conditions));
                document.getElementById('med_conditions').value = JSON.stringify(med.conditions);
                //$('#med_conditions').val = JSON.stringify(med.conditions);
                //alert($('#med_conditions').val);

                //post the values to the server
                $.ajax({
                    type: "POST",
                    url: "ws/process-med-conditions.php",
                    data: $('form.medical_conditions_form').serialize(),
                    success: function (msg) {
                        //$('#sql_stmt').val(msg);
                        //$('#signature-success').removeClass('hide');
                        //$.wait(function () { $('#signature-success').addClass('hide') }, 2);
                        //alert("success");
                        //document.getElementById('med_return').value = msg;
                        //alert(msg);
                        //disable controls for the sign form
                        //$('#signatureform input').attr('readonly', true);
                        //$('#signatureform select').attr('disabled', true);
                        $('#medcond-success').removeClass('hide');
                        $.wait(function () { $('#medcond-success').addClass('hide') }, 2);
                    },
                    error: function () {
                        alert("Error saving medical conditions");
                    }
                });
            })

            //######################################################################### Blood Type buttons ############################################
            //####################################################### Blood Type EDIT Button ##################################
            $('#btn-edit-blood').on('click', function (e) {
                //alert("test");
                $(this).hide();
                $('#btn-cancel-blood').removeClass('hide');
                $('#btn-save-blood').removeClass('hide');

                $("#blood_type").prop('disabled', false);

                $bloodtype = $("#blood_type").val();


            })

            //####################################################### Blood Type Save Button ##################################
            $('#btn-save-blood').on('click', function (e) {

                $.ajax({
                    type: "POST",
                    url: "ws/process-blood.php",
                    data: $('form.medical_blood_form').serialize(),
                    success: function (msg) {
                        $('#alert-success-bloodtype').removeClass('hide');
                        $.wait(function () { $('#alert-success-bloodtype').addClass('hide') }, 2);
                        $("#blood_type").prop('disabled', true);
                    },
                    error: function () {
                        alert("Error saving blood type");
                    }
                });

                //disable controls for the misc form
                $("#blood_type").prop('disabled', false);

                //hide / show buttons
                $('#btn-save-blood').addClass('hide');
                $('#btn-cancel-blood').addClass('hide');
                $('#btn-edit-blood').show();

            })
            //####################################################### Blood Type CANCEL Button ##################################
            $('#btn-cancel-blood').on('click', function (e) {
                //revert the values
                $("#blood_type").prop('disabled', true);

                $("#blood_type").val($bloodtype);

                //hide / show buttons
                $('#btn-save-blood').addClass('hide');
                $('#btn-cancel-blood').addClass('hide');
                $('#btn-edit-blood').show();


            })

            //######################################################################### Miscellaneous buttons ############################################
            //####################################################### Miscellaneous Info EDIT Button ##################################
            $('#btn-edit-misc').on('click', function (e) {
                //alert("test");
                $(this).hide();
                $('#btn-cancel-misc').removeClass('hide');
                $('#btn-save-misc').removeClass('hide');

                $("#medical_misc_form textarea").removeAttr('readonly');

                $text1 = $("#textarea-hospitalizations").val();
                $text2 = $("#textarea-serious-injuries").val();
                $text3 = $("#textarea-special-diets").val();
                $text4 = $("#textarea-notes").val();

            })

            //####################################################### Miscellaneous Info Save Button ##################################
            $('#btn-save-misc').on('click', function (e) {

                $.ajax({
                    type: "POST",
                    url: "ws/process-misc.php",
                    data: $('form.medical_misc_form').serialize(),
                    success: function (msg) {
                        $('#misc-success').removeClass('hide');
                        $.wait(function () { $('#misc-success').addClass('hide') }, 2);
                    },
                    error: function () {
                        alert("Error saving miscellaneous information");
                    }
                });

                //disable controls for the misc form
                $('textarea').attr('readonly', 'readonly');

                //hide / show buttons
                $('#btn-save-misc').addClass('hide');
                $('#btn-cancel-misc').addClass('hide');
                $('#btn-edit-misc').show();

            })
            //####################################################### Misc Info CANCEL Button ##################################
            $('#btn-cancel-misc').on('click', function (e) {
                //revert the values
                $("#textarea-hospitalizations").val($text1);
                $("#textarea-serious-injuries").val($text2);
                $("#textarea-special-diets").val($text3);
                $("#textarea-notes").val($text4);

                //disable controls for the misc form
                //$("#medical_misc_form textarea").addAttr('readonly');
                $('textarea').attr('readonly', 'readonly');

                //hide / show buttons
                $('#btn-save-misc').addClass('hide');
                $('#btn-cancel-misc').addClass('hide');
                $('#btn-edit-misc').show();


            })


            //######################################################################### Additional Medical Conditions buttons ############################################
            //####################################################### EDIT Button ##################################
            $('#btn_edit_medical_conds').on('click', function (e) {
                //alert("test");
                $(this).hide();
                $('#btn-cancel-add-med').removeClass('hide');
                $('#btn-save-add-med').removeClass('hide');

                $("#textarea-medicalconditions").removeAttr('readonly');

                $text_medicalconditions1 = $("#textarea-medicalconditions").val();
            })

            //####################################################### Additional Medical Conditions Save Button ##################################
            $('#btn-save-add-med').on('click', function (e) {
                var values = {
                    'add_conds': document.getElementById('textarea-medicalconditions').value,
                    'userid': document.getElementById('userid').value
                };
                //console.log(values);
                $.ajax({
                    type: "POST",
                    url: "ws/process-add-med-cond.php",
                    data: values,
                    success: function (msg) {
                        $('#medcond-success').removeClass('hide');
                        $.wait(function () { $('#medcond-success').addClass('hide') }, 2);
                    },
                    error: function () {
                        alert("Error saving additional medical conditions, please try again.");
                    }
                });
                
                //disable controls for the misc form
                $('textarea-medicalconditions').attr('readonly', 'readonly');

                //hide / show buttons
                $('#btn-save-add-med').addClass('hide');
                $('#btn-cancel-add-med').addClass('hide');
                $('#btn_edit_medical_conds').show();

            })
            //####################################################### Additional Medical Conditions CANCEL Button ##################################
            $('#btn-cancel-add-med').on('click', function (e) {
                //revert the values
                $("#textarea-medicalconditions").val($text_medicalconditions1);

                //disable controls for the misc form
                //$("#medical_misc_form textarea").addAttr('readonly');
                $('textarea-medicalconditions').attr('readonly', 'readonly');

                //hide / show buttons
                $('#btn-save-add-med').addClass('hide');
                $('#btn-cancel-add-med').addClass('hide');
                $('#btn_edit_medical_conds').show();

            })


            //####################################################### Add Medication Button #######################################
            $('#btn_add_medication').on('click', function (e) {

                $('#addMedication').modal('show');

            })

            //############################################## button save medicaton ################################################
            $('#btn_save_medication').on('click', function (e) {

                $.ajax({
                    type: "POST",
                    url: "ws/process-medication.php",
                    data: $('form.add_medication_form').serialize(),
                    success: function (msg) {

                        //alert(msg);
                        $('#medication-success').removeClass('hide');
                        $.wait(function () {
                            $('#medication-success').addClass('hide'),
                            $('#addMedication').modal('hide');

                            $('#add_medication_form').find('input[type=text]').val('');
                            location.reload();

                        }, 2);
                    },
                    error: function () {
                        alert("Error saving medication information");
                    }
                });

            })

            //################################################## medication edit button ###############################################
            //set up the button styling and click functionality

            $('.editButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {

                    //set which record we're editing so we can update it later
                    record = $(this).parents('.record');
                    //populate the editing form within the dialog
                    ////$('#myInput').val(record.find('.name').html());
                    var med_item = record.find('.name').html();

                    //alert(med_item);
                    //show the dialog
                    //$("#dialogContent").dialog("open")

                    $.getJSON('ws/get-medication.php?itemno=' + med_item, function (data) {
                        //alert(data); //uncomment this for debug
                        //alert (data.name+" "+data.dosage+" "+data.howoften); //further debug
                        //$('#showdata').html("<p>item1=" + data.item1 + " item2=" + data.item2 + " item3=" + data.item3 + "</p>");
                        //look up the record values, using the record number
                        $('#addMedication').modal('show');
                        $('#medication_modal_title').text("Edit Medication");
                        $('#mode').val("edit");
                        $('#itemno').val(med_item);
                        $("#med_name").val(data.name);
                        $("#dosage").val(data.dosage);
                        $("#how_often").val(data.howoften);
                    });

                });


            //################################################## medication delete button ###############################################
            //set up the button styling and click functionality
            $('.deleteButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {
                    //set which record we're editing so we can update it later
                    swal({
                      title: 'Remove record?',
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonColor: '#41A24D',
                      cancelButtonColor: '#D54F49',
                      confirmButtonText: 'Yes',
                      cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                            record = $(this).parents('.record');
                            //populate the editing form within the dialog
                            ////$('#myInput').val(record.find('.name').html());
                            var med_item = record.find('.name').html();
                            //alert(med_item);

                            $.get("ws/delete-medication.php?itemno=" + med_item, function (data, status) {
                                //alert("Data: " + data + "\nStatus: " + status);
                            });

                            $('#med-deleted').removeClass('hide');

                            $.wait(function () {
                                $('#add-med-deleted').addClass('hide'),
                                location.reload()
                            }, 2);
                        swal(
                          'Deleted!',
                          'Record has been deleted.',
                          'success'
                        )
                      }
                    })
                });


            //####################################################### Add Allergy Button #######################################
            $('#btn_add_allergy').on('click', function (e) {

                $('#addAllergy').modal('show');

            })

            //############################################## button save Allergy ################################################
            $('#btn_save_allergy').on('click', function (e) {

                $.ajax({
                    type: "POST",
                    url: "ws/process-allergy.php",
                    data: $('form.add_allergy_form').serialize(),
                    success: function (msg) {

                        //alert(msg);
                        $('#add-allergy-success').removeClass('hide');
                        $.wait(function () {
                            $('#add-allergy-success').addClass('hide'),
                            $('#addAllergy').modal('hide');

                            $('#add_allergy_form').find('input[type=text]').val('');
                            location.reload();

                        }, 2);
                    },
                    error: function () {
                        alert("Error saving allergy information");
                    }
                });

            })


            //################################################## Allergy edit button ###############################################
            //set up the button styling and click functionality

            $('.editAllergyButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {

                    //set which record we're editing so we can update it later
                    record_allergy = $(this).parents('.record_allergy');
                    //populate the editing form within the dialog
                    ////$('#myInput').val(record.find('.name').html());
                    var allergy_item = record_allergy.find('.name').html();

                    //alert(allergy_item);
                    //show the dialogr
                    //$("#dialogContent").dialog("open")

                    $.getJSON('ws/get-allergy.php?itemno=' + allergy_item, function (data) {
                        //alert(data); //uncomment this for debug
                        //alert (data.description+" "+data.notes+);
                        //look up the record values, using the record number
                        $('#addAllergy').modal('show');
                        $('#allergy_modal_title').text("Edit Allergy");
                        $('#allergy_mode').val("edit");
                        $('#allergy_itemno').val(allergy_item);
                        $("#allergy_desc").val(data.description);
                        $("#allergy_notes").val(data.notes);
                    });

                });


            //################################################## allergy delete button ###############################################
            $('.deleteAllergyButton')
                //.button({ icons: { primary: "ui-icon-document" } })
                .click(function () {
                    //set which record we're editing so we can update it later
                    swal({
                      title: 'Remove record?',
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonColor: '#41A24D',
                      cancelButtonColor: '#D54F49',
                      confirmButtonText: 'Yes',
                      cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.value) {
                    //set which record we're editing so we can update it later
                    record_allergy = $(this).parents('.record_allergy');
                    //populate the editing form within the dialog
                    ////$('#myInput').val(record.find('.name').html());
                    var allergy_item = record_allergy.find('.name').html();
                    //alert(med_item);

                    $.get("ws/delete-allergy.php?itemno=" + allergy_item, function (data, status) {
                        //alert("Data: " + data + "\nStatus: " + status);
                    });

                    $('#allergy-deleted').removeClass('hide');

                    $.wait(function () {
                        $('#allergy-deleted').addClass('hide'),
                        location.reload()
                    }, 2);
                        swal(
                          'Deleted!',
                          'Record has been deleted.',
                          'success'
                        )
                      }
                    })
                });
        });


    </script>




</body>
</html>
